#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
@author: jsaffe

======================================================================
Carga Base de Datos: sqlite3 a PostgreSQL
======================================================================
Local:
 pgloader db.sqlite3 postgresql://postgres:postgres@postgresdb:5432/jdconicet

Yaku:
 producción:
 - pgloader _db.sqlite3 postgresql://jdconicet:iV2wThb_conicettt@172.16.48.10:5456/jdconicet
 
 testing:
 - pgloader _db.sqlite3 postgresql://jdconicet:iV2wThb_conicettt@172.16.48.10:5457/jdconicet

 local:
 - pgloader _db.sqlite3 postgresql://postgres:postgres@postgresdb:5432/jdconicet

"""