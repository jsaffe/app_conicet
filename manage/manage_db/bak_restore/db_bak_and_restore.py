# --natural will use a more durable representation of foreign keys. In django they are called "natural keys"
# --indent=4 make it human readable.
# -e sessions exclude session data
# -e admin exclude history of admin actions on admin site
# -e contenttypes -e auth.Permission exclude objects which are recreated automatically from schema every time during syncdb. Only use it together with --natural or else you might end up with badly aligned id numbers.

# python manage.py dumpdata --natural-foreign --natural-primary -e auth.permission -e contenttypes -e sessions -e admin --indent=4 > db.json
# python manage.py loaddata db.json

# python manage.py dumpdata -e sessions -e admin -e contenttypes --natural-primary --natural-foreign > db2.json
# python manage.py loaddata db2.json
