#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
@author: jsaffe

Modelo de script para agregar nuevas disciplinas para un deporte y edición de
juego deportivo (JD) específico luego de haberse inicializado el JD en la
base de datos.

1. Modificar este script según sea necesario.
2. Para ejecutar, dentro del container de webapp:
   * python manage.py shell
   * exec(open('ruta_absoluta_a_este_script').read())
"""
from torneo.models import Olimpiada, Deporte, Disciplina

# Modificar estos campos según corresponda
n_olimpiada = 3

olimpiada=Olimpiada.objects.get(id=n_olimpiada)


# Aquí crear tantas disciplinas como sea necesario.
# Para más ejemplos de creación de disciplinas se puede chequear el script
# "inicializar_nuevo_juego_deportivo.py"
deporte=Deporte.objects.get(nombre='Tenis de Mesa')
Disciplina.objects.create(nombre='Individual',limite_edad=40,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
Disciplina.objects.create(nombre='Individual',limite_edad=40,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)

deporte=Deporte.objects.get(nombre='Recreativos')
Disciplina.objects.create(nombre='Truco',limite_edad=0,deporte=deporte,tipo='Grupal',genero='Mixto', olimpiada=olimpiada, max_jugadores_lbf=3)