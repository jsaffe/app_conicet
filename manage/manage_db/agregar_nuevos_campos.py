from torneo.models import Olimpiada, Deporte, Disciplina

# Add Max_jugadores_equipo
olimpiada = Olimpiada.objects.get(ano=2018)

deporte=Deporte.objects.get(nombre='Atletismo')
disciplina = Disciplina.objects.get(nombre='Posta 4x100m',limite_edad=0, deporte=deporte,tipo='Grupal',genero='Masculino', olimpiada=olimpiada)
disciplina.max_jugadores_lbf = 4
disciplina.save()
disciplina = Disciplina.objects.get(nombre='Posta 4x100m',limite_edad=0, deporte=deporte,tipo='Grupal',genero='Femenino', olimpiada=olimpiada)
disciplina.max_jugadores_lbf = 4
disciplina.save()
disciplina = Disciplina.objects.get(nombre='Posta 4x100m',limite_edad=0, deporte=deporte,tipo='Grupal',genero='Mixto', olimpiada=olimpiada)
disciplina.max_jugadores_lbf = 4
disciplina.save()