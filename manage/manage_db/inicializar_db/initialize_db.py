#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 15 02:55:33 2018

@author: jsaffe
"""
from torneo.models import Olimpiada, Deporte, Disciplina, Equipo, Persona, Delegacion, Arte
import warnings
warnings.filterwarnings("ignore")


# =============================================================================
# Para Ejecutar en la carpeta de Django:
# 1) python manage.py shell
# 2) %run ./varios/bb_test.py o
#	 exec(open('ruta_absoluta_a:_/varios/bbdd_inicializar.py').read())
#    exec(open('/home/conicet/app_conicet/jjoo/varios/bbdd_inicializar.py').read())
# =============================================================================
carga_arte = True
carga_delegaciones = True
carga_deportes = True

# Testing
carga_personas = False
carga_equipos = False

# -----------------------------------------------------------------------------
# ESPACIOS ARTISTICOS
# -----------------------------------------------------------------------------
if carga_arte:
    danza = Arte.objects.create(nombre='Danza')
    escultura = Arte.objects.create(nombre='Escultura')
    fotografia = Arte.objects.create(nombre='Fotografía')
    literatura = Arte.objects.create(nombre='Literatura')
    musical = Arte.objects.create(nombre='Musical')
    pintura = Arte.objects.create(nombre='Pintura')
    teatro = Arte.objects.create(nombre='Teatro')
    otros = Arte.objects.create(nombre='Otros')

# -----------------------------------------------------------------------------
# DELEGACIONES
# -----------------------------------------------------------------------------
if carga_delegaciones:
    bahiablanca = Delegacion.objects.create(nombre='Bahía Blanca')
    buenosaires = Delegacion.objects.create(nombre='Buenos Aires')
    cordoba = Delegacion.objects.create(nombre='Córdoba')
    laplata = Delegacion.objects.create(nombre='La Plata')
    mardelplata = Delegacion.objects.create(nombre='Mar del Plata')
    mendoza = Delegacion.objects.create(nombre='Mendoza')
    nordeste = Delegacion.objects.create(nombre='Nordeste')
    patagonia = Delegacion.objects.create(nombre='Patagonia')
    rosario = Delegacion.objects.create(nombre='Rosario')
    sanjuan = Delegacion.objects.create(nombre='San Juan')
    sanluis = Delegacion.objects.create(nombre='San Luis')
    salta = Delegacion.objects.create(nombre='Salta')
    santafe = Delegacion.objects.create(nombre='Santa Fé')
    tandil = Delegacion.objects.create(nombre='Tandil')
    tucuman = Delegacion.objects.create(nombre='Tucumán')
    santiago = Delegacion.objects.create(nombre='Santiago del Estero')
    otras = Delegacion.objects.create(nombre='Otras')

# -----------------------------------------------------------------------------
# DEPORTES
# Resultados Parciales: Paddle, Tenis, Tenis de Mesa, Voley
# Consultar:
# Ajedrez -> como se puntuan los partidos ganador/perderdor, eliminatorio, etc.
# Atletismo y Natacion -> se cuenta los resultados parciales, finales???
# Ciclismo -> se cuenta mejor tiempo?
# Vela -> como se cuenta, mejor tiempo? un solo tiempo? hay clasificación?
# -----------------------------------------------------------------------------
if carga_deportes:
    aguasabiertas = Deporte.objects.create(nombre='Aguas Abiertas')
    ajedrez = Deporte.objects.create(nombre='Ajedrez')
    atletismo = Deporte.objects.create(nombre='Atletismo')
    basquet = Deporte.objects.create(nombre='Basquet')
    bochas = Deporte.objects.create(nombre='Bochas')
    ciclismo = Deporte.objects.create(nombre='Ciclismo')
    futbol = Deporte.objects.create(nombre='Fútbol')
    handball = Deporte.objects.create(nombre='Handball')
    hockey = Deporte.objects.create(nombre='Hockey')
    natacion = Deporte.objects.create(nombre='Natación')
    paddle = Deporte.objects.create(nombre='Pádel')
    pesca = Deporte.objects.create(nombre='Pesca')
    tenis = Deporte.objects.create(nombre='Tenis')
    tenismesa = Deporte.objects.create(nombre='Tenis de Mesa')
    vela = Deporte.objects.create(nombre='Vela')
    voley = Deporte.objects.create(nombre='Voley')
    carrera = Deporte.objects.create(nombre='Caminata/Carrera')

# -----------------------------------------------------------------------------
# PERSONAS
# -----------------------------------------------------------------------------
if carga_personas:
    import openpyxl
    doc = openpyxl.load_workbook('./varios/preinscriptos.xlsx')
    hoja = doc['Completa']  # seleccionamos la hoja

    for fila in range(2, hoja.max_row+1):
        # for fila in range (2,20):
        print(hoja['B'+str(fila)].value + ' ' +
              hoja['A'+str(fila)].value + ' | '+hoja['K'+str(fila)].value)

        delegacion = Delegacion.objects.get(nombre=hoja['K'+str(fila)].value)

        persona = Persona.objects.create(
            apellido=hoja['A'+str(fila)].value,
            nombre=hoja['B'+str(fila)].value,
            dni=str(hoja['C'+str(fila)].value),
            fecha_nacimiento=(hoja['D'+str(fila)].value),
            sexo=hoja['E'+str(fila)].value,
            mail=hoja['G'+str(fila)].value,
            telefono=str(hoja['H'+str(fila)].value),

            localidad=hoja['L'+str(fila)].value,
            telefono_laboral=hoja['M'+str(fila)].value,
            institucion=hoja['N'+str(fila)].value,
            tipo_personal=hoja['O'+str(fila)].value,
            actividad_laboral=hoja['P'+str(fila)].value,

            delegacion=delegacion,
        )
        persona.olimpiadas.add(olimpiada)

        print('Deportes: ' + hoja['S'+str(fila)].value+' / ' +
              hoja['T'+str(fila)].value + ' / ' + hoja['U'+str(fila)].value)
        print()

        if hoja['S'+str(fila)].value not in 'Ninguno':
            persona.deportes.add(Deporte.objects.get(
                nombre=hoja['S'+str(fila)].value))
        if hoja['T'+str(fila)].value not in 'Ninguno':
            persona.deportes.add(Deporte.objects.get(
                nombre=hoja['T'+str(fila)].value))
        if hoja['U'+str(fila)].value not in 'Ninguno':
            persona.deportes.add(Deporte.objects.get(
                nombre=hoja['U'+str(fila)].value))

# -----------------------------------------------------------------------------
# EQUIPOS
# -----------------------------------------------------------------------------
if carga_equipos:
    deporte = Deporte.objects.get(nombre='Fútbol')
    disciplina = Disciplina.objects.get(
        nombre='11', genero='Masculino', limite_edad=0, deporte=deporte)
    Equipo.objects.create(nombre='Cordoba', disciplina=disciplina)
    Equipo.objects.create(nombre='SantaFe', disciplina=disciplina)
    Equipo.objects.create(nombre='Mendoza', disciplina=disciplina)
    Equipo.objects.create(nombre='BahiaBlanca', disciplina=disciplina)
    Equipo.objects.create(nombre='Patagonia', disciplina=disciplina)
    Equipo.objects.create(nombre='Nordeste', disciplina=disciplina)
    Equipo.objects.create(nombre='SanJuan', disciplina=disciplina)
    Equipo.objects.create(nombre='Rosario', disciplina=disciplina)

    deporte = Deporte.objects.get(nombre='Voley')
    disciplina = Disciplina.objects.get(
        nombre='', genero='Masculino', limite_edad=0, deporte=deporte)
    Equipo.objects.create(nombre='Cordoba', disciplina=disciplina)
    Equipo.objects.create(nombre='SantaFe', disciplina=disciplina)
    Equipo.objects.create(nombre='Mendoza', disciplina=disciplina)
    Equipo.objects.create(nombre='BahiaBlanca', disciplina=disciplina)
    Equipo.objects.create(nombre='Patagonia', disciplina=disciplina)
    Equipo.objects.create(nombre='Nordeste', disciplina=disciplina)
    Equipo.objects.create(nombre='SanJuan', disciplina=disciplina)
    Equipo.objects.create(nombre='Rosario', disciplina=disciplina)
