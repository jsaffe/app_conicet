# exec(open('/home/conicet/app_conicet/jjoo/varios/copiar_inscripciones.py').read())

from torneo.models import Olimpiada, Persona, Inscripcion

olimpiada=Olimpiada.objects.get(id=1)
personas = Persona.objects.all()

for persona in personas:
	inscripcion = Inscripcion.objects.create(olimpiada = olimpiada, jugador = persona,

							    alojamiento = persona.alojamiento, 
							    acompanantes = persona.acompanantes,

							    deporte1 = persona.deporte1,
							    deporte2 = persona.deporte2, 
							    deporte3 = persona.deporte3,
							    deporte_maraton = persona.deporte_maraton,
							    
							    #DOCUMENTOS
							    certificado_medico = persona.certificado_medico,
							    comprobante_pago = persona.comprobante_pago,
							    vinculacion_conicet = persona.vinculacion_conicet,
							    
							    #OTROS
							    divulgacion = persona.divulgacion,
							    colaborador = persona.colaborador,
							    
							    inscripcion_completa = persona.inscripcion_completa,
							    inscripcion_fecha = persona.inscripcion_fecha,
							    ultima_modificacion = persona.ultima_modificacion)

	inscripcion.deportes.set(persona.deportes.all())
	inscripcion.artes.set(persona.artes.all())
	inscripcion.disciplinas_deporte1.set(persona.disciplinas_deporte1.all())
	inscripcion.disciplinas_deporte2.set(persona.disciplinas_deporte2.all())
	inscripcion.disciplinas_deporte3.set(persona.disciplinas_deporte3.all())
	inscripcion.save()