#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 15 02:55:33 2018

@author: jsaffe

# Para ejecutar, dentro del container de webapp:
# 1) python manage.py shell
# 2) exec(open('ruta_absoluta_a_este_script').read())
"""
import warnings
warnings.filterwarnings("ignore")

from torneo.models import Olimpiada, Deporte, Disciplina, Delegacion, Arte

crear_olimpiada = False
crear_disciplinas = True

# -----------------------------------------------------------------------------
# Olimpiada
# -----------------------------------------------------------------------------
if crear_olimpiada:
    # ESPACIOS ARTISTICOS
    danza = Arte.objects.get(nombre='Danza')
    escultura = Arte.objects.get(nombre='Escultura')
    fotografia = Arte.objects.get(nombre='Fotografía')
    literatura = Arte.objects.get(nombre='Literatura')
    musical = Arte.objects.get(nombre='Musical')
    pintura = Arte.objects.get(nombre='Pintura')
    teatro = Arte.objects.get(nombre='Teatro')
    otros = Arte.objects.get(nombre='Otros')

    # DELEGACIONES
    bahiablanca = Delegacion.objects.get(nombre='Bahía Blanca')
    buenosaires = Delegacion.objects.get(nombre='Buenos Aires')
    cordoba = Delegacion.objects.get(nombre='Córdoba')
    laplata = Delegacion.objects.get(nombre='La Plata')
    mardelplata = Delegacion.objects.get(nombre='Mar del Plata')
    mendoza = Delegacion.objects.get(nombre='Mendoza')
    nordeste = Delegacion.objects.get(nombre='Nordeste')
    patagonia = Delegacion.objects.get(nombre='Patagonia')
    rosario = Delegacion.objects.get(nombre='Rosario')
    sanjuan = Delegacion.objects.get(nombre='San Juan')
    sanluis = Delegacion.objects.get(nombre='San Luis')
    salta = Delegacion.objects.get(nombre='Salta')
    santafe = Delegacion.objects.get(nombre='Santa Fé')
    tandil = Delegacion.objects.get(nombre='Tandil')
    tucuman = Delegacion.objects.get(nombre='Tucumán')
    santiago = Delegacion.objects.get(nombre='Santiago del Estero')
    otras = Delegacion.objects.get(nombre='Otras')

    # DEPORTES
    aguasabiertas = Deporte.objects.get(nombre='Aguas Abiertas')
    ajedrez = Deporte.objects.get(nombre='Ajedrez')
    atletismo = Deporte.objects.get(nombre='Atletismo')
    basquet = Deporte.objects.get(nombre='Basquet')
    bochas = Deporte.objects.get(nombre='Bochas')
    ciclismo = Deporte.objects.get(nombre='Ciclismo')
    futbol = Deporte.objects.get(nombre='Fútbol')
    handball = Deporte.objects.get(nombre='Handball')
    hockey = Deporte.objects.get(nombre='Hockey')
    natacion = Deporte.objects.get(nombre='Natación')
    paddle = Deporte.objects.get(nombre='Pádel')
    pesca = Deporte.objects.get(nombre='Pesca')
    tenis = Deporte.objects.get(nombre='Tenis')
    tenismesa = Deporte.objects.get(nombre='Tenis de Mesa')
    vela = Deporte.objects.get(nombre='Vela')
    voley = Deporte.objects.get(nombre='Voley')
    carrera = Deporte.objects.get(nombre='Caminata/Carrera')

    olimpiada = Olimpiada.objects.create(
        nombre='San Juan', ano=2022, fecha_corte_edad='2022-10-22', 
        fecha_inicio='2022-10-23', fecha_fin='2022-10-26',
        fecha_inicio_inscripcion='2022-07-15',
        fecha_fin_inscripcion='2022-08-15')
    olimpiada.delegaciones.add(
        bahiablanca, buenosaires,cordoba, laplata, mardelplata, mendoza,
        nordeste, patagonia, rosario, sanjuan, sanluis, salta, santafe,
        tandil, tucuman, santiago, otras)
    olimpiada.deportes.add(
        ajedrez, atletismo, basquet, bochas, ciclismo, futbol, handball,
        hockey, natacion, paddle, pesca, tenis, tenismesa, vela, voley,
        carrera)
    olimpiada.artisticos.add(danza, escultura, danza, fotografia, literatura,
        musical, pintura, teatro, otros)
else:
	olimpiada=Olimpiada.objects.get(id=3)

# -----------------------------------------------------------------------------
# DISCIPLINAS
# nomenclatura -> Deporte_tipodisciplina_Genero_Restricciones
# -----------------------------------------------------------------------------
if crear_disciplinas:
    # Aguas Abiertas ----------------------------------------------------------
    deporte=Deporte.objects.get(nombre='Aguas Abiertas')    
    Disciplina.objects.create(nombre='',limite_edad=0, deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='',limite_edad=35,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='',limite_edad=45,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='',limite_edad=55,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='',limite_edad=0, deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='',limite_edad=35,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='',limite_edad=45,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='',limite_edad=55,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)       

    # Ajedrez -----------------------------------------------------------------
    deporte=Deporte.objects.get(nombre='Ajedrez')
    Disciplina.objects.create(nombre='',limite_edad=0,deporte=deporte,tipo='Individual',genero='Mixto',olimpiada=olimpiada)
    
    # Atletismo ---------------------------------------------------------------
    deporte=Deporte.objects.get(nombre='Atletismo')
    Disciplina.objects.create(nombre='100m',limite_edad=0, deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)   
    Disciplina.objects.create(nombre='100m',limite_edad=35,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)   
    Disciplina.objects.create(nombre='100m',limite_edad=45,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)   
    Disciplina.objects.create(nombre='100m',limite_edad=55,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='100m',limite_edad=65,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='100m',limite_edad=0, deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='100m',limite_edad=35,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='100m',limite_edad=45,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='100m',limite_edad=55,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='100m',limite_edad=60,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    
    Disciplina.objects.create(nombre='200m',limite_edad=0, deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='200m',limite_edad=35,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='200m',limite_edad=45,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='200m',limite_edad=55,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='200m',limite_edad=65,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='200m',limite_edad=0, deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='200m',limite_edad=35,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='200m',limite_edad=45,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='200m',limite_edad=55,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='200m',limite_edad=60,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    
    Disciplina.objects.create(nombre='400m',limite_edad=0, deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='400m',limite_edad=35,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='400m',limite_edad=45,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='400m',limite_edad=55,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='400m',limite_edad=65,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='400m',limite_edad=0, deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='400m',limite_edad=35,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='400m',limite_edad=45,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='400m',limite_edad=55,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='400m',limite_edad=60,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    
    Disciplina.objects.create(nombre='800m',limite_edad=0, deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='800m',limite_edad=35,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='800m',limite_edad=45,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='800m',limite_edad=55,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='800m',limite_edad=65,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='800m',limite_edad=0, deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='800m',limite_edad=35,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='800m',limite_edad=45,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='800m',limite_edad=55,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='800m',limite_edad=60,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    
    Disciplina.objects.create(nombre='1500m',limite_edad=0, deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='1500m',limite_edad=35,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='1500m',limite_edad=45,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='1500m',limite_edad=55,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='1500m',limite_edad=65,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='1500m',limite_edad=0, deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='1500m',limite_edad=35,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='1500m',limite_edad=45,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='1500m',limite_edad=55,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='1500m',limite_edad=60,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)

    Disciplina.objects.create(nombre='Salto en Largo',limite_edad=0, deporte=deporte,tipo='Individual', genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='Salto en Largo',limite_edad=35,deporte=deporte,tipo='Individual', genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='Salto en Largo',limite_edad=45,deporte=deporte,tipo='Individual', genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='Salto en Largo',limite_edad=55,deporte=deporte,tipo='Individual', genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='Salto en Largo',limite_edad=65,deporte=deporte,tipo='Individual', genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='Salto en Largo',limite_edad=0, deporte=deporte,tipo='Individual', genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='Salto en Largo',limite_edad=35,deporte=deporte,tipo='Individual', genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='Salto en Largo',limite_edad=45,deporte=deporte,tipo='Individual', genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='Salto en Largo',limite_edad=55,deporte=deporte,tipo='Individual', genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='Salto en Largo',limite_edad=60,deporte=deporte,tipo='Individual', genero='Femenino', olimpiada=olimpiada)
    
    Disciplina.objects.create(nombre='Posta 4x100m',limite_edad=0, deporte=deporte,tipo='Grupal',genero='Masculino', olimpiada=olimpiada, max_jugadores_lbf=4)
    Disciplina.objects.create(nombre='Posta 4x100m',limite_edad=0, deporte=deporte,tipo='Grupal',genero='Femenino', olimpiada=olimpiada, max_jugadores_lbf=4)
    Disciplina.objects.create(nombre='Posta 4x100m',limite_edad=0, deporte=deporte,tipo='Grupal',genero='Mixto', olimpiada=olimpiada, max_jugadores_lbf=4)   

    # Basquet -----------------------------------------------------------------
    deporte=Deporte.objects.get(nombre='Basquet')
    Disciplina.objects.create(nombre='',limite_edad=0,deporte=deporte,tipo='Grupal', genero='Masculino', olimpiada=olimpiada, max_jugadores_lbf=12, max_jugadores_extras = 3)
    Disciplina.objects.create(nombre='3',limite_edad=40,deporte=deporte,tipo='Grupal', genero='Masculino', olimpiada=olimpiada, max_jugadores_lbf=8, max_jugadores_extras = 3)
    Disciplina.objects.create(nombre='3',limite_edad=0,deporte=deporte,tipo='Grupal', genero='Femenino', olimpiada=olimpiada, max_jugadores_lbf=8, max_jugadores_extras = 3)

    # Bochas ------------------------------------------------------------------
    deporte=Deporte.objects.get(nombre='Bochas')
    Disciplina.objects.create(nombre='Dobles',limite_edad=0,deporte=deporte,tipo='Grupal', genero='Mixto', olimpiada=olimpiada, max_jugadores_lbf=3)
    
    # Ciclismo ----------------------------------------------------------------
    deporte=Deporte.objects.get(nombre='Ciclismo')
    Disciplina.objects.create(nombre='Mountain Bike-Rally Bike',limite_edad=0,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='Mountain Bike-Rally Bike',limite_edad=0,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='Mountain Bike-Rally Bike',limite_edad=35,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='Mountain Bike-Rally Bike',limite_edad=35,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    
    # Fútbol ------------------------------------------------------------------
    deporte=Deporte.objects.get(nombre='Fútbol')
    Disciplina.objects.create(nombre='11',limite_edad=0,deporte=deporte,tipo='Grupal', genero='Masculino', olimpiada=olimpiada, max_jugadores_lbf=18, max_jugadores_extras = 3)
    Disciplina.objects.create(nombre='7',limite_edad=40,deporte=deporte,tipo='Grupal', genero='Masculino', olimpiada=olimpiada, max_jugadores_lbf=12, max_jugadores_extras = 3)
    Disciplina.objects.create(nombre='7',limite_edad=0,deporte=deporte,tipo='Grupal', genero='Femenino', olimpiada=olimpiada, max_jugadores_lbf=12, max_jugadores_extras = 3)
    
    # Handball ----------------------------------------------------------------
    deporte=Deporte.objects.get(nombre='Handball')
    Disciplina.objects.create(nombre='',limite_edad=0,deporte=deporte,tipo='Grupal',genero='Masculino',olimpiada=olimpiada, max_jugadores_lbf=12, max_jugadores_extras=3)
    Disciplina.objects.create(nombre='',limite_edad=0,deporte=deporte,tipo='Grupal',genero='Femenino',olimpiada=olimpiada, max_jugadores_lbf=12, max_jugadores_extras=3)
    
    # Hockey ------------------------------------------------------------------
    deporte=Deporte.objects.get(nombre='Hockey')
    Disciplina.objects.create(nombre='',limite_edad=0,deporte=deporte,tipo='Grupal', genero='Femenino', olimpiada=olimpiada, max_jugadores_lbf=12, max_jugadores_extras=3)
    
    # Natación ----------------------------------------------------------------
    deporte=Deporte.objects.get(nombre='Natación')
    Disciplina.objects.create(nombre='25m Libre',limite_edad=0,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)          
    Disciplina.objects.create(nombre='25m Libre',limite_edad=0, deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='25m Libre',limite_edad=35,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)          
    Disciplina.objects.create(nombre='25m Libre',limite_edad=35, deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='25m Libre',limite_edad=45,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)          
    Disciplina.objects.create(nombre='25m Libre',limite_edad=45, deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='25m Libre',limite_edad=55,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)          
    Disciplina.objects.create(nombre='25m Libre',limite_edad=55, deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='25m Libre',limite_edad=65,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='25m Libre',limite_edad=60, deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)

    Disciplina.objects.create(nombre='25m Pecho',limite_edad=0,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='25m Pecho',limite_edad=0,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='25m Pecho',limite_edad=35,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='25m Pecho',limite_edad=35,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='25m Pecho',limite_edad=45,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='25m Pecho',limite_edad=45,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='25m Pecho',limite_edad=55,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='25m Pecho',limite_edad=55,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='25m Pecho',limite_edad=65,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='25m Pecho',limite_edad=60,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)

    Disciplina.objects.create(nombre='25m Mariposa',limite_edad=0,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='25m Mariposa',limite_edad=0,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='25m Mariposa',limite_edad=35,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='25m Mariposa',limite_edad=35,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='25m Mariposa',limite_edad=45,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='25m Mariposa',limite_edad=45,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='25m Mariposa',limite_edad=55,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='25m Mariposa',limite_edad=55,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='25m Mariposa',limite_edad=65,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='25m Mariposa',limite_edad=60,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)

    Disciplina.objects.create(nombre='25m Espalda',limite_edad=0,deporte=deporte,tipo='Individual', genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='25m Espalda',limite_edad=0,deporte=deporte,tipo='Individual', genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='25m Espalda',limite_edad=35,deporte=deporte,tipo='Individual', genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='25m Espalda',limite_edad=35,deporte=deporte,tipo='Individual', genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='25m Espalda',limite_edad=45,deporte=deporte,tipo='Individual', genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='25m Espalda',limite_edad=45,deporte=deporte,tipo='Individual', genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='25m Espalda',limite_edad=55,deporte=deporte,tipo='Individual', genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='25m Espalda',limite_edad=55,deporte=deporte,tipo='Individual', genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='25m Espalda',limite_edad=65,deporte=deporte,tipo='Individual', genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='25m Espalda',limite_edad=60,deporte=deporte,tipo='Individual', genero='Femenino', olimpiada=olimpiada)    

    Disciplina.objects.create(nombre='50m Libre',limite_edad=0,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='50m Libre',limite_edad=0,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='50m Libre',limite_edad=35,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='50m Libre',limite_edad=35,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='50m Libre',limite_edad=45,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='50m Libre',limite_edad=45,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='50m Libre',limite_edad=55,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='50m Libre',limite_edad=55,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='50m Libre',limite_edad=65,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='50m Libre',limite_edad=60,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)

    Disciplina.objects.create(nombre='50m Pecho',limite_edad=0,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='50m Pecho',limite_edad=0,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='50m Pecho',limite_edad=35,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='50m Pecho',limite_edad=35,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='50m Pecho',limite_edad=45,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='50m Pecho',limite_edad=45,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='50m Pecho',limite_edad=55,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='50m Pecho',limite_edad=55,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='50m Pecho',limite_edad=65,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='50m Pecho',limite_edad=60,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)

    Disciplina.objects.create(nombre='50m Mariposa',limite_edad=0,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='50m Mariposa',limite_edad=0,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='50m Mariposa',limite_edad=35,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='50m Mariposa',limite_edad=35,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='50m Mariposa',limite_edad=45,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='50m Mariposa',limite_edad=45,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='50m Mariposa',limite_edad=55,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='50m Mariposa',limite_edad=55,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='50m Mariposa',limite_edad=65,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='50m Mariposa',limite_edad=60,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)

    Disciplina.objects.create(nombre='50m Espalda',limite_edad=0,deporte=deporte,tipo='Individual', genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='50m Espalda',limite_edad=0,deporte=deporte,tipo='Individual', genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='50m Espalda',limite_edad=35,deporte=deporte,tipo='Individual', genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='50m Espalda',limite_edad=35,deporte=deporte,tipo='Individual', genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='50m Espalda',limite_edad=45,deporte=deporte,tipo='Individual', genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='50m Espalda',limite_edad=45,deporte=deporte,tipo='Individual', genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='50m Espalda',limite_edad=55,deporte=deporte,tipo='Individual', genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='50m Espalda',limite_edad=55,deporte=deporte,tipo='Individual', genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='50m Espalda',limite_edad=65,deporte=deporte,tipo='Individual', genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='50m Espalda',limite_edad=60,deporte=deporte,tipo='Individual', genero='Femenino', olimpiada=olimpiada)

    Disciplina.objects.create(nombre='100m Libre',limite_edad=0,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='100m Libre',limite_edad=0,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='100m Libre',limite_edad=35,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='100m Libre',limite_edad=35,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='100m Libre',limite_edad=45,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='100m Libre',limite_edad=45,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='100m Libre',limite_edad=55,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='100m Libre',limite_edad=55,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='100m Libre',limite_edad=65,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='100m Libre',limite_edad=60,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)    

    Disciplina.objects.create(nombre='100m Medley',limite_edad=0,deporte=deporte,tipo='Individual', genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='100m Medley',limite_edad=0,deporte=deporte,tipo='Individual', genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='100m Medley',limite_edad=35,deporte=deporte,tipo='Individual', genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='100m Medley',limite_edad=35,deporte=deporte,tipo='Individual', genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='100m Medley',limite_edad=45,deporte=deporte,tipo='Individual', genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='100m Medley',limite_edad=45,deporte=deporte,tipo='Individual', genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='100m Medley',limite_edad=55,deporte=deporte,tipo='Individual', genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='100m Medley',limite_edad=55,deporte=deporte,tipo='Individual', genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='100m Medley',limite_edad=65,deporte=deporte,tipo='Individual', genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='100m Medley',limite_edad=60,deporte=deporte,tipo='Individual', genero='Femenino', olimpiada=olimpiada)    

    Disciplina.objects.create(nombre='Posta 4x25m',limite_edad=0,deporte=deporte,tipo='Grupal',genero='Masculino', olimpiada=olimpiada, max_jugadores_lbf=4)           
    Disciplina.objects.create(nombre='Posta 4x25m',limite_edad=0,deporte=deporte,tipo='Grupal',genero='Femenino', olimpiada=olimpiada, max_jugadores_lbf=4)            
    Disciplina.objects.create(nombre='Posta 4x25m',limite_edad=0,deporte=deporte,tipo='Grupal',genero='Mixto', olimpiada=olimpiada, max_jugadores_lbf=4)               
    
    Disciplina.objects.create(nombre='Posta Americana',limite_edad=0, deporte=deporte,tipo='Grupal',genero='Masculino', olimpiada=olimpiada, max_jugadores_lbf=4)   
    Disciplina.objects.create(nombre='Posta Americana',limite_edad=0, deporte=deporte,tipo='Grupal',genero='Femenino', olimpiada=olimpiada, max_jugadores_lbf=4)   
    Disciplina.objects.create(nombre='Posta Americana',limite_edad=0, deporte=deporte,tipo='Grupal',genero='Mixto', olimpiada=olimpiada, max_jugadores_lbf=4)   
    
    # Pádel -------------------------------------------------------------------
    deporte=Deporte.objects.get(nombre='Pádel')
    Disciplina.objects.create(nombre='Dobles',limite_edad=0,deporte=deporte,tipo='Grupal',genero='Masculino', olimpiada=olimpiada, max_jugadores_lbf=2)           
    Disciplina.objects.create(nombre='Dobles',limite_edad=0,deporte=deporte,tipo='Grupal',genero='Femenino', olimpiada=olimpiada, max_jugadores_lbf=2)            
    Disciplina.objects.create(nombre='Dobles',limite_edad=0,deporte=deporte,tipo='Grupal',genero='Mixto', olimpiada=olimpiada, max_jugadores_lbf=2)               
    Disciplina.objects.create(nombre='Dobles',limite_edad=45,deporte=deporte,tipo='Grupal',genero='Masculino', olimpiada=olimpiada, max_jugadores_lbf=2)
    Disciplina.objects.create(nombre='Dobles',limite_edad=45,deporte=deporte,tipo='Grupal',genero='Femenino', olimpiada=olimpiada, max_jugadores_lbf=2)            
    Disciplina.objects.create(nombre='Dobles',limite_edad=45,deporte=deporte,tipo='Grupal',genero='Mixto', olimpiada=olimpiada, max_jugadores_lbf=2)               
    
    # Pesca -------------------------------------------------------------------
    deporte=Deporte.objects.get(nombre='Pesca')
    Disciplina.objects.create(nombre='Pieza Mayor',limite_edad=0,deporte=deporte,tipo='Individual',genero='Mixto', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='Cantidad',limite_edad=0,deporte=deporte,tipo='Individual',genero='Mixto', olimpiada=olimpiada)
    
    # Tenis -------------------------------------------------------------------
    deporte=Deporte.objects.get(nombre='Tenis')
    Disciplina.objects.create(nombre='Individual',limite_edad=0,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='Individual',limite_edad=0,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='Individual',limite_edad=40,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='Individual',limite_edad=40,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='Dobles',limite_edad=0,deporte=deporte,tipo='Grupal',genero='Masculino', olimpiada=olimpiada, max_jugadores_lbf=2)
    Disciplina.objects.create(nombre='Dobles',limite_edad=0,deporte=deporte,tipo='Grupal',genero='Femenino', olimpiada=olimpiada, max_jugadores_lbf=2)
    Disciplina.objects.create(nombre='Dobles',limite_edad=0,deporte=deporte,tipo='Grupal',genero='Mixto', olimpiada=olimpiada, max_jugadores_lbf=2)
    
    # Tenis de Mesa -----------------------------------------------------------
    deporte=Deporte.objects.get(nombre='Tenis de Mesa')
    Disciplina.objects.create(nombre='Individual',limite_edad=0,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='Individual',limite_edad=0,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='Individual',limite_edad=40,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='Individual',limite_edad=40,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)    
    Disciplina.objects.create(nombre='Dobles',limite_edad=0,deporte=deporte,tipo='Grupal',genero='Mixto', olimpiada=olimpiada, max_jugadores_lbf=2)
    Disciplina.objects.create(nombre='Dobles',limite_edad=0,deporte=deporte,tipo='Grupal',genero='Masculino', olimpiada=olimpiada, max_jugadores_lbf=2)
    Disciplina.objects.create(nombre='Dobles',limite_edad=0,deporte=deporte,tipo='Grupal',genero='Femenino', olimpiada=olimpiada, max_jugadores_lbf=2)

    # Vela --------------------------------------------------------------------
    deporte=Deporte.objects.get(nombre='Vela')
    Disciplina.objects.create(nombre='Dobles',limite_edad=0,deporte=deporte,tipo='Grupal',genero='Mixto', olimpiada=olimpiada, max_jugadores_lbf=3)          
    
    # Voley -------------------------------------------------------------------
    deporte=Deporte.objects.get(nombre='Voley')
    Disciplina.objects.create(nombre='',limite_edad=0,deporte=deporte,tipo='Grupal',genero='Masculino', olimpiada=olimpiada, max_jugadores_lbf=12, max_jugadores_extras = 3)
    Disciplina.objects.create(nombre='',limite_edad=0,deporte=deporte,tipo='Grupal',genero='Femenino', olimpiada=olimpiada, max_jugadores_lbf=12, max_jugadores_extras = 3)
    
    # Caminata / Maratón ------------------------------------------------------
    deporte=Deporte.objects.get(nombre='Caminata/Carrera')
    Disciplina.objects.create(nombre='Carrera 10K',limite_edad=0,deporte=deporte,tipo='Individual', genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='Carrera 10K',limite_edad=35,deporte=deporte,tipo='Individual', genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='Carrera 10K',limite_edad=45,deporte=deporte,tipo='Individual', genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='Carrera 10K',limite_edad=55,deporte=deporte,tipo='Individual', genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='Carrera 10K',limite_edad=65,deporte=deporte,tipo='Individual', genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='Carrera 10K',limite_edad=0,deporte=deporte,tipo='Individual', genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='Carrera 10K',limite_edad=35,deporte=deporte,tipo='Individual', genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='Carrera 10K',limite_edad=45,deporte=deporte,tipo='Individual', genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='Carrera 10K',limite_edad=55,deporte=deporte,tipo='Individual', genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='Carrera 10K',limite_edad=60,deporte=deporte,tipo='Individual', genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='Carrera 5K',limite_edad=0,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)  
    Disciplina.objects.create(nombre='Carrera 5K',limite_edad=35,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)  
    Disciplina.objects.create(nombre='Carrera 5K',limite_edad=45,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)  
    Disciplina.objects.create(nombre='Carrera 5K',limite_edad=55,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)  
    Disciplina.objects.create(nombre='Carrera 5K',limite_edad=65,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='Carrera 5K',limite_edad=0,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)  
    Disciplina.objects.create(nombre='Carrera 5K',limite_edad=35,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)  
    Disciplina.objects.create(nombre='Carrera 5K',limite_edad=45,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)  
    Disciplina.objects.create(nombre='Carrera 5K',limite_edad=55,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)     
    Disciplina.objects.create(nombre='Carrera 5K',limite_edad=60,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)     
    Disciplina.objects.create(nombre='Caminata 5K',limite_edad=0,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='Caminata 5K',limite_edad=35,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='Caminata 5K',limite_edad=45,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='Caminata 5K',limite_edad=55,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='Caminata 5K',limite_edad=65,deporte=deporte,tipo='Individual',genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='Caminata 5K',limite_edad=0,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='Caminata 5K',limite_edad=35,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='Caminata 5K',limite_edad=45,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='Caminata 5K',limite_edad=55,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='Caminata 5K',limite_edad=60,deporte=deporte,tipo='Individual',genero='Femenino', olimpiada=olimpiada)

    # Trail ------------------------------------------------------
    deporte=Deporte.objects.get(nombre='Trail')
    Disciplina.objects.create(nombre='Trail 5K',limite_edad=0,deporte=deporte,tipo='Individual', genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='Trail 5K',limite_edad=30,deporte=deporte,tipo='Individual', genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='Trail 5K',limite_edad=40,deporte=deporte,tipo='Individual', genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='Trail 5K',limite_edad=50,deporte=deporte,tipo='Individual', genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='Trail 10K',limite_edad=0,deporte=deporte,tipo='Individual', genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='Trail 10K',limite_edad=30,deporte=deporte,tipo='Individual', genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='Trail 10K',limite_edad=40,deporte=deporte,tipo='Individual', genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='Trail 10K',limite_edad=50,deporte=deporte,tipo='Individual', genero='Masculino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='Trail 5K',limite_edad=0,deporte=deporte,tipo='Individual', genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='Trail 5K',limite_edad=30,deporte=deporte,tipo='Individual', genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='Trail 5K',limite_edad=40,deporte=deporte,tipo='Individual', genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='Trail 5K',limite_edad=50,deporte=deporte,tipo='Individual', genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='Trail 10K',limite_edad=0,deporte=deporte,tipo='Individual', genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='Trail 10K',limite_edad=30,deporte=deporte,tipo='Individual', genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='Trail 10K',limite_edad=40,deporte=deporte,tipo='Individual', genero='Femenino', olimpiada=olimpiada)
    Disciplina.objects.create(nombre='Trail 10K',limite_edad=50,deporte=deporte,tipo='Individual', genero='Femenino', olimpiada=olimpiada)

    # Recreativo ------------------------------------------------------
    deporte=Deporte.objects.get(nombre='Recreativo')
    Disciplina.objects.create(nombre='Truco',limite_edad=0,deporte=deporte,tipo='Grupal',genero='Mixto', olimpiada=olimpiada, max_jugadores_lbf=3)