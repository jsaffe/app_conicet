from torneo.models import Disciplina, Persona, Deporte, Olimpiada

# =============================================================================
# Logging
# =============================================================================
import logging
path_out= '/home/jsaffe/Downloads/'
logger_name = 'Test'
logger = logging.getLogger(logger_name)
logger.setLevel(logging.DEBUG)   

#File Handler
fh_debug = logging.handlers.TimedRotatingFileHandler(
                        filename=path_out+'INFO.txt', when='d', interval=1, 
                        backupCount=0, encoding=None, delay=False, utc=False)
fh_debug.setLevel(logging.INFO) 
fh_debug.setFormatter(logging.Formatter('%(message)s'))
logger.addHandler(fh_debug)


cordoba = Olimpiada.objects.get(ano='2018')

handball = Deporte.objects.get(nombre='Handball')
handball_masculino = Disciplina.objects.get(olimpiada=cordoba, genero='Masculino', deporte=handball)
handball_femenino = Disciplina.objects.get(olimpiada=cordoba, genero='Femenino', deporte=handball)

futbol = Deporte.objects.get(nombre='Fútbol')
futbol11 = Disciplina.objects.get(olimpiada=cordoba, nombre='11', genero='Masculino', deporte=futbol)
futbol7_masc = Disciplina.objects.get(olimpiada=cordoba, nombre='7', genero='Masculino', deporte=futbol)
futbol7_fem = Disciplina.objects.get(olimpiada=cordoba, nombre='7', genero='Femenino', deporte=futbol)


for disciplina in [futbol11,futbol7_masc]:
	print ('')
	print ('')
	print (str(disciplina))
   
	num=1
	total=1
	nombre_delegacion = disciplina.participantes.order_by('delegacion')[0].delegacion.nombre
	for participante in (disciplina.participantes.order_by('delegacion')):
		if nombre_delegacion not in participante.delegacion.nombre: #reiniciamos cuenta si cambia delegacion
			num=1
			nombre_delegacion = participante.delegacion.nombre
			print ('')
		print (str(total)+'  '+str(num)+' '+participante.nombre+' '+participante.apellido+' - '+str(participante.delegacion))
		num=num+1
		total=total+1


for disciplina in Disciplina.objects.filter(olimpiada=cordoba).all():
	print ('')
	print ('')
	print (str(disciplina))
   
	num=1
	total=1
	if len(disciplina.participantes.all()):
		nombre_delegacion = disciplina.participantes.order_by('delegacion')[0].delegacion.nombre
	for participante in (disciplina.participantes.order_by('delegacion')):
		if nombre_delegacion not in participante.delegacion.nombre: #reiniciamos cuenta si cambia delegacion
			num=1
			nombre_delegacion = participante.delegacion.nombre
			print ('')
		print (str(total)+'  '+str(num)+' '+participante.nombre+' '+participante.apellido+' - '+str(participante.delegacion))
		num=num+1
		total=total+1
		

for disciplina in Disciplina.objects.filter(olimpiada=cordoba).all():
	logger.info ('')
	logger.info ('')
	logger.info (str(disciplina))
   
	num=1
	total=1
	if len(disciplina.participantes.all()):
		nombre_delegacion = disciplina.participantes.order_by('delegacion')[0].delegacion.nombre
	for participante in (disciplina.participantes.order_by('delegacion')):
		if nombre_delegacion not in participante.delegacion.nombre: #reiniciamos cuenta si cambia delegacion
			num=1
			nombre_delegacion = participante.delegacion.nombre
			logger.info ('')
		logger.info (str(total)+'  '+str(num)+' '+participante.nombre+' '+participante.apellido+' - '+str(participante.delegacion))
		num=num+1
		total=total+1


#for disciplina in [futbol11,futbol7_masc]:
#	print ('')
#	print (str(disciplina))
#	for participante in (disciplina.participantes.order_by('delegacion')):
#		print (participante.nombre+' '+participante.apellido+' - '+str(participante.delegacion))
		

# for disciplina in [futbol11,futbol7_masc]:
#	print ('')
#   print (str(disciplina))
    
#    num=1
#   delegacion = disciplina.participantes.order_by('delegacion')[0].delegacion.nombre
#	for participante in (disciplina.participantes.order_by('delegacion')):
#		if delegacion not in participante.delegacion.nombre: #reiniciamos cuenta si cambia delegacion
#			num=1
#		print (str(num)+' '+participante.nombre+' '+participante.apellido+' - '+str(participante.delegacion))	
#		num=num+1


danza = Arte.objects.get(nombre='Danza')
danzadores = Persona.objects.filter(artes=danza)

for arte in Arte.objects.all():
	logger.info ('')
	logger.info ('')
	logger.info (str(arte))
   
	participantes = Persona.objects.filter(artes=arte)

	num=1
	total=1
	if len(participantes.all()):
		nombre_delegacion = participantes.order_by('delegacion')[0].delegacion.nombre
	for participante in (participantes.order_by('delegacion')):
		if nombre_delegacion not in participante.delegacion.nombre: #reiniciamos cuenta si cambia delegacion
			num=1
			nombre_delegacion = participante.delegacion.nombre
			logger.info ('')
		logger.info (str(total)+'  '+str(num)+' '+participante.nombre+' '+participante.apellido+' - '+str(participante.delegacion))
		num=num+1
		total=total+1

