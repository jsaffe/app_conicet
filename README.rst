Inicialización Proyecto
------------------------
1) Instalar Dependencias
* git
* docker-engine

2) Clonamos Repositorio
* git clone https://gitlab.com/jsaffe/app_conicet.git
* Se debe tener una cuenta en gitlab.com y ser miembro del proyecto.

3) Inicializamos Contenedores
* docker-compose up -d --build

4) Inicilización de base de datos
  - Inicializamos desde cero
    * docker exec -it <id_container_webapp> bash
    * python manage.py makemigrations
    * python manage.py makemigrations torneo
    * python manage.py migrate
    * python manage.py createsuperuser
    * python manage.py shell
      - exec(open('/app/website/manage/manage_db/iniciar_juego_deportivo/<edicion>/iniciar_jd.py').read())
      - ej: exec(open('/app/website/manage/manage_db/iniciar_juego_deportivo/2023_Salta/iniciar_jd.py').read())

  - Restaurar copia de version operativa (opcional)
    * Restaurar base de datos
      * Abrir shell en el container "postgres"
      * su postgres
      * pg_restore -d jdconicet /home/jdconicet_20230511.tar
      * repetir la linea anterior una vez mas
    
    * Restaurar carpeta media
      * Descargar la carpeta "media" desde server remoto.
      * Mover carpeta descargada en "../media"
      * python manage.py thumbnail clear_delete_all


Inicializar Nuevo Juego Deportivo
---------------------------------
* Ingresar a container webapp:
  docker ps
  copiar <id_container_webapp>
  docker exec -it <id_container_webapp> bash

* Desde container webapp:
  python manage.py shell

  cargar nueva edición y disciplinas:
  - exec(open('/app/website/manage/manage_db/iniciar_juego_deportivo/<edicion>/iniciar_jd.py').read())
  - ej: exec(open('/app/website/manage/manage_db/iniciar_juego_deportivo/2024_Cordoba/iniciar_jd.py').read())

* Desde admin webapp
  - Modificar Olimpiada creada
     * Cargar Logo
     * Modificar cualquier otro campo necesario.

 - Cargar Imagenes del "Slider" principal:
  * Abrir web admin e ir a Fotos
 	* Click Boton "Add Fotos"
  * Título: "__slider__"
  * Cargar imagen desde "Choose File".
 	  Los nombres de las imagenes es aconsejable que tengan el siguiente formato:
    "__slider_nombreOlimpiada_numSlider"
    Ej:"__slider_Cordoba18_1"
 	  Las imagenes deben tener una resolución óptima de 2000x400 px (formato apaisado)
  * Completar campos de Fecha y Hora
  * Asociar a una olimpiada
  * Save

 - Cargar VideoPortada
  * Abrir web admin e ir a Posts
 	* Add Post
  * Título: Video_Portada_NombreEdicion22
  * Video Link: <link a video youtube>
    Abrir el video en youtube, ir a share, de "Embed Video" copiar "src" url.
  * Resumen: *
  * Text: *
  * Etiqueta: Video_Portada
  * Olimpiadas: seleccionar edicion
  * Save

Backup Base de Datos
---------------------
* Desde container postgres:
  pg_dump -U postgres jdconicet -Ft > /home/bak_<date>.tar
* Se almacenará en ./baks
