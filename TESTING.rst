Testing Modalidades y Tablas
-----------------------------
- 2022 SJ
 * PASS: Posición Manual Deportes Individuales
 * PASS: Posición Manual Deportes Grupales
 * PASS: Competencia Única Deportes Individuales
 * PASS: Sumotaria Tabla Deporte
 * PASS: Sumotaria Copa Conjunto
 * PASS: Trail:
    - Test pos manual
    - Test comp unica 
    - Sumatoria tabla deporte y copa conjunto
 * PASS: Playoff_ 8 equipos


Test Pendientes Salta 23'
-------------------------
  * FIX: Error al actualizar fixture: Si resultados_arte no está creado tira
    error, tiene.un get, usar filter e if
    models > Olimpiada > actualizar_tabla_copaconjunto >
      #Se realiza la sumatoria de los puntos por Arte

  * Inscripciones: ver recreativos, mepa que por defecto todos se inscriben al
    deporte, entonces despues cuando filtras desde admin no filtra nada, deberían
    inscribirse solo sí lo elegiste, o si no elegis ninguna actividad eliminar el
    deporte recreativo de la inscripcion.

  * playoffs_4
  * playoffs_6
  * grupos+playoffs