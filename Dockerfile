FROM  registry.gitlab.com/jsaffe/app_conicet/webapp:base

# -----------------------------------------------------------------------------
# Inicializamos
# -----------------------------------------------------------------------------
# EXPOSE 8003
COPY . /app/website/

WORKDIR /app/website/

CMD ["python", "manage.py", "runserver", "0.0.0.0:8003"]