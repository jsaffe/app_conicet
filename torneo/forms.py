# -*- coding: utf-8 -*-
from .models import Resultado, Persona, Disciplina, Olimpiada, Arte, Grupos_mas_Playoff, Inscripcion, Deporte
from django import forms
from django.core.exceptions import ValidationError    
from django.template.defaultfilters import filesizeformat
from django.utils.translation import ugettext_lazy as _  


class ResultadoCompetenciaForm(forms.Form):
    participante = forms.CharField(label='Equipo/Competidor', max_length=100,  disabled=False)
    goles_favor = forms.IntegerField(label='Goles a Favor', min_value=0, max_value=100)
    amarillas = forms.IntegerField(label='Amarillas', min_value=0, max_value=50)
    rojas = forms.IntegerField(label='Rojas', min_value=0, max_value=25)
    penales = forms.IntegerField(label='Def. por Penales -> N°Convertidos', min_value=0, max_value=25)
    

class ResultadoForm(forms.ModelForm):
    class Meta:
        model = Resultado
        fields = '__all__'


class FutbolForm(forms.ModelForm):
    class Meta:
        model = Resultado
        fields = ('equipo','participante','goles_favor', 'penales', 'amarillas', 'rojas', 'presentado')
        widgets = { 'goles_favor':forms.TextInput(attrs={'size':'4'}), #achica el ancho de la celda
                    'penales':forms.TextInput(attrs={'size':'4'}),   
                    'amarillas':forms.TextInput(attrs={'size':'4'}),   
                    'rojas':forms.TextInput(attrs={'size':'4'}),}

    def __init__(self, *args, **kwargs):
            super(FutbolForm, self).__init__(*args, **kwargs)
            self.fields['participante'].disabled = True  #read-only field
            self.fields['equipo'].disabled = True
        

class HandballForm(forms.ModelForm):
    class Meta:
        model = Resultado
        fields = ('equipo','participante','goles_favor', 'penales', 'amarillas', 'rojas','azules', 'presentado')
        widgets = { 'goles_favor':forms.TextInput(attrs={'size':'4'}),
                    'penales':forms.TextInput(attrs={'size':'4'}),   
                    'amarillas':forms.TextInput(attrs={'size':'4'}),   
                    'rojas':forms.TextInput(attrs={'size':'4'}),   
                    'azules':forms.TextInput(attrs={'size':'4'}),}

    def __init__(self, *args, **kwargs):
            super(HandballForm, self).__init__(*args, **kwargs)
            self.fields['participante'].disabled = True
            self.fields['equipo'].disabled = True


class HockeyForm(forms.ModelForm):
    class Meta:
        model = Resultado
        fields = ('equipo','participante','goles_favor', 'penales', 'amarillas', 'rojas','verdes', 'presentado')
        widgets = { 'goles_favor':forms.TextInput(attrs={'size':'4'}),
                    'penales':forms.TextInput(attrs={'size':'4'}),   
                    'amarillas':forms.TextInput(attrs={'size':'4'}),   
                    'rojas':forms.TextInput(attrs={'size':'4'}),   
                    'verdes':forms.TextInput(attrs={'size':'4'}), }

    def __init__(self, *args, **kwargs):
            super(HockeyForm, self).__init__(*args, **kwargs)
            self.fields['participante'].disabled = True
            self.fields['equipo'].disabled = True


class PuntosForm(forms.ModelForm):
    class Meta:
        model = Resultado
        fields = ('equipo','participante','puntos_favor', 'presentado')
        widgets = { 'puntos_favor':forms.TextInput(attrs={'size':'4'}), }

    def __init__(self, *args, **kwargs):
            super(PuntosForm, self).__init__(*args, **kwargs)
            self.fields['participante'].disabled = True
            self.fields['equipo'].disabled = True


class TiemposForm(forms.ModelForm):
    class Meta:
        model = Resultado
        fields = ('equipo','participante','tiempo', 'presentado')
        widgets = { 'tiempo':forms.TextInput(attrs={'size':'7'}),   }

    def __init__(self, *args, **kwargs):
        super(TiemposForm, self).__init__(*args, **kwargs)
        self.fields['participante'].disabled = True
        self.fields['equipo'].disabled = True


class AtletismoForm(forms.ModelForm):
    class Meta:
        model = Resultado
        fields = ('equipo','participante','distancia', 'presentado')
        widgets = { 'distancia':forms.TextInput(attrs={'size':'7'}), }

    def __init__(self, *args, **kwargs):
            super(AtletismoForm, self).__init__(*args, **kwargs)
            self.fields['participante'].disabled = True
            self.fields['equipo'].disabled = True


class PescaForm(forms.ModelForm):
    class Meta:
        model = Resultado
        fields = ('equipo','participante','peso','cantidad','presentado')
        widgets = { 'peso':forms.TextInput(attrs={'size':'4'}),
                    'cantidad':forms.TextInput(attrs={'size':'4'}),   }

    def __init__(self, *args, **kwargs):
            super(PescaForm, self).__init__(*args, **kwargs)
            self.fields['participante'].disabled = True
            self.fields['equipo'].disabled = True


class SetPuntosForm(forms.ModelForm):
    class Meta:
        model = Resultado
        fields = (  'equipo','participante',
                    'puntos_favor_1set', 'puntos_favor_2set', 'puntos_favor_3set', 
                    'amarillas', 'rojas', 'presentado',)
        widgets = { 'puntos_favor_1set':forms.TextInput(attrs={'size':'4'}),
                    'puntos_favor_2set':forms.TextInput(attrs={'size':'4'}),   
                    'puntos_favor_3set':forms.TextInput(attrs={'size':'4'}),   
                    'amarillas':forms.TextInput(attrs={'size':'4'}),   
                    'rojas':forms.TextInput(attrs={'size':'4'}), }

    def __init__(self, *args, **kwargs):
            super(SetPuntosForm, self).__init__(*args, **kwargs)
            self.fields['participante'].disabled = True
            self.fields['equipo'].disabled = True


class SetGamesForm(forms.ModelForm):
    class Meta:
        model = Resultado
        fields = ('equipo','participante','games_favor_1set','games_favor_2set','games_favor_3set', 'presentado')
        widgets = { 'games_favor_1set':forms.TextInput(attrs={'size':'4'}),
                    'games_favor_2set':forms.TextInput(attrs={'size':'4'}),   
                    'games_favor_3set':forms.TextInput(attrs={'size':'4'}),}

    def __init__(self, *args, **kwargs):
            super(SetGamesForm, self).__init__(*args, **kwargs)
            self.fields['participante'].disabled = True
            self.fields['equipo'].disabled = True


class PosicionForm(forms.ModelForm):
    class Meta:
        model = Resultado
        fields = ('equipo','participante','posicion', 'presentado')
        widgets = { 'posicion':forms.TextInput(attrs={'size':'7'}), }

    def __init__(self, *args, **kwargs):
            super(PosicionForm, self).__init__(*args, **kwargs)
            self.fields['participante'].disabled = True
            self.fields['equipo'].disabled = True


class InscripcionParte1Form(forms.ModelForm):   
    class Meta:
        model = Persona
        fields = '__all__'
        exclude = ('olimpiadas','deportes','disciplinas_deporte1','disciplinas_deporte2','disciplinas_deporte3',
                    'inscripcion_completa','inscripcion_fecha','mostrar_edad','usuario',
                    'ultima_modificacion', 'mail_reset_password_enviado','mail_inscripcion_enviado',
                    'certificado_medico','comprobante_pago','vinculacion_conicet',
                    'deporte1','deporte2','deporte3','artes',
                    'alojamiento','acompanantes','deporte_maraton',
                    'divulgacion','colaborador','alimentacion')
        widgets = {'fecha_nacimiento': forms.DateInput(attrs={'type':'date'})}

    def __init__(self, *args, **kwargs):
        id_olimpiada = kwargs.pop('id_olimpiada')
        super(InscripcionParte1Form, self).__init__(*args, **kwargs)


    def clean(self):                    
        # Chequeamos Tamaño y Formato de Archivos Subidos
        MAX_UPLOAD_SIZE = 3670016 #bytes = 3.5MB
        
        #Chequo Tamaño y Formato de Foto
        if 'foto' in self.cleaned_data.keys():
            foto  = self.cleaned_data['foto']

            if 'FieldFile' not in str(type(foto)):     #cuando se modifica el formulario y no se cambian las fotos, el objeto devuelto es de tipo FieldFile
                                                       #solo comprobamos tamaño y tipo cuando son imagenes nuevas
                print ('Foto',foto.content_type)        
                content_type = foto.content_type.split('/')[0]
                if content_type not in ['image']:
                    print ('Error en el formato de imagen. Por favor suba un formato de imagen válida')
                    raise forms.ValidationError('Error en el formato de imagen. Por favor suba un formato de imagen válida')
        
                if foto._size > MAX_UPLOAD_SIZE:
                    print (_('Por favor reduzca el tamaño de la Foto a menos de %s. El tamaño actual es de %s') % (filesizeformat(MAX_UPLOAD_SIZE), filesizeformat(foto._size)))
                    raise forms.ValidationError(_('Por favor reduzca el tamaño de la Foto a menos de %s. El tamaño actual es de %s') % (filesizeformat(MAX_UPLOAD_SIZE), filesizeformat(foto._size)))

            
        #Chequeo Tamaño y Formato de Archivos
        file1 = self.cleaned_data['documento_digitalizado'] if ('documento_digitalizado' in self.cleaned_data.keys()) else None
        
        for [file,file_name] in ((file1,'Documento Digitalizado'),):
            if file:
                if 'FieldFile' not in str(type(file)):
                    print (file_name, file.content_type)        
                    file_format = file.content_type.split('/')[1]
                    if file_format not in ['pdf','jpeg','gif','png']:
                        print ('Error en el formato del '+ file_name +'. Por favor suba el documento en pdf o mediante un formato de imagen válido (png,jpg,gif)')
                        raise forms.ValidationError('Error en el formato del '+ file_name +'. Por favor suba el documento en pdf o mediante un formato de imagen válido (png,jpg,gif)')
                    
                    if file._size > MAX_UPLOAD_SIZE:
                        print (_('Por favor reduzca el tamaño de %s a menos de %s. El tamaño actual es de %s') % (file_name, filesizeformat(MAX_UPLOAD_SIZE), filesizeformat(file._size)))
                        raise forms.ValidationError(_('Por favor reduzca el tamaño de %s a menos de %s. El tamaño actual es de %s') % (file_name, filesizeformat(MAX_UPLOAD_SIZE), filesizeformat(file._size)))
        

class InscripcionParte2Form(forms.ModelForm):   
    class Meta:
        model = Inscripcion
        # fields = '__all__'
        fields = (
            'aceptacion_terminos', 'estado_salud',
            'deporte1', 'deporte2', 'deporte3', 'deporte_maraton',
            'certificado_medico', 'comprobante_pago', 'vinculacion_conicet',
            'artes', 
            'alojamiento', 'alimentacion', 'acompanantes',
            # 'divulgacion', colaborador'
            )
            
        exclude = (
            'olimpiada','jugador', 'deportes', 
            'disciplinas_deporte1','disciplinas_deporte2','disciplinas_deporte3',
            'actividades_recreativas',
            'inscripcion_completa','inscripcion_fecha','ultima_modificacion')        

    def __init__(self, *args, **kwargs):
        id_olimpiada = kwargs.pop('id_olimpiada')
        id_jugador = kwargs.pop('id_jugador')
        self.id_jugador = id_jugador

        super(InscripcionParte2Form, self).__init__(*args, **kwargs)

        olimpiada = Olimpiada.objects.get(pk=id_olimpiada)

        self.fields["artes"].widget = forms.widgets.CheckboxSelectMultiple()
        self.fields["artes"].help_text = ""
        self.fields["artes"].queryset = olimpiada.artisticos.all()

        self.fields['deporte1'].queryset = olimpiada.deportes.all().exclude(nombre='Caminata/Carrera')
        self.fields['deporte2'].queryset = olimpiada.deportes.all().exclude(nombre='Caminata/Carrera')

        # Para habilitar el 3 tercer deporte se debe reestablecer el queryset
        # (descomentar la siguiente línea)
        # self.fields['deporte3'].queryset = olimpiada.deportes.all().exclude(nombre='Caminata/Carrera')
        # (y comentar esta línea)
        self.fields['deporte3'].queryset = olimpiada.deportes.none()

    def clean_aceptacion_terminos(self):
        accept_terms = self.cleaned_data.get('aceptacion_terminos')
        if not accept_terms:
            raise ValidationError('Debes aceptar los términos y condiciones para continuar.')
        return accept_terms

    def clean(self):                    
        # Chequeamos Tamaño y Formato de Archivos Subidos
        MAX_UPLOAD_SIZE = 3670016 #bytes = 3.5MB
        
        # Chequo Tamaño y Formato de Foto
        if 'foto' in self.cleaned_data.keys():
            foto  = self.cleaned_data['foto']

            if 'FieldFile' not in str(type(foto)):     #cuando se modifica el formulario y no se cambian las fotos, el objeto devuelto es de tipo FieldFile
                                                       #solo comprobamos tamaño y tipo cuando son imagenes nuevas
                print ('Foto',foto.content_type)        
                content_type = foto.content_type.split('/')[0]
                if content_type not in ['image']:
                    print ('Error en el formato de imagen. Por favor suba un formato de imagen válida')
                    raise forms.ValidationError('Error en el formato de imagen. Por favor suba un formato de imagen válida')
        
                if foto._size > MAX_UPLOAD_SIZE:
                    print (_('Por favor reduzca el tamaño de la Foto a menos de %s. El tamaño actual es de %s') % (filesizeformat(MAX_UPLOAD_SIZE), filesizeformat(foto._size)))
                    raise forms.ValidationError(_('Por favor reduzca el tamaño de la Foto a menos de %s. El tamaño actual es de %s') % (filesizeformat(MAX_UPLOAD_SIZE), filesizeformat(foto._size)))

            
        # Chequeo Tamaño y Formato de Archivos
        file2 = self.cleaned_data['certificado_medico'] if ('certificado_medico' in self.cleaned_data.keys()) else None
        file3 = self.cleaned_data['comprobante_pago'] if ('comprobante_pago' in self.cleaned_data.keys()) else None
        file4 = self.cleaned_data['vinculacion_conicet'] if ('vinculacion_conicet' in self.cleaned_data.keys()) else None
            
        for [file,file_name] in ((file2,'Certificado Médico'),(file3,'Comprobante de Pago'),(file4,'Certificado Vinculacion CONICET')):
            if file:
                if 'FieldFile' not in str(type(file)):
                    print (file_name, file.content_type)        
                    file_format = file.content_type.split('/')[1]
                    if file_format not in ['pdf','jpeg','gif','png']:
                        print ('Error en el formato del '+ file_name +'. Por favor suba el documento en pdf o mediante un formato de imagen válido (png,jpg,gif)')
                        raise forms.ValidationError('Error en el formato del '+ file_name +'. Por favor suba el documento en pdf o mediante un formato de imagen válido (png,jpg,gif)')
                    
                    if file._size > MAX_UPLOAD_SIZE:
                        print (_('Por favor reduzca el tamaño de %s a menos de %s. El tamaño actual es de %s') % (file_name, filesizeformat(MAX_UPLOAD_SIZE), filesizeformat(file._size)))
                        raise forms.ValidationError(_('Por favor reduzca el tamaño de %s a menos de %s. El tamaño actual es de %s') % (file_name, filesizeformat(MAX_UPLOAD_SIZE), filesizeformat(file._size)))

        jugador = Persona.objects.get(pk=self.id_jugador) 

        # Comprobación del certificado de vinculación: si es personal conicet o ex-conicet es obligatorio
        if (jugador.tipo_personal in ['NO CONICET','EX-CONICET']):
            if self.cleaned_data['vinculacion_conicet'] is None:
                raise forms.ValidationError('El certificado de vinculación es obligatorio si NO es personal de CONICET o si es EX-CONICET. Por favor suba un certificado válido.')

        # # Comprobacion link web laboral si se eligio participar en espacio de divulgación
        # if (self.cleaned_data['divulgacion'] in ['Sí','Tal vez']):
        #     if jugador.link_web_laboral is None:
        #         raise forms.ValidationError('Eligió que desea o tal vez desee participar del espacio de divulgación, es este caso es obligatorio completar el link a su sitio web laboral.')
            
        # Comprobamos que no se repitan los deportes elegidos
        deporte1 = str(self.cleaned_data['deporte1'])
        deporte2 = str(self.cleaned_data['deporte2'])
        deporte3 = str(self.cleaned_data['deporte3'])       
    
        if (deporte1 not in 'None') and (deporte1 in [deporte2,deporte3]):
            raise forms.ValidationError('No es posible elegir 2 veces el mismo deporte.')
        
        if ((deporte2 not in 'None') and (deporte3 not in 'None')) and (deporte2 in deporte3): #si el deporte2 = deporte3 y no son nulos ambos
            raise forms.ValidationError('No es posible elegir 2 veces el mismo deporte.')


class InscripcionParte2bForm(forms.ModelForm):
    class Meta:
        model = Inscripcion
        fields = ('disciplinas_deporte1',
                  'disciplinas_deporte2',
                  'disciplinas_deporte3',
                  'actividades_recreativas')
        widgets = {'disciplinas_deporte1': forms.CheckboxSelectMultiple,
                   'disciplinas_deporte2': forms.CheckboxSelectMultiple,
                   'disciplinas_deporte3': forms.CheckboxSelectMultiple,
                   'actividades_recreativas': forms.CheckboxSelectMultiple}


    def __init__(self, *args, **kwargs):
        id_olimpiada = kwargs.pop('id_olimpiada')
        id_jugador = kwargs.pop('id_jugador')

        super(InscripcionParte2bForm, self).__init__(*args, **kwargs)
        
        olimpiada = Olimpiada.objects.get(pk=id_olimpiada) 
        jugador = Persona.objects.get(pk=id_jugador)  
        
        # --------------------------------------------------------------------------------------------------------------
        # Reglas de Selección de Disciplinas:
        # GENERALES: simpre se filtra por olimpiada, deporte y género.
        # 
        # PARTICULARES POR DEPORTE:
        # * Natación y Atletismo:
        #   - Disciplinas Individuales se muestran solo aquellas en el rango de edad del participante
        #   - Disciplinas Grupales (Postas) se muestran todas.
        #   - Se usan los rangos de edades 0,+35,+45,+55,+65
        # * Pesca: se muestra una sola disciplina pero luego se selecciona la otra automáticamente.
        # * Caso Deportes Individuales con Edades Estandarizadas (0,+35,+45,+55,+65)
        # * Restantes Deportes: se muestran todas las disciplinas que tengan categorías con
        #   edad limite igual o inferior a la edad del participante.
        # --------------------------------------------------------------------------------------------------------------
        #Genero Opuesto
        if jugador.genero == 'Masculino':
            genero_opuesto = 'Femenino'
        elif jugador.genero == 'Femenino':
            genero_opuesto = 'Masculino'
        else:
            genero_opuesto = ''

        #Edad en Olimpiada
        edad_jugador = jugador.calcular_edad_en_olimpiada(olimpiada)
        
        #Categorias de Edad de Atletismo y Natacion
        if edad_jugador < 35:
            edad=0
        elif edad_jugador >= 35 and edad_jugador < 45:
            edad=35
        elif edad_jugador >= 45 and edad_jugador < 55:
            edad=45
        elif edad_jugador >= 55 and edad_jugador < 65:
            edad=55
        else:
            edad=65

        # Filtros por Deporte
        # Se filtran las disciplinas a mostrarse en funcion del deporte, edad, genero y
        # otras políticas adoptadas por los delegados.
        # Deporte Principal
        if self.instance.deporte1 is not None:
            #print ('Deporte1',self.instance.deporte1)
            # Caso Deportes Individuales y Grupales con Edades Estandarizadas (0,+35,+45,+55,+65)
            if (self.instance.deporte1.nombre in ['Atletismo','Natación']):
                queryset_individuales = Disciplina.objects.filter(olimpiada=olimpiada, deporte=self.instance.deporte1, tipo='Individual').exclude(genero=genero_opuesto).filter(limite_edad=edad).order_by('limite_edad')
                queryset_grupales = Disciplina.objects.filter(olimpiada=olimpiada, deporte=self.instance.deporte1, tipo='Grupal').exclude(genero=genero_opuesto).filter(limite_edad__lte=edad_jugador).order_by('limite_edad')  
                self.fields['disciplinas_deporte1'].queryset = (queryset_grupales | queryset_individuales).order_by('-tipo')

            # Caso Deportes Individuales con Edades Estandarizadas (0,+35,+45,+55,+65)
            elif (self.instance.deporte1.nombre in ['Aguas Abiertas', 'Ciclismo', 'Trail']):
                self.fields['disciplinas_deporte1'].queryset = Disciplina.objects.filter(olimpiada=olimpiada, deporte=self.instance.deporte1, tipo='Individual').exclude(genero=genero_opuesto).filter(limite_edad=edad).order_by('limite_edad')

            # Caso Pesca
            elif (self.instance.deporte1.nombre in ['Pesca']):
                self.fields['disciplinas_deporte1'].queryset = Disciplina.objects.filter(olimpiada=olimpiada, deporte=self.instance.deporte1, nombre='Pieza Mayor')

            # Casos Restantes: se muestran todas las categorías para el género de igual o menor categoría
            else:
                self.fields['disciplinas_deporte1'].queryset = Disciplina.objects.filter(olimpiada=olimpiada, deporte=self.instance.deporte1).exclude(genero=genero_opuesto).filter(limite_edad__lte=edad_jugador).order_by('limite_edad')
        else:
            self.fields['disciplinas_deporte1'].queryset = Disciplina.objects.filter(olimpiada=olimpiada, deporte=self.instance.deporte1).exclude(genero=genero_opuesto).filter(limite_edad__lte=edad_jugador).order_by('limite_edad')  
            
        # Deporte Secundario
        if self.instance.deporte2 is not None: 
            #print ('Deporte2',self.instance.deporte2)    
            if (self.instance.deporte2.nombre in ['Atletismo','Natación']):
                queryset_individuales = Disciplina.objects.filter(olimpiada=olimpiada, deporte=self.instance.deporte2, tipo='Individual').exclude(genero=genero_opuesto).filter(limite_edad=edad).order_by('limite_edad')  
                queryset_grupales = Disciplina.objects.filter(olimpiada=olimpiada, deporte=self.instance.deporte2, tipo='Grupal').exclude(genero=genero_opuesto).filter(limite_edad__lte=edad_jugador).order_by('limite_edad')  
                self.fields['disciplinas_deporte2'].queryset = (queryset_grupales | queryset_individuales).order_by('-tipo')
            elif (self.instance.deporte2.nombre in ['Aguas Abiertas', 'Ciclismo', 'Trail']):
                self.fields['disciplinas_deporte2'].queryset = Disciplina.objects.filter(olimpiada=olimpiada, deporte=self.instance.deporte2, tipo='Individual').exclude(genero=genero_opuesto).filter(limite_edad=edad).order_by('limite_edad')
            elif (self.instance.deporte2.nombre in ['Pesca']):
                self.fields['disciplinas_deporte2'].queryset = Disciplina.objects.filter(olimpiada=olimpiada, deporte=self.instance.deporte2,nombre='Pieza Mayor')
            else:
                self.fields['disciplinas_deporte2'].queryset = Disciplina.objects.filter(olimpiada=olimpiada, deporte=self.instance.deporte2).exclude(genero=genero_opuesto).filter(limite_edad__lte=edad_jugador).order_by('limite_edad') 
        else:
            self.fields['disciplinas_deporte2'].queryset = Disciplina.objects.filter(olimpiada=olimpiada, deporte=self.instance.deporte2).exclude(genero=genero_opuesto).filter(limite_edad__lte=edad_jugador).order_by('limite_edad') 
        
        # Deporte Terciario
        if self.instance.deporte3 is not None:
            #print ('Deporte3',self.instance.deporte3)
            if (self.instance.deporte3.nombre in ['Atletismo','Natación']):
                queryset_individuales = Disciplina.objects.filter(olimpiada=olimpiada, deporte=self.instance.deporte3, tipo='Individual').exclude(genero=genero_opuesto).filter(limite_edad=edad).order_by('limite_edad')  
                queryset_grupales = Disciplina.objects.filter(olimpiada=olimpiada, deporte=self.instance.deporte3, tipo='Grupal').exclude(genero=genero_opuesto).filter(limite_edad__lte=edad_jugador).order_by('limite_edad')  
                self.fields['disciplinas_deporte3'].queryset = (queryset_grupales | queryset_individuales).order_by('-tipo')
            elif (self.instance.deporte3.nombre in ['Aguas Abiertas', 'Ciclismo', 'Trail']):
                self.fields['disciplinas_deporte3'].queryset = Disciplina.objects.filter(olimpiada=olimpiada, deporte=self.instance.deporte3, tipo='Individual').exclude(genero=genero_opuesto).filter(limite_edad=edad).order_by('limite_edad')
            elif (self.instance.deporte3.nombre in ['Pesca']):
                self.fields['disciplinas_deporte3'].queryset = Disciplina.objects.filter(olimpiada=olimpiada, deporte=self.instance.deporte3,nombre='Pieza Mayor')
            else:
                self.fields['disciplinas_deporte3'].queryset = Disciplina.objects.filter(olimpiada=olimpiada, deporte=self.instance.deporte3).exclude(genero=genero_opuesto).filter(limite_edad__lte=edad_jugador).order_by('limite_edad')
        else:
            self.fields['disciplinas_deporte3'].queryset = Disciplina.objects.filter(olimpiada=olimpiada, deporte=self.instance.deporte3).exclude(genero=genero_opuesto).filter(limite_edad__lte=edad_jugador).order_by('limite_edad') 
    
        #Actividades Recreativas
        recreativo = Deporte.objects.get(nombre='Recreativos')
        self.fields['actividades_recreativas'].queryset = Disciplina.objects.filter(olimpiada=olimpiada, deporte=recreativo).exclude(genero=genero_opuesto).filter(limite_edad__lte=edad_jugador).order_by('limite_edad')  


    def clean(self):
        max_disciplinas_generales ={'Tenis':3, 'Ciclismo':1}       
        
        deporte1= self.instance.deporte1
        deporte2= self.instance.deporte2
        deporte3= self.instance.deporte3
        disciplinas_deporte1 = self.cleaned_data.get('disciplinas_deporte1')
        disciplinas_deporte2 = self.cleaned_data.get('disciplinas_deporte2')
        disciplinas_deporte3 = self.cleaned_data.get('disciplinas_deporte3')        
        
        # --------------------------------------------------------------------------------------------------------------
        # Reglas de Validación para la Selección de Disciplinas     
        # --------------------------------------------------------------------------------------------------------------
        for [deporte,disciplinas_deporte]in [(deporte1,disciplinas_deporte1),(deporte2,disciplinas_deporte2),(deporte3,disciplinas_deporte3)]:
            if deporte:
                # Regla de obligatoriedad de elección de al menos una disciplina por deporte.
                if not disciplinas_deporte: 
                    raise ValidationError('Debe seleccionar al menos 1 disciplina para '+deporte.nombre+'.')
                
                # Reglas Selección Disciplinas Natación y Atletismo:
                # restricciones máximas solo en disciplinas individuales, las postas se pueden elegir todas.
                if (deporte.nombre in ['Natación','Atletismo']):
                    max_disciplinas_individuales = 5
                    n_disciplinas_individuales = 0  

                    max_disciplinas_postaamericana = 1
                    n_disciplinas_postaamericana = 0
                    
                    if disciplinas_deporte:
                        for disciplina in disciplinas_deporte: #contabilizamos las disciplinas seleccionadas y verificamos cuantas de tipo grupal e individual hay (no se cuenta aguas abiertas)
                            if (disciplina.tipo in 'Individual') and (disciplina.nombre not in 'Aguas Abiertas'):
                                n_disciplinas_individuales = n_disciplinas_individuales + 1

                            if ((disciplina.tipo in 'Grupal') and (disciplina.nombre in 'Posta Americana')):
                                n_disciplinas_postaamericana = n_disciplinas_postaamericana + 1            

                        if n_disciplinas_individuales > max_disciplinas_individuales:
                            if deporte.nombre in ['Natación']:
                                raise ValidationError('Solo se permite la inscripción a un máximo de '+ str(max_disciplinas_individuales) + ' disciplinas individuales, más la competencia de aguas abiertas, las 2 postas 4x25 y una sola posta americana para Natación.')
                               
                            if deporte.nombre in ['Atletismo']:
                                raise ValidationError('Solo se permite la inscripción a un máximo de '+ str(max_disciplinas_individuales) + ' disciplinas individuales y a todas las postas grupales para Atletismo.')

                        if n_disciplinas_postaamericana > max_disciplinas_postaamericana:
                            raise ValidationError('Solo se permite la inscripción a un máximo de '+ str(max_disciplinas_individuales) + ' disciplinas individuales, más la competencia de aguas abiertas, las 2 postas 4x25 y una sola posta americana para Natación.')

                # Regla General para deportes con límites máximos de diciplinas a inscribirse
                # sin diferenciar el tipo de disciplina. 
                # Configurable con diccionario: max_disciplinas_generales
                # Ej. max_disciplinas_generales ={'Tenis':3, 'Ciclismo':1} se permitirìa un máximo
                # de 3 disciplinas en Tenis y una sola en ciclismo.
                if (deporte.nombre in max_disciplinas_generales.keys()):
                    print (deporte.nombre)                   
                    if disciplinas_deporte:  #si hay disciplinas seleccionadas                       
                        if disciplinas_deporte.count() > max_disciplinas_generales[deporte.nombre]:  #si hay mas disciplinas seleccionadas que el permitido
                            raise ValidationError('Solo se permite un máximo de '+ str(max_disciplinas_generales[deporte.nombre]) + ' disciplinas para '+deporte.nombre+'.')
             

class profileForm(forms.ModelForm):
    class Meta:
        model = Persona
        fields = ('foto','facebook','telefono_personal',
                  'mostrar_edad', 'descripcion_personal',
        		  'localidad','institucion',
                  'link_web_laboral', 'telefono_laboral'
                  # 'certificado_medico','comprobante_pago',
                  # 'vinculacion_conicet'
                  )


class ConfGruposEquiposForm(forms.ModelForm):
    class Meta:
        model = Grupos_mas_Playoff
        fields = ('__all__')
        exclude = ('disciplina','grupo_p1','grupo_p2','grupo_p3','grupo_p4','grupo_p5')

    def __init__(self, *args, **kwargs):
        id_disciplina = kwargs.pop('id_disciplina')
        super(ConfGruposEquiposForm, self).__init__(*args, **kwargs)

        disciplina = Disciplina.objects.get(pk=id_disciplina) 
        self.fields['grupo_e1'].queryset = disciplina.equipos.all()
        self.fields['grupo_e2'].queryset = disciplina.equipos.all()
        self.fields['grupo_e3'].queryset = disciplina.equipos.all()
        self.fields['grupo_e4'].queryset = disciplina.equipos.all()
        self.fields['grupo_e5'].queryset = disciplina.equipos.all()


class ConfGruposPersonasForm(forms.ModelForm):
    class Meta:
        model = Grupos_mas_Playoff
        fields = ('__all__')
        exclude = ('disciplina','grupo_e1','grupo_e2','grupo_e3','grupo_e4','grupo_e5')

    def __init__(self, *args, **kwargs):
        id_disciplina = kwargs.pop('id_disciplina')
        super(ConfGruposPersonasForm, self).__init__(*args, **kwargs)

        disciplina = Disciplina.objects.get(pk=id_disciplina) 
        # self.fields["grupo_p1"].widget = forms.widgets.CheckboxSelectMultiple()
        # self.fields["grupo_p2"].widget = forms.widgets.CheckboxSelectMultiple()
        # self.fields["grupo_p3"].widget = forms.widgets.CheckboxSelectMultiple()
        # self.fields["grupo_p4"].widget = forms.widgets.CheckboxSelectMultiple()
        # self.fields["grupo_p5"].widget = forms.widgets.CheckboxSelectMultiple()

        self.fields['grupo_p1'].queryset = disciplina.participantes.all()
        self.fields['grupo_p2'].queryset = disciplina.participantes.all()
        self.fields['grupo_p3'].queryset = disciplina.participantes.all()
        self.fields['grupo_p4'].queryset = disciplina.participantes.all()
        self.fields['grupo_p5'].queryset = disciplina.participantes.all()

