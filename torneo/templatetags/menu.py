from torneo.models import Olimpiada
from django import template

from torneo.models import Persona, Equipo

register = template.Library()

@register.simple_tag
def get_olimpiada():
    #olimpiada = Olimpiada.objects.get(ano=now.year)
    olimpiada = Olimpiada.objects.all().order_by('-ano').first()
    return olimpiada


@register.filter
def in_group(objeto, grupo):
    return objeto.filter(grupo=grupo)


@register.filter
def in_zone(objeto, zona):
    return objeto.filter(zona=zona)


@register.filter
def get_dict_value(objeto, key):
    return objeto[key]


@register.filter(name='ifinlist')
def ifinlist(value, list):
	return True if value in list else False
	# {% value|ifinlist:"val1,val2,val3" %}


@register.filter
def get_participante_fullname_from_pk(objeto, pk):
	if pk is not None:
		participante = Persona.objects.get(pk=int(pk))		
		return participante.apellido + ', '+participante.nombre

@register.filter
def get_equipo_name_from_pk(objeto, pk):
	if pk is not None:
		equipo = Equipo.objects.get(pk=int(pk))
		return equipo.nombre




@register.filter
def get_object_name (objeto, field):
    return objeto.nombre