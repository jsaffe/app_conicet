
#Políticas para evaluar ganadores/perdedores en las competencias.
politica_grupos_segun_disciplina = {
         'Fútbol':[('goles_favor','mas'),],
         'Hockey':[('goles_favor','mas'),],
         'Handball':[('goles_favor','mas'),],
         
         'Voley': [('puntos_favor_1set','mas'),('puntos_favor_2set','mas'),('puntos_favor_3set','mas')],
         'Tenis de Mesa': [('puntos_favor_1set','mas'),('puntos_favor_2set','mas'),('puntos_favor_3set','mas')],
         
         'Basquet': [('puntos_favor','mas'),],
         'Bochas': [('puntos_favor','mas'),],
         
         'Tenis': [('games_favor_1set','mas'),('games_favor_2set','mas'),('games_favor_3set','mas'),],
         'Pádel': [('games_favor_1set','mas'),('games_favor_2set','mas'),('games_favor_3set','mas'),],

         'Pesca': [('peso','mas'),('cantidad','mas')],
         
         'Atletismo': [('tiempo','menos'),('distancia','mas')],
         
         'Natación': [('tiempo','menos'),],
         'Aguas Abiertas': [('tiempo','menos'),],
         'Ciclismo': [('tiempo','menos'),],
         'Vela': [('tiempo','menos'),],
         'Caminata/Carrera': [('tiempo','menos'),],
         'Trail': [('tiempo','menos'),],
         'Kayak': [('tiempo','menos'),],

         'Recreativos': [('puntos_favor','mas'),], 
        }


politica_playoff_segun_disciplina = {
         'Fútbol':[('goles_favor','mas'),('goles_contra','menos'),('penales','mas'),],
         'Hockey':[('goles_favor','mas'),('goles_contra','menos'),('penales','mas'),],
         'Handball':[('goles_favor','mas'),('goles_contra','menos'),('penales','mas'),],
         
         'Voley': [('puntos_favor_1set','mas'),('puntos_favor_2set','mas'),('puntos_favor_3set','mas'),],
         'Tenis de Mesa': [('puntos_favor_1set','mas'),('puntos_favor_2set','mas'),('puntos_favor_3set','mas'),],
         
         'Basquet': [('puntos_favor','mas'),],
         'Bochas': [('puntos_favor','mas'),],
         
         'Tenis': [('games_favor_1set','mas'),('games_favor_2set','mas'),('games_favor_3set','mas'),],
         'Pádel': [('games_favor_1set','mas'),('games_favor_2set','mas'),('games_favor_3set','mas'),],

         'Pesca': [('peso','mas'),('cantidad','mas')],
         
         'Atletismo': [('tiempo','menos'),('distancia','mas')],
         
         'Natación': [('tiempo','menos'),],
         'Aguas Abiertas': [('tiempo','menos'),],
         'Ciclismo': [('tiempo','menos'),],
         'Vela': [('tiempo','menos'),],
         'Caminata/Carrera': [('tiempo','menos'),],
         'Trail': [('tiempo','menos'),],
         'Kayak': [('tiempo','menos'),],

         'Recreativos': [('puntos_favor','mas'),],
        }



#Políticas para evaluar las posiciones en la tabla 
#de las disciplinas, ante igualdad de puntos.
politica_segun_deporte_tabla = {
        'Fútbol':[('goles_dif','-goles_dif'),('goles_favor','-goles_favor'),('rojas','rojas'),('amarillas','amarillas')],
        'Hockey':[('goles_dif','-goles_dif'),('goles_favor','-goles_favor'),('rojas','rojas'),('amarillas','amarillas'),('verdes','verdes')],
        'Handball':[('goles_dif','-goles_dif'),('goles_favor','-goles_favor'),('rojas','rojas'),('amarillas','amarillas'),('azules','azules')],

        'Voley':[('sets_dif','-sets_dif'),('sets_favor','-sets_favor'),('puntos_dif','-puntos_dif'),('puntos_favor','-puntos_favor'),('rojas','rojas'),('amarillas','amarillas')],
        'Tenis de Mesa':[('sets_dif','-sets_dif'),('sets_favor','-sets_favor'),('puntos_dif','-puntos_dif'),('puntos_favor','-puntos_favor'),('rojas','rojas'),('amarillas','amarillas')],

        'Basquet':[('puntos_dif','-puntos_dif'),('puntos_favor','-puntos_favor')],
        'Bochas':[('puntos_dif','-puntos_dif'),('puntos_favor','-puntos_favor')],

        'Tenis':[('games_dif','-games_dif'),('games_favor','-games_favor')],
        'Pádel':[('games_dif','-games_dif'),('games_favor','-games_favor')],

        'Pesca':[('peso','-peso'),('cantidad','-cantidad')],                                    

        'Atletismo':[('tiempo','tiempo'),('distancia','-distancia')],                 

        'Natación':[('tiempo','tiempo'),],
        'Aguas Abiertas':[('tiempo','tiempo'),],
        'Ciclismo':[('tiempo','tiempo'),],                                    
        'Vela':[('tiempo','tiempo'),],                                    
        'Caminata/Carrera':[('tiempo','tiempo'),],
        'Trail':[('tiempo','tiempo'),],
        'Kayak':[('tiempo','tiempo'),],

        'Recreativos':[('puntos_dif','-puntos_dif'),('puntos_favor','-puntos_favor')],                                 
        }



#Cantidad de puntos asignados por deporte para 
#la tabla del deporte y para la copa conjunto
#Se asignan puntos al 1,2,3 y 4 puesto únicamente.
puntos_por_deporte = {  'Atletismo':[10,6,3,1], 
                        'Basquet':[10,6,3,1],
                        'Bochas':[10,6,3,1],
                        'Fútbol':[10,6,3,1],
                        'Handball':[10,6,3,1],
                        'Hockey':[10,6,3,1],
                        'Natación':[10,6,3,1],
                        'Pádel':[10,6,3,1],
                        'Tenis':[10,6,3,1],
                        'Tenis de Mesa':[10,6,3,1],
                        'Vela':[10,6,3,1],
                        'Voley':[10,6,3,1],
                        'Caminata/Carrera':[10,6,3,1],
                        'Trail':[10,6,3,1],
                        'Kayak':[10,6,3,1],

                        'Ciclismo':[6,3,2,1],
                        'Ajedrez':[6,3,2,1],
                        'Aguas Abiertas':[6,3,2,1],
                        'Pesca':[6,3,2,1],

                        'Recreativos':[0,0,0,0],
                         }


#Puntos asignados por competencia según deporte
puntos_por_competencia_segun_deporte = {  
        'Atletismo':{'ganado':3,'perdido':0,'empatado':1,'nopresentado':0}, 
        'Basquet':  {'ganado':3,'perdido':0,'empatado':1,'nopresentado':0}, 
        'Bochas':   {'ganado':3,'perdido':0,'empatado':1,'nopresentado':0},
        'Fútbol':   {'ganado':3,'perdido':0,'empatado':1,'nopresentado':0},
        'Handball': {'ganado':3,'perdido':0,'empatado':1,'nopresentado':0},
        'Hockey':   {'ganado':3,'perdido':0,'empatado':1,'nopresentado':0},
        'Natación': {'ganado':3,'perdido':0,'empatado':1,'nopresentado':0},
        'Pádel':    {'ganado':3,'perdido':0,'empatado':1,'nopresentado':0},
        'Tenis':    {'ganado':3,'perdido':0,'empatado':1,'nopresentado':0},
        'Vela':     {'ganado':3,'perdido':0,'empatado':1,'nopresentado':0},
        'Voley':    {'ganado':3,'perdido':0,'empatado':1,'nopresentado':0},
        'Ciclismo': {'ganado':3,'perdido':0,'empatado':1,'nopresentado':0},
        'Ajedrez':  {'ganado':3,'perdido':0,'empatado':1,'nopresentado':0},
        'Pesca':    {'ganado':3,'perdido':0,'empatado':1,'nopresentado':0},
        'Tenis de Mesa':   {'ganado':3,'perdido':0,'empatado':1,'nopresentado':0},
        'Caminata/Carrera':{'ganado':3,'perdido':0,'empatado':1,'nopresentado':0},
        'Trail':{'ganado':3,'perdido':0,'empatado':1,'nopresentado':0},
        'Kayak':{'ganado':3,'perdido':0,'empatado':1,'nopresentado':0},
        'Recreativos':  {'ganado':3,'perdido':0,'empatado':1,'nopresentado':0}, 
                 }

#Puntos asignados por competencia según disciplina
#Si no está registrada la disciplina se toman por defecto
#los puntos_por_competencia_segun_deporte
puntos_por_competencia_segun_disciplina = {  
        'Basquet':  {'3':{'ganado':2,'perdido':1,'empatado':1,'nopresentado':0},
                        '':{'ganado':2,'perdido':1,'empatado':1,'nopresentado':0} },
                }


#Categorización de deportes según tipo de resultados
#Este campo se utiliza para la evaluación de las competencias
deportes_grupales_resultados_finales = [
    'Fútbol','Handball','Hockey', 'Bochas','Basquet', 'Atletismo','Natación','Vela', 'Recreativos']

deportes_grupales_resultados_parciales = [
        'Voley','Tenis de Mesa','Tenis','Pádel']

deportes_individuales_resultados_finales = [
        'Atletismo', 'Natación', 'Ciclismo','Pesca', 'Caminata/Carrera', 'Trail', 'Kayak', 'Recreativos']

deportes_individuales_resultados_parciales = ['Tenis de Mesa','Pádel','Tenis']


# Puntos Asignados a la Delegación por presentarse a algún espacio de arte.
puntos_arte = 3
