from django.contrib import admin
from .models import Olimpiada, Deporte, Disciplina,  Competencia, Equipo, Persona, Delegacion, Post, Cancha, Tag
from .models import Resultado, Posicion_disciplina, Posicion_deporte, Posicion_olimpiada
from .models import Grupos_mas_Playoff, CompetenciaUnica, Playoff, PosicionManual, RondasClasificacion
from .models import Arte, Arte_puntuacion, Foto, Galeria, Inscripcion

from django import forms
from django.core.exceptions import ValidationError    

from import_export.admin import ExportMixin
from import_export import resources, fields
from import_export.widgets import ManyToManyWidget
 
from django.utils import timezone

class PersonaResource(resources.ModelResource):   
    class Meta:
        model = Persona
        fields = ('id','nombre', 'apellido',
                  'genero','fecha_nacimiento',
                  'tipo_documento','numero_documento',
                  'grupo_sanguineo',
                  'talle_remera',
                  'mail', 'facebook',
                  'delegacion__nombre','localidad',
                  'institucion', 'tipo_personal',
                  'actividad_laboral','telefono_laboral',
                  'telefono_personal','link_web_laboral',
                  'descripcion_personal')

        export_order = ('id','nombre','apellido', 
                        'genero',
                        'tipo_documento','numero_documento',
                        'fecha_nacimiento',
                        'delegacion__nombre',
                        'mail', 'facebook',
                        'telefono_personal',
                        'grupo_sanguineo',
                        'talle_remera',
                        'localidad',
                        'institucion',
                        'tipo_personal',
                        'actividad_laboral',
                        'telefono_laboral',
                        'link_web_laboral',
                        'descripcion_personal')


class PersonaForm(forms.ModelForm):
  class Meta:
    model = Persona
    fields = '__all__'
    exlude = ('certificado_medico',
              'comprobante_pago',
              'vinculacion_conicet',
              'alojamiento',
              'acompanantes',
              'deportes',
              'deporte1',
              'deporte2',
              'deporte3',
              'deporte_maraton',
              'disciplinas_deporte1',
              'disciplinas_deporte2',
              'disciplinas_deporte3',
              'artes',
              'divulgacion',
              'colaborador',
              'inscripcion_completa',
              'inscripcion_fecha',
              'ultima_modificacion')


  def __init__(self, *args, **kwargs):
    super(PersonaForm, self).__init__(*args, **kwargs)


class PersonaAdmin(ExportMixin):
    form = PersonaForm
    resource_class = PersonaResource

    list_display = ('id','apellido', 'nombre', 
                    'numero_documento','delegacion',
                    'mail', 'telefono_personal')
    
    list_filter = ('olimpiadas','delegacion__nombre','genero')

    fieldsets = (
        ('Datos Personales', {'fields': ('nombre', 'apellido', 'genero',
                                         'fecha_nacimiento','mostrar_edad',
                                         'tipo_documento','numero_documento',
                                         'grupo_sanguineo',
                                         'talle_remera','mail','facebook',
                                         'telefono_personal','foto')}),

        ('Datos Laborales y Delegación', {'fields': ('delegacion','localidad',
                                                     'institucion','tipo_personal',
                                                     'actividad_laboral','telefono_laboral',
                                                     'link_web_laboral',
                                                     'descripcion_personal')}),

        ('Documentos Adjuntos', {'fields': ('documento_digitalizado',)}),

        ('Logs y Flags', {'fields': ('mail_reset_password_enviado',
                                       'mail_inscripcion_enviado')})
        )

    search_fields = ('delegacion__nombre',
                     'apellido', 'nombre',
                     'mail')

    ordering = (['apellido','nombre','delegacion','id'])
    filter_horizontal = ()

    readonly_fields = ('mail_reset_password_enviado',
                       'mail_inscripcion_enviado')



class InscripcionResource(resources.ModelResource):   
    disciplinas1 = fields.Field(widget=ManyToManyWidget(Disciplina))
    disciplinas2 = fields.Field(widget=ManyToManyWidget(Disciplina))
    disciplinas3 = fields.Field(widget=ManyToManyWidget(Disciplina))
    recreativas = fields.Field()
    olimpiada = fields.Field()
    delegacion = fields.Field()
    apellido = fields.Field()
    nombre = fields.Field()
    genero = fields.Field()
    edadenlosjuegos = fields.Field()
    numerodocumento = fields.Field()
    telefonopersonal = fields.Field()
    telefonolaboral =fields.Field()
    mail = fields.Field()
    talleremera = fields.Field()
    artes = fields.Field()
    tipo_personal = fields.Field()
    inscripcioncompleta = fields.Field()
    fecha_nacimiento = fields.Field()


    class Meta:
        model = Inscripcion
        fields = ('deporte1__nombre',
                  'deporte2__nombre',
                  'deporte3__nombre',
                  'deporte_maraton',
                  'divulgacion',
                  'colaborador','alimentacion',
                  'alojamiento','acompanantes',
                  'tipo_personal',
                  'fecha_nacimiento',
                  'inscripcion_fecha')

        export_order = ('olimpiada', 'delegacion',
                        'apellido','nombre', 
                        'genero','edadenlosjuegos',
                        'deporte1__nombre', 'disciplinas1',
                        'deporte2__nombre', 'disciplinas2',
                        'deporte3__nombre', 'disciplinas3',
                        'deporte_maraton',
                        'recreativas',
                        'artes', 'divulgacion',
                        'colaborador','alimentacion','talleremera',
                        'alojamiento','acompanantes',
                        'numerodocumento', 'telefonopersonal',
                        'telefonolaboral','mail',
                        'tipo_personal',
                        'fecha_nacimiento',
                        'inscripcion_fecha',
                        'inscripcioncompleta')
   
    def dehydrate_olimpiada(self, inscripcion):
      return str(inscripcion.olimpiada)
      
    def dehydrate_delegacion(self, inscripcion):
      return str(inscripcion.jugador.delegacion)

    def dehydrate_apellido(self, inscripcion):
      return str(inscripcion.jugador.apellido).capitalize()

    def dehydrate_nombre(self, inscripcion):
      return str(inscripcion.jugador.nombre).capitalize()      

    def dehydrate_genero(self, inscripcion):
      return str(inscripcion.jugador.genero)

    def dehydrate_edadenlosjuegos(self, inscripcion):
      return str(inscripcion.jugador.calcular_edad_en_olimpiada(olimpiada=inscripcion.olimpiada))

    def dehydrate_numerodocumento(self, inscripcion):
      return str(inscripcion.jugador.numero_documento)

    def dehydrate_telefonopersonal(self, inscripcion):
      return str(inscripcion.jugador.telefono_personal)

    def dehydrate_telefonolaboral(self, inscripcion):
      return str(inscripcion.jugador.telefono_laboral)

    def dehydrate_mail(self, inscripcion):
      return str(inscripcion.jugador.mail)
    
    def dehydrate_talleremera(self, inscripcion):
      return str(inscripcion.jugador.talle_remera)

    def dehydrate_artes(self, inscripcion):
      artes=[]
      for arte in inscripcion.artes.all():
        artes.append(str(arte.nombre)+', ')
      return artes

    def dehydrate_tipo_personal(self, inscripcion):
      return str(inscripcion.jugador.tipo_personal)

    def dehydrate_fecha_nacimiento(self, inscripcion):
      return str(inscripcion.jugador.fecha_nacimiento)

    def dehydrate_inscripcioncompleta(self, inscripcion):
        if inscripcion.inscripcion_completa:
          return 'Sí'
        else:
          return 'INCOMPLETA - REVISAR'

    def dehydrate_disciplinas1(self, inscripcion):
        disciplinas=[]
        for disciplina in inscripcion.disciplinas_deporte1.all():
        
          if disciplina.limite_edad is 0:
              limite = 'Libre'
          else:
              limite = '+'+str(disciplina.limite_edad)
          
          if disciplina.nombre not in '': 
            nombre_disciplina = disciplina.nombre + ' ' + disciplina.genero + ' (' + limite + ')'
          else: 
            nombre_disciplina = disciplina.genero + ' (' + limite + ')'

          disciplinas.append(str(nombre_disciplina)+', ')
        return disciplinas

    def dehydrate_disciplinas2(self, inscripcion):
        disciplinas=[]
        for disciplina in inscripcion.disciplinas_deporte2.all():
        
          if disciplina.limite_edad is 0:
              limite = 'Libre'
          else:
              limite = '+'+str(disciplina.limite_edad)
          
          if disciplina.nombre not in '': 
            nombre_disciplina = disciplina.nombre + ' ' + disciplina.genero + ' (' + limite + ')'
          else: 
            nombre_disciplina = disciplina.genero + ' (' + limite + ')'

          disciplinas.append(str(nombre_disciplina)+', ')
        return disciplinas

    def dehydrate_disciplinas3(self, inscripcion):
        disciplinas=[]
        for disciplina in inscripcion.disciplinas_deporte3.all():
        
          if disciplina.limite_edad is 0:
              limite = 'Libre'
          else:
              limite = '+'+str(disciplina.limite_edad)
          
          if disciplina.nombre not in '': 
            nombre_disciplina = disciplina.nombre + ' ' + disciplina.genero + ' (' + limite + ')'
          else: 
            nombre_disciplina = disciplina.genero + ' (' + limite + ')'

          disciplinas.append(str(nombre_disciplina)+', ')
        return disciplinas


    def dehydrate_recreativas(self, inscripcion):
      recreativas=[]
      for actividad_recreativa in inscripcion.actividades_recreativas.all():
        recreativas.append(str(actividad_recreativa.nombre)+', ')
      return recreativas


class InscripcionForm(forms.ModelForm):
  class Meta:
    model = Inscripcion
    fields = ( 'jugador',
               'olimpiada',
               'certificado_medico',
               'comprobante_pago',
               'vinculacion_conicet',
               'alojamiento',
               'acompanantes',
               'deportes',
               'deporte1',
               'deporte2',
               'deporte3',
               'deporte_maraton',
               'disciplinas_deporte1',
               'disciplinas_deporte2',
               'disciplinas_deporte3',
               'artes',
               'divulgacion',
               'colaborador',
               'alimentacion',
               'inscripcion_completa',
               'inscripcion_fecha',
               'mail_inscripcion_enviado',
               )


  def __init__(self, *args, **kwargs):
    super(InscripcionForm, self).__init__(*args, **kwargs)

  
  def clean(self):      
    #Si se modifican los deportes/disciplinas para la presente olimpiada, 
    #hay que eliminar el jugador de las disciplinas seleccionadas previamente
    #y equipos.
    olimpiada = self.cleaned_data['olimpiada']
    jugador = self.cleaned_data['jugador']

    print ('Modificando Ficha Inscripcion de '+str(jugador))
    
    #eliminamos el jugador de las disciplinas inscriptas
    disciplinas = Disciplina.objects.filter(olimpiada=olimpiada, 
                                            participantes=jugador)
    for disciplina in disciplinas:
        print ('  Eliminando de Disciplina:'+str(disciplina))
        disciplina.participantes.remove(jugador)

        #si el jugador está inscripto en algun equipo 
        #de la disciplinalo eliminamos
        equipos = Equipo.objects.filter(disciplina=disciplina, 
                                        jugadores=jugador)
        for equipo in equipos:
            print ('    Eliminando de Equipo:'+str(equipo))
            equipo.jugadores.remove(jugador)

    #==============================================================================
    # Caso Carrera/Caminata: se registra el participante en la disciplina correspondiente
    # en funcion de la edad y género
    #==============================================================================
    edad_jugador = jugador.calcular_edad_en_olimpiada(olimpiada)
    if edad_jugador < 35:
        edad=0
    elif edad_jugador >= 35 and edad_jugador < 45:
        edad=35
    elif edad_jugador >= 45 and edad_jugador < 55:
        edad=45
    elif edad_jugador >= 55 and edad_jugador < 65:
        edad=55
    else:
        edad=65 
    
    maraton = Deporte.objects.get(nombre='Caminata/Carrera')    
    if self.cleaned_data['deporte_maraton'] not in 'No':
        #con este filtro hay una sola opcion posible pero 
        #el queryset es para poder registrarlo en el for de abajo
        disciplina_maraton = Disciplina.objects.filter(olimpiada=olimpiada, 
                                                       deporte=maraton, 
                                                       genero=jugador.genero, 
                                                       limite_edad=edad, 
                                                       nombre=self.cleaned_data['deporte_maraton'])  
        print (disciplina_maraton)
    else:
        disciplina_maraton = Disciplina.objects.filter(nombre='xxx') #queryset vacio
        maraton = None   

    # =============================================================================
    # Registramos Deportes y Disciplinas Seleccionadas
    # =============================================================================
    self.cleaned_data['deportes'] = Disciplina.objects.filter(nombre='xxx') #queryset vacio, reinicio del campo deportes

    deporte1 = self.cleaned_data['deporte1']
    disciplinas_deporte1 = self.cleaned_data['disciplinas_deporte1']
    deporte2 = self.cleaned_data['deporte2']
    disciplinas_deporte2 = self.cleaned_data['disciplinas_deporte2']
    deporte3 = self.cleaned_data['deporte3']
    disciplinas_deporte3 = self.cleaned_data['disciplinas_deporte3']

    lista_deportes=[]
    for deporte, disciplinas in [(deporte1,disciplinas_deporte1),
                                 (deporte2,disciplinas_deporte2),
                                 (deporte3,disciplinas_deporte3),
                                 (maraton,disciplina_maraton)]:
        if deporte:
            lista_deportes.append(deporte.pk)
            for disciplina in disciplinas.all():
                disciplina.participantes.add(jugador)   
                disciplina.save()   
                print ('  Incorporado a '+str(disciplina)) 
    self.cleaned_data['deportes'] = Deporte.objects.filter(id__in=lista_deportes)
        
    # =============================================================================
    # Seteamos la finalización completa de la inscripción y marca de tiempo
    # =============================================================================
    self.cleaned_data['ultima_modificacion'] = timezone.now() 


class InscripcionAdmin(ExportMixin):
    form = InscripcionForm
    resource_class = InscripcionResource
    actions=['delete_selected']

    list_display = ('jugador',
                    # 'jugador__delegacion__nombre',
                    'olimpiada',
                    'deporte1',#'disciplinas_deporte1__nombre',
                    'deporte2',#'disciplinas_deporte2__nombre',
                    'deporte3',#'disciplinas_deporte3__nombre',
                    'deporte_maraton',
                    'inscripcion_completa',
                    'mail_inscripcion_enviado')
    
    list_filter = ('olimpiada',
                   'deportes', 
                   'deporte1','deporte2','deporte3',
                   'deporte_maraton')

    search_fields = ('artes__nombre','alojamiento',
                     'disciplinas_deporte1__nombre',
                     'disciplinas_deporte2__nombre',
                     'disciplinas_deporte3__nombre',
                     'deporte_maraton',
                     'jugador__nombre',
                     'jugador__apellido',
                     'jugador__numero_documento',
                     'jugador__delegacion__nombre')

    # ordering = (['jugador__apellido','jugador__nombre','jugador__delegacion','jugador__id'])
    filter_horizontal = ()

    readonly_fields = ('ultima_modificacion',
                       'inscripcion_fecha',
                       'inscripcion_completa')

    filter_vertical = ('disciplinas_deporte1',
                       'disciplinas_deporte2',
                       'disciplinas_deporte3') 


    def delete_selected(self, request, queryset):
      for obj in queryset:
          obj.delete()


class DelegacionAdmin(admin.ModelAdmin):
  filter_vertical = ('responsables',) 


class DisciplinaAdminForm(forms.ModelForm):
  class Meta:
    model = Disciplina
    fields = '__all__'

  def __init__(self, *args, **kwargs):
    super(DisciplinaAdminForm, self).__init__(*args, **kwargs)

    #Valores Iniciales (solo funciona cuando se edita, no al crear la primera vez)
    # if hasattr(self.instance, 'deporte'):
    #   if self.instance.genero not in 'Mixto':
    #     self.fields['participantes'].queryset = Persona.objects.filter(
    #         deportes=self.instance.deporte, genero=self.instance.genero, 
    #         olimpiadas=self.instance.olimpiada)
    #   else:
    #     self.fields['participantes'].queryset = Persona.objects.filter(
    #         deportes=self.instance.deporte,olimpiadas=self.instance.olimpiada)

    if hasattr(self.instance, 'deporte'):
      if self.instance.genero not in 'Mixto':
        inscripciones = Inscripcion.objects.filter(deportes=self.instance.deporte, 
                                                   jugador__genero=self.instance.genero, 
                                                   olimpiada=self.instance.olimpiada)
        jugadores = set()
        for inscripcion in inscripciones.all():
          jugadores.add(inscripcion.jugador.pk)

        self.fields['participantes'].queryset = Persona.objects.filter(pk__in = jugadores)

      else:
        inscripciones = Inscripcion.objects.filter(deportes=self.instance.deporte,
                                                   olimpiada=self.instance.olimpiada)

        jugadores = set()
        for inscripcion in inscripciones.all():
          jugadores.add(inscripcion.jugador.pk)

        self.fields['participantes'].queryset = Persona.objects.filter(pk__in = jugadores)


class DisciplinaAdmin(admin.ModelAdmin):
    form = DisciplinaAdminForm

    list_display = ('deporte','nombre','genero',
                    'limite_edad','finalizada')
    
    list_filter = ('olimpiada', 'deporte', 'genero',
                    'limite_edad', 'finalizada')

    search_fields = ('deporte__nombre','genero',
                     'limite_edad','nombre')

    ordering = (['deporte__nombre'])

    filter_horizontal = ()

    filter_vertical = ('participantes',) 

    fieldsets = (
      ('Disciplina', {'fields': (
          'deporte','nombre',
          'genero','limite_edad',
          'tipo','olimpiada',
          'max_jugadores_lbf',
          'max_jugadores_extras',)}),

      ('Modalidad y Responsable', {'fields': (
          'modalidad','responsables')}),

      ('Inscriptos en la Disciplina', {'fields': (
          'participantes',)}),

      ('Descripción y Reglamento', {'fields': (
          'descripcion',
          'reglamento_pdf',)}),

      ('Flags / Logs', {'fields': (
          'fixture_creado','finalizada')}),
    )

    readonly_fields = (
          'deporte','nombre',
          'genero','limite_edad',
          'tipo',
          'olimpiada',
          # 'max_jugadores_lbf',
          # 'max_jugadores_extras',
          'finalizada',
          'fixture_creado'
          )


class EquipoAdminForm(forms.ModelForm):
  class Meta:
    model = Equipo
    fields = '__all__'

  def __init__(self, *args, **kwargs):
    super(EquipoAdminForm, self).__init__(*args, **kwargs)
    
    if self.instance.pk:
      # Solo muestra los jugadores que no están incluidos en ningún equipo.
      # self.fields['jugadores'].queryset = self.instance.disciplina.participantes
      # self.fields['capitanes'].queryset = self.instance.disciplina.participantes
      disciplina = Disciplina.objects.get(pk=self.instance.disciplina.pk)
      
      queryset_jugadores_disponibles = self.instance.disciplina.participantes.all()
      #print ('Tipo: ' + str(type(queryset_jugadores_disponibles)))

      for equipo in disciplina.equipos.all():
        if self.instance.pk != equipo.pk:
          queryset_jugadores_disponibles = queryset_jugadores_disponibles.exclude(pk__in=equipo.jugadores.all())

      self.fields['jugadores'].queryset = queryset_jugadores_disponibles
      self.fields['capitanes'].queryset = queryset_jugadores_disponibles

  
  def clean(self): 
    # =============================================================================
    # Comprobamos cantidad de jugadores por equipo
    # =============================================================================
    jugadores_extras_a_lbf_por_deporte = 3

    njugadores_equipo = len(self.cleaned_data['jugadores'].all())
    maxjugadores_equipo = self.cleaned_data['disciplina'].max_jugadores_lbf+jugadores_extras_a_lbf_por_deporte

    if njugadores_equipo > maxjugadores_equipo:
      raise forms.ValidationError('El número de participantes supera el máximo permitido para el deporte.'+
                                  ' Seleccionados: '+ str(njugadores_equipo) +
                                  ' / MaxPermitido: '+str(maxjugadores_equipo))



class EquipoAdmin(admin.ModelAdmin):
  form = EquipoAdminForm
  
  list_display = ('nombre','disciplina',)
  list_filter = ('disciplina__deporte','disciplina__olimpiada__nombre',)
  # search_fields = ('nombre',)
  ordering = (['disciplina'])
  filter_horizontal = ()  
  filter_vertical = ('jugadores','capitanes')


  fieldsets = (
    ('Equipo', {'fields': (
        'nombre','disciplina',)}),

    ('Jugadores y Capitán', {'fields': (
        'jugadores','capitanes')}),

    ('Descripción y Foto', {'fields': (
        'descripcion','foto_equipo')}),
  )


class CompetenciaAdminForm(forms.ModelForm):
  class Meta:
    model = Competencia
    fields = '__all__'
    #exclude = ['participantes']

  def __init__(self, *args, **kwargs):
    super(CompetenciaAdminForm, self).__init__(*args, **kwargs)

    #Valores Iniciales (solo funciona cuando se edita, no al crear la primera vez)
    if self.instance.pk:
      self.fields['equipos'].queryset = self.instance.disciplina.equipos
      self.fields['participantes'].queryset = self.instance.disciplina.participantes
    
    # #Ocultar Campos (en funcion del tipo de deporte)
    # if self.instance.pk:
    #    if self.instance.disciplina.tipo in 'Grupal':
    #        self.fields['participantes'].widget = forms.HiddenInput()
    #    else: 
    #        self.fields['equipos'].widget = forms.HiddenInput()

class CompetenciaAdmin(admin.ModelAdmin):
    form = CompetenciaAdminForm

    readonly_fields = (
          'nombre','zona',
          'proxima_competencia',
          'disciplina')



from tinymce.widgets import TinyMCE
class PostAdminForm(forms.ModelForm):
  class Meta:
    model = Post
    fields = '__all__'
    widgets = {'text': TinyMCE( attrs={'cols': 80, 'rows': 30}, 
                                #mce_attrs={'theme': 'advanced','plugins':'template,lists,searchreplace,autolink',}
                              )}

class PostAdmin(admin.ModelAdmin):
  form = PostAdminForm

  # list_display = ('titulo','orden')
  
  list_filter = ('etiqueta__tag','olimpiadas')

  # search_fields = ('etiqueta__tag','titulo','orden')

  # filter_vertical = ('etiqueta',) 

  # fieldsets = (

  #   ('Info Post', {'fields': (
  #       'titulo','titulo_slider','subtitulo',
  #       'resumen','cuerpo',)}),

  #   ('Imagenes / Videos / Animaciones', {'fields': (
  #       'imagen','epigrafe','video_link',
  #       'gif')}),

  #   ('Etiquetas y Flags', {'fields': (
  #       'etiqueta','principal_seccion','orden')}),

  #   ('Autor y Fecha', {'fields': (
  #       'autor','fecha_creacion',
  #       'fecha_publicacion')}),

    
  # )


class GruposMasPlayoffForm(forms.ModelForm):
  class Meta:
    model = Grupos_mas_Playoff
    fields = '__all__'
    #exclude = ['participantes']

  def __init__(self, *args, **kwargs):
    super(GruposMasPlayoffForm, self).__init__(*args, **kwargs)

    #Valores Iniciales (solo funciona cuando se edita, no al crear la primera vez)
    if self.instance.pk:
      if self.instance.disciplina.tipo in ['Grupal']:
        self.fields['grupo_e1'].queryset = self.instance.disciplina.equipos
        self.fields['grupo_e2'].queryset = self.instance.disciplina.equipos
        self.fields['grupo_e3'].queryset = self.instance.disciplina.equipos
        self.fields['grupo_e4'].queryset = self.instance.disciplina.equipos
        self.fields['grupo_e5'].queryset = self.instance.disciplina.equipos
        self.fields['grupo_e6'].queryset = self.instance.disciplina.equipos
        self.fields['grupo_e7'].queryset = self.instance.disciplina.equipos
        self.fields['grupo_e8'].queryset = self.instance.disciplina.equipos
        self.fields['grupo_e9'].queryset = self.instance.disciplina.equipos
        self.fields['grupo_e10'].queryset = self.instance.disciplina.equipos

        empty_queryset = Persona.objects.filter(numero_documento='')
        self.fields['grupo_p1'].queryset = empty_queryset
        self.fields['grupo_p2'].queryset = empty_queryset
        self.fields['grupo_p3'].queryset = empty_queryset
        self.fields['grupo_p4'].queryset = empty_queryset
        self.fields['grupo_p5'].queryset = empty_queryset
        self.fields['grupo_p6'].queryset = empty_queryset
        self.fields['grupo_p7'].queryset = empty_queryset
        self.fields['grupo_p8'].queryset = empty_queryset
        self.fields['grupo_p9'].queryset = empty_queryset
        self.fields['grupo_p10'].queryset = empty_queryset

      if self.instance.disciplina.tipo in ['Individual']:
        self.fields['grupo_p1'].queryset = self.instance.disciplina.participantes
        self.fields['grupo_p2'].queryset = self.instance.disciplina.participantes
        self.fields['grupo_p3'].queryset = self.instance.disciplina.participantes
        self.fields['grupo_p4'].queryset = self.instance.disciplina.participantes
        self.fields['grupo_p5'].queryset = self.instance.disciplina.participantes
        self.fields['grupo_p6'].queryset = self.instance.disciplina.participantes
        self.fields['grupo_p7'].queryset = self.instance.disciplina.participantes
        self.fields['grupo_p8'].queryset = self.instance.disciplina.participantes
        self.fields['grupo_p9'].queryset = self.instance.disciplina.participantes
        self.fields['grupo_p10'].queryset = self.instance.disciplina.participantes

        empty_queryset = Equipo.objects.filter(nombre='*******')
        self.fields['grupo_e1'].queryset = empty_queryset
        self.fields['grupo_e2'].queryset = empty_queryset
        self.fields['grupo_e3'].queryset = empty_queryset
        self.fields['grupo_e4'].queryset = empty_queryset
        self.fields['grupo_e5'].queryset = empty_queryset
        self.fields['grupo_e6'].queryset = empty_queryset
        self.fields['grupo_e7'].queryset = empty_queryset
        self.fields['grupo_e8'].queryset = empty_queryset
        self.fields['grupo_e9'].queryset = empty_queryset
        self.fields['grupo_e10'].queryset = empty_queryset
    
    #Ocultar Campos (en funcion del tipo de deporte)
    # if self.instance.pk:
    #    if self.instance.disciplina.tipo in 'Grupal':
    #        self.fields['grupo_p1'].widget = forms.HiddenInput()
    #        self.fields['grupo_p2'].widget = forms.HiddenInput()
    #        self.fields['grupo_p3'].widget = forms.HiddenInput()
    #        self.fields['grupo_p4'].widget = forms.HiddenInput()
    #        self.fields['grupo_p5'].widget = forms.HiddenInput()
    #    else: 
    #        self.fields['grupo_e1'].widget = forms.HiddenInput()
    #        self.fields['grupo_e2'].widget = forms.HiddenInput()
    #        self.fields['grupo_e3'].widget = forms.HiddenInput()
    #        self.fields['grupo_e4'].widget = forms.HiddenInput()
    #        self.fields['grupo_e5'].widget = forms.HiddenInput()


class GruposMasPlayoffAdmin(admin.ModelAdmin):
    form = GruposMasPlayoffForm

    filter_vertical = ('grupo_e1','grupo_e2','grupo_e3','grupo_e4','grupo_e5',
                       'grupo_e6','grupo_e7','grupo_e8','grupo_e9','grupo_e10',
                       'grupo_p1','grupo_p2','grupo_p3','grupo_p4','grupo_p5',
                       'grupo_p6','grupo_p7','grupo_p8','grupo_p9','grupo_p10')

    readonly_fields = (
          'disciplina',
          'participantes_finalistas',
          'equipos_finalistas',
          'playoff_iniciado',
          'fase_grupos_finalizada',
          'fase_grupos_iniciada',)

    fieldsets = (
    ('General', {'fields': (
        'disciplina','min_participantes_por_grupo')}),

    ('Sección para Disciplinas GRUPALES', {'fields': (
      'grupo_e1','grupo_e2','grupo_e3','grupo_e4','grupo_e5',
      'grupo_e6','grupo_e7','grupo_e8','grupo_e9','grupo_e10',)}),

    ('Sección para Disciplinas INDIVIDUALES', {'fields': (
      'grupo_p1','grupo_p2','grupo_p3','grupo_p4','grupo_p5',
      'grupo_p6','grupo_p7','grupo_p8','grupo_p9','grupo_p10')}),

    ('Configuración Finalistas por Grupo', {'fields': (
      'nclasificados_grupo1','nclasificados_grupo2',
      'nclasificados_grupo3','nclasificados_grupo4',
      'nclasificados_grupo5','nclasificados_grupo6',
      'nclasificados_grupo7','nclasificados_grupo8',
      'nclasificados_grupo9','nclasificados_grupo10',
      'nclasificados_mejores_segundos',
      'nclasificados_mejores_terceros',
      )}),

    ('Flags', {'fields': (
        'fase_grupos_iniciada',
        'fase_grupos_finalizada',
        'playoff_iniciado')}),
  )



class RondasClasificacionForm(forms.ModelForm):
  class Meta:
    model = RondasClasificacion
    fields = '__all__'

  def __init__(self, *args, **kwargs):
    super(RondasClasificacionForm, self).__init__(*args, **kwargs)

    #Valores Iniciales (solo funciona cuando se edita, no al crear la primera vez)
    if self.instance.pk:
      if self.instance.disciplina.tipo in ['Grupal']:
        self.fields['grupo_e1'].queryset = self.instance.disciplina.equipos
        self.fields['grupo_e2'].queryset = self.instance.disciplina.equipos
        self.fields['grupo_e3'].queryset = self.instance.disciplina.equipos
        self.fields['grupo_e4'].queryset = self.instance.disciplina.equipos
        self.fields['grupo_e5'].queryset = self.instance.disciplina.equipos
        self.fields['grupo_e6'].queryset = self.instance.disciplina.equipos
        self.fields['grupo_e7'].queryset = self.instance.disciplina.equipos
        self.fields['grupo_e8'].queryset = self.instance.disciplina.equipos

        empty_queryset = Persona.objects.filter(numero_documento='')
        self.fields['grupo_p1'].queryset = empty_queryset
        self.fields['grupo_p2'].queryset = empty_queryset
        self.fields['grupo_p3'].queryset = empty_queryset
        self.fields['grupo_p4'].queryset = empty_queryset
        self.fields['grupo_p5'].queryset = empty_queryset
        self.fields['grupo_p6'].queryset = empty_queryset
        self.fields['grupo_p7'].queryset = empty_queryset
        self.fields['grupo_p8'].queryset = empty_queryset

      if self.instance.disciplina.tipo in ['Individual']:
        self.fields['grupo_p1'].queryset = self.instance.disciplina.participantes
        self.fields['grupo_p2'].queryset = self.instance.disciplina.participantes
        self.fields['grupo_p3'].queryset = self.instance.disciplina.participantes
        self.fields['grupo_p4'].queryset = self.instance.disciplina.participantes
        self.fields['grupo_p5'].queryset = self.instance.disciplina.participantes
        self.fields['grupo_p6'].queryset = self.instance.disciplina.participantes
        self.fields['grupo_p7'].queryset = self.instance.disciplina.participantes
        self.fields['grupo_p8'].queryset = self.instance.disciplina.participantes

        empty_queryset = Equipo.objects.filter(nombre='*******')
        self.fields['grupo_e1'].queryset = empty_queryset
        self.fields['grupo_e2'].queryset = empty_queryset
        self.fields['grupo_e3'].queryset = empty_queryset
        self.fields['grupo_e4'].queryset = empty_queryset
        self.fields['grupo_e5'].queryset = empty_queryset
        self.fields['grupo_e6'].queryset = empty_queryset
        self.fields['grupo_e7'].queryset = empty_queryset
        self.fields['grupo_e8'].queryset = empty_queryset
    

class RondasClasificacionAdmin(admin.ModelAdmin):
    form = RondasClasificacionForm

    filter_vertical = ('grupo_e1','grupo_e2','grupo_e3','grupo_e4','grupo_e5',
                       'grupo_e6','grupo_e7','grupo_e8',
                       'grupo_p1','grupo_p2','grupo_p3','grupo_p4','grupo_p5',
                       'grupo_p6','grupo_p7','grupo_p8',)

    readonly_fields = (
          'disciplina',
          'participantes_finalistas',
          'equipos_finalistas',
          'semifinal_e1',
          'semifinal_e2',
          'semifinal_e3', 
          'semifinal_e4', 
          'semifinal_p1',
          'semifinal_p2',
          'semifinal_p3', 
          'semifinal_p4', 
          'fase_clasificacion_iniciada',
          'fase_clasificacion_finalizada',
          'fase_semifinal_iniciada',
          'fase_semifinal_finalizada', 
          'fase_final_iniciada',
          )

    

    fieldsets = (
    ('General', {'fields': (
        'disciplina','nrondas')}),

    ('Sección para Disciplinas GRUPALES', {'fields': (
      'grupo_e1','grupo_e2','grupo_e3','grupo_e4','grupo_e5',
      'grupo_e6','grupo_e7','grupo_e8')}),

    ('Sección para Disciplinas INDIVIDUALES', {'fields': (
      'grupo_p1','grupo_p2','grupo_p3','grupo_p4','grupo_p5',
      'grupo_p6','grupo_p7','grupo_p8')}),

    ('Ronda de Clasificacion', {'fields': (
      'nclasificados_grupo1','nclasificados_grupo2',
      'nclasificados_grupo3','nclasificados_grupo4',
      'nclasificados_grupo5','nclasificados_grupo6',
      'nclasificados_grupo7','nclasificados_grupo8',
      'nclasificados_mejores_segundos_clasificacion',
      'nclasificados_mejores_terceros_clasificacion',
    )}),

    ('Semifinales', {'fields': (
      'max_participantes_semifinal1',
      'nclasificados_semifinal1',
      'max_participantes_semifinal2',
      'nclasificados_semifinal2',
      'max_participantes_semifinal3',
      'nclasificados_semifinal3',
      'max_participantes_semifinal4',
      'nclasificados_semifinal4',
      'nclasificados_mejores_segundos_semifinales',
      'nclasificados_mejores_terceros_semifinales',
      )}),

    ('Flags', {'fields': (
      'fase_clasificacion_iniciada',
      'fase_clasificacion_finalizada',
      'fase_semifinal_iniciada',
      'fase_semifinal_finalizada', 
      'fase_final_iniciada')}),
  )



class DeporteAdmin(admin.ModelAdmin):
  filter_vertical = ('responsables',) 

class ArtePuntuacionAdmin(admin.ModelAdmin):
  filter_vertical = ('delegaciones',) 

class PosicionManualAdmin(admin.ModelAdmin):
  list_filter = ('disciplina__olimpiada',)


admin.site.register(Post,PostAdmin)
admin.site.register(Tag)
admin.site.register(Olimpiada)
admin.site.register(Deporte, DeporteAdmin)
admin.site.register(Persona,PersonaAdmin)
admin.site.register(Inscripcion,InscripcionAdmin)

admin.site.register(Disciplina, DisciplinaAdmin)
admin.site.register(Competencia,CompetenciaAdmin)
admin.site.register(Delegacion,DelegacionAdmin)
admin.site.register(Equipo, EquipoAdmin)
admin.site.register(Resultado)
admin.site.register(Cancha)

admin.site.register(Posicion_disciplina)
admin.site.register(Posicion_deporte)
admin.site.register(Posicion_olimpiada)

admin.site.register(Grupos_mas_Playoff, GruposMasPlayoffAdmin)
admin.site.register(RondasClasificacion, RondasClasificacionAdmin)


admin.site.register(Playoff)
admin.site.register(CompetenciaUnica)
admin.site.register(PosicionManual,PosicionManualAdmin)

admin.site.register(Arte)
admin.site.register(Arte_puntuacion,ArtePuntuacionAdmin)

admin.site.register(Foto)
admin.site.register(Galeria)

