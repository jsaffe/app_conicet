from django.db import models
from django.utils import timezone
from django.http import HttpResponse

import torneo.politicas as politicas

# Autenticación
from allauth.account.adapter import DefaultAccountAdapter
class MyAccountAdapter(DefaultAccountAdapter):    
    def is_open_for_signup(self, request):
        return False


class Post (models.Model):
    titulo = models.CharField(max_length=100)
    subtitulo = models.CharField(max_length=150, blank=True, null=True)
    foto = models.ImageField(upload_to = 'posts_imagenes/', default = 'posts_imagenes/default_post.jpg')
    video_link = models.URLField(blank=True, null=True)
    epigrafe = models.CharField(max_length=200,blank=True, null=True)
    resumen = models.TextField(max_length=400)
    text = models.TextField()
    author = models.ForeignKey('auth.User', on_delete=models.CASCADE,blank=True, null=True)

    etiqueta = models.ManyToManyField('Tag',blank=True)   

    created_date = models.DateTimeField(default=timezone.now)
    published_date = models.DateTimeField(blank=True, null=True)

    archivo=models.FileField(upload_to = 'documentos_posts/', blank=True, null=True)

    olimpiadas = models.ManyToManyField('Olimpiada', related_name="posts", blank=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.titulo
        

class Tag (models.Model):
    tag = models.CharField(max_length=100)

    def __str__(self):
        return self.tag


class Foto(models.Model):
    titulo = models.CharField(max_length=200, blank=True, null=True)
    imagen = models.ImageField(upload_to='fotos')
    fecha_subida = models.DateTimeField('Fecha y hora')
    fecha_tomada = models.DateTimeField('Fecha y hora')
    publica = models.BooleanField('Se muestra?', default=True)

    galerias = models.ManyToManyField('Galeria', related_name='fotos', blank=True)
    competencia = models.ForeignKey('Competencia', related_name='fotos', on_delete=models.CASCADE, blank=True, null=True)
    disciplina = models.ForeignKey('Disciplina', related_name='fotos', on_delete=models.CASCADE, blank=True, null=True)
    deporte = models.ForeignKey('Deporte', related_name='fotos', on_delete=models.CASCADE, blank=True, null=True)
    delegacion = models.ManyToManyField('Delegacion', related_name='fotos', blank=True)
    olimpiada = models.ForeignKey('Olimpiada', related_name='fotos', on_delete=models.CASCADE, blank=True, null=True)


    def __str__(self):
       return str(self.imagen.url)


class Galeria (models.Model):
    titulo = models.CharField(max_length=200)
    descripcion = models.TextField(blank=True, null=True)
    fecha_creacion = models.DateTimeField('Fecha y hora')
    publica = models.BooleanField('Se muestra?', default=True)

    def __str__(self):
       return str(titulo)    


class Olimpiada(models.Model):
    nombre = models.CharField(max_length=200, verbose_name='JuegoDeportivo')
    ano = models.CharField(max_length=200)
    
    fecha_inicio = models.DateField('Fecha Inicio Olimpiada')
    fecha_fin = models.DateField('Fecha Fin Olimpiada')
    fecha_corte_edad = models.DateField('Fecha de corte para calcular la edad de los participantes')
    
    fecha_inicio_inscripcion = models.DateField('Fecha Inicio Inscripcion')
    fecha_fin_inscripcion = models.DateField('Fecha Fin Inscripcion')


    descripcion = models.TextField(blank=True, null=True)
    delegaciones = models.ManyToManyField('Delegacion', related_name="olimpiadas", blank=True)
    deportes = models.ManyToManyField('Deporte', related_name="olimpiadas", blank=True)
    artisticos = models.ManyToManyField('Arte', related_name="olimpiadas", blank=True)

    puntos_por_deporte = politicas.puntos_por_deporte

    logo = models.ImageField(upload_to='olimpiadas_logos/', default = 'olimpiadas_logos/default_logo.jpg')

    def actualizar_tablas_deportes (self):
        #Inicializamos/Reiniciamos las entradas de la tabla de posiciones
        for deporte in self.deportes.all():
            for delegacion in self.delegaciones.all():
                if not Posicion_deporte.objects.filter(delegacion=delegacion, olimpiada=self, deporte=deporte).exists():
                    Posicion_deporte.objects.create(delegacion=delegacion, olimpiada=self, deporte=deporte)
                else:
                    posicion = Posicion_deporte.objects.get(delegacion=delegacion, olimpiada=self, deporte=deporte)
                    posicion.reset_values()
                    posicion.save()                   
        
        for deporte in self.deportes.all():
            puntos_dpte = self.puntos_por_deporte[deporte.nombre]

            #Se realiza la sumatoria de puntos de las respectivas disciplina
            disciplinas = Disciplina.objects.filter(deporte=deporte, olimpiada=self)
            for disciplina in disciplinas:
                if disciplina.finalizada:
                    if disciplina.modalidad in ['CompetenciaUnica', 'PosicionManual']:
                        posiciones_disciplina = Posicion_disciplina.objects.filter(disciplina=disciplina).order_by('posicion')
                        # print ('Actualizando tabla deporte, modalidad CompetenciaUnica. Disciplina: ',disciplina, '(',disciplina.pk,')')
                    elif disciplina.modalidad in ['Grupos_mas_Playoff','Playoff_']:
                        posiciones_disciplina = Posicion_disciplina.objects.filter(disciplina=disciplina, zona='Eliminatoria').order_by('posicion')
                        # print ('Actualizando tabla deporte, modalidad G+P o Playoff. Disciplina: ',disciplina, '(',disciplina.pk,')')
                    elif disciplina.modalidad in ['RondasClasificacion',]:
                        posiciones_disciplina = Posicion_disciplina.objects.filter(disciplina=disciplina, zona='Final').order_by('posicion')
                        # print ('Actualizando tabla deporte, modalidad RondasClasificacion. Disciplina: ',disciplina, '(',disciplina.pk,')')
                    elif disciplina.modalidad in ['Liga']:
                        posiciones_disciplina = Posicion_disciplina.objects.filter(disciplina=disciplina, zona='Clasificatoria').order_by('posicion')
                        # print ('Actualizando tabla deporte, modalidad Liga. Disciplina: ',disciplina, '(',disciplina.pk,')')
                    else:
                        posiciones_disciplina = Posicion_disciplina.objects.filter(disciplina=disciplina).order_by('posicion')
                        print ('Modalidad no reconocida al actualizar tabla deportes. Disciplina: ', disciplina, '(',disciplina.pk,')')


                    #Calculamos la cantidad de posiciones empatadas
                    pos_anterior = -1
                    pos_empatados = {1:0,2:0,3:0,4:0}
                    for posicion_disciplina in posiciones_disciplina:
                        if posicion_disciplina.posicion > 4:
                            break
                        # print ('Posicion Disciplina: '+str(posicion_disciplina.posicion))
                        if posicion_disciplina.posicion == pos_anterior:
                            pos_empatados[posicion_disciplina.posicion] = pos_empatados[posicion_disciplina.posicion] + 1
                            print ('empatados')
                        else:
                            pos_anterior = posicion_disciplina.posicion

                    # for posicion in pos_empatados.keys():
                    #     print (posicion, pos_empatados[posicion])




                    for n, posicion_disciplina in enumerate (posiciones_disciplina):
                        if posicion_disciplina.disciplina.tipo in 'Grupal':
                            njugadores = len(posicion_disciplina.equipo.jugadores.all())
                            
                            #No evaluamos rellenos
                            if posicion_disciplina.equipo.nombre == 'Relleno':
                                print ('Se descarta relleno en evaluacion de tabla deporte')
                                continue
                            # if posicion_disciplina.puntos == 0.000:
                            #     continue
                            if posicion_disciplina.posicion > 4:
                                break
                        
                            #Si es grupal chequea delegaciones de sus miembros y agrega puntos prorrateados a sus 
                            #respectivas delegaciones en (posicion_deporte) correspondientes.
                            for jugador in posicion_disciplina.equipo.jugadores.all():
                                posicion_deporte = Posicion_deporte.objects.get(delegacion=jugador.delegacion, olimpiada=self, deporte=deporte)

                                if pos_empatados[posicion_disciplina.posicion]:
                                    puntos_a_sumar = 0
                                    for n in range (pos_empatados[posicion_disciplina.posicion]+1):
                                        if posicion_disciplina.posicion + n < 4:
                                            puntos_a_sumar = puntos_a_sumar + puntos_dpte[posicion_disciplina.posicion+n-1]
                                    puntos_a_sumar = puntos_a_sumar / (pos_empatados[posicion_disciplina.posicion] + 1)
                                    
                                    posicion_deporte.puntos = posicion_deporte.puntos + (puntos_a_sumar / njugadores) 
                                else:
                                    posicion_deporte.puntos = posicion_deporte.puntos + (puntos_dpte[posicion_disciplina.posicion-1] / njugadores)

                                posicion_deporte.save()


                        #Si es individual chequea delegación a la que pertenece y agrega puntos 
                        #a delegación en (posicion_deporte) correspondiente
                        else:                           
                            #No evaluamos rellenos
                            if posicion_disciplina.participante.nombre == 'Relleno':
                                print ('Se descarta relleno en evaluacion de tabla deporte')
                                continue
                            # if posicion_disciplina.puntos == 0.000:
                            #     continue
                            if posicion_disciplina.posicion > 4:
                                break
                        
                            posicion_deporte = Posicion_deporte.objects.get(delegacion=posicion_disciplina.participante.delegacion, olimpiada=self, deporte=deporte)
                            
                            if pos_empatados[posicion_disciplina.posicion]:
                                puntos_a_sumar = 0
                                for n in range (pos_empatados[posicion_disciplina.posicion]+1):
                                    if posicion_disciplina.posicion + n < 4:
                                        puntos_a_sumar = puntos_a_sumar + puntos_dpte[posicion_disciplina.posicion+n-1]
                                puntos_a_sumar = puntos_a_sumar / (pos_empatados[posicion_disciplina.posicion] + 1)
                                
                                posicion_deporte.puntos = posicion_deporte.puntos + puntos_a_sumar
                            else:
                                posicion_deporte.puntos = posicion_deporte.puntos + puntos_dpte[posicion_disciplina.posicion-1]

                            posicion_deporte.save()


                
            # #Se actualizan las posiciones en función de los puntos obtenidos
            posiciones_deporte = Posicion_deporte.objects.filter(olimpiada=self, deporte=deporte).order_by('-puntos')
            pos_anterior = -1
            puntos_anterior = -1
            for n, posicion_deporte in enumerate (posiciones_deporte):
                if posicion_deporte.puntos == puntos_anterior:
                    posicion_deporte.posicion = pos_anterior
                else:
                    posicion_deporte.posicion = n+1
                    pos_anterior = n+1
                    puntos_anterior = posicion_deporte.puntos
                posicion_deporte.save()                


        #Actualizamos Tabla Copa Conjunto
        self.actualizar_tabla_copaconjunto()

    def actualizar_tabla_copaconjunto(self):
        #Inicializamos/Reiniciamos las entradas de la tabla de posiciones
        for delegacion in self.delegaciones.all():
            if not Posicion_olimpiada.objects.filter(delegacion=delegacion, olimpiada=self).exists():
                Posicion_olimpiada.objects.create(delegacion=delegacion, olimpiada=self)
            else:
                posicion = Posicion_olimpiada.objects.get(delegacion=delegacion, olimpiada=self)
                posicion.reset_values()
                posicion.save()                   
        
        #Se realiza la sumatoria de puntos de los deportes
        for deporte in self.deportes.all():
            print ('Deporte: '+deporte.nombre)

            puntos_dpte = self.puntos_por_deporte[deporte.nombre]
            posiciones_deporte = Posicion_deporte.objects.filter(olimpiada=self, deporte=deporte).order_by('posicion')
            
            #Calculamos la cantidad de posiciones empatadas
            pos_anterior = -1
            pos_empatados = {1:0,2:0,3:0,4:0}
            for posicion_deporte in posiciones_deporte:
                if posicion_deporte.puntos == 0.000:
                    continue
                if posicion_deporte.posicion > 4:
                    break
                # print ('Posicion Deporte: '+str(posicion_deporte.posicion))

                if posicion_deporte.posicion == pos_anterior:
                    pos_empatados[posicion_deporte.posicion] = pos_empatados[posicion_deporte.posicion] + 1
                    print ('empatados')
                else:
                    pos_anterior = posicion_deporte.posicion

            for posicion in pos_empatados.keys():
                print (posicion, pos_empatados[posicion])

            #Sumamos los puntos de los deportes contemplando las posiciones empatadas
            for n, posicion_deporte in enumerate (posiciones_deporte):                
                if posicion_deporte.puntos == 0.000:
                    continue
                if posicion_deporte.posicion > 4:
                    break
            
                posicion_olimpiada = Posicion_olimpiada.objects.get(delegacion=posicion_deporte.delegacion, olimpiada=self)
                
                # ej: puntos_dpte = 10,6,3,1
                # ej: pos_empatados = {'1':1,'2':0,'3':0,'4':0}
                if pos_empatados[posicion_deporte.posicion]:
                    puntos_a_sumar = 0
                    for n in range (pos_empatados[posicion_deporte.posicion]+1):
                        if posicion_deporte.posicion + n < 4:
                            puntos_a_sumar = puntos_a_sumar + puntos_dpte[posicion_deporte.posicion+n-1]
                    puntos_a_sumar = puntos_a_sumar / (pos_empatados[posicion_deporte.posicion] + 1)
                    
                    posicion_olimpiada.puntos = posicion_olimpiada.puntos + puntos_a_sumar
                else:
                    posicion_olimpiada.puntos = posicion_olimpiada.puntos + puntos_dpte[posicion_deporte.posicion-1]

                posicion_olimpiada.save()
                # print ('\t',posicion_olimpiada.delegacion.nombre,' | Puntos a sumar: ',str(puntos_dpte[i]),' (despues: ',posicion_olimpiada.puntos,')' )

        #Se realiza la sumatoria de los puntos por Arte
        try:
            arte_puntuacion = Arte_puntuacion.objects.filter(olimpiada=self)
            delegaciones = arte_puntuacion.delegaciones.all()

            for delegacion in delegaciones:
                posicion_olimpiada = Posicion_olimpiada.objects.get(delegacion=delegacion, olimpiada=self)
                posicion_olimpiada.puntos = posicion_olimpiada.puntos + politicas.puntos_arte
                posicion_olimpiada.save()
        except Exception as e:
            print('ERROR: No se pudo procesar puntos de Artes.')
            pass

        #Se actualizan las posiciones en función de los puntos obtenidos y se calcula rendimiento
        posiciones_olimpiada = Posicion_olimpiada.objects.filter(olimpiada=self).order_by('-puntos')
        pos_anterior = -1
        puntos_anterior = -1
        for n, posicion_olimpiada in enumerate (posiciones_olimpiada):
            #establecemos posiciones en función del puntaje obtenido
            if posicion_olimpiada.puntos == puntos_anterior:
                posicion_olimpiada.posicion = pos_anterior
            else:
                posicion_olimpiada.posicion = n+1
                pos_anterior = n+1
                puntos_anterior = posicion_olimpiada.puntos

            #calculamos rendimiento de delegaciones (puntos/n_miembros_delegacion) )
            n_miembros_delegacion = Persona.objects.filter(delegacion=posicion_olimpiada.delegacion, 
                                                          olimpiadas=self).count()
            if n_miembros_delegacion:
                posicion_olimpiada.rendimiento = posicion_olimpiada.puntos / n_miembros_delegacion
            else: 
                posicion_olimpiada.rendimiento = 0

            posicion_olimpiada.save()                
   

        #Se actualizan las posiciones en función del rendimiento obtenido
        posiciones_olimpiada = Posicion_olimpiada.objects.filter(olimpiada=self).order_by('-rendimiento')
        pos_anterior = -1
        rendimiento_anterior = -1
        for n, posicion_olimpiada in enumerate (posiciones_olimpiada):
            #establecemos posiciones en función del puntaje obtenido
            if posicion_olimpiada.rendimiento == rendimiento_anterior:
                posicion_olimpiada.posicion_rendimiento = pos_anterior
            else:
                posicion_olimpiada.posicion_rendimiento = n+1
                pos_anterior = n+1
                rendimiento_anterior = posicion_olimpiada.rendimiento
            posicion_olimpiada.save()


    def __str__(self):
        return self.nombre + str(self.ano)


class Deporte(models.Model):
    class Meta:
        permissions = ( ('deporte_Ajedrez','deporte_Ajedrez'),('deporte_Atletismo','deporte_Atletismo'),
                        ('deporte_Basquet','deporte_Basquet'),('deporte_Bochas','deporte_Bochas'),
                        ('deporte_Ciclismo','deporte_Ciclismo'),('deporte_Fútbol','deporte_Fútbol'),
                        ('deporte_Handball','deporte_Handball'),('deporte_Hockey','deporte_Hockey'),
                        ('deporte_Natación','deporte_Natación'),('deporte_Pádel','deporte_Pádel'),
                        ('deporte_Pesca','deporte_Pesca'),('deporte_Tenis','deporte_Tenis'),
                        ('deporte_Tenis de Mesa','deporte_Tenis de Mesa'),('deporte_Vela','deporte_Vela'),
                        ('deporte_Voley','deporte_Voley'),('deporte_Caminata/Carrera','deporte_Caminata/Carrera'),
                        ('deporte_Kayak','deporte_Kayak'), ('deporte_Aguas Abiertas','deporte_Aguas Abiertas'),
                      )

    nombre = models.CharField(max_length=200)
    responsables = models.ManyToManyField('Persona', verbose_name='Responsable/s', blank=True)  
    icono_deporte = models.ImageField(upload_to = 'deportes_iconos/', default = 'deportes_iconos/default_deporte.png')

    def __str__(self):
        return self.nombre



class Delegacion(models.Model):
    nombre = models.CharField(max_length=200)
    responsables = models.ManyToManyField('Persona', related_name="responsable_delegacion", blank=True, verbose_name='Responsable/s')
    foto_delegacion = models.ImageField(upload_to = 'delegaciones_fotos/', default = 'delegaciones_fotos/default_delegacion.jpg')
    descripcion = models.CharField(max_length=1000, blank=True, null=True)

    def __str__(self):
        return self.nombre      


class Equipo (models.Model):
    nombre = models.CharField(max_length=200)
    descripcion = models.TextField(blank=True, null=True)
    disciplina = models.ForeignKey('Disciplina', related_name='equipos', on_delete=models.CASCADE)
    foto_equipo = models.ImageField(upload_to = 'equipos_fotos/', default = 'equipos_fotos/default_equipo.jpg')
    jugadores = models.ManyToManyField('Persona', related_name='equipos', blank=True)
    capitanes = models.ManyToManyField('Persona', blank=True)    #TODO: limitar a las personas dentro del equipo (jugadores)           

    def __str__(self):
        return str(self.disciplina) + ' | ' + self.nombre


class Persona (models.Model):
    def generate_path_personas (instance,filename):
        return 'participantes/'+str(instance.apellido)+'_'+str(instance.numero_documento)+'/'+filename

    usuario = models.OneToOneField('auth.User', on_delete=models.CASCADE,blank=True, null=True)

    # DATOS PERSONALES
    nombre = models.CharField(max_length=200)
    apellido = models.CharField(max_length=200)
    
    genero_opc = [('Masculino','Masculino'), ('Femenino','Femenino'), ('No Binario','No Binario')]
    genero = models.CharField('Género',max_length=10, choices=genero_opc)
    
    fecha_nacimiento = models.DateField('Fecha de Nacimiento')
    mostrar_edad = models.BooleanField('¿Mostrar Edad en Sitio Público?:',default=True) 

    tipo_documento_opc = [('DNI','DNI'), ('PASAPORTE','PASAPORTE'),('L.U.','L.U.'),('Otro','Otro')]
    tipo_documento = models.CharField('Tipo de Documento', choices=tipo_documento_opc, default='DNI', max_length=200)
    numero_documento = models.CharField('Número de Documento', max_length=50, unique=True)                                        
    
    grupo_sanguineo_opc = [('0+','0+'), ('0-','0-'),('A+','A+'),('A-','A-'),('B+','B+'),('B-','B-'),
                           ('AB+','AB+'),('AB-','AB-')]            
    grupo_sanguineo = models.CharField('Grupo Sanguíneo',choices=grupo_sanguineo_opc, max_length=200)
    
    alimentacion_opc = [('Sin consideraciones particulares','Sin consideraciones particulares'), 
                        ('Celíaca/o','Celíaca/o'),('Vegana/o','Vegana/o'),('Vegetariana/o','Vegetariana/o'),
                        ('Diabética/o','Diabética/o'), ('Otro','Otro')]
    alimentacion =  models.CharField('Consideraciones Particulares Alimentacion',  max_length=200,
                                     choices=alimentacion_opc)
    
    talle_remera_opc = [('XS','XS'), ('S','S'),('M','M'),('L','L'),('XL','XL'),('XXL','XXL')]            
    talle_remera = models.CharField('Talle de Remera',choices=talle_remera_opc, max_length=200)
    
    mail = models.EmailField('e-mail',max_length=254, unique=True) 
    facebook = models.CharField('Facebook (opcional)', max_length=200, blank=True, null=True)
    foto= models.FileField('Foto Carnet (max.:3.5MB)',upload_to = generate_path_personas, max_length=200) #, default = 'fotos_participantes/no-img.jpg') 
   
    # DATOS LABORALES y DELEGACION:
    olimpiadas = models.ManyToManyField('olimpiada', related_name='personas', blank=True)                                                   
    delegacion = models.ForeignKey('Delegacion', on_delete=models.CASCADE)
    localidad = models.CharField('Localidad', max_length=200)
    institucion = models.CharField('Institución', max_length=200)
    tipo_personal_opc = [
        ('CONICET','CONICET'), ('Vinculado/a','Vinculado/a'), ('Jubilado/a','Jubilado/a'), ('EX-CONICET','EX-CONICET'),
        ('Acompañante Colaborador/a','Acompañante Colaborador/a'), ('Invitado/a Especial','Invitado/a Especial'),]
    tipo_personal = models.CharField('Tipo de Personal',choices=tipo_personal_opc, max_length=200)
    actividad_laboral_opc = [('Administrativo/a','Administrativo/a'), ('Becario/a','Becario/a'),('Acompañante Colaborador','Acompañante Colaborador'),
                             ('Investigador/a','Investigador/a'),('Personal de Apoyo','Personal de Apoyo'),('Otro/a','Otro/a'),]
    actividad_laboral= models.CharField('Actividad Laboral', choices=actividad_laboral_opc, max_length=200)
    telefono_laboral= models.CharField('Telefono Laboral', max_length=200) 
    telefono_personal = models.CharField('Telefono Personal/Celular', max_length=200)  
    link_web_laboral = models.CharField('Link a Web Laboral (opcional)', max_length=300, blank=True, null=True,
                                        help_text='Ej. Sitio web del laboratorio, departamento, institución, u otro sitio donde se encuentre publicado el trabajo de investigación, extensión y/o docencia que realiza.')  
    descripcion_personal = models.TextField('Descripción Personal (opcional)',blank=True, null=True)

    # DOCUMENTOS
    documento_digitalizado = models.FileField('Documento Escaneado (max.:3.5MB)', upload_to = generate_path_personas, max_length=200)

    #==================================================================================================================================================
    # CAMPOS A BORRAR
    #==================================================================================================================================================
    certificado_medico = models.FileField('Certificado Médico (max.:3.5MB)', upload_to = generate_path_personas, blank=True, null=True, max_length=200)
    comprobante_pago = models.FileField('Comprobante de Pago (max.:3.5MB)', upload_to = generate_path_personas, max_length=200)
    vinculacion_conicet = models.FileField('Certificado de Vinculacion (max.:3.5MB)',upload_to = generate_path_personas, blank=True, null=True,
                                           help_text='Es obligatorio adjuntar en caso de NO ser personal de CONICET o ser EX-CONICET.', max_length=200)

    # ALOJAMIENTO y ACOMPAÑANTES
    alojamiento_opc = [('Sí, me alojaré en algunos de los alojamientos sugeridos.','Sí, me alojaré en algunos de los alojamientos sugeridos.'), 
                       ('No, me alojaré por mi cuenta en otro lugar.','No, me alojaré por mi cuenta en otro lugar.')]            
    alojamiento = models.CharField('Lugar de Alojamiento', choices=alojamiento_opc, 
                                   help_text='¿Se alojará en algunos de los alojamientos sugeridos?', max_length=200)
    acompanantes_opc = [(0,'0'),(1,'1'),(2,'2'),(3,'3'),(4,'4'),(5,'5'),(6,'6'),(7,'7'),(8,'8'),(9,'9'),(10,'10')]
    acompanantes = models.IntegerField('Número de Acompañantes', choices=acompanantes_opc, default=0)

    # DEPORTES
    deportes = models.ManyToManyField('Deporte', blank=True)                                                                                    
    deporte1 = models.ForeignKey('Deporte', related_name='participantes_primarios', on_delete=models.CASCADE, verbose_name = 'Deporte Principal', 
                                 help_text="Este es el deporte al que le darás prioridad por sobre todos los otros.", blank=True, null=True)                
    deporte2 = models.ForeignKey('Deporte', related_name='participantes_secundarios', on_delete=models.CASCADE, verbose_name = 'Deporte Secundario',
                                 help_text="Este es un deporte de segunda prioridad.",blank=True, null=True)
    deporte3 = models.ForeignKey('Deporte', related_name='participantes_terciarios',on_delete=models.CASCADE, verbose_name = 'Deporte Terciario',
                                 help_text="Este es un deporte de tercera prioridad.",blank=True, null=True)
    deporte_maraton_opc = [('No','No'),('Carrera 5K','Carrera 5K'), ('Carrera 10K','Carrera 10K'),('Caminata 5K','Caminata 5K')]            
    deporte_maraton = models.CharField('Carrera/Caminata',choices=deporte_maraton_opc, max_length=200)

    disciplinas_deporte1 = models.ManyToManyField('Disciplina', related_name='participantes_prioridad1', verbose_name = 'Disciplinas del Deporte Principal', blank=True)          
    disciplinas_deporte2 = models.ManyToManyField('Disciplina', related_name='participantes_prioridad2', verbose_name = 'Disciplinas del Deporte Secundario', blank=True)     
    disciplinas_deporte3 = models.ManyToManyField('Disciplina', related_name='participantes_prioridad3', verbose_name = 'Disciplinas del Deporte Terciario', blank=True)
    
    # OTROS
    artes = models.ManyToManyField('Arte', verbose_name='Participación en Espacio Artístico',blank=True)           
    divulgacion_opc = [('No','No'),('Sí','Sí'),('Tal vez','Tal vez')]            
    divulgacion = models.CharField('¿Desea Participar en el Espacio de Divulgación?', choices=divulgacion_opc, max_length=200,
                                   help_text='En caso de responder sí o tal vez deberá completar el link a su sitio web laboral.')
    
    colaborador_opc = [('No','No'),('Sí','Sí'),('Tal vez','Tal vez')]            
    colaborador =  models.CharField('¿Desea colaborar con la Organización?', choices=colaborador_opc, max_length=200,
                                    help_text= 'Podes colaborar en la organización de los juegos en las áreas de administración, deportes, \
                                    comunicación, publicidad, etc., además podrás participar en los deportes en los que te inscribiste.')
    
    inscripcion_completa = models.BooleanField (default=False)
    inscripcion_fecha = models.DateTimeField('Fecha de Inscripcion', blank=True, null=True)
    ultima_modificacion = models.DateTimeField('Fecha de Última Modificación', blank=True, null=True)

    mail_reset_password_enviado = models.BooleanField (default=False)
    mail_inscripcion_enviado = models.BooleanField (default=False)

    @property
    def name_to_print(self):
        nombres = self.nombre.split(' ')
        if len(nombres) > 1:
            res = nombres[0][0].upper() + nombres[0][1:].lower()  + ' ' + nombres[1][0].upper() + '. '
        else:
            res = nombres[0][0].upper() + nombres[0][1:].lower()  + ' '

        res += self.apellido[0].upper() + self.apellido.lower()[1:]
        return res


    #METODOS
    def calcular_edad_en_olimpiada (self,olimpiada):
        fecha_nacimiento = self.fecha_nacimiento
        fecha_corte_edad = olimpiada.fecha_corte_edad
        return fecha_corte_edad.year - fecha_nacimiento.year - ((fecha_corte_edad.month, fecha_corte_edad.day) < (fecha_nacimiento.month, fecha_nacimiento.day))
    
    def __str__(self):
        return str(self.delegacion) + ' | ' +  self.apellido.capitalize() + ' ' + self.nombre.capitalize()


class Inscripcion (models.Model):
    olimpiada = models.ForeignKey('Olimpiada', related_name='inscripcion', on_delete=models.CASCADE, blank=True, null=True)
    jugador = models.ForeignKey('Persona', related_name='inscripcion', on_delete=models.CASCADE, blank=True, null=True)

    def generate_path_personas (instance,filename):
        return 'participantes/'+str(instance.jugador.apellido)+'_'+str(instance.jugador.numero_documento)+'/'+filename

    # RESPONSABILIDAD Y SALUD
    aceptacion_terminos = models.BooleanField(
        'Acepto los términos y condiciones',
        default=False)

    estado_salud = models.BooleanField(
        'Tengo afecciones de salud que condicionan las actividades deportivas en las que puedo participar',
        default=False)

    # DEPORTES
    deportes = models.ManyToManyField('Deporte', blank=True)                                                                                    
    deporte1 = models.ForeignKey(
        'Deporte', related_name='inscripciones_primarios', on_delete=models.CASCADE, verbose_name = 'Deporte Principal', 
        help_text="Este es el deporte al que le darás prioridad por sobre todos los otros.", blank=True, null=True)                
    deporte2 = models.ForeignKey(
        'Deporte', related_name='inscripciones_secundarios', on_delete=models.CASCADE, verbose_name = 'Deporte Secundario',
        help_text="Este es un deporte de segunda prioridad.",blank=True, null=True)
    deporte3 = models.ForeignKey(
        'Deporte', related_name='inscripciones_terciarios',on_delete=models.CASCADE, verbose_name = 'Deporte Terciario',
        help_text="Este es un deporte de tercera prioridad.",blank=True, null=True)
    deporte_maraton_opc = [
        ('No','No'),('Carrera 5K','Carrera 5K'), ('Carrera 10K','Carrera 10K'),('Caminata 5K','Caminata 5K')]            
    deporte_maraton = models.CharField('Carrera/Caminata',choices=deporte_maraton_opc, max_length=200)

    disciplinas_deporte1 = models.ManyToManyField('Disciplina', related_name='inscripciones_prioridad1', verbose_name = 'Disciplinas del Deporte Principal', blank=True)          
    disciplinas_deporte2 = models.ManyToManyField('Disciplina', related_name='inscripciones_prioridad2', verbose_name = 'Disciplinas del Deporte Secundario', blank=True)     
    disciplinas_deporte3 = models.ManyToManyField('Disciplina', related_name='inscripciones_prioridad3', verbose_name = 'Disciplinas del Deporte Terciario', blank=True)

    actividades_recreativas = models.ManyToManyField('Disciplina', related_name='recreativos', verbose_name = 'Actividades Recreativos', blank=True)

    # DOCUMENTOS
    certificado_medico = models.FileField('Certificado Médico + Ergometría (max.:3.5MB)', upload_to = generate_path_personas, blank=True, null=True, max_length=200)
    comprobante_pago = models.FileField('Comprobante de Pago (max.:3.5MB)', upload_to = generate_path_personas, max_length=200)
    vinculacion_conicet = models.FileField(
        'Certificado de Vinculación (max.:3.5MB)',upload_to = generate_path_personas, blank=True, null=True,
        help_text='Es obligatorio adjuntar en caso de NO ser personal de CONICET o ser EX-CONICET.', max_length=200)
    
    # OTROS
    artes = models.ManyToManyField('Arte', verbose_name='Participación en Espacio Artístico',blank=True)
    divulgacion_opc = [('No','No'),('Sí','Sí'),('Tal vez','Tal vez')]            
    divulgacion = models.CharField(
        '¿Desea Participar en el Espacio de Divulgación?', choices=divulgacion_opc, max_length=200,
        help_text='En caso de responder sí o tal vez, deberá completar el link a su sitio web laboral.')

    # ALOJAMIENTO y ACOMPAÑANTES
    alojamiento_opc = [
        ('Sí, me alojaré en algunos de los alojamientos sugeridos.','Sí, me alojaré en algunos de los alojamientos sugeridos.'), 
        ('No, me alojaré por mi cuenta en otro lugar.','No, me alojaré por mi cuenta en otro lugar.')]            
    alojamiento = models.CharField(
        'Lugar de Alojamiento', choices=alojamiento_opc, 
        help_text='¿Se alojará en algunos de los alojamientos sugeridos?', max_length=200)

    alimentacion_opc = [
        ('Sin consideraciones particulares','Sin consideraciones particulares'), 
        ('Celiaca/o','Celiaca/o'),('Vegana/o','Vegana/o'),('Vegetariana/o','Vegetariana/o'),('Otro','Otro')]
    alimentacion =  models.CharField(
        'Consideraciones Particulares Alimentacion',  max_length=200,
        choices=alimentacion_opc, default='Sin consideraciones particulares')

    acompanantes_opc = [(0,'0'),(1,'1'),(2,'2'),(3,'3'),(4,'4'),(5,'5'),(6,'6'),(7,'7'),(8,'8'),(9,'9'),(10,'10')]
    acompanantes = models.IntegerField('Número de Acompañantes', choices=acompanantes_opc, default=0)

    colaborador_opc = [('No','No'),('Sí','Sí'),('Tal vez','Tal vez')]            
    colaborador =  models.CharField(
        '¿Desea colaborar con la Organización?', choices=colaborador_opc, max_length=200,
        help_text= 'Podes colaborar en la organización de los juegos en las áreas de administración, deportes, \
            comunicación, publicidad, etc., además podrás participar en los deportes en los que te inscribiste.')

    inscripcion_completa = models.BooleanField (default=False)
    inscripcion_fecha = models.DateTimeField('Fecha de Inscripcion', blank=True, null=True)
    mail_inscripcion_enviado = models.BooleanField (default=False)
    ultima_modificacion = models.DateTimeField('Fecha de Última Modificación', blank=True, null=True)


    def __str__(self):
        return str(self.jugador) + ' | ' + str(self.olimpiada)

    def delete(self):
        print ('Eliminando Ficha Inscripcion de '+str(self.jugador))
        #eliminamos el jugador de la olimpiada
        self.jugador.olimpiadas.remove(self.olimpiada)

        #eliminamos el jugador de las disciplinas inscriptas
        disciplinas = Disciplina.objects.filter(olimpiada=self.olimpiada, 
                                                participantes=self.jugador)
        for disciplina in disciplinas:
            print ('\tDisciplina:'+str(disciplina))
            disciplina.participantes.remove(self.jugador)

            #si el jugador está inscripto en algun equipo 
            #de la disciplinalo eliminamos
            equipos = Equipo.objects.filter(disciplina=disciplina, 
                                            jugadores=self.jugador)
            for equipo in equipos:
                print ('\t\tEquipo:'+str(equipo))
                equipo.jugadores.remove(self.jugador)

        #eliminamos ficha de inscripción
        Inscripcion.objects.filter(pk=self.pk).delete()


class Arte (models.Model):
    nombre = models.CharField(max_length=200)
    descripcion = models.TextField(blank=True, null=True)   
    reglamento = models.TextField(default='', blank=True)
    reglamento_pdf = models.FileField(upload_to = 'reglamentos/', blank=True, null=True)    
    
    def __str__(self):
        return self.nombre
        

class Arte_puntuacion (models.Model):
    olimpiada = models.ForeignKey('Olimpiada', on_delete=models.CASCADE)
    delegaciones = models.ManyToManyField('Delegacion', blank=True)

    def __str__(self):
        return 'PuntuacionArte '+ self.olimpiada.nombre + ' ' + self.olimpiada.ano


class Cancha (models.Model):
    nombre = models.CharField(max_length=200)
    direccion = models.CharField(max_length=1000)
    mapa = models.CharField(max_length=200)
    foto = models.ImageField(upload_to = 'canchas_fotos/', default = 'canchas_fotos/default_cancha.jpg')
    
    def __str__(self):
        return self.nombre

        
class Disciplina (models.Model):
    class Meta:
        permissions = ( ('editarfixture', 'Editar Fixtures'),)

    nombre = models.CharField(max_length=200)
    genero_opc = [('Masculino','Masculino'), ('Femenino','Femenino'), ('Mixto','Mixto')]
    genero = models.CharField('Género',max_length=10, choices=genero_opc, default='Masculino')
    limite_edad_opc = [(0,'Libre'),(35,'+35'),(40,'+40'),(45,'+45'),(55,'+55'),(60,'+60'),(65,'+65')]
    limite_edad = models.IntegerField('Limite de Edad de la Disciplina', choices=limite_edad_opc, default=0)
    descripcion = models.TextField(blank=True, null=True)
    reglamento_pdf = models.FileField(upload_to = 'reglamentos/', blank=True, null=True)    
    deporte = models.ForeignKey('Deporte', on_delete=models.CASCADE)
    olimpiada = models.ForeignKey('Olimpiada', on_delete=models.CASCADE)
    responsables = models.ManyToManyField('Persona', verbose_name='Responsable/s', blank=True)  

    #MODALIDAD y TIPO de DISCIPLINA
    tipos = [('Grupal','Grupal'), ('Individual','Individual')]
    tipo = models.CharField(max_length=10, choices=tipos, default='Grupal')

    modalidades = [('CompetenciaUnica','CompetenciaUnica'),
                   ('Grupos_mas_Playoff','Grupos_mas_Playoff'),
                   ('Playoff_','Playoff'),
                   ('Liga','Liga'),
                   ('PosicionManual','PosicionManual'),
                   ('RondasClasificacion','RondasClasificacion'),]
    modalidad = models.CharField(max_length=20, choices=modalidades, default='CompetenciaUnica')

    #PARTICIPANTES / EQUIPOS
    participantes = models.ManyToManyField('Persona', related_name='disciplinas', blank=True, verbose_name='Competidores')
    max_jugadores_lbf = models.IntegerField('Maximos Jugadores en Lista de Buena Fé', default=1)
    max_jugadores_extras = models.IntegerField('Juegadores Extras al Máximo de la Lista de Buena Fé', default=0, 
                                                help_text=('Cada equipo puede estar integrado por el número máximo de jugadores especificados por la lista de buena fé, más un número extra de jugadores.'+
                                                           ' En los distintos partidos solo pueden jugar (presentarse en cancha) el número máximo establecido por la lista de buena fé.'+
                                                           ' La lista de buena fé puede variar de partido a partido entre todos los integrantes del equipo.'))
    #FIXTURE
    fixture_creado = models.BooleanField (default=False)
    finalizada = models.BooleanField (default=False)
    
    def iniciar_modalidad(self):
        if self.modalidad in 'Grupos_mas_Playoff':
            Grupos_mas_Playoff.objects.create(disciplina=self)
        
        if self.modalidad in 'RondasClasificacion':
            RondasClasificacion.objects.create(disciplina=self)


        if self.modalidad in 'Liga':
            Grupos_mas_Playoff.objects.create(disciplina=self)

            if self.tipo in 'Grupal':
                self.grupos_mas_playoff.first().grupo_e1.set(self.equipos.all())
                self.grupos_mas_playoff.first().save()
            else:
                self.grupos_mas_playoff.first().grupo_p1.set(self.participantes.all())
                self.grupos_mas_playoff.first().save()

            self.grupos_mas_playoff.first().iniciar_grupos()

    def reiniciar_modalidad(self):
        if self.modalidad in 'Grupos_mas_Playoff':
            self.grupos_mas_playoff.first().reiniciar_modalidad()

        if self.modalidad in 'RondasClasificacion':
            self.rondas_clasificacion.first().reiniciar_modalidad()
            
        if self.modalidad in 'Liga':
            self.grupos_mas_playoff.first().reiniciar_modalidad()

        if self.modalidad in 'Playoff_':
            self.playoff.first().reiniciar_modalidad()

        if self.modalidad in 'CompetenciaUnica':
            self.competencia_unica.first().reiniciar_modalidad()

        if self.modalidad in 'PosicionManual':
            self.posicion_manual.first().reiniciar_modalidad()

    def crear_fixture(self):
        if self.modalidad in 'Playoff_':
            if self.tipo in 'Grupal':
                nequipos = self.equipos.count()  
                if nequipos in [4,8,16]:
                    playoff = Playoff.objects.create(disciplina=self, min_participantes_playoff=nequipos)
                    playoff.crear_fixture(integrantes=self.equipos)
                    
            else:     
                nparticipantes = self.participantes.count()
                if nparticipantes in [4,8,16]:
                    playoff = Playoff.objects.create(disciplina=self, min_participantes_playoff=nparticipantes)
                    playoff.crear_fixture(integrantes=self.participantes)


        if self.modalidad in 'CompetenciaUnica':
            competencia_unica = CompetenciaUnica.objects.create(disciplina=self)
            competencia_unica.crear_fixture()

        if self.modalidad in 'PosicionManual':
            competencia_unica = PosicionManual.objects.create(disciplina=self)
            competencia_unica.crear_fixture()

        if self.modalidad in 'Grupos_mas_Playoff':
            grupos_mas_playoff = Grupos_mas_Playoff.objects.filter(disciplina=self).first()
            grupos_mas_playoff.crear_fixture()

        if self.modalidad in 'RondasClasificacion':
            rondas_clasificacion = RondasClasificacion.objects.get(disciplina=self)
            rondas_clasificacion.iniciar_final()
           
    def reiniciar_fixture(self):
        if self.modalidad in ['Playoff_']:
            self.playoff.first().reiniciar_fixture()

        if self.modalidad in ['CompetenciaUnica']:
            self.competencia_unica.first().reiniciar_fixture()

        if self.modalidad in ['PosicionManual']:
            self.posicion_manual.first().reiniciar_fixture()

        if self.modalidad in 'Grupos_mas_Playoff':
            self.grupos_mas_playoff.first().reiniciar_fixture()

        if self.modalidad in 'RondasClasificacion':
            self.rondas_clasificacion.first().reiniciar_final()

    def actualizar_fixture(self):
        if self.modalidad in 'Playoff_':
            self.playoff.first().actualizar_fixture()

        if self.modalidad in 'CompetenciaUnica':
            self.competencia_unica.first().actualizar_fixture()

        if self.modalidad in 'PosicionManual':
            self.posicion_manual.first().actualizar_fixture()
        
        if self.modalidad in 'Grupos_mas_Playoff':
            self.grupos_mas_playoff.first().actualizar_fixture() 

        if self.modalidad in 'RondasClasificacion':
            self.rondas_clasificacion.first().actualizar_final()



    def iniciar_grupos(self):
        if self.modalidad in 'Grupos_mas_Playoff':
            self.grupos_mas_playoff.first().iniciar_grupos()

        if self.modalidad in 'RondasClasificacion':
            self.rondas_clasificacion.first().iniciar_grupos()

    def reiniciar_grupos(self):
        if self.modalidad in 'Grupos_mas_Playoff':
            self.grupos_mas_playoff.first().reiniciar_grupos()

        if self.modalidad in 'RondasClasificacion':
            self.rondas_clasificacion.first().reiniciar_grupos()

        if self.modalidad in 'Liga':
            self.grupos_mas_playoff.first().reiniciar_grupos()

    def actualizar_grupos(self):
        if self.modalidad in 'Grupos_mas_Playoff':
            self.grupos_mas_playoff.first().actualizar_grupos()    

        if self.modalidad in 'RondasClasificacion':
            self.rondas_clasificacion.first().actualizar_grupos()

        if self.modalidad in 'Liga':
            self.grupos_mas_playoff.first().actualizar_grupos() 

            if self.grupos_mas_playoff.first().fase_grupos_finalizada:
                self.finalizada =True
                self.save()

                #Se actualiza la tabla del deporte/copa conjunto. 
                self.olimpiada.actualizar_tablas_deportes()


    def iniciar_semifinales(self):
        if self.modalidad in 'RondasClasificacion':
            self.rondas_clasificacion.first().iniciar_semifinales()

    def reiniciar_semifinales(self):
        if self.modalidad in 'RondasClasificacion':
            self.rondas_clasificacion.first().reiniciar_semifinales()

    def actualizar_semifinales(self):       
        if self.modalidad in 'RondasClasificacion':
            self.rondas_clasificacion.first().actualizar_semifinales()

   
    def __str__(self):
        if self.limite_edad is 0:
            limite = 'Libre'
        else:
            limite = '+'+str(self.limite_edad)
            
        return (self.deporte.nombre + ' ' + self.nombre + ' ' + self.genero + ' (' + limite) + ') ' + str(self.olimpiada)


class Competencia (models.Model):
    #DATOS COMPETENCIA
    nombre = models.CharField(max_length=15, default='')
    fecha = models.DateTimeField(default=timezone.now)
    
    cancha = models.ForeignKey('Cancha', related_name='competencias', on_delete=models.CASCADE, blank=True, null=True)
    disciplina = models.ForeignKey('Disciplina', related_name='competencias', on_delete=models.CASCADE)
       
    #PARTICIPANTES
    equipos = models.ManyToManyField('Equipo', related_name='competencias', blank=True, verbose_name='Equipos')
    participantes = models.ManyToManyField('Persona', related_name='competencias', blank=True, verbose_name='Participantes')
    
    #Campos para MODALIDAD
    zona = models.CharField(max_length=20, blank=True, null=True) #Clasificatoria; Eliminatoria
    grupo = models.CharField(max_length=20, blank=True, null=True)
    proxima_competencia = models.ForeignKey('Competencia', blank=True, null=True, verbose_name='Proxima Competencia', on_delete=models.CASCADE)    
    
    #METODOS
    def __str__(self):
        return str(self.disciplina) + ' | ' + self.nombre + ' | '+ (self.fecha).strftime("%d/%m %H:%M") + ' | ' + str(self.zona) + ' | ' + str(self.grupo)


class Resultado (models.Model):
    disciplina  = models.ForeignKey('Disciplina', related_name='resultados', on_delete=models.CASCADE)
    competencia = models.ForeignKey('Competencia', related_name='resultados', on_delete=models.CASCADE)
    equipo = models.ForeignKey('Equipo', related_name='resultados', on_delete=models.CASCADE, blank=True, null=True)
    participante = models.ForeignKey('Persona', related_name='resultados', on_delete=models.CASCADE, blank=True, null=True)

    #PosicionManual
    posicion = models.IntegerField(default=-1)

    #Futbol, Hockey, Handball  
    goles_favor = models.IntegerField(default=-1)
    goles_contra = models.IntegerField(default=0)
    amarillas = models.IntegerField(default=0)
    rojas = models.IntegerField(default=0)
    verdes = models.IntegerField(default=0)
    azules = models.IntegerField(default=0)
    penales = models.IntegerField(default=0,verbose_name='Definicion por Penales (convertidos)')
    
    #Basquet, Bochas
    puntos_favor = models.IntegerField(default=-1)
    puntos_contra = models.IntegerField(default=0)
    
    #Voley, Tenis de Mesa
    puntos_favor_1set = models.IntegerField(default=-1)
    puntos_contra_1set = models.IntegerField(default=0)
    puntos_favor_2set = models.IntegerField(default=0)
    puntos_contra_2set = models.IntegerField(default=0)
    puntos_favor_3set = models.IntegerField(default=0)
    puntos_contra_3set = models.IntegerField(default=0)
    sets_favor = models.IntegerField(default=0)
    sets_contra = models.IntegerField(default=0)
    
    #Tenis, Pádel
    games_favor_1set = models.IntegerField(default=-1)
    games_contra_1set = models.IntegerField(default=0)
    games_favor_2set = models.IntegerField(default=0)
    games_contra_2set = models.IntegerField(default=0)
    games_favor_3set = models.IntegerField(default=0)
    games_contra_3set = models.IntegerField(default=0)

    #Atletismo, Natación
    tiempo = models.FloatField(default=-1)
    distancia = models.FloatField(default=-1)

    #Pesca
    cantidad = models.IntegerField(default=-1)
    peso = models.FloatField(default=-1)

    presentado = models.BooleanField (default=True)

    #METODOS
    def __str__(self):
        if self.disciplina.tipo in 'Grupal':
            return str(self.disciplina.olimpiada.nombre) + ' | ' + str(self.disciplina) + ' | ' + str(self.competencia.nombre) + ' | ' + str(self.equipo.nombre)  
        else:
            return str(self.disciplina.olimpiada.nombre) + ' | ' + str(self.disciplina) + ' | ' + str(self.competencia.nombre) + ' | ' + str(self.participante.nombre) 

# for resultado in [resultado0, resultado1]:
#     print ('por evaluar resultados cargados')
#     if (resultado.posicion == -1 
#         and resultado.puntos_favor == -1
#         and resultado.puntos_favor_1set == -1
#         and resultado.games_favor_1set == -1
#         and resultado.tiempo == -1
#         and resultado.distancia == -1
#         and resultado.cantidad == -1
#         and resultado.peso == -1):
        
#         self.fase_grupos_finalizada = False
#         self.save()
#         print (competencia.nombre+': resultado no cargado, no se puede evaluar.')    
#         continue


# ================================================================================ 
# Tablas de Posiciones
# ================================================================================
class Posicion_disciplina (models.Model):
    disciplina  = models.ForeignKey('Disciplina', related_name='posiciones', on_delete=models.CASCADE)
    equipo = models.ForeignKey('Equipo', on_delete=models.CASCADE, blank=True, null=True)
    participante = models.ForeignKey('Persona', related_name='posiciones', on_delete=models.CASCADE, blank=True, null=True)

    #Generales
    posicion = models.IntegerField(default=0)
    puntos = models.IntegerField(default=0)
    partidos_ganados = models.IntegerField(default=0)
    partidos_perdidos = models.IntegerField(default=0)
    partidos_empatados = models.IntegerField(default=0)

    #Futbol, Hockey, Handball  
    goles_favor = models.IntegerField(default=0)
    goles_contra = models.IntegerField(default=0)
    goles_dif = models.IntegerField(default=0)
    amarillas = models.IntegerField(default=0)
    rojas = models.IntegerField(default=0)
    verdes = models.IntegerField(default=0)
    azules = models.IntegerField(default=0)
    
    #Basquet/Voley/Tenis/Pádel/Bochas
    puntos_favor = models.IntegerField(default=0)
    puntos_contra = models.IntegerField(default=0)
    puntos_dif = models.IntegerField(default=0)
    games_favor = models.IntegerField(default=0)
    games_contra = models.IntegerField(default=0)
    games_dif = models.IntegerField(default=0)
    sets_favor = models.IntegerField(default=0)
    sets_contra = models.IntegerField(default=0)
    sets_dif = models.IntegerField(default=0)

    #Atletismo/Natación
    tiempo = models.FloatField(default=0.0)
    distancia = models.FloatField(default=0.0)

    #Pesca
    peso = models.FloatField(default=0.0)
    cantidad = models.IntegerField(default=0)

    #Campos para Modalidad
    zona = models.CharField(max_length=20, blank=True, null=True) #Clasificatoria; Eliminatoria
    grupo = models.CharField(max_length=20, blank=True, null=True)


    #METODOS
    def reset_values (self):
        self.posicion = 0
        self.puntos = 0
        self.partidos_ganados = 0
        self.partidos_perdidos = 0
        self.partidos_dif = 0
        self.goles_favor = 0
        self.goles_contra = 0
        self.goles_dif = 0
        self.amarillas = 0
        self.rojas = 0
        self.verdes = 0
        self.azules = 0       
        self.puntos_favor = 0
        self.puntos_contra = 0
        self.puntos_dif = 0
        self.games_favor = 0
        self.games_contra = 0
        self.games_dif = 0
        self.sets_favor = 0
        self.sets_contra = 0
        self.sets_dif = 0
        self.tiempo = 0
        self.distancia = 0
        self.peso = 0
        self.cantidad = 0
        self.save()

    def __str__(self):
        if self.disciplina.tipo in 'Grupal':
            return str(self.disciplina.olimpiada.nombre) + ' | ' + str(self.disciplina) + ' | ' + str(self.equipo.nombre)  + ' | ' + str(self.zona) + ' | ' + str(self.grupo)    
        else:
            return str(self.disciplina.olimpiada.nombre) + ' | ' + str(self.disciplina) + ' | ' + str(self.participante.nombre)  + ' | ' + str(self.zona) + ' | ' + str(self.grupo)



class Posicion_deporte (models.Model):
    olimpiada = models.ForeignKey('Olimpiada', on_delete=models.CASCADE)
    deporte = models.ForeignKey('Deporte', related_name='posiciones', on_delete=models.CASCADE)
    delegacion = models.ForeignKey('Delegacion', related_name='posiciones_deportes', on_delete=models.CASCADE) 
    
    posicion = models.IntegerField(default=0)
    puntos = models.FloatField(default=0.0)

    def reset_values (self):
        self.posicion = 0
        self.puntos = 0
        self.save()

    def __str__(self):
        return str(self.olimpiada.nombre) + ' | ' + str(self.deporte.nombre) + ' | ' + str(self.delegacion.nombre)  


class Posicion_olimpiada (models.Model):
    olimpiada = models.ForeignKey('Olimpiada', on_delete=models.CASCADE)
    delegacion = models.ForeignKey('Delegacion', related_name='posiciones_olimpiadas', on_delete=models.CASCADE)

    posicion = models.IntegerField(default=0)
    puntos = models.IntegerField(default=0)

    posicion_rendimiento = models.IntegerField(default=0)
    rendimiento = models.FloatField(default=0.0)

    def reset_values (self):
        self.posicion = 0
        self.puntos = 0
        self.posicion_rendimiento = 0
        self.rendimiento = 0
        self.save()

    def __str__(self):
        return str(self.olimpiada.nombre) + ' | ' + str(self.delegacion.nombre)


class Playoff (models.Model):
    disciplina = models.ForeignKey('Disciplina', related_name='playoff', on_delete=models.CASCADE)
   
    equipos = models.ManyToManyField('Equipo', blank=True)
    participantes = models.ManyToManyField('Persona', blank=True)
    min_participantes_playoff = models.IntegerField(default=8)
    min_participantes_competencia = models.IntegerField(default=2)

    def __str__(self):
        return str(self.disciplina) + ' | Playoff ' + str(self.min_participantes_playoff)
    
    
    def crear_fixture (self,integrantes):
        #Cargamos equipos/participantes y chequeamos si están todos los necesarios
        if self.disciplina.tipo in 'Grupal':
            self.equipos.set( integrantes.all().order_by('nombre') )
            if self.equipos.count() != self.min_participantes_playoff:
                raise ValueError('Numero de Equipos Incorrecto')
            self.save()
        else:
            self.participantes.set ( integrantes.all().order_by('nombre') )
            if self.participantes.count() != self.min_participantes_playoff:
                raise ValueError('Numero de Competidores Incorrecto')
            self.save()

       
        #Inicializamos las Comptencias
        if self.min_participantes_playoff == 4:
            if self.disciplina.tipo in 'Grupal':
                #Creando Competencias con Equipos  
                S1 = Competencia.objects.create(nombre='S1', disciplina=self.disciplina, zona='Eliminatoria')
                S1.equipos.add(self.equipos.all()[0],self.equipos.all()[1]); S1.save();
                S2 = Competencia.objects.create(nombre='S2', disciplina=self.disciplina, zona='Eliminatoria')
                S2.equipos.add(self.equipos.all()[2],self.equipos.all()[3]); S2.save();       

            else:            
                #Creando Competencias con Competidores Individuales
                S1 = Competencia.objects.create(nombre='S1', disciplina=self.disciplina, zona='Eliminatoria')
                S1.participantes.add(self.participantes.all()[0],self.participantes.all()[1]); S1.save();            
                S2 = Competencia.objects.create(nombre='S2', disciplina=self.disciplina, zona='Eliminatoria')
                S2.participantes.add(self.participantes.all()[2],self.participantes.all()[3]); S2.save();          
            
            Competencia.objects.create(nombre='3P', disciplina=self.disciplina, zona='Eliminatoria')        
            Competencia.objects.create(nombre='Final', disciplina=self.disciplina, zona='Eliminatoria')


        if self.min_participantes_playoff == 8:
            if self.disciplina.tipo in 'Grupal':
                #Creando Competencias con Equipos  
                C1 = Competencia.objects.create(nombre='C1', disciplina=self.disciplina, zona='Eliminatoria')
                C1.equipos.add(self.equipos.all()[0],self.equipos.all()[1]); C1.save();
                C2 = Competencia.objects.create(nombre='C2', disciplina=self.disciplina, zona='Eliminatoria')
                C2.equipos.add(self.equipos.all()[2],self.equipos.all()[3]); C2.save();       
                C3 = Competencia.objects.create(nombre='C3', disciplina=self.disciplina, zona='Eliminatoria')
                C3.equipos.add(self.equipos.all()[4],self.equipos.all()[5]); C3.save();            
                C4 = Competencia.objects.create(nombre='C4', disciplina=self.disciplina, zona='Eliminatoria')
                C4.equipos.add(self.equipos.all()[6],self.equipos.all()[7]); C4.save();
                
            
            else:            
                #Creando Competencias con Competidores Individuales
                C1 = Competencia.objects.create(nombre='C1', disciplina=self.disciplina, zona='Eliminatoria')
                C1.participantes.add(self.participantes.all()[0],self.participantes.all()[1]); C1.save();            
                C2 = Competencia.objects.create(nombre='C2', disciplina=self.disciplina, zona='Eliminatoria')
                C2.participantes.add(self.participantes.all()[2],self.participantes.all()[3]); C2.save();          
                C3 = Competencia.objects.create(nombre='C3', disciplina=self.disciplina, zona='Eliminatoria')
                C3.participantes.add(self.participantes.all()[4],self.participantes.all()[5]); C3.save();            
                C4 = Competencia.objects.create(nombre='C4', disciplina=self.disciplina, zona='Eliminatoria')
                C4.participantes.add(self.participantes.all()[6],self.participantes.all()[7]); C4.save();
                

            S1 = Competencia.objects.create(nombre='S1', disciplina=self.disciplina, zona='Eliminatoria')        
            C1.proxima_competencia=S1; C1.save()
            C2.proxima_competencia=S1; C2.save()

            S2 = Competencia.objects.create(nombre='S2', disciplina=self.disciplina, zona='Eliminatoria')              
            C3.proxima_competencia=S2; C3.save()
            C4.proxima_competencia=S2; C4.save()
            
            Competencia.objects.create(nombre='3P', disciplina=self.disciplina, zona='Eliminatoria')        
            Competencia.objects.create(nombre='Final', disciplina=self.disciplina, zona='Eliminatoria')


        if self.min_participantes_playoff == 16:
            if self.disciplina.tipo in 'Grupal':
                #Creando Competencias con Equipos  
                O1 = Competencia.objects.create(nombre='O1', disciplina=self.disciplina, zona='Eliminatoria')
                O1.equipos.add(self.equipos.all()[0],self.equipos.all()[1]); O1.save();
                O2 = Competencia.objects.create(nombre='O2', disciplina=self.disciplina, zona='Eliminatoria')
                O2.equipos.add(self.equipos.all()[2],self.equipos.all()[3]); O2.save();       
                O3 = Competencia.objects.create(nombre='O3', disciplina=self.disciplina, zona='Eliminatoria')
                O3.equipos.add(self.equipos.all()[4],self.equipos.all()[5]); O3.save();            
                O4 = Competencia.objects.create(nombre='O4', disciplina=self.disciplina, zona='Eliminatoria')
                O4.equipos.add(self.equipos.all()[6],self.equipos.all()[7]); O4.save();
                O5 = Competencia.objects.create(nombre='O5', disciplina=self.disciplina, zona='Eliminatoria')
                O5.equipos.add(self.equipos.all()[8],self.equipos.all()[9]); O5.save();
                O6 = Competencia.objects.create(nombre='O6', disciplina=self.disciplina, zona='Eliminatoria')
                O6.equipos.add(self.equipos.all()[10],self.equipos.all()[11]); O6.save();       
                O7 = Competencia.objects.create(nombre='O7', disciplina=self.disciplina, zona='Eliminatoria')
                O7.equipos.add(self.equipos.all()[12],self.equipos.all()[13]); O7.save();            
                O8 = Competencia.objects.create(nombre='O8', disciplina=self.disciplina, zona='Eliminatoria')
                O8.equipos.add(self.equipos.all()[14],self.equipos.all()[15]); O8.save();
            
            else:            
                #Creando Competencias con Competidores Individuales
                O1 = Competencia.objects.create(nombre='O1', disciplina=self.disciplina, zona='Eliminatoria')
                O1.participantes.add(self.participantes.all()[0],self.participantes.all()[1]); O1.save();            
                O2 = Competencia.objects.create(nombre='O2', disciplina=self.disciplina, zona='Eliminatoria')
                O2.participantes.add(self.participantes.all()[2],self.participantes.all()[3]); O2.save();          
                O3 = Competencia.objects.create(nombre='O3', disciplina=self.disciplina, zona='Eliminatoria')
                O3.participantes.add(self.participantes.all()[4],self.participantes.all()[5]); O3.save();            
                O4 = Competencia.objects.create(nombre='O4', disciplina=self.disciplina, zona='Eliminatoria')
                O4.participantes.add(self.participantes.all()[6],self.participantes.all()[7]); O4.save();
                O5 = Competencia.objects.create(nombre='O5', disciplina=self.disciplina, zona='Eliminatoria')
                O5.participantes.add(self.participantes.all()[8],self.participantes.all()[9]); O5.save();            
                O6 = Competencia.objects.create(nombre='O6', disciplina=self.disciplina, zona='Eliminatoria')
                O6.participantes.add(self.participantes.all()[10],self.participantes.all()[11]); O6.save();          
                O7 = Competencia.objects.create(nombre='O7', disciplina=self.disciplina, zona='Eliminatoria')
                O7.participantes.add(self.participantes.all()[12],self.participantes.all()[13]); O7.save();            
                O8 = Competencia.objects.create(nombre='O8', disciplina=self.disciplina, zona='Eliminatoria')
                O8.participantes.add(self.participantes.all()[14],self.participantes.all()[15]); O8.save();

            C1 = Competencia.objects.create(nombre='C1', disciplina=self.disciplina, zona='Eliminatoria')        
            O1.proxima_competencia=C1; O1.save()
            O2.proxima_competencia=C1; O2.save()

            C2 = Competencia.objects.create(nombre='C2', disciplina=self.disciplina, zona='Eliminatoria')        
            O3.proxima_competencia=C2; O3.save()
            O4.proxima_competencia=C2; O4.save()

            C3 = Competencia.objects.create(nombre='C3', disciplina=self.disciplina, zona='Eliminatoria')        
            O5.proxima_competencia=C3; O5.save()
            O6.proxima_competencia=C3; O6.save()

            C4 = Competencia.objects.create(nombre='C4', disciplina=self.disciplina, zona='Eliminatoria')        
            O7.proxima_competencia=C4; O7.save()
            O8.proxima_competencia=C4; O8.save()

            S1 = Competencia.objects.create(nombre='S1', disciplina=self.disciplina, zona='Eliminatoria')        
            C1.proxima_competencia=S1; C1.save()
            C2.proxima_competencia=S1; C2.save()

            S2 = Competencia.objects.create(nombre='S2', disciplina=self.disciplina, zona='Eliminatoria')              
            C3.proxima_competencia=S2; C3.save()
            C4.proxima_competencia=S2; C4.save()
            
            Competencia.objects.create(nombre='3P', disciplina=self.disciplina, zona='Eliminatoria')        
            Competencia.objects.create(nombre='Final', disciplina=self.disciplina, zona='Eliminatoria')


        #Inicializamos las entradas de la tabla de posiciones
        if self.disciplina.tipo in 'Grupal':
            for equipo in self.equipos.all():
                Posicion_disciplina.objects.create(disciplina=self.disciplina, equipo=equipo, zona='Eliminatoria')
        else:
            for participante in self.participantes.all():
                Posicion_disciplina.objects.create(disciplina=self.disciplina, participante=participante, zona='Eliminatoria')

        #Actualizamos Flags
        self.disciplina.fixture_creado = True
        self.disciplina.save()

    
    def actualizar_fixture (self):
        #Cargamos equipos/participantes y chequeamos si están todos los necesarios
        if self.disciplina.tipo in 'Grupal':
            print (self.equipos.count())
            print (self.min_participantes_playoff)
            if self.equipos.count() != self.min_participantes_playoff:
                raise ValueError('Numero de Equipos Incorrecto')
        else:
            if self.participantes.count() != self.min_participantes_playoff:
                raise ValueError('Numero de Competidores Incorrecto')

        #Inicializamos/Reiniciamos las entradas de la tabla de posiciones
        if self.disciplina.tipo in 'Grupal':
            for equipo in self.equipos.all():
                if not Posicion_disciplina.objects.filter(disciplina=self.disciplina, equipo=equipo, zona='Eliminatoria').exists():
                    Posicion_disciplina.objects.create(disciplina=self.disciplina, equipo=equipo, zona='Eliminatoria')
                else:
                    posicion = Posicion_disciplina.objects.get(disciplina=self.disciplina, equipo=equipo, zona='Eliminatoria')
                    posicion.reset_values()
                    posicion.save()                   
        else:
            for participante in self.participantes.all():
                if not Posicion_disciplina.objects.filter(disciplina=self.disciplina, participante=participante, zona='Eliminatoria').exists():
                    Posicion_disciplina.objects.create(disciplina=self.disciplina, participante=participante, zona='Eliminatoria')
                else:
                    posicion = Posicion_disciplina.objects.get(disciplina=self.disciplina, participante=participante, zona='Eliminatoria')
                    posicion.reset_values()
                    posicion.save()   
               
        #Reiniciamos equipos/participantes asignados a competencias posteriores
        if self.min_participantes_playoff == 4:
            for nombre_competencia in ['3P','Final']:
                Competencia.objects.get(nombre=nombre_competencia, disciplina=self.disciplina, zona='Eliminatoria').equipos.clear()
                Competencia.objects.get(nombre=nombre_competencia, disciplina=self.disciplina, zona='Eliminatoria').participantes.clear()

        if self.min_participantes_playoff == 8:
            for nombre_competencia in ['S1','S2','3P','Final']:
                Competencia.objects.get(nombre=nombre_competencia, disciplina=self.disciplina, zona='Eliminatoria').equipos.clear()
                Competencia.objects.get(nombre=nombre_competencia, disciplina=self.disciplina, zona='Eliminatoria').participantes.clear()

        if self.min_participantes_playoff == 16:
            for nombre_competencia in ['C1','C2','C3','C4','S1','S2','3P','Final']:
                Competencia.objects.get(nombre=nombre_competencia, disciplina=self.disciplina, zona='Eliminatoria').equipos.clear()
                Competencia.objects.get(nombre=nombre_competencia, disciplina=self.disciplina, zona='Eliminatoria').participantes.clear()

        #Evaluamos los resultados de cada competencia
        for competencia in Competencia.objects.filter(disciplina=self.disciplina, zona='Eliminatoria').order_by('pk'):           
            
            #Lógica Deportes Grupales
            if competencia.disciplina.tipo in 'Grupal':                
                #Eliminamos resultados de equipos que no forman parte de la competencia
                for resultado in competencia.resultados.all():
                    borrar_resultado=True
                    for equipo in competencia.equipos.all():
                        if (resultado.equipo == equipo):
                            borrar_resultado=False
                    if borrar_resultado:
                        print ('Resultado incorrecto de disciplina eliminado: ' + resultado.equipo.nombre)
                        resultado.delete()
                

                #Chequeamos que la cantidad de resultados cargados sea igual al numero de equipos
                if ( (len(competencia.resultados.all()) == self.min_participantes_competencia) and 
                     (len(competencia.equipos.all()) == self.min_participantes_competencia) ):  
                    resultado0 = competencia.resultados.all()[0]
                    resultado1 = competencia.resultados.all()[1]
                    resultado0_dict = Resultado.objects.filter(pk=resultado0.pk).values().first()
                    resultado1_dict = Resultado.objects.filter(pk=resultado1.pk).values().first()

                else:         #resultados o equipos faltantes
                    print (competencia.nombre+': resultados faltantes, no se puede evaluar.')    
                    continue


                #Chequeamos que los resultados hayan sido cargados 
                resultado_faltante = False
                for resultado in competencia.resultados.all():
                    if (resultado.posicion == -1 
                        and resultado.puntos_favor == -1
                        and resultado.puntos_favor_1set == -1
                        and resultado.games_favor_1set == -1
                        and resultado.tiempo == -1
                        and resultado.distancia == -1
                        and resultado.cantidad == -1
                        and resultado.peso == -1
                        and resultado.presentado is True):
                        print ('Resultado no cargado, no se puede evaluar.')     
                        resultado_faltante = True
                        break
                if resultado_faltante:
                    continue


                #Definimos ganador/perdedor de la competencia segun las politicas de la disciplina    
                #Deportes Grupales c/Resultados Finales
                if self.disciplina.deporte.nombre in politicas.deportes_grupales_resultados_finales:                       
                    ganador = None
                    perdedor = None
                    for valor_a_comparar in politicas.politica_playoff_segun_disciplina[self.disciplina.deporte.nombre]:                       
                        #comparamos los resultados para determinar ganador / perdedor
                        if ((resultado0_dict[valor_a_comparar[0]] > resultado1_dict[valor_a_comparar[0]]) and (valor_a_comparar[1] in 'mas')) or \
                           ((resultado0_dict[valor_a_comparar[0]] < resultado1_dict[valor_a_comparar[0]]) and (valor_a_comparar[1] in 'menos')):                                   
                           ganador = resultado0.equipo
                           perdedor = resultado1.equipo
                           break  #cortamos el for para evaluar esta y luego pasar a analizar la siguiente competencia.

                        elif ( ((resultado0_dict[valor_a_comparar[0]] < resultado1_dict[valor_a_comparar[0]]) and (valor_a_comparar[1] in 'mas')) or \
                           ((resultado0_dict[valor_a_comparar[0]] > resultado1_dict[valor_a_comparar[0]]) and (valor_a_comparar[1] in 'menos')) ): 
                           ganador = resultado1.equipo
                           perdedor = resultado0.equipo
                           break  #cortamos el for para evaluar esta y luego pasar a analizar la siguiente competencia.

                        else: #ambos resultados son iguales, comparamos el siguiente campo.
                            pass  

                #Deportes Grupales c/Resultados Parciales
                if self.disciplina.deporte.nombre in politicas.deportes_grupales_resultados_parciales:  
                    #Contamos los parciales ganados (ej: sets ganados) segun las politicas de la disciplina
                    resultado0.sets_favor=0
                    resultado1.sets_favor=0
                    resultado0.sets_contra=0
                    resultado1.sets_contra=0

                    for valor_a_comparar in politicas.politica_playoff_segun_disciplina[self.disciplina.deporte.nombre]:                                
                        if ((resultado0_dict[valor_a_comparar[0]] > resultado1_dict[valor_a_comparar[0]]) and (valor_a_comparar[1] in 'mas')) or \
                           ((resultado0_dict[valor_a_comparar[0]] < resultado1_dict[valor_a_comparar[0]]) and (valor_a_comparar[1] in 'menos')):                                   
                               resultado0.sets_favor=resultado0.sets_favor+1
                               resultado1.sets_contra=resultado1.sets_contra+1
                               continue
                          
                        elif ( ((resultado0_dict[valor_a_comparar[0]] < resultado1_dict[valor_a_comparar[0]]) and (valor_a_comparar[1] in 'mas')) or \
                               ((resultado0_dict[valor_a_comparar[0]] > resultado1_dict[valor_a_comparar[0]]) and (valor_a_comparar[1] in 'menos')) ): 
                                   resultado1.sets_favor=resultado1.sets_favor+1
                                   resultado0.sets_contra=resultado0.sets_contra+1
                                   continue
                                
                        else:   #ambos resultados son iguales, comparamos el siguiente parcial.
                            print ('set empate')
                            print (resultado0.sets_favor )
                            print (resultado0.sets_contra )
                            print (resultado1.sets_favor )
                            print (resultado1.sets_contra )
                            pass
                    
                    resultado0.save()
                    resultado1.save()

                    #Definimos ganador/perdedor de la competencia segun los sets ganados
                    ganador = None
                    perdedor = None
                    if resultado0.sets_favor > resultado1.sets_favor: #ganador equipo 0
                        ganador = resultado0.equipo
                        perdedor = resultado1.equipo
                    else:
                        ganador = resultado1.equipo
                        perdedor = resultado0.equipo


                #Actualizamos el fixture en funcion del ganador/perdedor
                if ganador is not None:
                    if competencia.nombre not in ['S1','S2','3P','Final']:
                        competencia.proxima_competencia.equipos.add(ganador)

                        #equipo ganador suma 3 puntos
                        posicion = Posicion_disciplina.objects.get(disciplina=self.disciplina, equipo=ganador, zona='Eliminatoria')
                        posicion.puntos = posicion.puntos + 3
                        posicion.save()
                    
                    elif competencia.nombre in ['S1','S2']:
                        final = Competencia.objects.get(disciplina=self.disciplina, nombre='Final', zona='Eliminatoria')
                        final.equipos.add(ganador)
                        _3p = Competencia.objects.get(disciplina=self.disciplina, nombre='3P', zona='Eliminatoria')
                        _3p.equipos.add(perdedor)

                        #equipo ganador suma 3 puntos
                        posicion = Posicion_disciplina.objects.get(disciplina=self.disciplina, equipo=ganador, zona='Eliminatoria')
                        posicion.puntos = posicion.puntos + 3
                        posicion.save()
                    
                    elif competencia.nombre in ['3P']:                   
                        #equipo ganador suma 2 puntos / perdedor suma 1 punto
                        posicion = Posicion_disciplina.objects.get(disciplina=self.disciplina, equipo=ganador, zona='Eliminatoria')
                        posicion.puntos = posicion.puntos + 2
                        posicion.save()
                        posicion = Posicion_disciplina.objects.get(disciplina=self.disciplina, equipo=perdedor, zona='Eliminatoria')
                        posicion.puntos = posicion.puntos + 1
                        posicion.save()

                    elif competencia.nombre in ['Final']:
                        #equipo ganador suma 1 punto / perdedor suma 0 puntos
                        posicion = Posicion_disciplina.objects.get(disciplina=self.disciplina, equipo=ganador, zona='Eliminatoria')
                        posicion.puntos = posicion.puntos + 1
                        posicion.save()
                        
                        self.disciplina.finalizada = True #las posiciones del deporte p/delegación solo se
                                                          #computarán sí la disciplina se finalizó
                        self.disciplina.save()
                    else:
                        pass


            #Deportes Individuales
            else:
                #Eliminamos resultados de personas que no forman parte de la competencia
                for resultado in competencia.resultados.all():
                    borrar_resultado=True
                    for participante in competencia.participantes.all():
                        if (resultado.participante == participante):
                            borrar_resultado=False
                    if borrar_resultado:
                        print ('Resultado incorrecto de disciplina eliminado: ' + resultado.participante.apellido +', ' +resultado.participante.nombre)
                        resultado.delete()
                

                #Chequeamos que la cantidad de resultados cargados sea igual al numero de participantes
                if ( (len(competencia.resultados.all()) == self.min_participantes_competencia) and 
                     (len(competencia.participantes.all()) == self.min_participantes_competencia) ):  
                    resultado0 = competencia.resultados.all()[0]
                    resultado1 = competencia.resultados.all()[1]
                    resultado0_dict = Resultado.objects.filter(pk=resultado0.pk).values().first()
                    resultado1_dict = Resultado.objects.filter(pk=resultado1.pk).values().first()
                else:         #resultados o participantes faltantes
                    print (competencia.nombre+': resultados faltantes, no se puede evaluar.')    
                    continue

                #Chequeamos que los resultados hayan sido cargados 
                resultado_faltante = False
                for resultado in competencia.resultados.all():
                    if (resultado.posicion == -1 
                        and resultado.puntos_favor == -1
                        and resultado.puntos_favor_1set == -1
                        and resultado.games_favor_1set == -1
                        and resultado.tiempo == -1
                        and resultado.distancia == -1
                        and resultado.cantidad == -1
                        and resultado.peso == -1
                        and resultado.presentado is True):
                        print ('Resultado no cargado, no se puede evaluar.')     
                        resultado_faltante = True
                        break
                if resultado_faltante:
                    continue


                #Definimos ganador/perdedor de la competencia segun las politicas de la disciplina                    
                #Deportes Individuales c/Resultados Finales                    
                if self.disciplina.deporte.nombre in politicas.deportes_individuales_resultados_finales:
                    ganador = None
                    perdedor = None
                    for valor_a_comparar in politicas.politica_playoff_segun_disciplina[self.disciplina.deporte.nombre]:
                        # print ('Comparando '+valor_a_comparar[0])                
                        
                        #comparamos los resultados para determinar ganador / perdedor
                        if ((resultado0_dict[valor_a_comparar[0]] > resultado1_dict[valor_a_comparar[0]]) and (valor_a_comparar[1] in 'mas')) or \
                           ((resultado0_dict[valor_a_comparar[0]] < resultado1_dict[valor_a_comparar[0]]) and (valor_a_comparar[1] in 'menos')):                                   
                           ganador = resultado0.participante
                           perdedor = resultado1.participante
                           break  #cortamos el for para evaluar esta y luego pasar a analizar la siguiente competencia.

                        elif ( ((resultado0_dict[valor_a_comparar[0]] < resultado1_dict[valor_a_comparar[0]]) and (valor_a_comparar[1] in 'mas')) or \
                           ((resultado0_dict[valor_a_comparar[0]] > resultado1_dict[valor_a_comparar[0]]) and (valor_a_comparar[1] in 'menos')) ): 
                           ganador = resultado1.participante
                           perdedor = resultado0.participante
                           break  #cortamos el for para evaluar esta y luego pasar a analizar la siguiente competencia.

                        else: #ambos resultados son iguales, comparamos el siguiente campo.
                            pass  

                #Deportes Individuales c/Resultados Parciales                  
                if self.disciplina.deporte.nombre in politicas.deportes_individuales_resultados_parciales: 
                    #Contamos los parciales ganados (ej: sets ganados) segun las politicas de la disciplina
                    resultado0.sets_favor=0
                    resultado1.sets_favor=0
                    resultado0.sets_contra=0
                    resultado1.sets_contra=0

                    for valor_a_comparar in politicas.politica_playoff_segun_disciplina[self.disciplina.deporte.nombre]:
                        if ((resultado0_dict[valor_a_comparar[0]] > resultado1_dict[valor_a_comparar[0]]) and (valor_a_comparar[1] in 'mas')) or \
                           ((resultado0_dict[valor_a_comparar[0]] < resultado1_dict[valor_a_comparar[0]]) and (valor_a_comparar[1] in 'menos')):                                   
                               resultado0.sets_favor=resultado0.sets_favor+1
                               resultado1.sets_contra=resultado1.sets_contra+1
                               continue
                          
                        elif ( ((resultado0_dict[valor_a_comparar[0]] < resultado1_dict[valor_a_comparar[0]]) and (valor_a_comparar[1] in 'mas')) or \
                               ((resultado0_dict[valor_a_comparar[0]] > resultado1_dict[valor_a_comparar[0]]) and (valor_a_comparar[1] in 'menos')) ): 
                                   resultado1.sets_favor=resultado1.sets_favor+1
                                   resultado0.sets_contra=resultado0.sets_contra+1
                                   continue
                                
                        else:   #ambos resultados son iguales, comparamos el siguiente parcial.
                            pass

                    resultado0.save()
                    resultado1.save()

                    #Definimos ganador/perdedor de la competencia segun los sets ganados
                    ganador = None
                    perdedor = None
                    if resultado0.sets_favor > resultado1.sets_favor: #ganador equipo 0
                        ganador = resultado0.participante
                        perdedor = resultado1.participante
                    else:
                        ganador = resultado1.participante
                        perdedor = resultado0.participante



                #Actualizamos el fixture en funcion del ganador/perdedor
                if ganador is not None:
                    if competencia.nombre not in ['S1','S2','3P','Final']:
                        competencia.proxima_competencia.participantes.add(ganador)

                        #equipo ganador suma 3 puntos
                        posicion = Posicion_disciplina.objects.get(disciplina=self.disciplina, participante=ganador, zona='Eliminatoria')
                        posicion.puntos = posicion.puntos + 3
                        posicion.save()
                    
                    elif competencia.nombre in ['S1','S2']:
                        final = Competencia.objects.get(disciplina=self.disciplina, nombre='Final', zona='Eliminatoria')
                        final.participantes.add(ganador)
                        _3p = Competencia.objects.get(disciplina=self.disciplina, nombre='3P', zona='Eliminatoria')
                        _3p.participantes.add(perdedor)

                        #equipo ganador suma 3 puntos
                        posicion = Posicion_disciplina.objects.get(disciplina=self.disciplina, participante=ganador, zona='Eliminatoria')
                        posicion.puntos = posicion.puntos + 3
                        posicion.save()
                    
                    elif competencia.nombre in ['3P']:                   
                        #equipo ganador suma 2 puntos / perdedor suma 1 punto
                        posicion = Posicion_disciplina.objects.get(disciplina=self.disciplina, participante=ganador, zona='Eliminatoria')
                        posicion.puntos = posicion.puntos + 2
                        posicion.save()
                        posicion = Posicion_disciplina.objects.get(disciplina=self.disciplina, participante=perdedor, zona='Eliminatoria')
                        posicion.puntos = posicion.puntos + 1
                        posicion.save()

                    elif competencia.nombre in ['Final']:
                        #equipo ganador suma 1 punto / perdedor suma 0 puntos
                        posicion = Posicion_disciplina.objects.get(disciplina=self.disciplina, participante=ganador, zona='Eliminatoria')
                        posicion.puntos = posicion.puntos + 1
                        posicion.save()
                        
                        self.disciplina.finalizada = True #las posiciones del deporte p/delegación solo se
                                               #computarán sí la disciplina se finalizó
                        self.disciplina.save()
                    else:
                        pass                                               
                
            
            #Actualizamos los campos de posiciones_disciplinas
            for resultado in [resultado0,resultado1]:
                if resultado.competencia.disciplina.tipo in 'Grupal':
                    posicion = Posicion_disciplina.objects.get(disciplina=self.disciplina, equipo=resultado.equipo, zona='Eliminatoria')
                else:
                    posicion = Posicion_disciplina.objects.get(disciplina=self.disciplina, participante=resultado.participante, zona='Eliminatoria')
      
                #Futbol, Hockey, Handball  
                if self.disciplina.deporte.nombre in ['Fútbol','Hockey','Handball']:
                    posicion.goles_favor = posicion.goles_favor + resultado.goles_favor
                    posicion.goles_contra = posicion.goles_contra + resultado.goles_contra
                    posicion.goles_dif = posicion.goles_favor - posicion.goles_contra 
                    posicion.amarillas = posicion.amarillas + resultado.amarillas
                    posicion.rojas = posicion.rojas + resultado.rojas
                    posicion.verdes = posicion.verdes + resultado.verdes
                    posicion.azules = posicion.azules + resultado.azules
                    posicion.save()
                
                #Basquet/Bochas
                if self.disciplina.deporte.nombre in ['Basquet','Bochas']:
                    posicion.puntos_favor = posicion.puntos_favor + resultado.puntos_favor
                    posicion.puntos_contra = posicion.puntos_contra + resultado.puntos_contra
                    posicion.puntos_dif = posicion.puntos_favor - posicion.puntos_contra
                    posicion.save()
                
                if (self.disciplina.deporte.nombre == 'Tenis' or 
                    self.disciplina.deporte.nombre in ['Pádel']):
                    posicion.games_favor = (posicion.games_favor + 
                                            resultado.games_favor_1set+
                                            resultado.games_favor_2set+
                                            resultado.games_favor_3set)
                    posicion.games_contra = (posicion.games_contra + 
                                             resultado.games_contra_1set+
                                             resultado.games_contra_2set+
                                             resultado.games_contra_3set)
                    posicion.games_dif = posicion.games_favor - posicion.games_contra

                    posicion.sets_favor = posicion.sets_favor + resultado.sets_favor
                    posicion.sets_contra = posicion.sets_contra + resultado.sets_contra    
                    posicion.sets_dif = posicion.sets_favor - posicion.sets_contra

                    posicion.save()

                if (self.disciplina.deporte.nombre in ['Voley'] or 
                    self.disciplina.deporte.nombre == 'Tenis de Mesa'):
                    posicion.puntos_favor = ( posicion.puntos_favor + 
                                              resultado.puntos_favor_1set +
                                              resultado.puntos_favor_2set +
                                              resultado.puntos_favor_3set )

                    posicion.puntos_contra = (posicion.puntos_contra + 
                                              resultado.puntos_contra_1set +
                                              resultado.puntos_contra_2set +
                                              resultado.puntos_contra_3set )

                    posicion.puntos_dif = posicion.puntos_favor - posicion.puntos_contra

                    posicion.sets_favor = posicion.sets_favor + resultado.sets_favor
                    posicion.sets_contra = posicion.sets_contra + resultado.sets_contra    
                    posicion.sets_dif = posicion.sets_favor - posicion.sets_contra

                    posicion.save()

                if (self.disciplina.deporte.nombre in 
                    ['Natación','Ciclismo','Vela','Caminata/Carrera','Atletismo','Pesca']):
                    posicion.tiempo = posicion.tiempo + resultado.tiempo
                    posicion.distancia = posicion.distancia + resultado.distancia
                    posicion.save()
                                          
        self.actualizar_tabla()            
            

        
    def actualizar_tabla (self): 
        #Ordena Tabla de Posiciones de Playoff
        posiciones = Posicion_disciplina.objects.filter(disciplina=self.disciplina, zona='Eliminatoria').order_by('-puntos')
        ordenar_posiciones_disciplina (posiciones)

        #Actualizamos la Tabla del Deporte / CopaConjunto. 
        if self.disciplina.finalizada:
            self.disciplina.olimpiada.actualizar_tablas_deportes()


    def reiniciar_fixture(self):
        #Eliminamos todas las competencias de la disciplina
        competencias = Competencia.objects.filter(disciplina=self.disciplina, zona='Eliminatoria')
        for competencia in competencias:
            competencia.delete()

        #Inicializamos/Reiniciamos las entradas de la tabla de posiciones
        posiciones = Posicion_disciplina.objects.filter(disciplina=self.disciplina, zona='Eliminatoria')
        for posicion in posiciones:
            posicion.delete()

        #Actualizamos Flags
        self.disciplina.fixture_creado = False
        self.disciplina.finalizada = False
        self.disciplina.save()

        #Se actualiza la tabla del deporte/copa conjunto. 
        self.disciplina.olimpiada.actualizar_tablas_deportes()

        #Reiniciamos Competencias Fixture.
        if self.disciplina.tipo in 'Grupal':
            self.crear_fixture(integrantes=self.equipos)    
        else:     
            self.crear_fixture(integrantes=self.participantes)



    def reiniciar_modalidad(self):
        #Eliminamos todas las competencias de la disciplina
        competencias = Competencia.objects.filter(disciplina=self.disciplina)
        for competencia in competencias:
            competencia.delete()

        #Inicializamos/Reiniciamos las entradas de la tabla de posiciones
        posiciones = Posicion_disciplina.objects.filter(disciplina=self.disciplina)
        for posicion in posiciones:
            posicion.delete()

        #Actualizamos Flags
        self.disciplina.fixture_creado = False
        self.disciplina.finalizada = False
        self.disciplina.save()

        #Se actualiza la tabla del deporte/copa conjunto. 
        self.disciplina.olimpiada.actualizar_tablas_deportes()

        self.delete()


class CompetenciaUnica (models.Model):
    disciplina = models.ForeignKey('Disciplina', related_name='competencia_unica', on_delete=models.CASCADE)
   
    participantes=[]
    min_participantes_competencia = 2

    
    def __str__(self):
        return str(self.disciplina) + ' | CompetenciaUnica'
    
    
    def crear_fixture (self):
        if Competencia.objects.filter(disciplina=self.disciplina).exists():
            competencia_unica = Competencia.objects.get(disciplina=self.disciplina)
            # competencia.delete()
            competencia_unica.delete()
        competencia_unica = Competencia.objects.create(nombre=str(self.disciplina), disciplina=self.disciplina)

        #Inicializamos Equipos/Participantes
        if self.disciplina.tipo in 'Grupal':                  
            self.equipos = self.disciplina.equipos.all().order_by('nombre')             #Cargamos los Equipos            
            for equipo in self.disciplina.equipos.all().order_by('nombre'):
                competencia_unica.equipos.add(equipo); 
        else:            
            self.participantes = self.disciplina.participantes.order_by('nombre')       #Cargamos los Competidores
            for participante in self.disciplina.participantes.order_by('nombre'):
                competencia_unica.participantes.add(participante); 

        competencia_unica.save();            
            

        #Inicializamos las entradas de la tabla de posiciones
        if self.disciplina.tipo in 'Grupal':
            for equipo in self.equipos:
                Posicion_disciplina.objects.create(disciplina=self.disciplina, equipo=equipo)
        else:
            for participante in self.participantes:
                Posicion_disciplina.objects.create(disciplina=self.disciplina, participante=participante)

        #Actualizamos Flags
        self.disciplina.fixture_creado = True
        self.disciplina.save()

        
    def actualizar_fixture (self):
        competencia = self.disciplina.competencias.first()
        
        #Chequeamos que la cantidad de resultados cargados sea igual al numero de participantes
        if self.disciplina.tipo in 'Grupal':
            if not ( 
             ( len(competencia.resultados.all()) == len(competencia.equipos.all())   ) and 
             ( len(competencia.equipos.all()) >= self.min_participantes_competencia  ) ):  
                raise ValueError (competencia.nombre+': resultados faltantes, no se puede evaluar.')
                # return HttpResponse("No es posible evaluar la competencia: resultado/s faltante/s.")        
        else:
            if not ( 
             ( len(competencia.resultados.all()) == len(competencia.participantes.all())   ) and 
             ( len(competencia.participantes.all()) >= self.min_participantes_competencia  ) ):  
                raise ValueError (competencia.nombre+': resultados faltantes, no se puede evaluar.') 
                # return HttpResponse("No es posible evaluar la competencia: resultado/s faltante/s.")       

        #Chequeamos que los resultados hayan sido cargados 
        for resultado in competencia.resultados.all():
            if (resultado.posicion == -1 
                and resultado.puntos_favor == -1
                and resultado.puntos_favor_1set == -1
                and resultado.games_favor_1set == -1
                and resultado.tiempo == -1
                and resultado.distancia == -1
                and resultado.cantidad == -1
                and resultado.peso == -1
                and resultado.presentado is True):
                raise ValueError (competencia.nombre+': resultados faltantes, no se puede evaluar.')
                # return HttpResponse("No es posible evaluar la competencia: resultado/s faltante/s.")    
            print ('Resultado: ',resultado.participante, 'posicion:', resultado.posicion, 'tiempo:', resultado.tiempo)


        #Cargamos equipos/participantes
        if self.disciplina.tipo in 'Grupal':
            self.equipos = self.disciplina.equipos.all().order_by('nombre')
        else:
            self.participantes = self.disciplina.participantes.all().order_by('nombre')

        #Inicializamos/Reiniciamos las entradas de la tabla de posiciones
        if self.disciplina.tipo in 'Grupal':
            for equipo in self.equipos:
                if not Posicion_disciplina.objects.filter(disciplina=self.disciplina, equipo=equipo).exists():
                    Posicion_disciplina.objects.create(disciplina=self.disciplina, equipo=equipo)
                else:
                    posicion = Posicion_disciplina.objects.get(disciplina=self.disciplina, equipo=equipo)
                    posicion.reset_values()
                    posicion.save()                   
        else:
            for participante in self.participantes:
                if not Posicion_disciplina.objects.filter(disciplina=self.disciplina, participante=participante).exists():
                    Posicion_disciplina.objects.create(disciplina=self.disciplina, participante=participante)
                else:
                    posicion = Posicion_disciplina.objects.get(disciplina=self.disciplina, participante=participante)
                    posicion.reset_values()
                    posicion.save()   
               

        #Actualizamos los resultados de las posiciones de la disciplina
        competencia_unica = Competencia.objects.get(disciplina=self.disciplina)
        # print ('Nombre Competencia: '+competencia.nombre)
        for resultado in competencia_unica.resultados.all():
            if resultado.competencia.disciplina.tipo in 'Grupal':
                posicion = Posicion_disciplina.objects.get(disciplina=self.disciplina, equipo=resultado.equipo)               
            else:
                posicion = Posicion_disciplina.objects.get(disciplina=self.disciplina, participante=resultado.participante)
                print ('Participante: '+resultado.participante.nombre)

            
            if self.disciplina.deporte.nombre in ['Natación', 'Ciclismo', 'Vela', 'Caminata/Carrera', 'Trail', 'Kayak', 'Atletismo', 'Pesca']:
                if resultado.presentado:
                    posicion.tiempo = resultado.tiempo
                    posicion.distancia = resultado.distancia
                    posicion.peso = resultado.peso
                    posicion.cantidad = resultado.cantidad
                    posicion.save()
                else:
                    posicion.tiempo = 1000000
                    posicion.distancia = -2
                    posicion.peso = -2
                    posicion.cantidad = -2
                    posicion.save()

            else:
                raise ValueError ('Error: La modalidad CompetenciaÚnica solo es válida para Natación, Ciclismo, Vela, Caminata/Carrera, Atletismo y Pesca.')    

        
        #Marcamos finalizacion de disciplina para que se contabilicen los puntos
        self.disciplina.finalizada = True 
        self.disciplina.save()
                                          
        #Actualizamos la tabla de la disciplina, deporte y copa conjunto. 
        self.actualizar_tabla()   

    def actualizar_tabla (self):
        if self.disciplina.deporte.nombre in ['Pesca']:
            if self.disicplina.nombre in ['Cantidad']:
                posiciones = Posicion_disciplina.objects.filter(disciplina=self.disciplina).order_by('-cantidad')
                #Asignamos posiciones en función del orden
                pos_anterior = -1
                metrica_anterior = -1
                for n, posicion in enumerate (posiciones):
                    if posicion.cantidad == -1:
                        continue
                    if posicion.cantidad == metrica_anterior:
                        posicion.posicion = pos_anterior
                    else:
                        posicion.posicion = n+1
                        pos_anterior = n+1
                        metrica_anterior = posicion.cantidad
                    posicion.save() 

            if self.disicplina.nombre in ['Pieza Mayor']:
                posiciones = Posicion_disciplina.objects.filter(disciplina=self.disciplina).order_by('-peso')

                #Asignamos posiciones en función del orden
                pos_anterior = -1
                metrica_anterior = -1
                for n, posicion in enumerate (posiciones):
                    if posicion.peso == -1:
                        continue
                    if posicion.peso == metrica_anterior:
                        posicion.posicion = pos_anterior
                    else:
                        posicion.posicion = n+1
                        pos_anterior = n+1
                        metrica_anterior = posicion.peso
                    posicion.save()  



        if self.disciplina.deporte.nombre in ['Atletismo']:
            if self.disciplina.nombre in ['Salto en Largo']:
                posiciones = Posicion_disciplina.objects.filter(disciplina=self.disciplina).order_by('-distancia')

                #Asignamos posiciones en función del orden
                pos_anterior = -1
                metrica_anterior = -1
                for n, posicion in enumerate (posiciones):
                    if posicion.distancia == -1:
                        continue
                    if posicion.distancia == metrica_anterior:
                        posicion.posicion = pos_anterior
                    else:
                        posicion.posicion = n+1
                        pos_anterior = n+1
                        metrica_anterior = posicion.distancia
                    posicion.save()  

            else:
                posiciones = Posicion_disciplina.objects.filter(disciplina=self.disciplina).order_by('tiempo')

                #Asignamos posiciones en función del orden
                pos_anterior = -1
                metrica_anterior = -1
                for n, posicion in enumerate (posiciones):
                    if posicion.tiempo == 1000000:
                        continue
                    if posicion.tiempo == metrica_anterior:
                        posicion.posicion = pos_anterior
                    else:
                        posicion.posicion = n+1
                        pos_anterior = n+1
                        metrica_anterior = posicion.tiempo
                    posicion.save() 

        if self.disciplina.deporte.nombre in ['Natación', 'Ciclismo', 'Vela', 'Caminata/Carrera', 'Trail', 'Kayak']:            
            posiciones = Posicion_disciplina.objects.filter(disciplina=self.disciplina).order_by('tiempo')
        
            #Asignamos posiciones en función del orden
            pos_anterior = -1
            metrica_anterior = -1
            for n, posicion in enumerate (posiciones):
                if posicion.tiempo == 1000000:
                    continue
                if posicion.tiempo == metrica_anterior:
                    posicion.posicion = pos_anterior
                else:
                    posicion.posicion = n+1
                    pos_anterior = n+1
                    metrica_anterior = posicion.tiempo
                posicion.save()  

        #Reescribimos posiciones de no presentados.
        for n, posicion in enumerate (posiciones):
            if (posicion.cantidad == -2 or posicion.distancia == -2 or 
                posicion.peso == -2 or posicion.cantidad == -2 or
                posicion.tiempo == 1000000):
                posicion.posicion = 99
                posicion.save() 
                    

        #Se actualiza la tabla del deporte/copa conjunto. 
        self.disciplina.olimpiada.actualizar_tablas_deportes()

    def reiniciar_fixture(self):
        #Eliminamos todas las competencias de la disciplina
        competencias = Competencia.objects.filter(disciplina=self.disciplina)
        for competencia in competencias:
            competencia.delete()

        #Inicializamos/Reiniciamos las entradas de la tabla de posiciones
        posiciones = Posicion_disciplina.objects.filter(disciplina=self.disciplina)
        for posicion in posiciones:
            posicion.delete()

        #Actualiza la tabla del deporte/copa conjunto si la disciplina estaba finalizada. 
        if self.disciplina.finalizada:
            self.disciplina.finalizada = False
            self.disciplina.save()
            self.disciplina.olimpiada.actualizar_tablas_deportes()

        #Actualizamos Flags
        self.disciplina.fixture_creado = False
        self.disciplina.finalizada = False
        self.disciplina.save()

        #Borramos fixture.
        self.crear_fixture()

    def reiniciar_modalidad(self):
        #Eliminamos todas las competencias de la disciplina
        competencias = Competencia.objects.filter(disciplina=self.disciplina)
        for competencia in competencias:
            competencia.delete()

        #Inicializamos/Reiniciamos las entradas de la tabla de posiciones
        posiciones = Posicion_disciplina.objects.filter(disciplina=self.disciplina)
        for posicion in posiciones:
            posicion.delete()

        #Actualizamos Flags
        self.disciplina.fixture_creado = False
        self.disciplina.finalizada = False
        self.disciplina.save()

        #Se actualiza la tabla del deporte/copa conjunto. 
        self.disciplina.olimpiada.actualizar_tablas_deportes()

        self.delete()


class PosicionManual (models.Model):
    disciplina = models.ForeignKey('Disciplina', related_name='posicion_manual', on_delete=models.CASCADE)
   
    participantes=[]
    min_participantes_competencia = 2

    
    def __str__(self):
        return str(self.disciplina) + ' | PosicionManual'
    
    
    def crear_fixture (self):
        if Competencia.objects.filter(disciplina=self.disciplina).exists():
            competencia = Competencia.objects.get(disciplina=self.disciplina)
            competencia.delete()
        competencia_unica = Competencia.objects.create(nombre=str(self.disciplina), disciplina=self.disciplina, zona='Manual')

        #Inicializamos Equipos/Participantes
        if self.disciplina.tipo in 'Grupal':                  
            self.equipos = self.disciplina.equipos.all().order_by('nombre')             #Cargamos los Equipos            
            for equipo in self.disciplina.equipos.all().order_by('nombre'):
                competencia_unica.equipos.add(equipo); 
        else:            
            self.participantes = self.disciplina.participantes.order_by('nombre')       #Cargamos los Competidores
            for participante in self.disciplina.participantes.order_by('nombre'):
                competencia_unica.participantes.add(participante); 

        competencia_unica.save();            
            

        #Inicializamos las entradas de la tabla de posiciones
        if self.disciplina.tipo in 'Grupal':
            for equipo in self.equipos:
                Posicion_disciplina.objects.create(disciplina=self.disciplina, equipo=equipo)
        else:
            for participante in self.participantes:
                Posicion_disciplina.objects.create(disciplina=self.disciplina, participante=participante)

        #Actualizamos Flags
        self.disciplina.fixture_creado = True
        self.disciplina.save()

    def actualizar_fixture (self):
        competencia = self.disciplina.competencias.first()
        
        #Chequeamos que la cantidad de resultados cargados sea igual al numero de participantes
        if self.disciplina.tipo in 'Grupal':
            if not ( 
             ( len(competencia.resultados.all()) == len(competencia.equipos.all())   ) and 
             ( len(competencia.equipos.all()) >= self.min_participantes_competencia  ) ):  
                raise ValueError (competencia.nombre+': resultados faltantes, no se puede evaluar.')    
        else:
            if not ( 
             ( len(competencia.resultados.all()) == len(competencia.participantes.all())   ) and 
             ( len(competencia.participantes.all()) >= self.min_participantes_competencia  ) ):  
                raise ValueError (competencia.nombre+': resultados faltantes, no se puede evaluar.')    

        #Chequeamos que los resultados hayan sido cargados 
        for resultado in competencia.resultados.all():
            if (resultado.posicion == -1 
                and resultado.puntos_favor == -1
                and resultado.puntos_favor_1set == -1
                and resultado.games_favor_1set == -1
                and resultado.tiempo == -1
                and resultado.distancia == -1
                and resultado.cantidad == -1
                and resultado.peso == -1
                and resultado.presentado is True):
                raise ValueError (competencia.nombre+': resultados faltantes, no se puede evaluar.')
                # return HttpResponse("No es posible evaluar la competencia: resultado/s faltante/s.")    
            # print ('Resultado: ',resultado.participante, 'posicion:', resultado.posicion, 'tiempo:', resultado.tiempo, 'compitio?:', resultado.presentado)



        #Cargamos equipos/participantes
        if self.disciplina.tipo in 'Grupal':
            self.equipos = self.disciplina.equipos.all().order_by('nombre')
        else:
            self.participantes = self.disciplina.participantes.all().order_by('nombre')

        #Inicializamos/Reiniciamos las entradas de la tabla de posiciones
        if self.disciplina.tipo in 'Grupal':
            for equipo in self.equipos:
                if not Posicion_disciplina.objects.filter(disciplina=self.disciplina, equipo=equipo).exists():
                    Posicion_disciplina.objects.create(disciplina=self.disciplina, equipo=equipo)
                else:
                    posicion = Posicion_disciplina.objects.get(disciplina=self.disciplina, equipo=equipo)
                    posicion.reset_values()
                    posicion.save()                   
        else:
            for participante in self.participantes:
                if not Posicion_disciplina.objects.filter(disciplina=self.disciplina, participante=participante).exists():
                    Posicion_disciplina.objects.create(disciplina=self.disciplina, participante=participante)
                else:
                    posicion = Posicion_disciplina.objects.get(disciplina=self.disciplina, participante=participante)
                    posicion.reset_values()
                    posicion.save()   
               

        #Actualizamos los resultados de las posiciones de la disciplina
        competencia_unica = Competencia.objects.get(disciplina=self.disciplina)
        # print ('Nombre Competencia: '+competencia.nombre)
        for resultado in competencia_unica.resultados.all():
            if resultado.competencia.disciplina.tipo in 'Grupal':
                posicion = Posicion_disciplina.objects.get(disciplina=self.disciplina, equipo=resultado.equipo)               
            else:
                posicion = Posicion_disciplina.objects.get(disciplina=self.disciplina, participante=resultado.participante)

            if resultado.presentado:
                posicion.posicion = resultado.posicion
                posicion.save()
            else:
                posicion.posicion = 99
                posicion.save()
        
        
        #Marcamos finalizacion de disciplina para que se contabilicen los puntos
        self.disciplina.finalizada = True 
        self.disciplina.save()
                                          
        #Actualizamos la tabla de la disciplina, deporte y copa conjunto. 
        self.actualizar_tabla()   

    def actualizar_tabla (self):
        # Las posiciones de la disciplina fueron asignadas manualmente

        #Se actualiza la tabla del deporte/copa conjunto. 
        self.disciplina.olimpiada.actualizar_tablas_deportes()

    def reiniciar_fixture(self):
        #Eliminamos todas las competencias de la disciplina
        competencias = Competencia.objects.filter(disciplina=self.disciplina)
        for competencia in competencias:
            competencia.delete()

        #Inicializamos/Reiniciamos las entradas de la tabla de posiciones
        posiciones = Posicion_disciplina.objects.filter(disciplina=self.disciplina)
        for posicion in posiciones:
            posicion.delete()

        #Actualizamos Flags
        self.disciplina.fixture_creado = False
        self.disciplina.finalizada = False
        self.disciplina.save()

        #Se actualiza la tabla del deporte/copa conjunto. 
        self.disciplina.olimpiada.actualizar_tablas_deportes()

        #Borramos fixture.
        self.crear_fixture()

    def reiniciar_modalidad(self):
        #Eliminamos todas las competencias de la disciplina
        competencias = Competencia.objects.filter(disciplina=self.disciplina)
        for competencia in competencias:
            competencia.delete()

        #Inicializamos/Reiniciamos las entradas de la tabla de posiciones
        posiciones = Posicion_disciplina.objects.filter(disciplina=self.disciplina)
        for posicion in posiciones:
            posicion.delete()

        #Actualizamos Flags
        self.disciplina.fixture_creado = False
        self.disciplina.finalizada = False
        self.disciplina.save()

        #Se actualiza la tabla del deporte/copa conjunto. 
        self.disciplina.olimpiada.actualizar_tablas_deportes()

        self.delete()































class RondasClasificacion (models.Model):
    disciplina = models.ForeignKey('Disciplina', related_name='rondas_clasificacion', on_delete=models.CASCADE)
   
    #Campos para definir estructura fase de grupos y clasificados
    nrondas = models.IntegerField(default=2)

    grupo_e1 = models.ManyToManyField('Equipo', related_name='grupor1e1', blank=True, verbose_name='Clasificación 1 (Equipos)')
    grupo_e2 = models.ManyToManyField('Equipo', related_name='grupor1e2', blank=True, verbose_name='Clasificación 2 (Equipos)')
    grupo_e3 = models.ManyToManyField('Equipo', related_name='grupor1e3', blank=True, verbose_name='Clasificación 3 (Equipos)')
    grupo_e4 = models.ManyToManyField('Equipo', related_name='grupor1e4', blank=True, verbose_name='Clasificación 4 (Equipos)')
    grupo_e5 = models.ManyToManyField('Equipo', related_name='grupor1e5', blank=True, verbose_name='Clasificación 5 (Equipos)')
    grupo_e6 = models.ManyToManyField('Equipo', related_name='grupor1e6', blank=True, verbose_name='Clasificación 6 (Equipos)')
    grupo_e7 = models.ManyToManyField('Equipo', related_name='grupor1e7', blank=True, verbose_name='Clasificación 7 (Equipos)')
    grupo_e8 = models.ManyToManyField('Equipo', related_name='grupor1e8', blank=True, verbose_name='Clasificación 8 (Equipos)')
    semifinal_e1 = models.ManyToManyField('Equipo', related_name='semifinale1', blank=True, verbose_name='Semifinal 1 (Equipos)')
    semifinal_e2 = models.ManyToManyField('Equipo', related_name='semifinale2', blank=True, verbose_name='Semifinal 2 (Equipos)')
    semifinal_e3 = models.ManyToManyField('Equipo', related_name='semifinale3', blank=True, verbose_name='Semifinal 3 (Equipos)')
    semifinal_e4 = models.ManyToManyField('Equipo', related_name='semifinale4', blank=True, verbose_name='Semifinal 4 (Equipos)')
    equipos_finalistas = models.ManyToManyField('Equipo', blank=True, verbose_name='Final (Equipos)')

    grupo_p1 = models.ManyToManyField('Persona', related_name='grupor1p1', blank=True, verbose_name='Clasificación 1 (Jugadores)')
    grupo_p2 = models.ManyToManyField('Persona', related_name='grupor1p2', blank=True, verbose_name='Clasificación 2 (Jugadores)')
    grupo_p3 = models.ManyToManyField('Persona', related_name='grupor1p3', blank=True, verbose_name='Clasificación 3 (Jugadores)')
    grupo_p4 = models.ManyToManyField('Persona', related_name='grupor1p4', blank=True, verbose_name='Clasificación 4 (Jugadores)')
    grupo_p5 = models.ManyToManyField('Persona', related_name='grupor1p5', blank=True, verbose_name='Clasificación 5 (Jugadores)')
    grupo_p6 = models.ManyToManyField('Persona', related_name='grupor1p6', blank=True, verbose_name='Clasificación 6 (Jugadores)')
    grupo_p7 = models.ManyToManyField('Persona', related_name='grupor1p7', blank=True, verbose_name='Clasificación 7 (Jugadores)')
    grupo_p8 = models.ManyToManyField('Persona', related_name='grupor1p8', blank=True, verbose_name='Clasificación 8 (Jugadores)')
    semifinal_p1 = models.ManyToManyField('Persona', related_name='semifinalp1', blank=True, verbose_name='Semifinal 1 (Equipos)')
    semifinal_p2 = models.ManyToManyField('Persona', related_name='semifinalp2', blank=True, verbose_name='Semifinal 2 (Equipos)')
    semifinal_p3 = models.ManyToManyField('Persona', related_name='semifinalp3', blank=True, verbose_name='Semifinal 3 (Equipos)')
    semifinal_p4 = models.ManyToManyField('Persona', related_name='semifinalp4', blank=True, verbose_name='Semifinal 4 (Equipos)')
    participantes_finalistas = models.ManyToManyField('Persona', blank=True, verbose_name='Finalistas (Jugadores)')
    
    nclasificados_grupo1 = models.IntegerField(default=0)
    nclasificados_grupo2 = models.IntegerField(default=0)
    nclasificados_grupo3 = models.IntegerField(default=0)
    nclasificados_grupo4 = models.IntegerField(default=0)
    nclasificados_grupo5 = models.IntegerField(default=0)
    nclasificados_grupo6 = models.IntegerField(default=0)
    nclasificados_grupo7 = models.IntegerField(default=0)
    nclasificados_grupo8 = models.IntegerField(default=0)
    nclasificados_mejores_segundos_clasificacion =  models.IntegerField(default=0)
    nclasificados_mejores_terceros_clasificacion =  models.IntegerField(default=0)
    max_participantes_semifinal1 = models.IntegerField(default=8, verbose_name='Número de Participantes Semifinal 1')
    nclasificados_semifinal1 = models.IntegerField(default=0, verbose_name='Número de Clasificados Semifinal 1')
    max_participantes_semifinal2 = models.IntegerField(default=8, verbose_name='Número de Participantes Semifinal 2')
    nclasificados_semifinal2 = models.IntegerField(default=0, verbose_name='Número de Clasificados Semifinal 2')
    max_participantes_semifinal3 = models.IntegerField(default=8, verbose_name='Número de Participantes Semifinal 3')
    nclasificados_semifinal3 = models.IntegerField(default=0, verbose_name='Número de Clasificados Semifinal 3')
    max_participantes_semifinal4 = models.IntegerField(default=8, verbose_name='Número de Participantes Semifinal 4')
    nclasificados_semifinal4 = models.IntegerField(default=0, verbose_name='Número de Clasificados Semifinal 4')
    nclasificados_mejores_segundos_semifinales =  models.IntegerField(default=0)
    nclasificados_mejores_terceros_semifinales =  models.IntegerField(default=0)
    
    fase_clasificacion_iniciada = models.BooleanField('Fase de Grupos Iniciada?', default=False)
    fase_clasificacion_finalizada = models.BooleanField('Fase de Grupos Finalizada?', default=False)
    fase_semifinal_iniciada = models.BooleanField('Fase de Semifinales Iniciada?', default=False)
    fase_semifinal_finalizada = models.BooleanField('Fase de Semifinales Finalizada?', default=False)
    fase_final_iniciada = models.BooleanField('Fase Final Iniciada?', default=False)


    def __str__(self):
        return str(self.disciplina) + ' | ' + str(self.disciplina.deporte.nombre)


    def reiniciar_modalidad(self):
        competencias = Competencia.objects.filter(disciplina=self.disciplina)
        for competencia in competencias:
            competencia.delete()

        #Inicializamos/Reiniciamos las entradas de la tabla de posiciones
        posiciones = Posicion_disciplina.objects.filter(disciplina=self.disciplina)
        for posicion in posiciones:
            posicion.delete()

        #Actualizamos Flags
        self.disciplina.fixture_creado = False
        self.disciplina.finalizada = False
        self.disciplina.save()

        #Se actualiza la tabla del deporte/copa conjunto. 
        self.disciplina.olimpiada.actualizar_tablas_deportes()

        self.delete()



    def iniciar_grupos(self):
        #Inicializamos las competencias correspondientes de cada grupo 
        if self.disciplina.tipo in 'Grupal':
            grupos = [self.grupo_e1,self.grupo_e2,self.grupo_e3,self.grupo_e4,self.grupo_e5,
                      self.grupo_e6,self.grupo_e7,self.grupo_e8]
            for grupo_id, grupo in enumerate(grupos):
                competencia = Competencia.objects.create(disciplina=self.disciplina, zona='Clasificatoria', grupo='grupo_'+str(grupo_id+1) )
                for equipo in grupo.all():
                    competencia.equipos.add (equipo)

        if self.disciplina.tipo in 'Individual':
            grupos = [self.grupo_p1,self.grupo_p2,self.grupo_p3,self.grupo_p4,self.grupo_p5,
                      self.grupo_p6,self.grupo_p7,self.grupo_p8]
            for grupo_id, grupo in enumerate(grupos):
                competencia = Competencia.objects.create(disciplina=self.disciplina, zona='Clasificatoria', grupo='grupo_'+str(grupo_id+1) )
                for persona in grupo.all():    
                    competencia.participantes.add (persona)

        #Inicializamos las entradas de la tabla de posiciones
        if self.disciplina.tipo in 'Grupal':
            for grupo_id, grupo in enumerate(grupos):
                for equipo in grupo.all():
                    Posicion_disciplina.objects.create(disciplina=self.disciplina, equipo=equipo, zona='Clasificatoria', grupo='grupo_'+str(grupo_id+1))
        else:
            for grupo_id, grupo in enumerate(grupos):
                for participante in grupo.all():
                    Posicion_disciplina.objects.create(disciplina=self.disciplina, participante=participante, zona='Clasificatoria', grupo='grupo_'+str(grupo_id+1))

        self.fase_clasificacion_iniciada = True
        # self.fase_clasificacion_finalizada = False

        if self.nrondas == 2:
            self.fase_semifinal_iniciada = False
            self.fase_semifinal_finalizada = True
        # self.fase_final_iniciada = False
        self.save()


    def reiniciar_grupos(self):
        competencias = Competencia.objects.filter(disciplina=self.disciplina, zona='Clasificatoria')
        for competencia in competencias:
            competencia.delete()

        #Inicializamos/Reiniciamos las entradas de la tabla de posiciones
        posiciones = Posicion_disciplina.objects.filter(disciplina=self.disciplina, zona='Clasificatoria')
        for posicion in posiciones:
            posicion.delete()                  


        #Actualizamos Flags
        self.fase_clasificacion_iniciada = False
        self.fase_clasificacion_finalizada = False
        self.fase_semifinal_iniciada = False
        self.fase_semifinal_finalizada = False
        self.fase_final_iniciada = False
        self.save()
    
        self.disciplina.finalizada = False
        self.disciplina.save()



    def actualizar_grupos(self):
        if self.disciplina.tipo in 'Grupal':
            grupos = [self.grupo_e1,self.grupo_e2,self.grupo_e3,self.grupo_e4,self.grupo_e5,
                      self.grupo_e6,self.grupo_e7,self.grupo_e8]
        else:
            grupos = [self.grupo_p1,self.grupo_p2,self.grupo_p3,self.grupo_p4,self.grupo_p5,
                      self.grupo_p6,self.grupo_p7,self.grupo_p8]


        self.fase_clasificacion_finalizada = True
        self.save()
        for grupo_id, integrantes_grupo in enumerate(grupos):
            
            if integrantes_grupo.count() == 0: 
                continue

            #Cargamos equipos/participantes
            if self.disciplina.tipo in 'Grupal':
                self.equipos = integrantes_grupo.all().order_by('nombre')               
            else:
                self.participantes = integrantes_grupo.all().order_by('nombre')


            #Inicializamos/Reiniciamos las entradas de la tabla de posiciones
            if self.disciplina.tipo in 'Grupal':
                for equipo in self.equipos:
                    if not Posicion_disciplina.objects.filter(disciplina=self.disciplina, equipo=equipo, zona='Clasificatoria', grupo='grupo_'+str(grupo_id+1)).exists():
                        Posicion_disciplina.objects.create(disciplina=self.disciplina, equipo=equipo, zona='Clasificatoria', grupo='grupo_'+str(grupo_id+1))
                    else:
                        posicion = Posicion_disciplina.objects.get(disciplina=self.disciplina, equipo=equipo, zona='Clasificatoria', grupo='grupo_'+str(grupo_id+1))
                        posicion.reset_values()
                        posicion.save()                   
            else:
                for participante in self.participantes:
                    if not Posicion_disciplina.objects.filter(disciplina=self.disciplina, participante=participante, zona='Clasificatoria', grupo='grupo_'+str(grupo_id+1)).exists():
                        Posicion_disciplina.objects.create(disciplina=self.disciplina, participante=participante, zona='Clasificatoria', grupo='grupo_'+str(grupo_id+1))
                    else:
                        posicion = Posicion_disciplina.objects.get(disciplina=self.disciplina, participante=participante, zona='Clasificatoria', grupo='grupo_'+str(grupo_id+1))
                        posicion.reset_values()
                        posicion.save()   

            #Evaluamos los resultados de la competencia
            competencia = Competencia.objects.get(disciplina=self.disciplina, zona='Clasificatoria', grupo='grupo_'+str(grupo_id+1))
            
            #Chequeamos que la cantidad de resultados cargados sea igual al numero de participantes
            if self.disciplina.tipo in 'Grupal':
                if not len(competencia.resultados.all()) == len(competencia.equipos.all()):  
                    self.fase_clasificacion_finalizada = False
                    self.save()
                    raise ValueError (str(competencia)+': resultados faltantes, no se puede evaluar.')    
            else:
                if not len(competencia.resultados.all()) == len(competencia.participantes.all()):  
                    self.fase_clasificacion_finalizada = False
                    self.save()
                    raise ValueError (str(competencia)+': resultados faltantes, no se puede evaluar.')    

            #Chequeamos que los resultados hayan sido cargados 
            resultado_faltante = False
            for resultado in competencia.resultados.all():
                if (resultado.posicion == -1 
                    and resultado.puntos_favor == -1
                    and resultado.puntos_favor_1set == -1
                    and resultado.games_favor_1set == -1
                    and resultado.tiempo == -1
                    and resultado.distancia == -1
                    and resultado.cantidad == -1
                    and resultado.peso == -1
                    and resultado.presentado is True):
                    print ('Resultado no cargado, no se puede evaluar.')     
                    resultado_faltante = True
                    break
            if resultado_faltante:
                self.fase_clasificacion_finalizada = False
                self.save()
                raise ValueError (str(competencia)+': resultados faltantes, no se puede evaluar.')

            #Actualizamos los resultados de las posiciones de la disciplina
            # print ('Nombre Competencia: '+competencia.nombre)
            for resultado in competencia.resultados.all():
                if resultado.competencia.disciplina.tipo in 'Grupal':
                    posicion = Posicion_disciplina.objects.get(disciplina=self.disciplina, equipo=resultado.equipo, zona='Clasificatoria', grupo='grupo_'+str(grupo_id+1))               
                else:
                    posicion = Posicion_disciplina.objects.get(disciplina=self.disciplina, participante=resultado.participante, zona='Clasificatoria', grupo='grupo_'+str(grupo_id+1))

                if self.disciplina.deporte.nombre in ['Natación','Ciclismo','Vela','Caminata/Carrera','Atletismo','Pesca']:
                    # posicion.tiempo = resultado.tiempo
                    # posicion.distancia = resultado.distancia
                    # posicion.peso = resultado.peso
                    # posicion.cantidad = resultado.cantidad
                    # posicion.save()

                    if resultado.presentado:
                        posicion.tiempo = resultado.tiempo
                        posicion.distancia = resultado.distancia
                        posicion.peso = resultado.peso
                        posicion.cantidad = resultado.cantidad
                        posicion.save()
                    else:
                        posicion.tiempo = 1000000
                        posicion.distancia = -2
                        posicion.peso = -2
                        posicion.cantidad = -2
                        posicion.save()
        

                else:
                    raise ValueError ('Error: La modalidad CompetenciaÚnica solo es válida para Natación, Ciclismo, Vela, Caminata/Carrera, Atletismo y Pesca.')    

            
        #Actualizamos la tabla de la disciplina, deporte y copa conjunto.                                              
        self.actualizar_tabla_clasificacion()            
    
        # if self.fase_clasificacion_finalizada and self.nrondas == 2:
        #     self.seleccionar_finalistas()
        # else:
        #     self.seleccionar_semifinalistas()
            

        
    def actualizar_tabla_clasificacion (self):
        if self.disciplina.tipo in 'Grupal':
            grupos = [self.grupo_e1,self.grupo_e2,self.grupo_e3,self.grupo_e4,self.grupo_e5,
                      self.grupo_e6,self.grupo_e7,self.grupo_e8]
                      
        else:
            grupos = [self.grupo_p1,self.grupo_p2,self.grupo_p3,self.grupo_p4,self.grupo_p5,
                      self.grupo_p6,self.grupo_p7,self.grupo_p8] 

        for grupo_id, integrantes_grupo in enumerate(grupos):
            
            if self.disciplina.deporte.nombre in ['Pesca']:
                if self.disicplina.nombre in ['Cantidad']:
                    posiciones = Posicion_disciplina.objects.filter(disciplina=self.disciplina, zona='Clasificatoria', grupo='grupo_'+str(grupo_id+1)).order_by('-cantidad')
                    #Asignamos posiciones en función del orden
                    pos_anterior = -1
                    metrica_anterior = -1
                    for n, posicion in enumerate (posiciones):
                        if posicion.cantidad == metrica_anterior:
                            posicion.posicion = pos_anterior
                        else:
                            posicion.posicion = n+1
                            pos_anterior = n+1
                            metrica_anterior = posicion.cantidad
                        posicion.save() 

                if self.disicplina.nombre in ['Pieza Mayor']:
                    posiciones = Posicion_disciplina.objects.filter(disciplina=self.disciplina, zona='Clasificatoria', grupo='grupo_'+str(grupo_id+1)).order_by('-peso')

                    #Asignamos posiciones en función del orden
                    pos_anterior = -1
                    metrica_anterior = -1
                    for n, posicion in enumerate (posiciones):
                        if posicion.peso == metrica_anterior:
                            posicion.posicion = pos_anterior
                        else:
                            posicion.posicion = n+1
                            pos_anterior = n+1
                            metrica_anterior = posicion.peso
                        posicion.save()  



            if self.disciplina.deporte.nombre in ['Atletismo']:
                if self.disciplina.nombre in ['Salto en Largo']:
                    posiciones = Posicion_disciplina.objects.filter(disciplina=self.disciplina, zona='Clasificatoria', grupo='grupo_'+str(grupo_id+1)).order_by('-distancia')

                    #Asignamos posiciones en función del orden
                    pos_anterior = -1
                    metrica_anterior = -1
                    for n, posicion in enumerate (posiciones):
                        if posicion.distancia == metrica_anterior:
                            posicion.posicion = pos_anterior
                        else:
                            posicion.posicion = n+1
                            pos_anterior = n+1
                            metrica_anterior = posicion.distancia
                        posicion.save()  


                else:
                    posiciones = Posicion_disciplina.objects.filter(disciplina=self.disciplina, zona='Clasificatoria', grupo='grupo_'+str(grupo_id+1)).order_by('tiempo')

                    #Asignamos posiciones en función del orden
                    pos_anterior = -1
                    metrica_anterior = -1
                    for n, posicion in enumerate (posiciones):
                        if posicion.tiempo == metrica_anterior:
                            posicion.posicion = pos_anterior
                        else:
                            posicion.posicion = n+1
                            pos_anterior = n+1
                            metrica_anterior = posicion.tiempo
                        posicion.save() 


            if self.disciplina.deporte.nombre in ['Natación','Ciclismo','Vela','Caminata/Carrera']:            
                posiciones = Posicion_disciplina.objects.filter(disciplina=self.disciplina, zona='Clasificatoria', grupo='grupo_'+str(grupo_id+1)).order_by('tiempo')
            
                #Asignamos posiciones en función del orden
                pos_anterior = -1
                metrica_anterior = -1
                for n, posicion in enumerate (posiciones):
                    if posicion.tiempo == metrica_anterior:
                        posicion.posicion = pos_anterior
                    else:
                        posicion.posicion = n+1
                        pos_anterior = n+1
                        metrica_anterior = posicion.tiempo
                    posicion.save()  


            #Reescribimos posiciones de no presentados.
            for n, posicion in enumerate (posiciones):
                if (posicion.cantidad == -2 or posicion.distancia == -2 or 
                    posicion.peso == -2 or posicion.cantidad == -2 or
                    posicion.tiempo == 1000000):
                    posicion.posicion = 99
                    posicion.save() 


    def seleccionar_semifinalistas(self):
        # Carga los clasificados de los grupos
        print ('')
        print ('Seleccionando Semifinalistas de los Grupos')
        if self.disciplina.tipo in 'Grupal':
            grupos = [self.grupo_e1,self.grupo_e2,self.grupo_e3,self.grupo_e4,self.grupo_e5,
                      self.grupo_e6,self.grupo_e7,self.grupo_e8]
        else:
            grupos = [self.grupo_p1,self.grupo_p2,self.grupo_p3,self.grupo_p4,self.grupo_p5,
                      self.grupo_p6,self.grupo_p7,self.grupo_p8]

        grupos_semis = []
        for grupo_id in range (4):
            nclasificados=getattr(self, 'nclasificados_semifinal'+str(grupo_id+1))
            if self.disciplina.tipo in 'Grupal':
                if nclasificados:
                    grupo_semi=getattr(self, 'semifinal_e'+str(grupo_id+1))
                    grupos_semis.append(grupo_semi)
            else:
                if nclasificados:
                    grupo_semi=getattr(self, 'semifinal_p'+str(grupo_id+1))
                    grupos_semis.append(grupo_semi)
                    print ('Agregando grupo de semifinal: ','semifinal_p'+str(grupo_id+1),' | NGrupos de Semifianles agregados',len(grupos_semis))

                # grupos_semis = [self.semifinal_p1,self.semifinal_p2,self.semifinal_p3,self.semifinal_p4]
        print (grupos_semis)

        #Selecionamos los Semifinalistas indicados de cada Grupo
        nsemifinal_activa = 0

        for grupo_id, integrantes_grupo in enumerate(grupos):
            nclasificados=getattr(self, 'nclasificados_grupo'+str(grupo_id+1))
            
            if not nclasificados:
                continue

            posiciones = Posicion_disciplina.objects.filter(disciplina=self.disciplina, zona='Clasificatoria', grupo='grupo_'+str(grupo_id+1)).order_by('posicion')

            for i in range(0,nclasificados):
                print ('Participante ',i)
                max_participantes = getattr(self, 'max_participantes_semifinal'+str(nsemifinal_activa+1))
                print ('max_participantes_semifinal'+str(nsemifinal_activa+1),': ', max_participantes)

                if self.disciplina.tipo in 'Grupal':
                    if grupos_semis[nsemifinal_activa].count() < max_participantes:
                        grupos_semis[nsemifinal_activa].add(posiciones[i].equipo)
                    else:
                        nsemifinal_activa = nsemifinal_activa + 1
                        max_participantes = getattr(self, 'max_participantes_semifinal'+str(nsemifinal_activa+1))
                        grupos_semis[nsemifinal_activa].add(posiciones[i].equipo)
                else:
                    print ('nagregados: ',grupos_semis[nsemifinal_activa].count(), 'de', max_participantes)
                    if grupos_semis[nsemifinal_activa].count() < max_participantes:
                        grupos_semis[nsemifinal_activa].add(posiciones[i].participante)
                        print ('Agregando Semifinalista: ',posiciones[i].participante, 'en grupo ', nsemifinal_activa)
                    else:
                        print ('Grupo anterior lleno, cambiamos Grupo')
                        nsemifinal_activa = nsemifinal_activa + 1
                        max_participantes = getattr(self, 'max_participantes_semifinal'+str(nsemifinal_activa+1))
                        grupos_semis[nsemifinal_activa].add(posiciones[i].participante)
                        print ('Agregando Semifinalista: ',posiciones[i].participante, 'en grupo ', nsemifinal_activa)


        #Seleccionamos Mejores Segundos/Terceros de los Grupos --------------------------------------------------         
        if (self.nclasificados_mejores_segundos_clasificacion or self.nclasificados_mejores_terceros_clasificacion):
            print ('')
            print ('Evaluando Mejores Segundos')
            for grupo_id, integrantes_grupo in enumerate(grupos):
                nclasificados=getattr(self, 'nclasificados_grupo'+str(grupo_id+1))
                for i in range(nclasificados,integrantes_grupo.count()):
                    #Copiamos Posicion_disciplina de todos los participantes menos los clasificados
                    if self.disciplina.tipo in 'Grupal':
                        posicion = Posicion_disciplina.objects.get(
                            equipo = integrantes_grupo.all()[i],
                            disciplina=self.disciplina, zona='Clasificatoria', 
                            grupo='grupo_'+str(grupo_id+1))
                    else:
                        posicion = Posicion_disciplina.objects.get(
                            participante = integrantes_grupo.all()[i],
                            disciplina=self.disciplina, zona='Clasificatoria', 
                            grupo='grupo_'+str(grupo_id+1))

                    #hacemos copia de objeto
                    posicion.pk = None
                    posicion.grupo = 'Segundos'
                    posicion.zona = 'Segundos'
                    posicion.save()


            #Las ordenamos con el criterio de la disciplina
            if self.disciplina.deporte.nombre in ['Pesca']:
                if self.disicplina.nombre in ['Cantidad']:
                    posiciones = Posicion_disciplina.objects.filter(disciplina=self.disciplina, zona='Segundos').order_by('-cantidad')
                if self.disicplina.nombre in ['Pieza Mayor']:
                    posiciones = Posicion_disciplina.objects.filter(disciplina=self.disciplina, zona='Segundos').order_by('-peso')

            if self.disciplina.deporte.nombre in ['Atletismo']:
                if self.disciplina.nombre in ['Salto en Largo']:
                    posiciones = Posicion_disciplina.objects.filter(disciplina=self.disciplina, zona='Segundos').order_by('-distancia')
                else:
                    posiciones = Posicion_disciplina.objects.filter(disciplina=self.disciplina, zona='Segundos').order_by('tiempo')

            if self.disciplina.deporte.nombre in ['Natación','Ciclismo','Vela','Caminata/Carrera']:            
                posiciones = Posicion_disciplina.objects.filter(disciplina=self.disciplina, zona='Segundos').order_by('tiempo')
            
            #Se agregan los mejores segundos/terceros al grupo de finalistas.
            if self.nclasificados_mejores_segundos_clasificacion:
                nclasificantes = self.nclasificados_mejores_segundos_clasificacion
            if self.nclasificados_mejores_terceros_clasificacion:
                nclasificantes = self.nclasificados_mejores_terceros_clasificacion

            nagregados=0
            nsemifinal_activa = 0
            
            for posicion in posiciones: 
                if nagregados == nclasificantes:
                    break

                if self.disciplina.tipo in 'Grupal':
                    if grupos_semis[nsemifinal_activa].count() < max_participantes:
                        grupos_semis[nsemifinal_activa].add(posicion.equipo)
                        nagregados = nagregados+1
                    else:
                        nsemifinal_activa = nsemifinal_activa + 1
                        grupos_semis[nsemifinal_activa].add(posicion.equipo)
                        nagregados = nagregados+1
                else:
                    if grupos_semis[nsemifinal_activa].count() < max_participantes:
                        grupos_semis[nsemifinal_activa].add(posicion.participante)
                        nagregados = nagregados+1
                        print ('Agregando Semifinalista: ',posicion.participante, 'en grupo ', nsemifinal_activa)
                    else:
                        nsemifinal_activa = nsemifinal_activa + 1
                        grupos_semis[nsemifinal_activa].add(posicion.participante)
                        nagregados = nagregados+1
                        print ('Agregando Semifinalista: ',posicion.participante, 'en grupo ', nsemifinal_activa)

        self.save()

        




    def iniciar_semifinales(self):
        #Seleccionamos Semifinalistas
        self.seleccionar_semifinalistas()

        #Inicializamos las competencias correspondientes de cada grupo 
        if self.disciplina.tipo in 'Grupal':
            grupos = [self.semifinal_e1,self.semifinal_e2,self.semifinal_e3,self.semifinal_e4]
            for grupo_id, grupo in enumerate(grupos):
                competencia = Competencia.objects.create(disciplina=self.disciplina, zona='Semifinales', grupo='semifinal_'+str(grupo_id+1) )
                for equipo in grupo.all():
                    competencia.equipos.add (equipo)

        if self.disciplina.tipo in 'Individual':
            grupos = [self.semifinal_p1,self.semifinal_p2,self.semifinal_p3,self.semifinal_p4]
            for grupo_id, grupo in enumerate(grupos):
                competencia = Competencia.objects.create(disciplina=self.disciplina, zona='Semifinales', grupo='semifinal_'+str(grupo_id+1) )
                for persona in grupo.all():    
                    competencia.participantes.add (persona)

        #Inicializamos las entradas de la tabla de posiciones
        if self.disciplina.tipo in 'Grupal':
            for grupo_id, grupo in enumerate(grupos):
                for equipo in grupo.all():
                    Posicion_disciplina.objects.create(disciplina=self.disciplina, equipo=equipo, zona='Semifinales', grupo='semifinal_'+str(grupo_id+1))
        else:
            for grupo_id, grupo in enumerate(grupos):
                for participante in grupo.all():
                    Posicion_disciplina.objects.create(disciplina=self.disciplina, participante=participante, zona='Semifinales', grupo='semifinal_'+str(grupo_id+1))

        self.fase_semifinal_iniciada = True
        # self.fase_semifinal_finalizada = False
        # self.fase_final_iniciada = False
        self.save()


    def reiniciar_semifinales(self):
        competencias = Competencia.objects.filter(disciplina=self.disciplina, zona='Semifinales')
        for competencia in competencias:
            competencia.delete()

        #Inicializamos/Reiniciamos las entradas de la tabla de posiciones
        posiciones = Posicion_disciplina.objects.filter(disciplina=self.disciplina, zona='Semifinales')
        for posicion in posiciones:
            posicion.delete()     

        posiciones = Posicion_disciplina.objects.filter(disciplina=self.disciplina, zona='Segundos')
        for posicion in posiciones:
            posicion.delete()             

        #Reiniciamos Grupo de Finalistas
        self.semifinal_e1.clear()
        self.semifinal_e2.clear()
        self.semifinal_e3.clear()
        self.semifinal_e4.clear()
        self.semifinal_p1.clear()
        self.semifinal_p2.clear()
        self.semifinal_p3.clear()
        self.semifinal_p4.clear()
        

        #Actualizamos Flags
        self.fase_semifinal_iniciada = False
        self.fase_semifinal_finalizada = False
        # self.fase_final_iniciada = False
        self.save()
    
        self.disciplina.finalizada = False
        self.disciplina.save()



    def actualizar_semifinales(self):
        if self.disciplina.tipo in 'Grupal':
            grupos = [self.semifinal_e1,self.semifinal_e2,self.semifinal_e3,self.semifinal_e4]
        else:
            grupos = [self.semifinal_p1,self.semifinal_p2,self.semifinal_p3,self.semifinal_p4]


        self.fase_semifinal_finalizada = True
        self.save()
        for grupo_id, integrantes_grupo in enumerate(grupos):
            
            if integrantes_grupo.count() == 0: 
                continue

            #Cargamos equipos/participantes
            if self.disciplina.tipo in 'Grupal':
                self.equipos = integrantes_grupo.all().order_by('nombre')               
            else:
                self.participantes = integrantes_grupo.all().order_by('nombre')


            #Inicializamos/Reiniciamos las entradas de la tabla de posiciones
            if self.disciplina.tipo in 'Grupal':
                for equipo in self.equipos:
                    if not Posicion_disciplina.objects.filter(disciplina=self.disciplina, equipo=equipo, zona='Semifinales', grupo='semifinal_'+str(grupo_id+1)).exists():
                        Posicion_disciplina.objects.create(disciplina=self.disciplina, equipo=equipo, zona='Semifinales', grupo='semifinal_'+str(grupo_id+1))
                    else:
                        posicion = Posicion_disciplina.objects.get(disciplina=self.disciplina, equipo=equipo, zona='Clasificatoria', grupo='semifinal_'+str(grupo_id+1))
                        posicion.reset_values()
                        posicion.save()                   
            else:
                for participante in self.participantes:
                    if not Posicion_disciplina.objects.filter(disciplina=self.disciplina, participante=participante, zona='Semifinales', grupo='semifinal_'+str(grupo_id+1)).exists():
                        Posicion_disciplina.objects.create(disciplina=self.disciplina, participante=participante, zona='Semifinales', grupo='semifinal_'+str(grupo_id+1))
                    else:
                        posicion = Posicion_disciplina.objects.get(disciplina=self.disciplina, participante=participante, zona='Semifinales', grupo='semifinal_'+str(grupo_id+1))
                        posicion.reset_values()
                        posicion.save()   

            #Evaluamos los resultados de la competencia
            competencia = Competencia.objects.get(disciplina=self.disciplina, zona='Semifinales', grupo='semifinal_'+str(grupo_id+1))
            
            #Chequeamos que la cantidad de resultados cargados sea igual al numero de participantes
            if self.disciplina.tipo in 'Grupal':
                if not len(competencia.resultados.all()) == len(competencia.equipos.all()):
                    self.fase_semifinal_finalizada = False
                    self.save()
                    raise ValueError (str(competencia)+': resultados faltantes, no se puede evaluar.')    
            else:
                if not len(competencia.resultados.all()) == len(competencia.participantes.all()):
                    self.fase_semifinal_finalizada = False
                    self.save()
                    raise ValueError (str(competencia)+': resultados faltantes, no se puede evaluar.')    

            #Chequeamos que los resultados hayan sido cargados 
            resultado_faltante = False
            for resultado in competencia.resultados.all():
                if (resultado.posicion == -1 
                    and resultado.puntos_favor == -1
                    and resultado.puntos_favor_1set == -1
                    and resultado.games_favor_1set == -1
                    and resultado.tiempo == -1
                    and resultado.distancia == -1
                    and resultado.cantidad == -1
                    and resultado.peso == -1
                    and resultado.presentado is True):
                    print ('Resultado no cargado, no se puede evaluar.')     
                    resultado_faltante = True
                    break
            if resultado_faltante:
                self.fase_semifinal_finalizada = False
                self.save()
                raise ValueError (str(competencia)+': resultados faltantes, no se puede evaluar.')


            #Actualizamos los resultados de las posiciones de la disciplina
            # print ('Nombre Competencia: '+competencia.nombre)
            for resultado in competencia.resultados.all():
                if resultado.competencia.disciplina.tipo in 'Grupal':
                    posicion = Posicion_disciplina.objects.get(disciplina=self.disciplina, equipo=resultado.equipo, zona='Semifinales', grupo='semifinal_'+str(grupo_id+1))               
                else:
                    posicion = Posicion_disciplina.objects.get(disciplina=self.disciplina, participante=resultado.participante, zona='Semifinales', grupo='semifinal_'+str(grupo_id+1))

                if self.disciplina.deporte.nombre in ['Natación','Ciclismo','Vela','Caminata/Carrera','Atletismo','Pesca']:
                    # posicion.tiempo = resultado.tiempo
                    # posicion.distancia = resultado.distancia
                    # posicion.peso = resultado.peso
                    # posicion.cantidad = resultado.cantidad
                    # posicion.save()
                    if resultado.presentado:
                        posicion.tiempo = resultado.tiempo
                        posicion.distancia = resultado.distancia
                        posicion.peso = resultado.peso
                        posicion.cantidad = resultado.cantidad
                        posicion.save()
                    else:
                        posicion.tiempo = 1000000
                        posicion.distancia = -2
                        posicion.peso = -2
                        posicion.cantidad = -2
                        posicion.save()
                else:
                    raise ValueError ('Error: La modalidad CompetenciaÚnica solo es válida para Natación, Ciclismo, Vela, Caminata/Carrera, Atletismo y Pesca.')    
        

        #Actualizamos la tabla de la disciplina, deporte y copa conjunto.                                              
        self.actualizar_tabla_semifinales()            
            
       
    def actualizar_tabla_semifinales (self):
        if self.disciplina.tipo in 'Grupal':
            grupos = [self.semifinal_e1,self.semifinal_e2,self.semifinal_e3,self.semifinal_e4]
        else:
            grupos = [self.semifinal_p1,self.semifinal_p2,self.semifinal_p3,self.semifinal_p4]

        for grupo_id, integrantes_grupo in enumerate(grupos):
            
            if self.disciplina.deporte.nombre in ['Pesca']:
                if self.disicplina.nombre in ['Cantidad']:
                    posiciones = Posicion_disciplina.objects.filter(disciplina=self.disciplina, zona='Semifinales', grupo='semifinal_'+str(grupo_id+1)).order_by('-cantidad')
                    #Asignamos posiciones en función del orden
                    pos_anterior = -1
                    metrica_anterior = -1
                    for n, posicion in enumerate (posiciones):
                        if posicion.cantidad == metrica_anterior:
                            posicion.posicion = pos_anterior
                        else:
                            posicion.posicion = n+1
                            pos_anterior = n+1
                            metrica_anterior = posicion.cantidad
                        posicion.save() 

                if self.disicplina.nombre in ['Pieza Mayor']:
                    posiciones = Posicion_disciplina.objects.filter(disciplina=self.disciplina, zona='Semifinales', grupo='semifinal_'+str(grupo_id+1)).order_by('-peso')

                    #Asignamos posiciones en función del orden
                    pos_anterior = -1
                    metrica_anterior = -1
                    for n, posicion in enumerate (posiciones):
                        if posicion.peso == metrica_anterior:
                            posicion.posicion = pos_anterior
                        else:
                            posicion.posicion = n+1
                            pos_anterior = n+1
                            metrica_anterior = posicion.peso
                        posicion.save()  



            if self.disciplina.deporte.nombre in ['Atletismo']:
                if self.disciplina.nombre in ['Salto en Largo']:
                    posiciones = Posicion_disciplina.objects.filter(disciplina=self.disciplina, zona='Semifinales', grupo='semifinal_'+str(grupo_id+1)).order_by('-distancia')

                    #Asignamos posiciones en función del orden
                    pos_anterior = -1
                    metrica_anterior = -1
                    for n, posicion in enumerate (posiciones):
                        if posicion.distancia == metrica_anterior:
                            posicion.posicion = pos_anterior
                        else:
                            posicion.posicion = n+1
                            pos_anterior = n+1
                            metrica_anterior = posicion.distancia
                        posicion.save()  


                else:
                    posiciones = Posicion_disciplina.objects.filter(disciplina=self.disciplina, zona='Semifinales', grupo='semifinal_'+str(grupo_id+1)).order_by('tiempo')

                    #Asignamos posiciones en función del orden
                    pos_anterior = -1
                    metrica_anterior = -1
                    for n, posicion in enumerate (posiciones):
                        if posicion.tiempo == metrica_anterior:
                            posicion.posicion = pos_anterior
                        else:
                            posicion.posicion = n+1
                            pos_anterior = n+1
                            metrica_anterior = posicion.tiempo
                        posicion.save() 


            if self.disciplina.deporte.nombre in ['Natación','Ciclismo','Vela','Caminata/Carrera']:            
                posiciones = Posicion_disciplina.objects.filter(disciplina=self.disciplina, zona='Semifinales', grupo='semifinal_'+str(grupo_id+1)).order_by('tiempo')
            
                #Asignamos posiciones en función del orden
                pos_anterior = -1
                metrica_anterior = -1
                for n, posicion in enumerate (posiciones):
                    if posicion.tiempo == metrica_anterior:
                        posicion.posicion = pos_anterior
                    else:
                        posicion.posicion = n+1
                        pos_anterior = n+1
                        metrica_anterior = posicion.tiempo
                    posicion.save()  


            #Reescribimos posiciones de no presentados.
            for n, posicion in enumerate (posiciones):
                if (posicion.cantidad == -2 or posicion.distancia == -2 or 
                    posicion.peso == -2 or posicion.cantidad == -2 or
                    posicion.tiempo == 1000000):
                    posicion.posicion = 99
                    posicion.save() 



    def seleccionar_finalistas(self):
        # Carga los clasificados de los grupos
        if self.nrondas == 2:
            print ('')
            print ('Copiando Finalistas de los Grupos')
            if self.disciplina.tipo in 'Grupal':
                grupos = [self.grupo_e1,self.grupo_e2,self.grupo_e3,self.grupo_e4,self.grupo_e5,
                          self.grupo_e6,self.grupo_e7,self.grupo_e8]
            else:
                grupos = [self.grupo_p1,self.grupo_p2,self.grupo_p3,self.grupo_p4,self.grupo_p5,
                          self.grupo_p6,self.grupo_p7,self.grupo_p8]


            #Selecionamos los Finalistas Indicados de cada Grupo
            for grupo_id, integrantes_grupo in enumerate(grupos):
                nclasificados=getattr(self, 'nclasificados_grupo'+str(grupo_id+1))
                posiciones = Posicion_disciplina.objects.filter(disciplina=self.disciplina, zona='Clasificatoria', grupo='grupo_'+str(grupo_id+1)).order_by('posicion')

                for i in range(0,nclasificados):
                    if self.disciplina.tipo in 'Grupal':
                        self.equipos_finalistas.add(posiciones[i].equipo)
                    else:
                        self.participantes_finalistas.add(posiciones[i].participante)
                        print ('Copiando ', posiciones[i].participante)


            #Seleccionamos Mejores Segundos/Terceros de los Grupos --------------------------------------------------         
            if (self.nclasificados_mejores_segundos_clasificacion or self.nclasificados_mejores_terceros_clasificacion):
                print ('')
                print ('Evaluando Mejores Segundos/Terceros')
                for grupo_id, integrantes_grupo in enumerate(grupos):
                    nclasificados=getattr(self, 'nclasificados_grupo'+str(grupo_id+1))
                    for i in range(nclasificados,integrantes_grupo.count()):
                        #Copiamos Posicion_disciplina de todos los participantes menos los clasificados
                        if self.disciplina.tipo in 'Grupal':
                            posicion = Posicion_disciplina.objects.get(
                                equipo = integrantes_grupo.all()[i],
                                disciplina=self.disciplina, zona='Clasificatoria', 
                                grupo='grupo_'+str(grupo_id+1))
                        else:
                            posicion = Posicion_disciplina.objects.get(
                                participante = integrantes_grupo.all()[i],
                                disciplina=self.disciplina, zona='Clasificatoria', 
                                grupo='grupo_'+str(grupo_id+1))

                        #hacemos copia de objeto
                        posicion.pk = None
                        posicion.grupo = 'Segundos'
                        posicion.zona = 'Segundos'
                        posicion.save()


                #Las ordenamos con el criterio de la disciplina
                if self.disciplina.deporte.nombre in ['Pesca']:
                    if self.disicplina.nombre in ['Cantidad']:
                        posiciones = Posicion_disciplina.objects.filter(disciplina=self.disciplina, zona='Segundos').order_by('-cantidad')
                    if self.disicplina.nombre in ['Pieza Mayor']:
                        posiciones = Posicion_disciplina.objects.filter(disciplina=self.disciplina, zona='Segundos').order_by('-peso')

                if self.disciplina.deporte.nombre in ['Atletismo']:
                    if self.disciplina.nombre in ['Salto en Largo']:
                        posiciones = Posicion_disciplina.objects.filter(disciplina=self.disciplina, zona='Segundos').order_by('-distancia')
                    else:
                        posiciones = Posicion_disciplina.objects.filter(disciplina=self.disciplina, zona='Segundos').order_by('tiempo')

                if self.disciplina.deporte.nombre in ['Natación','Ciclismo','Vela','Caminata/Carrera']:            
                    posiciones = Posicion_disciplina.objects.filter(disciplina=self.disciplina, zona='Segundos').order_by('tiempo')


                
                #Se agregan los mejores segundos/terceros al grupo de finalistas.
                if self.nclasificados_mejores_segundos_clasificacion:
                    nclasificantes = self.nclasificados_mejores_segundos_clasificacion
                if self.nclasificados_mejores_terceros_clasificacion:
                    nclasificantes = self.nclasificados_mejores_terceros_clasificacion

                nagregados=0
                for posicion in posiciones: 
                    if nagregados == nclasificantes:
                        break
                    if self.disciplina.tipo in 'Grupal':
                        if posicion not in self.equipos_finalistas.all():
                            self.equipos_finalistas.add(posicion.equipo)
                            nagregados = nagregados + 1
                    else:
                        if posicion not in self.participantes_finalistas.all():
                            self.participantes_finalistas.add(posicion.participante)
                            nagregados = nagregados + 1

            self.save()

        # Carga los clasificados de las semifinales
        if self.nrondas == 3:
            print ('')
            print ('Copiando Finalistas de las Semis')
            if self.disciplina.tipo in 'Grupal':
                grupos = [self.semifinal_e1,self.semifinal_e2,self.semifinal_e3,self.semifinal_e4]
            else:
                grupos = [self.semifinal_p1,self.semifinal_p2,self.semifinal_p3,self.semifinal_p4]


            #Selecionamos los Finalistas Indicados de cada Semifinal
            for grupo_id, integrantes_grupo in enumerate(grupos):
                nclasificados=getattr(self, 'nclasificados_semifinal'+str(grupo_id+1))
                posiciones = Posicion_disciplina.objects.filter(disciplina=self.disciplina, zona='Semifinales', grupo='semifinal_'+str(grupo_id+1)).order_by('posicion')

                for i in range(0,nclasificados):
                    if self.disciplina.tipo in 'Grupal':
                        self.equipos_finalistas.add(posiciones[i].equipo)
                    else:
                        self.participantes_finalistas.add(posiciones[i].participante)
                        print ('Agregando Finalista: ',posiciones[i].participante)


            #Seleccionamos Mejores Segundos/Terceros de los Grupos --------------------------------------------------         
            if (self.nclasificados_mejores_segundos_clasificacion or self.nclasificados_mejores_terceros_clasificacion):
                print ('')
                print ('Evaluando Mejores Segundos/Terceros')
                for grupo_id, integrantes_grupo in enumerate(grupos):
                    nclasificados=getattr(self, 'nclasificados_semifinal'+str(grupo_id+1))
                    for i in range(nclasificados,integrantes_grupo.count()):
                        #Copiamos Posicion_disciplina de todos los participantes menos los clasificados
                        if self.disciplina.tipo in 'Grupal':
                            posicion = Posicion_disciplina.objects.get(
                                equipo = integrantes_grupo.all()[i],
                                disciplina=self.disciplina, zona='Semifinales', 
                                grupo='semifinal_'+str(grupo_id+1))
                        else:
                            posicion = Posicion_disciplina.objects.get(
                                participante = integrantes_grupo.all()[i],
                                disciplina=self.disciplina, zona='Semifinales', 
                                grupo='semifinal_'+str(grupo_id+1))

                        #hacemos copia de objeto
                        posicion.pk = None
                        posicion.grupo = 'Segundos'
                        posicion.zona = 'Segundos'
                        posicion.save()


                #Las ordenamos con el criterio de la disciplina
                if self.disciplina.deporte.nombre in ['Pesca']:
                    if self.disicplina.nombre in ['Cantidad']:
                        posiciones = Posicion_disciplina.objects.filter(disciplina=self.disciplina, zona='Segundos').order_by('-cantidad')
                    if self.disicplina.nombre in ['Pieza Mayor']:
                        posiciones = Posicion_disciplina.objects.filter(disciplina=self.disciplina, zona='Segundos').order_by('-peso')

                if self.disciplina.deporte.nombre in ['Atletismo']:
                    if self.disciplina.nombre in ['Salto en Largo']:
                        posiciones = Posicion_disciplina.objects.filter(disciplina=self.disciplina, zona='Segundos').order_by('-distancia')
                    else:
                        posiciones = Posicion_disciplina.objects.filter(disciplina=self.disciplina, zona='Segundos').order_by('tiempo')

                if self.disciplina.deporte.nombre in ['Natación','Ciclismo','Vela','Caminata/Carrera']:            
                    posiciones = Posicion_disciplina.objects.filter(disciplina=self.disciplina, zona='Segundos').order_by('tiempo')

                #Se agregan los mejores segundos/terceros al grupo de finalistas.
                if self.nclasificados_mejores_segundos_clasificacion:
                    nclasificantes = self.nclasificados_mejores_segundos_clasificacion
                if self.nclasificados_mejores_terceros_clasificacion:
                    nclasificantes = self.nclasificados_mejores_terceros_clasificacion

                nagregados=0
                for posicion in posiciones: 
                    if nagregados == nclasificantes:
                        break
                    if self.disciplina.tipo in 'Grupal':
                        if posicion not in self.equipos_finalistas.all():
                            self.equipos_finalistas.add(posicion.equipo)
                            nagregados = nagregados + 1
                    else:
                        if posicion not in self.participantes_finalistas.all():
                            self.participantes_finalistas.add(posicion.participante)
                            nagregados = nagregados + 1
                            print ('Agregando Finalista 2/3ros: ',posicion.participante)

            self.save()




    def iniciar_final(self):
        #Seleccionamos Semifinalistas
        self.seleccionar_finalistas()

        #Inicializamos la competencia final
        competencia = Competencia.objects.create(disciplina=self.disciplina, zona='Final', grupo='final' )
        if self.disciplina.tipo in 'Grupal':            
            for equipo in self.equipos_finalistas.all():
                competencia.equipos.add (equipo)

        if self.disciplina.tipo in 'Individual':
            for persona in self.participantes_finalistas.all():    
                competencia.participantes.add (persona)


        #Inicializamos las entradas de la tabla de posiciones
        if self.disciplina.tipo in 'Grupal':
            for equipo in self.equipos_finalistas.all():
                Posicion_disciplina.objects.create(disciplina=self.disciplina, equipo=equipo, zona='Final')
        else:
            for participante in self.participantes_finalistas.all():    
                Posicion_disciplina.objects.create(disciplina=self.disciplina, participante=participante, zona='Final')

        self.fase_final_iniciada = True
        self.save()


    def reiniciar_final(self):
        competencias = Competencia.objects.filter(disciplina=self.disciplina, zona='Final')
        for competencia in competencias:
            competencia.delete()

        #Inicializamos/Reiniciamos las entradas de la tabla de posiciones
        posiciones = Posicion_disciplina.objects.filter(disciplina=self.disciplina, zona='Final')
        for posicion in posiciones:
            posicion.delete()                  

        posiciones = Posicion_disciplina.objects.filter(disciplina=self.disciplina, zona='Segundos')
        for posicion in posiciones:
            posicion.delete()

        #Reiniciamos Grupo de Finalistas
        self.participantes_finalistas.clear()
        self.equipos_finalistas.clear()


        #Actualizamos Flags
        self.fase_final_iniciada = False
        self.save()
    
        self.disciplina.finalizada = False
        self.disciplina.save()



    def actualizar_final(self):
        #Cargamos equipos/participantes
        if self.disciplina.tipo in 'Grupal':
            self.equipos = self.equipos_finalistas.all().order_by('nombre')               
        else:
            self.participantes = self.participantes_finalistas.all().order_by('nombre')


        #Inicializamos/Reiniciamos las entradas de la tabla de posiciones
        if self.disciplina.tipo in 'Grupal':
            for equipo in self.equipos:
                if not Posicion_disciplina.objects.filter(disciplina=self.disciplina, equipo=equipo, zona='Final').exists():
                    Posicion_disciplina.objects.create(disciplina=self.disciplina, equipo=equipo, zona='Final')
                else:
                    posicion = Posicion_disciplina.objects.get(disciplina=self.disciplina, equipo=equipo, zona='Final')
                    posicion.reset_values()
                    posicion.save()                   
        else:
            for participante in self.participantes:
                if not Posicion_disciplina.objects.filter(disciplina=self.disciplina, participante=participante, zona='Final').exists():
                    Posicion_disciplina.objects.create(disciplina=self.disciplina, participante=participante, zona='Final')
                else:
                    posicion = Posicion_disciplina.objects.get(disciplina=self.disciplina, participante=participante, zona='Final')
                    posicion.reset_values()
                    posicion.save()   

        #Evaluamos los resultados de la competencia
        competencia = Competencia.objects.get(disciplina=self.disciplina, zona='Final')
            
        #Chequeamos que la cantidad de resultados cargados sea igual al numero de participantes
        if self.disciplina.tipo in 'Grupal':
            if not len(competencia.resultados.all()) == len(competencia.equipos.all()):
                raise ValueError (str(competencia)+': resultados faltantes, no se puede evaluar.')    
        else:
            if not len(competencia.resultados.all()) == len(competencia.participantes.all()):
                raise ValueError (str(competencia)+': resultados faltantes, no se puede evaluar.')    


        #Chequeamos que los resultados hayan sido cargados 
        resultado_faltante = False
        for resultado in competencia.resultados.all():
            if (resultado.posicion == -1 
                and resultado.puntos_favor == -1
                and resultado.puntos_favor_1set == -1
                and resultado.games_favor_1set == -1
                and resultado.tiempo == -1
                and resultado.distancia == -1
                and resultado.cantidad == -1
                and resultado.peso == -1
                and resultado.presentado is True):
                print ('Resultado no cargado, no se puede evaluar.')     
                resultado_faltante = True
                break
        if resultado_faltante:
            raise ValueError (str(competencia)+': resultados faltantes, no se puede evaluar.')


        #Actualizamos los resultados de las posiciones de la disciplina
        # print ('Nombre Competencia: '+competencia.nombre)
        for resultado in competencia.resultados.all():
            if resultado.competencia.disciplina.tipo in 'Grupal':
                posicion = Posicion_disciplina.objects.get(disciplina=self.disciplina, equipo=resultado.equipo, zona='Final')               
            else:
                posicion = Posicion_disciplina.objects.get(disciplina=self.disciplina, participante=resultado.participante, zona='Final')

            if self.disciplina.deporte.nombre in ['Natación','Ciclismo','Vela','Caminata/Carrera','Atletismo','Pesca']:
                # posicion.tiempo = resultado.tiempo
                # posicion.distancia = resultado.distancia
                # posicion.peso = resultado.peso
                # posicion.cantidad = resultado.cantidad
                # posicion.save()

                if resultado.presentado:
                    posicion.tiempo = resultado.tiempo
                    posicion.distancia = resultado.distancia
                    posicion.peso = resultado.peso
                    posicion.cantidad = resultado.cantidad
                    posicion.save()
                else:
                    posicion.tiempo = 1000000
                    posicion.distancia = -2
                    posicion.peso = -2
                    posicion.cantidad = -2
                    posicion.save()
            else:
                raise ValueError ('Error: La modalidad CompetenciaÚnica solo es válida para Natación, Ciclismo, Vela, Caminata/Carrera, Atletismo y Pesca.')    

            
        self.disciplina.finalizada = True
        self.disciplina.save()

        #Actualizamos la tabla de la disciplina, deporte y copa conjunto.                                              
        self.actualizar_tabla_final()            
            

        
    def actualizar_tabla_final (self):           
        if self.disciplina.deporte.nombre in ['Pesca']:
            if self.disicplina.nombre in ['Cantidad']:
                posiciones = Posicion_disciplina.objects.filter(disciplina=self.disciplina, zona='Final').order_by('-cantidad')
                #Asignamos posiciones en función del orden
                pos_anterior = -1
                metrica_anterior = -1
                for n, posicion in enumerate (posiciones):
                    if posicion.cantidad == metrica_anterior:
                        posicion.posicion = pos_anterior
                    else:
                        posicion.posicion = n+1
                        pos_anterior = n+1
                        metrica_anterior = posicion.cantidad
                    posicion.save() 

            if self.disicplina.nombre in ['Pieza Mayor']:
                posiciones = Posicion_disciplina.objects.filter(disciplina=self.disciplina, zona='Final').order_by('-peso')

                #Asignamos posiciones en función del orden
                pos_anterior = -1
                metrica_anterior = -1
                for n, posicion in enumerate (posiciones):
                    if posicion.peso == metrica_anterior:
                        posicion.posicion = pos_anterior
                    else:
                        posicion.posicion = n+1
                        pos_anterior = n+1
                        metrica_anterior = posicion.peso
                    posicion.save()  



        if self.disciplina.deporte.nombre in ['Atletismo']:
            if self.disciplina.nombre in ['Salto en Largo']:
                posiciones = Posicion_disciplina.objects.filter(disciplina=self.disciplina, zona='Final').order_by('-distancia')

                #Asignamos posiciones en función del orden
                pos_anterior = -1
                metrica_anterior = -1
                for n, posicion in enumerate (posiciones):
                    if posicion.distancia == metrica_anterior:
                        posicion.posicion = pos_anterior
                    else:
                        posicion.posicion = n+1
                        pos_anterior = n+1
                        metrica_anterior = posicion.distancia
                    posicion.save()  


            else:
                posiciones = Posicion_disciplina.objects.filter(disciplina=self.disciplina, zona='Final').order_by('tiempo')

                #Asignamos posiciones en función del orden
                pos_anterior = -1
                metrica_anterior = -1
                for n, posicion in enumerate (posiciones):
                    if posicion.tiempo == metrica_anterior:
                        posicion.posicion = pos_anterior
                    else:
                        posicion.posicion = n+1
                        pos_anterior = n+1
                        metrica_anterior = posicion.tiempo
                    posicion.save() 


        if self.disciplina.deporte.nombre in ['Natación','Ciclismo','Vela','Caminata/Carrera']:            
            posiciones = Posicion_disciplina.objects.filter(disciplina=self.disciplina, zona='Final').order_by('tiempo')
        
            #Asignamos posiciones en función del orden
            pos_anterior = -1
            metrica_anterior = -1
            for n, posicion in enumerate (posiciones):
                if posicion.tiempo == metrica_anterior:
                    posicion.posicion = pos_anterior
                else:
                    posicion.posicion = n+1
                    pos_anterior = n+1
                    metrica_anterior = posicion.tiempo
                posicion.save()  


        #Reescribimos posiciones de no presentados.
        for n, posicion in enumerate (posiciones):
            if (posicion.cantidad == -2 or posicion.distancia == -2 or 
                posicion.peso == -2 or posicion.cantidad == -2 or
                posicion.tiempo == 1000000):
                posicion.posicion = 99
                posicion.save() 


        #Actualizamos la Tabla del Deporte / CopaConjunto. 
        if self.disciplina.finalizada:
            self.disciplina.olimpiada.actualizar_tablas_deportes()





















class Grupos_mas_Playoff (models.Model):
    disciplina = models.ForeignKey('Disciplina', related_name='grupos_mas_playoff', on_delete=models.CASCADE)
   
    #Campos para definir estructura fase de grupos y clasificados
    min_participantes_por_grupo = models.IntegerField(default=2)

    grupo_e1 = models.ManyToManyField('Equipo', related_name='grupoe1', blank=True, verbose_name='Grupo1 (Equipos)')
    grupo_e2 = models.ManyToManyField('Equipo', related_name='grupoe2', blank=True, verbose_name='Grupo2 (Equipos)')
    grupo_e3 = models.ManyToManyField('Equipo', related_name='grupoe3', blank=True, verbose_name='Grupo3 (Equipos)')
    grupo_e4 = models.ManyToManyField('Equipo', related_name='grupoe4', blank=True, verbose_name='Grupo4 (Equipos)')
    grupo_e5 = models.ManyToManyField('Equipo', related_name='grupoe5', blank=True, verbose_name='Grupo5 (Equipos)')
    grupo_e6 = models.ManyToManyField('Equipo', related_name='grupoe6', blank=True, verbose_name='Grupo6 (Equipos)')
    grupo_e7 = models.ManyToManyField('Equipo', related_name='grupoe7', blank=True, verbose_name='Grupo7 (Equipos)')
    grupo_e8 = models.ManyToManyField('Equipo', related_name='grupoe8', blank=True, verbose_name='Grupo8 (Equipos)')
    grupo_e9 = models.ManyToManyField('Equipo', related_name='grupoe9', blank=True, verbose_name='Grupo9 (Equipos)')
    grupo_e10 = models.ManyToManyField('Equipo', related_name='grupoe10', blank=True, verbose_name='Grupo10 (Equipos)')
    equipos_finalistas = models.ManyToManyField('Equipo', related_name='finalistas', blank=True, verbose_name='Finalistas (Equipos)')

    grupo_p1 = models.ManyToManyField('Persona', related_name='grupop1', blank=True, verbose_name='Grupo1 (Jugadores)')
    grupo_p2 = models.ManyToManyField('Persona', related_name='grupop2', blank=True, verbose_name='Grupo2 (Jugadores)')
    grupo_p3 = models.ManyToManyField('Persona', related_name='grupop3', blank=True, verbose_name='Grupo3 (Jugadores)')
    grupo_p4 = models.ManyToManyField('Persona', related_name='grupop4', blank=True, verbose_name='Grupo4 (Jugadores)')
    grupo_p5 = models.ManyToManyField('Persona', related_name='grupop5', blank=True, verbose_name='Grupo5 (Jugadores)')
    grupo_p6 = models.ManyToManyField('Persona', related_name='grupop6', blank=True, verbose_name='Grupo6 (Jugadores)')
    grupo_p7 = models.ManyToManyField('Persona', related_name='grupop7', blank=True, verbose_name='Grupo7 (Jugadores)')
    grupo_p8 = models.ManyToManyField('Persona', related_name='grupop8', blank=True, verbose_name='Grupo8 (Jugadores)')
    grupo_p9 = models.ManyToManyField('Persona', related_name='grupop9', blank=True, verbose_name='Grupo9 (Jugadores)')
    grupo_p10 = models.ManyToManyField('Persona', related_name='grupop10', blank=True, verbose_name='Grupo10 (Jugadores)')
    participantes_finalistas = models.ManyToManyField('Persona', related_name='finalistas', blank=True, verbose_name='Finalistas (Jugadores)')

    nclasificados_grupo1 =  models.IntegerField(default=0)
    nclasificados_grupo2 =  models.IntegerField(default=0)
    nclasificados_grupo3 =  models.IntegerField(default=0)
    nclasificados_grupo4 =  models.IntegerField(default=0)
    nclasificados_grupo5 =  models.IntegerField(default=0)
    nclasificados_grupo6 =  models.IntegerField(default=0)
    nclasificados_grupo7 =  models.IntegerField(default=0)
    nclasificados_grupo8 =  models.IntegerField(default=0)
    nclasificados_grupo9 =  models.IntegerField(default=0)
    nclasificados_grupo10 =  models.IntegerField(default=0)
    nclasificados_mejores_segundos =  models.IntegerField(default=0)
    nclasificados_mejores_terceros =  models.IntegerField(default=0)
    
    fase_grupos_iniciada = models.BooleanField('Fase de Grupos Iniciada?', default=False)
    fase_grupos_finalizada = models.BooleanField('Fase de Grupos Finalizada?', default=False)
    playoff_iniciado = models.BooleanField('Playoff Iniciado?', default=False)

    #METODOS
    def __str__(self):
        return str(self.disciplina) + ' | ' + str(self.disciplina.deporte.nombre)
    

    def reiniciar_modalidad(self):
        competencias = Competencia.objects.filter(disciplina=self.disciplina)
        for competencia in competencias:
            competencia.delete()

        #Inicializamos/Reiniciamos las entradas de la tabla de posiciones
        posiciones = Posicion_disciplina.objects.filter(disciplina=self.disciplina)
        for posicion in posiciones:
            posicion.delete()

        #Eliminamos Playoff Asociados
        if self.disciplina.playoff.count():
            self.disciplina.playoff.first().delete()

        #Actualizamos Flags
        self.disciplina.fixture_creado = False
        self.disciplina.finalizada = False
        self.disciplina.save()

        #Se actualiza la tabla del deporte/copa conjunto. 
        self.disciplina.olimpiada.actualizar_tablas_deportes()

        self.delete()



    def iniciar_grupos(self):
        #Inicializamos las competencias correspondientes de cada grupo 
        if self.disciplina.tipo in 'Grupal':
            grupos = [self.grupo_e1,self.grupo_e2,self.grupo_e3,self.grupo_e4,self.grupo_e5,
                      self.grupo_e6,self.grupo_e7,self.grupo_e8,self.grupo_e9,self.grupo_e10]
            for grupo_id, grupo in enumerate(grupos):
                for i, equipo_ref in enumerate (grupo.all()):
                    for j, equipo in enumerate (grupo.all()):
                        if i>=j : #esta condición evita que se creen 2 competencias con los mismos equipos
                                 #si se lo visualiza en una matriz es claro, siempre se crean las competencias
                                 #a la derecha de la diagonal principal
                            # print ('descartado:',i,j)
                            continue
                        else:
                            # print ('competencia creada: ',i,j)
                            competencia = Competencia.objects.create(disciplina=self.disciplina, zona='Clasificatoria', grupo='grupo_'+str(grupo_id+1) )
                            competencia.equipos.add (equipo)
                            competencia.equipos.add (equipo_ref)

        if self.disciplina.tipo in 'Individual':
            grupos = [self.grupo_p1,self.grupo_p2,self.grupo_p3,self.grupo_p4,self.grupo_p5,
                      self.grupo_p6,self.grupo_p7,self.grupo_p8,self.grupo_p9,self.grupo_p10]
            for grupo_id, grupo in enumerate(grupos):
                for i, persona_ref in enumerate (grupo.all()):
                    for j, persona in enumerate (grupo.all()):
                        if i>=j : #esta condición evita que se creen 2 competencias con los mismos equipos
                                 #si se lo visualiza en una matriz es claro, siempre se crean las competencias
                                 #a la derecha de la diagonal principal
                            # print ('descartado:',i,j)
                            continue
                        else:
                            # print ('competencia creada: ',i,j)
                            competencia = Competencia.objects.create(disciplina=self.disciplina, zona='Clasificatoria', grupo='grupo_'+str(grupo_id+1) )
                            competencia.participantes.add (persona)
                            competencia.participantes.add (persona_ref)

        #Inicializamos las entradas de la tabla de posiciones
        if self.disciplina.tipo in 'Grupal':
            for grupo_id, grupo in enumerate(grupos):
                for equipo in grupo.all():
                    Posicion_disciplina.objects.create(disciplina=self.disciplina, equipo=equipo, zona='Clasificatoria', grupo='grupo_'+str(grupo_id+1))
        else:
            for grupo_id, grupo in enumerate(grupos):
                for participante in grupo.all():
                    Posicion_disciplina.objects.create(disciplina=self.disciplina, participante=participante, zona='Clasificatoria', grupo='grupo_'+str(grupo_id+1))

        self.fase_grupos_iniciada = True
        self.save()


    def reiniciar_grupos(self):
        competencias = Competencia.objects.filter(disciplina=self.disciplina, zona='Clasificatoria')
        for competencia in competencias:
            competencia.delete()

        #Inicializamos/Reiniciamos las entradas de la tabla de posiciones
        posiciones = Posicion_disciplina.objects.filter(disciplina=self.disciplina, zona='Clasificatoria')
        for posicion in posiciones:
            posicion.delete()                  

        #Actualizamos Flags
        self.fase_grupos_iniciada = False
        self.fase_grupos_finalizada = False
        self.save()
    
        self.disciplina.finalizada = False
        self.disciplina.save()



    def actualizar_grupos(self):
        self.fase_grupos_finalizada = True
        self.save()

        if self.disciplina.tipo in 'Grupal':
            grupos = [self.grupo_e1,self.grupo_e2,self.grupo_e3,self.grupo_e4,self.grupo_e5,
                      self.grupo_e6,self.grupo_e7,self.grupo_e8,self.grupo_e9,self.grupo_e10]
        else:
            grupos = [self.grupo_p1,self.grupo_p2,self.grupo_p3,self.grupo_p4,self.grupo_p5,
                      self.grupo_p6,self.grupo_p7,self.grupo_p8,self.grupo_p9,self.grupo_p10]

        for grupo_id, integrantes_grupo in enumerate(grupos):
            
            # print ('grupo: '+str(grupo_id)+'nintegrantes grupo: '+str(integrantes_grupo.count()))
            if integrantes_grupo.count() < self.min_participantes_por_grupo: 
                # print ('salteando grupo')
                continue

            #Cargamos equipos/participantes y chequeamos si están todos los necesarios
            if self.disciplina.tipo in 'Grupal':
                self.equipos = integrantes_grupo.all().order_by('nombre')               
            else:
                self.participantes = integrantes_grupo.all().order_by('nombre')


            #Inicializamos/Reiniciamos las entradas de la tabla de posiciones
            if self.disciplina.tipo in 'Grupal':
                for equipo in self.equipos:
                    if not Posicion_disciplina.objects.filter(disciplina=self.disciplina, equipo=equipo, zona='Clasificatoria', grupo='grupo_'+str(grupo_id+1)).exists():
                        Posicion_disciplina.objects.create(disciplina=self.disciplina, equipo=equipo, zona='Clasificatoria', grupo='grupo_'+str(grupo_id+1))
                    else:
                        posicion = Posicion_disciplina.objects.get(disciplina=self.disciplina, equipo=equipo, zona='Clasificatoria', grupo='grupo_'+str(grupo_id+1))
                        posicion.reset_values()
                        posicion.save()                   
            else:
                for participante in self.participantes:
                    if not Posicion_disciplina.objects.filter(disciplina=self.disciplina, participante=participante, zona='Clasificatoria', grupo='grupo_'+str(grupo_id+1)).exists():
                        Posicion_disciplina.objects.create(disciplina=self.disciplina, participante=participante, zona='Clasificatoria', grupo='grupo_'+str(grupo_id+1))
                    else:
                        posicion = Posicion_disciplina.objects.get(disciplina=self.disciplina, participante=participante, zona='Clasificatoria', grupo='grupo_'+str(grupo_id+1))
                        posicion.reset_values()
                        posicion.save()   
                   

            #Evaluamos los resultados de cada competencia
            for competencia in Competencia.objects.filter(disciplina=self.disciplina, zona='Clasificatoria', grupo='grupo_'+str(grupo_id+1)).order_by('pk'):         

                #Lógica Deportes Grupales
                if competencia.disciplina.tipo in 'Grupal':                
                    #Eliminamos resultados de equipos que no forman parte de la competencia
                    for resultado in competencia.resultados.all():
                        borrar_resultado=True
                        for equipo in competencia.equipos.all():
                            if (resultado.equipo == equipo):
                                borrar_resultado=False
                        if borrar_resultado:
                            print ('Resultado incorrecto de disciplina eliminado: ' + resultado.equipo.nombre)
                            resultado.delete()
                    

                    #Chequeamos que la cantidad de resultados cargados sea igual al numero de equipos
                    if len(competencia.resultados.all()) ==  len(competencia.equipos.all()): 
                        resultado0 = competencia.resultados.all()[0]
                        resultado1 = competencia.resultados.all()[1]
                        resultado0_dict = Resultado.objects.filter(pk=resultado0.pk).values().first()
                        resultado1_dict = Resultado.objects.filter(pk=resultado1.pk).values().first()

                    else:         #resultados o equipos faltantes
                        self.fase_grupos_finalizada = False
                        self.save()
                        print (competencia.nombre+' > Resultados faltantes, no se puede evaluar.')    
                        continue

                    #Chequeamos que los resultados hayan sido cargados 
                    resultado_faltante = False
                    for resultado in competencia.resultados.all():
                        if (resultado.posicion == -1 
                            and resultado.puntos_favor == -1
                            and resultado.puntos_favor_1set == -1
                            and resultado.games_favor_1set == -1
                            and resultado.tiempo == -1
                            and resultado.distancia == -1
                            and resultado.cantidad == -1
                            and resultado.peso == -1
                            and resultado.presentado is True):
                            
                            self.fase_grupos_finalizada = False
                            self.save()
                            print ('Resultado no cargado, no se puede evaluar.')     
                            resultado_faltante = True
                            break
                    if resultado_faltante:
                        continue


                    #Definimos ganador/perdedor de la competencia segun las politicas de la disciplina    
                    #Deportes Grupales c/Resultados Finales
                    if self.disciplina.deporte.nombre in politicas.deportes_grupales_resultados_finales:                       
                        ganador = None
                        perdedor = None
                        # for valor_a_comparar in self.politica_grupos_segun_disciplina[self.disciplina.deporte.nombre]:
                        for valor_a_comparar in politicas.politica_grupos_segun_disciplina[self.disciplina.deporte.nombre]:
                            # print ('Comparando '+valor_a_comparar[0])                
                            
                            #comparamos los resultados para determinar ganador / perdedor
                            if ((resultado0_dict[valor_a_comparar[0]] > resultado1_dict[valor_a_comparar[0]]) and (valor_a_comparar[1] in 'mas')) or \
                               ((resultado0_dict[valor_a_comparar[0]] < resultado1_dict[valor_a_comparar[0]]) and (valor_a_comparar[1] in 'menos')):                                   
                               ganador = resultado0.equipo
                               perdedor = resultado1.equipo
                               break  #cortamos el for para evaluar esta y luego pasar a analizar la siguiente competencia.

                            elif ( ((resultado0_dict[valor_a_comparar[0]] < resultado1_dict[valor_a_comparar[0]]) and (valor_a_comparar[1] in 'mas')) or \
                               ((resultado0_dict[valor_a_comparar[0]] > resultado1_dict[valor_a_comparar[0]]) and (valor_a_comparar[1] in 'menos')) ): 
                               ganador = resultado1.equipo
                               perdedor = resultado0.equipo
                               break  #cortamos el for para evaluar esta y luego pasar a analizar la siguiente competencia.

                            else: #ambos resultados son iguales, comparamos el siguiente campo.
                                pass  


                    #Deportes Grupales c/Resultados Parciales                       
                    if self.disciplina.deporte.nombre in politicas.deportes_grupales_resultados_parciales:  
                        #Contamos los parciales ganados (ej: sets ganados) segun las politicas de la disciplina
                        resultado0.sets_favor=0
                        resultado1.sets_favor=0
                        resultado0.sets_contra=0
                        resultado1.sets_contra=0
                        # resultado0.save()
                        # resultado1.save()

                        for valor_a_comparar in politicas.politica_grupos_segun_disciplina[self.disciplina.deporte.nombre]:
                            # print ('Comparando '+valor_a_comparar[0])                                           

                            if ((resultado0_dict[valor_a_comparar[0]] > resultado1_dict[valor_a_comparar[0]]) and (valor_a_comparar[1] in 'mas')) or \
                               ((resultado0_dict[valor_a_comparar[0]] < resultado1_dict[valor_a_comparar[0]]) and (valor_a_comparar[1] in 'menos')):                                   
                                   resultado0.sets_favor=resultado0.sets_favor+1
                                   resultado1.sets_contra=resultado1.sets_contra+1
                                   continue
                              
                            elif ( ((resultado0_dict[valor_a_comparar[0]] < resultado1_dict[valor_a_comparar[0]]) and (valor_a_comparar[1] in 'mas')) or \
                                   ((resultado0_dict[valor_a_comparar[0]] > resultado1_dict[valor_a_comparar[0]]) and (valor_a_comparar[1] in 'menos')) ): 
                                       resultado1.sets_favor=resultado1.sets_favor+1
                                       resultado0.sets_contra=resultado0.sets_contra+1
                                       continue
                                    
                            else:   #ambos resultados son iguales, comparamos el siguiente parcial.
                                pass
                        
                        resultado0.save()
                        resultado1.save()

                        #Definimos ganador/perdedor de la competencia segun los sets ganados
                        ganador = None
                        perdedor = None
                        if resultado0.sets_favor > resultado1.sets_favor: #ganador equipo 0
                            ganador = resultado0.equipo
                            perdedor = resultado1.equipo
                        else:
                            ganador = resultado1.equipo
                            perdedor = resultado0.equipo


                    #Actualizamos el registro de cada participante en la 
                    #tabla de posiciones de la disciplina
                    if self.disciplina.deporte.nombre in politicas.puntos_por_competencia_segun_disciplina.keys():
                        if self.disciplina.nombre in politicas.puntos_por_competencia_segun_disciplina[self.disciplina.deporte.nombre].keys():
                            pts_ganado = politicas.puntos_por_competencia_segun_disciplina[self.disciplina.deporte.nombre][self.disciplina.nombre]['ganado']
                            pts_perdido = politicas.puntos_por_competencia_segun_disciplina[self.disciplina.deporte.nombre][self.disciplina.nombre]['perdido']
                            pts_empatado = politicas.puntos_por_competencia_segun_disciplina[self.disciplina.deporte.nombre][self.disciplina.nombre]['empatado']
                            pts_nopresentado = politicas.puntos_por_competencia_segun_disciplina[self.disciplina.deporte.nombre][self.disciplina.nombre]['nopresentado']
                    else:   
                        pts_ganado = politicas.puntos_por_competencia_segun_deporte[self.disciplina.deporte.nombre]['ganado']
                        pts_perdido = politicas.puntos_por_competencia_segun_deporte[self.disciplina.deporte.nombre]['perdido']
                        pts_empatado = politicas.puntos_por_competencia_segun_deporte[self.disciplina.deporte.nombre]['empatado']
                        pts_nopresentado = politicas.puntos_por_competencia_segun_deporte[self.disciplina.deporte.nombre]['nopresentado']


                    if ganador is not None:
                        posicion = Posicion_disciplina.objects.get(disciplina=self.disciplina, equipo=ganador, zona='Clasificatoria', grupo='grupo_'+str(grupo_id+1))
                        resultado = Resultado.objects.get(disciplina=self.disciplina, equipo=ganador, competencia=competencia)
                        if resultado.presentado:
                            posicion.puntos = posicion.puntos + pts_ganado
                            posicion.partidos_ganados = posicion.partidos_ganados + 1
                            posicion.save()
                        else:
                            posicion.puntos = posicion.puntos + pts_nopresentado
                            posicion.partidos_perdidos = posicion.partidos_perdidos + 1
                            posicion.save()

                        posicion = Posicion_disciplina.objects.get(disciplina=self.disciplina, equipo=perdedor, zona='Clasificatoria', grupo='grupo_'+str(grupo_id+1))
                        resultado = Resultado.objects.get(disciplina=self.disciplina, equipo=perdedor, competencia=competencia)
                        if resultado.presentado:
                            posicion.puntos = posicion.puntos + pts_perdido
                            posicion.partidos_perdidos = posicion.partidos_perdidos + 1
                            posicion.save()
                        else:
                            posicion.puntos = posicion.puntos + pts_nopresentado
                            posicion.partidos_perdidos = posicion.partidos_perdidos + 1
                            posicion.save()

                    else:
                        #equipos empatados suman 1 punto cada uno
                        posicion = Posicion_disciplina.objects.get(disciplina=self.disciplina, equipo=resultado0.equipo, zona='Clasificatoria', grupo='grupo_'+str(grupo_id+1))
                        posicion.puntos = posicion.puntos + pts_empatado
                        posicion.partidos_empatados = posicion.partidos_empatados + 1
                        posicion.save()

                        posicion = Posicion_disciplina.objects.get(disciplina=self.disciplina, equipo=resultado1.equipo, zona='Clasificatoria', grupo='grupo_'+str(grupo_id+1))
                        posicion.puntos = posicion.puntos + pts_empatado
                        posicion.partidos_empatados = posicion.partidos_empatados + 1
                        posicion.save()


                #Deportes Individuales
                else:
                    #Eliminamos resultados de personas que no forman parte de la competencia
                    for resultado in competencia.resultados.all():
                        borrar_resultado=True
                        for participante in competencia.participantes.all():
                            if (resultado.participante == participante):
                                borrar_resultado=False
                        if borrar_resultado:
                            print ('Resultado incorrecto de disciplina eliminado: ' + resultado.participante.apellido +', ' +resultado.participante.nombre)
                            resultado.delete()
                    

                    #Chequeamos que la cantidad de resultados cargados sea igual al numero de participantes
                    if len(competencia.resultados.all()) ==  len(competencia.participantes.all()): 
                        resultado0 = competencia.resultados.all()[0]
                        resultado1 = competencia.resultados.all()[1]
                        resultado0_dict = Resultado.objects.filter(pk=resultado0.pk).values().first()
                        resultado1_dict = Resultado.objects.filter(pk=resultado1.pk).values().first()
                    else:         #resultados o participantes faltantes
                        self.fase_grupos_finalizada = False
                        self.save()
                        print (competencia.nombre+' > Resultados faltantes, no se puede evaluar.')    
                        continue


                    #Chequeamos que los resultados hayan sido cargados 
                    resultado_faltante = False
                    for resultado in competencia.resultados.all():
                        if (resultado.posicion == -1 
                            and resultado.puntos_favor == -1
                            and resultado.puntos_favor_1set == -1
                            and resultado.games_favor_1set == -1
                            and resultado.tiempo == -1
                            and resultado.distancia == -1
                            and resultado.cantidad == -1
                            and resultado.peso == -1
                            and resultado.presentado is True):
                            
                            self.fase_grupos_finalizada = False
                            self.save()
                            print ('Resultado no cargado, no se puede evaluar.')     
                            resultado_faltante = True
                            break
                    if resultado_faltante:
                        continue


                    #Definimos ganador/perdedor de la competencia segun las politicas de la disciplina                    
                    #Deportes Individuales c/Resultados Finales                    
                    if self.disciplina.deporte.nombre in politicas.deportes_individuales_resultados_finales:  
                        ganador = None
                        perdedor = None
                        for valor_a_comparar in politicas.politica_grupos_segun_disciplina[self.disciplina.deporte.nombre]:
                            # print ('Comparando '+valor_a_comparar[0])                
                            
                            #comparamos los resultados para determinar ganador / perdedor
                            if ((resultado0_dict[valor_a_comparar[0]] > resultado1_dict[valor_a_comparar[0]]) and (valor_a_comparar[1] in 'mas')) or \
                               ((resultado0_dict[valor_a_comparar[0]] < resultado1_dict[valor_a_comparar[0]]) and (valor_a_comparar[1] in 'menos')):                                   
                               ganador = resultado0.participante
                               perdedor = resultado1.participante
                               break  #cortamos el for para evaluar esta y luego pasar a analizar la siguiente competencia.

                            elif ( ((resultado0_dict[valor_a_comparar[0]] < resultado1_dict[valor_a_comparar[0]]) and (valor_a_comparar[1] in 'mas')) or \
                               ((resultado0_dict[valor_a_comparar[0]] > resultado1_dict[valor_a_comparar[0]]) and (valor_a_comparar[1] in 'menos')) ): 
                               ganador = resultado1.participante
                               perdedor = resultado0.participante
                               break  #cortamos el for para evaluar esta y luego pasar a analizar la siguiente competencia.

                            else: #ambos resultados son iguales, comparamos el siguiente campo.
                                pass  


                    #Deportes Individuales c/Resultados Parciales
                    if self.disciplina.deporte.nombre in politicas.deportes_individuales_resultados_parciales: 
                        #Contamos los parciales ganados (ej: sets ganados) segun las politicas de la disciplina
                        resultado0.sets_favor=0
                        resultado1.sets_favor=0
                        resultado0.sets_contra=0
                        resultado1.sets_contra=0

                        for valor_a_comparar in politicas.politica_grupos_segun_disciplina[self.disciplina.deporte.nombre]:
                            # print ('Comparando '+valor_a_comparar[0])                                           

                            if ((resultado0_dict[valor_a_comparar[0]] > resultado1_dict[valor_a_comparar[0]]) and (valor_a_comparar[1] in 'mas')) or \
                               ((resultado0_dict[valor_a_comparar[0]] < resultado1_dict[valor_a_comparar[0]]) and (valor_a_comparar[1] in 'menos')):                                   
                                   resultado0.sets_favor=resultado0.sets_favor+1
                                   resultado1.sets_contra=resultado1.sets_contra+1
                                   continue
                              
                            elif ( ((resultado0_dict[valor_a_comparar[0]] < resultado1_dict[valor_a_comparar[0]]) and (valor_a_comparar[1] in 'mas')) or \
                                   ((resultado0_dict[valor_a_comparar[0]] > resultado1_dict[valor_a_comparar[0]]) and (valor_a_comparar[1] in 'menos')) ): 
                                       resultado1.sets_favor=resultado1.sets_favor+1
                                       resultado0.sets_contra=resultado0.sets_contra+1
                                       continue
                                    
                            else:   #ambos resultados son iguales, comparamos el siguiente parcial.
                                pass
                        
                        resultado0.save()
                        resultado1.save()

                        #Definimos ganador/perdedor de la competencia segun los sets ganados
                        ganador = None
                        perdedor = None
                        if resultado0.sets_favor > resultado1.sets_favor: #ganador equipo 0                        
                            ganador = resultado0.participante
                            perdedor = resultado1.participante
                        else:
                            ganador = resultado1.participante
                            perdedor = resultado0.participante


                    #Actualizamos el registro de cada participante en la 
                    #tabla de posiciones de la disciplina                                        
                    if self.disciplina.deporte.nombre in politicas.puntos_por_competencia_segun_disciplina.keys():
                        if self.disciplina.nombre in politicas.puntos_por_competencia_segun_disciplina[self.disciplina.deporte.nombre].keys():
                            pts_ganado = politicas.puntos_por_competencia_segun_disciplina[self.disciplina.deporte.nombre][self.disciplina.nombre]['ganado']
                            pts_perdido = politicas.puntos_por_competencia_segun_disciplina[self.disciplina.deporte.nombre][self.disciplina.nombre]['perdido']
                            pts_empatado = politicas.puntos_por_competencia_segun_disciplina[self.disciplina.deporte.nombre][self.disciplina.nombre]['empatado']
                            pts_nopresentado = politicas.puntos_por_competencia_segun_disciplina[self.disciplina.deporte.nombre][self.disciplina.nombre]['nopresentado']
                    else:   
                        pts_ganado = politicas.puntos_por_competencia_segun_deporte[self.disciplina.deporte.nombre]['ganado']
                        pts_perdido = politicas.puntos_por_competencia_segun_deporte[self.disciplina.deporte.nombre]['perdido']
                        pts_empatado = politicas.puntos_por_competencia_segun_deporte[self.disciplina.deporte.nombre]['empatado']
                        pts_nopresentado = politicas.puntos_por_competencia_segun_deporte[self.disciplina.deporte.nombre]['nopresentado']


                    if ganador is not None:                        
                        # posicion = Posicion_disciplina.objects.get(disciplina=self.disciplina, participante=ganador, zona='Clasificatoria', grupo='grupo_'+str(grupo_id+1))
                        # posicion.puntos = posicion.puntos + pts_ganado
                        # posicion.partidos_ganados = posicion.partidos_ganados + 1
                        # posicion.save()

                        # posicion = Posicion_disciplina.objects.get(disciplina=self.disciplina, participante=perdedor, zona='Clasificatoria', grupo='grupo_'+str(grupo_id+1))
                        # posicion.puntos = posicion.puntos + pts_perdido
                        # posicion.partidos_perdidos = posicion.partidos_perdidos + 1
                        # posicion.save()
                    
                        posicion = Posicion_disciplina.objects.get(disciplina=self.disciplina, participante=ganador, zona='Clasificatoria', grupo='grupo_'+str(grupo_id+1))
                        resultado = Resultado.objects.get(disciplina=self.disciplina, participante=ganador, competencia=competencia)
                        if resultado.presentado:
                            posicion.puntos = posicion.puntos + pts_ganado
                            posicion.partidos_ganados = posicion.partidos_ganados + 1
                            posicion.save()
                        else:
                            posicion.puntos = posicion.puntos + pts_nopresentado
                            posicion.partidos_perdidos = posicion.partidos_perdidos + 1
                            posicion.save()

                        posicion = Posicion_disciplina.objects.get(disciplina=self.disciplina, participante=perdedor, zona='Clasificatoria', grupo='grupo_'+str(grupo_id+1))
                        resultado = Resultado.objects.get(disciplina=self.disciplina, participante=perdedor, competencia=competencia)
                        if resultado.presentado:
                            posicion.puntos = posicion.puntos + pts_perdido
                            posicion.partidos_perdidos = posicion.partidos_perdidos + 1
                            posicion.save()
                        else:
                            posicion.puntos = posicion.puntos + pts_nopresentado
                            posicion.partidos_perdidos = posicion.partidos_perdidos + 1
                            posicion.save()


                    else:
                        #equipos empatados
                        posicion = Posicion_disciplina.objects.get(disciplina=self.disciplina, participante=resultado0.participante, zona='Clasificatoria', grupo='grupo_'+str(grupo_id+1))
                        posicion.puntos = posicion.puntos + pts_empatado
                        posicion.partidos_empatados = posicion.partidos_empatados + 1
                        posicion.save()
                        posicion = Posicion_disciplina.objects.get(disciplina=self.disciplina, participante=resultado1.participante, zona='Clasificatoria', grupo='grupo_'+str(grupo_id+1))
                        posicion.puntos = posicion.puntos + pts_empatado
                        posicion.partidos_empatados = posicion.partidos_empatados + 1
                        posicion.save()


                #Actualizamos los campos de posiciones_disciplinas
                for resultado in [resultado0,resultado1]:
                    # print ('Actualizando tabla grupo')         
                    if resultado.competencia.disciplina.tipo in 'Grupal':
                        posicion = Posicion_disciplina.objects.get(disciplina=self.disciplina, equipo=resultado.equipo, zona='Clasificatoria')
                    else:
                        posicion = Posicion_disciplina.objects.get(disciplina=self.disciplina, participante=resultado.participante, zona='Clasificatoria')

                    #Futbol, Hockey, Handball  
                    if self.disciplina.deporte.nombre in ['Fútbol','Hockey','Handball']:
                        posicion.goles_favor = posicion.goles_favor + resultado.goles_favor
                        posicion.goles_contra = posicion.goles_contra + resultado.goles_contra
                        posicion.goles_dif = posicion.goles_favor - posicion.goles_contra 
                        posicion.amarillas = posicion.amarillas + resultado.amarillas
                        posicion.rojas = posicion.rojas + resultado.rojas
                        posicion.verdes = posicion.verdes + resultado.verdes
                        posicion.azules = posicion.azules + resultado.azules
                        posicion.save()
                    
                    #Basquet/Bochas
                    if self.disciplina.deporte.nombre in ['Basquet','Bochas']:
                        posicion.puntos_favor = posicion.puntos_favor + resultado.puntos_favor
                        posicion.puntos_contra = posicion.puntos_contra + resultado.puntos_contra
                        posicion.puntos_dif = posicion.puntos_favor - posicion.puntos_contra
                        posicion.save()
                    
                    if (self.disciplina.deporte.nombre == 'Tenis' or 
                        self.disciplina.deporte.nombre in ['Pádel']):
                        posicion.games_favor = (posicion.games_favor + 
                                            resultado.games_favor_1set+
                                            resultado.games_favor_2set+
                                            resultado.games_favor_3set)
                        posicion.games_contra = (posicion.games_contra + 
                                             resultado.games_contra_1set+
                                             resultado.games_contra_2set+
                                             resultado.games_contra_3set)   
                        posicion.games_dif = posicion.games_favor - posicion.games_contra

                        posicion.sets_favor = posicion.sets_favor + resultado.sets_favor
                        posicion.sets_contra = posicion.sets_contra + resultado.sets_contra    
                        posicion.sets_dif = posicion.sets_favor - posicion.sets_contra

                        posicion.save()

                    if (self.disciplina.deporte.nombre in ['Voley'] or 
                        self.disciplina.deporte.nombre == 'Tenis de Mesa'):
                        posicion.puntos_favor = ( posicion.puntos_favor + 
                                                  resultado.puntos_favor_1set +
                                                  resultado.puntos_favor_2set +
                                                  resultado.puntos_favor_3set )

                        posicion.puntos_contra = (posicion.puntos_contra + 
                                                  resultado.puntos_contra_1set +
                                                  resultado.puntos_contra_2set +
                                                  resultado.puntos_contra_3set )

                        posicion.puntos_dif = posicion.puntos_favor - posicion.puntos_contra

                        posicion.sets_favor = posicion.sets_favor + resultado.sets_favor
                        posicion.sets_contra = posicion.sets_contra + resultado.sets_contra    
                        posicion.sets_dif = posicion.sets_favor - posicion.sets_contra

                        posicion.save()

                    if (self.disciplina.deporte.nombre in 
                        ['Natación','Ciclismo','Vela','Caminata/Carrera','Atletismo','Pesca']):
                        posicion.tiempo = posicion.tiempo + resultado.tiempo
                        posicion.distancia = posicion.distancia + resultado.distancia
                        posicion.save()

                                              
        self.actualizar_tabla()            
            

        
    def actualizar_tabla (self):
        if self.disciplina.tipo in 'Grupal':
            grupos = [self.grupo_e1,self.grupo_e2,self.grupo_e3,self.grupo_e4,self.grupo_e5,
                      self.grupo_e6,self.grupo_e7,self.grupo_e8,self.grupo_e9,self.grupo_e10]
                      
        else:
            grupos = [self.grupo_p1,self.grupo_p2,self.grupo_p3,self.grupo_p4,self.grupo_p5,
                      self.grupo_p6,self.grupo_p7,self.grupo_p8,self.grupo_p9,self.grupo_p10] 

        for grupo_id, integrantes_grupo in enumerate(grupos):
            #Orden de Tabla de Posiciones
            posiciones = Posicion_disciplina.objects.filter(disciplina=self.disciplina, zona='Clasificatoria', grupo='grupo_'+str(grupo_id+1)).order_by('-puntos')
            
            ordenar_posiciones_disciplina (posiciones)


    def crear_fixture (self):
        #Seleccionamos Finalistas de los Grupos --------------------------------------------------------
        print ('')
        print ('Copiando Finalistas')
        if self.disciplina.tipo in 'Grupal':
            grupos = [self.grupo_e1,self.grupo_e2,self.grupo_e3,self.grupo_e4,self.grupo_e5,
                      self.grupo_e6,self.grupo_e7,self.grupo_e8,self.grupo_e9,self.grupo_e10]
        else:
            grupos = [self.grupo_p1,self.grupo_p2,self.grupo_p3,self.grupo_p4,self.grupo_p5,
                      self.grupo_p6,self.grupo_p7,self.grupo_p8,self.grupo_p9,self.grupo_p10]

        for grupo_id, integrantes_grupo in enumerate(grupos):
            nclasificados=getattr(self, 'nclasificados_grupo'+str(grupo_id+1))
            posiciones = Posicion_disciplina.objects.filter(disciplina=self.disciplina, zona='Clasificatoria', grupo='grupo_'+str(grupo_id+1)).order_by('posicion')

            for i in range(0,nclasificados):
                if self.disciplina.tipo in 'Grupal':
                    print (i, posiciones[i].equipo)
                    self.equipos_finalistas.add(posiciones[i].equipo)
                else:
                    self.participantes_finalistas.add(posiciones[i].participante)


        #Seleccionamos Mejores Segundos de los Grupos --------------------------------------------------         
        if self.nclasificados_mejores_segundos:
            print ('')
            print ('Evaluando Mejores Segundos')
            for grupo_id, integrantes_grupo in enumerate(grupos):
                nclasificados=getattr(self, 'nclasificados_grupo'+str(grupo_id+1))
                print ('nclasificados: ',nclasificados, '| grupo: ',str(grupo_id+1))
                
                if integrantes_grupo.count() >= 2 and nclasificados >= 1:
                    print ('Se copia el segundo del grupo.')
                    segundo_clasificado = Posicion_disciplina.objects.filter(
                        disciplina=self.disciplina, zona='Clasificatoria', 
                        grupo='grupo_'+str(grupo_id+1)).order_by('-puntos')[1]
                    #hacemos copia de objeto
                    segundo_clasificado.pk = None
                    segundo_clasificado.grupo = 'Segundos'
                    segundo_clasificado.zona = 'Segundos'
                    segundo_clasificado.save()
                else:
                    print ('Grupo no se tiene en cuenta para mejores segundos.')

            #Las ordenamos con el criterio de la disciplina
            posiciones = Posicion_disciplina.objects.filter(disciplina=self.disciplina, zona='Segundos').order_by('-puntos')
            ordenar_posiciones_disciplina (posiciones)
            
            #Se agregan los mejores terceros al grupo de finalistas.
            #Si para algun grupo se fijo 3 finalistas y para el resto 2, ese 3ro es agregado inicialmente, 
            #independientemente de su performance respecto a los otros terceros.
            posiciones = Posicion_disciplina.objects.filter(disciplina=self.disciplina, zona='Segundos').order_by('posicion')
            nclasificantes = self.nclasificados_mejores_segundos
            nagregados=0
            for posicion in posiciones: 
                if nagregados == nclasificantes:
                    break
                if self.disciplina.tipo in 'Grupal':
                    if posicion not in self.equipos_finalistas.all():
                        print (i, posiciones[i].equipo)
                        self.equipos_finalistas.add(posiciones[i].equipo)
                        nagregados = nagregados + 1
                else:
                    if posicion not in self.participantes_finalistas.all():
                        print (i, posiciones[i].equipo)
                        self.participantes_finalistas.add(posiciones[i].participante)
                        nagregados = nagregados + 1


        #Seleccionamos Mejores Terceros de los Grupos --------------------------------------------------         
        #Hacemos una copia de las las posiciones_disciplinas que 
        #coinciden con el tercer equipo de cada grupo.
        #Solo se evalúan los grupos con 3 o más jugadores y
        #el número de clasificados sea mayor o igual a 2 (si hubiera
        #solo un clasificado en ese grupo el segundo quedaría afuera)
        if self.nclasificados_mejores_terceros:
            print ('Evaluando Mejores Terceros')
            for grupo_id, integrantes_grupo in enumerate(grupos):
                nclasificados=getattr(self, 'nclasificados_grupo'+str(grupo_id+1))
                print ('nclasificados: ',nclasificados, '| grupo: ',str(grupo_id+1))
                
                if integrantes_grupo.count() >= 3 and nclasificados >= 2:
                    print ('Se copia el tercero del grupo.')
                    tercer_clasificado = Posicion_disciplina.objects.filter(
                        disciplina=self.disciplina, zona='Clasificatoria', 
                        grupo='grupo_'+str(grupo_id+1)).order_by('-puntos')[2]
                    #hacemos copia de objeto
                    tercer_clasificado.pk = None
                    tercer_clasificado.grupo = 'Terceros'
                    tercer_clasificado.zona = 'Terceros'
                    tercer_clasificado.save()
                else:
                    print ('Grupo no se tiene en cuenta para mejores terceros.')

            #Las ordenamos con el criterio de la disciplina
            posiciones = Posicion_disciplina.objects.filter(disciplina=self.disciplina, zona='Terceros').order_by('-puntos')
            ordenar_posiciones_disciplina (posiciones)
            
            #Se agregan los mejores terceros al grupo de finalistas.
            #Si para algun grupo se fijo 3 finalistas y para el resto 2, ese 3ro es agregado inicialmente, 
            #independientemente de su performance respecto a los otros terceros.
            posiciones = Posicion_disciplina.objects.filter(disciplina=self.disciplina, zona='Terceros').order_by('posicion')
            nclasificantes = self.nclasificados_mejores_terceros
            nagregados=0
            for posicion in posiciones: 
                if nagregados == nclasificantes:
                    break
                if self.disciplina.tipo in 'Grupal':
                    if posicion not in self.equipos_finalistas.all():
                        print (i, posiciones[i].equipo)
                        self.equipos_finalistas.add(posiciones[i].equipo)
                        nagregados = nagregados + 1
                else:
                    if posicion not in self.participantes_finalistas.all():
                        print (i, posiciones[i].equipo)
                        self.participantes_finalistas.add(posiciones[i].participante)
                        nagregados = nagregados + 1





        #Creamos Fixture ----------------------------------------------------------------------
        if self.disciplina.tipo in 'Grupal':
            nfinalistas= self.equipos_finalistas.count()  
            print ('nfinalistas Iniciales',nfinalistas)

            #Rellenamos en caso de no tener 4,8 o 16 finalistas
            if nfinalistas > 16:
                print ('Tamaño de Playoff no soportado, njugadores > 16')
            nrellenos = 0
            if nfinalistas < 4:
                nrellenos = 4 - nfinalistas
            if nfinalistas > 4 and nfinalistas < 8:
                nrellenos = 8 - nfinalistas
            if nfinalistas > 8 and nfinalistas < 16:
                nrellenos = 16 - nfinalistas
            if nrellenos:
                print ('Agregando ',nrellenos,' relleno/s.')
                for i in range (0,nrellenos):
                    equipo_generico = Equipo.objects.create(nombre='Relleno', disciplina=self.disciplina)
                    self.equipos_finalistas.add(equipo_generico)
                print ('nfinalistas totales:', self.equipos_finalistas.count())

            #Creamos Playoff
            nfinalistas= self.equipos_finalistas.count()  
            if nfinalistas in [4,8,16]:
                playoff = Playoff.objects.create(disciplina=self.disciplina, min_participantes_playoff=nfinalistas)
                playoff.crear_fixture(integrantes=self.equipos_finalistas)
                
                #Actualizamos Flags
                self.disciplina.fixture_creado = True
                self.disciplina.save()
                self.playoff_iniciado = True
                self.save()

        else:     
            nfinalistas= self.participantes_finalistas.count()
            print ('nfinalistas Iniciales',nfinalistas)

            #Rellenamos en caso de no tener 4,8 o 16 finalistas
            if nfinalistas > 16:
                print ('Tamaño de Playoff no soportado, njugadores > 16')
                return
            nrellenos = 0
            if nfinalistas < 4:
                nrellenos = 4 - nfinalistas
            if nfinalistas > 4 and nfinalistas < 8:
                nrellenos = 8 - nfinalistas
            if nfinalistas > 8 and nfinalistas < 16:
                nrellenos = 16 - nfinalistas
            if nrellenos:               
                participantes_genericos = Persona.objects.filter(nombre='Relleno')
                print ('Agregando ',nrellenos,' relleno/s. ')
                for i in range (0,nrellenos):
                    self.disciplina.participantes.add(participantes_genericos[i])
                    self.participantes_finalistas.add(participantes_genericos[i])

                print ('nfinalistas Totales:', self.participantes_finalistas.count())

            #Creamos Playoff
            nfinalistas= self.participantes_finalistas.count()
            if nfinalistas in [4,8,16]:
                playoff = Playoff.objects.create(disciplina=self.disciplina, min_participantes_playoff=nfinalistas)
                playoff.crear_fixture(integrantes=self.participantes_finalistas)
                
                #Actualizamos Flags
                self.disciplina.fixture_creado = True
                self.disciplina.save()
                self.playoff_iniciado = True
                self.save()
            


    def actualizar_fixture (self):
        self.disciplina.playoff.first().actualizar_fixture()  


    def reiniciar_fixture (self):
        #1) Borrar competencias asociadas.
        competencias=Competencia.objects.filter(disciplina=self.disciplina.pk, zona='Eliminatoria')
        for competencia in competencias:
            competencia.delete()
        
        #2) Borramos posiciones_disciplinas asociadas
        posiciones=Posicion_disciplina.objects.filter(disciplina=self.disciplina.pk, zona='Eliminatoria')
        for posicion in posiciones:
            posicion.delete()

        #3) Limpiamos Equipos Finalistas
        if self.disciplina.tipo in 'Grupal':
            self.equipos_finalistas.clear()  
            self.save()
        else:     
            self.participantes_finalistas.clear()
            self.save()

        #4) Actualizamos Flags
        self.disciplina.fixture_creado = False
        self.disciplina.finalizada = False
        self.disciplina.save()

        self.playoff_iniciado = False
        self.save()

        #5) Se actualiza la tabla del deporte/copa conjunto. 
        self.disciplina.olimpiada.actualizar_tablas_deportes()

        #6) Borramos y creamos nuevo Fixture
        self.disciplina.playoff.first().delete()
        self.crear_fixture()



def ordenar_posiciones_disciplina (posiciones):
    #Asignamos posiciones en función de los puntos de los equipos/competidores
    for n, posicion in enumerate (posiciones):
        posicion.posicion = n+1
        posicion.save()

    #Si hay equipos/competidores con los mismos puntos lo derimimos
    #con las reglas de la disciplina
    for posicion_ref in posiciones:
        pk_empatados = []
        pos_empatados = []

        #Verificamos si hay posiciones empatadas
        for posicion in posiciones: 
            if posicion_ref.pk is not posicion.pk:
                if posicion_ref.puntos == posicion.puntos:
                    pk_empatados.append(posicion.pk)
                    pos_empatados.append(posicion.posicion)

        if pk_empatados: #si hay al menos una posicion empatada
            pk_empatados.append(posicion_ref.pk)
            pos_empatados.append(posicion_ref.posicion)               

            # print ('')
            # print ('')
            # for valor_a_comparar in self.politica_segun_deporte_tabla[self.disciplina.deporte.nombre]:                   
            for valor_a_comparar in politicas.politica_segun_deporte_tabla[posicion_ref.disciplina.deporte.nombre]:                   
                #Reasignamos ordenes en función del campo comparado.
                queryset_reordenado = Posicion_disciplina.objects.filter(pk__in=pk_empatados).order_by(valor_a_comparar[1])
                pos_empatados.sort()
                # print (pk_empatados)
                # print (queryset_reordenado)
                for i, posicion in enumerate (queryset_reordenado):
                    posicion.posicion = pos_empatados[i]
                    posicion.save()

                # for posicion_ref in queryset_reordenado:
                pk_empatados = []
                pos_empatados = []
                for posicion in queryset_reordenado: 
                    if posicion_ref.pk is not posicion.pk:
                        if getattr(posicion_ref,valor_a_comparar[0]) == getattr(posicion,valor_a_comparar[0]):
                            pk_empatados.append(posicion.pk)
                            pos_empatados.append(posicion.posicion)

                if pk_empatados: #si hay al menos una posicion empatada
                    pk_empatados.append(posicion_ref.pk)
                    pos_empatados.append(posicion_ref.posicion)
                    continue
                else: #si no hay más empatados dejamos de comparar la referencia
                    break