import datetime
from django.shortcuts import render
from django.shortcuts import redirect
from django.utils import timezone
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from .models import Olimpiada, Competencia, Persona, Equipo, Delegacion, Deporte, Disciplina, Post, Arte 
from .models import Posicion_deporte, Posicion_olimpiada, Grupos_mas_Playoff, Resultado, Foto, Inscripcion
from jjoo import settings
from django.contrib.auth.decorators import permission_required
from django.http import HttpResponse
from django.core.mail import send_mail

from .forms import FutbolForm, HandballForm, HockeyForm, PuntosForm, TiemposForm, PosicionForm 
from .forms import AtletismoForm, PescaForm, SetPuntosForm, SetGamesForm
from .models import Resultado

from django.forms import modelformset_factory

def inicio(request):    
    olimpiada = Olimpiada.objects.all().order_by('-ano').first()
    return redirect('vista_index', id_olimpiada=olimpiada.pk)


def index(request, id_olimpiada):    
    olimpiada = Olimpiada.objects.get(pk=id_olimpiada) #Menu
   
    noticias = Post.objects.filter(etiqueta__tag='Noticia', olimpiadas=olimpiada).order_by('-published_date')
    
    sliders = Foto.objects.filter(titulo__startswith='__slider__',olimpiada=olimpiada)

    video_portada=None
    if Post.objects.filter(etiqueta__tag='Video_Portada',olimpiadas=olimpiada).order_by('-published_date'):
        video_portada = Post.objects.filter(etiqueta__tag='Video_Portada',olimpiadas=olimpiada).order_by('-published_date')[0]
    
    video_portada2 = None
    if len (Post.objects.filter(etiqueta__tag='Video_Portada',olimpiadas=olimpiada).order_by('-published_date')) > 1:
        video_portada2 = Post.objects.filter(etiqueta__tag='Video_Portada',olimpiadas=olimpiada).order_by('-published_date')[1]
    
    competencias = Competencia.objects.filter(disciplina__olimpiada__pk=id_olimpiada)[:8]
    posiciones = Posicion_olimpiada.objects.filter(olimpiada=olimpiada).order_by('posicion')
    posiciones_rendimiento = Posicion_olimpiada.objects.filter(olimpiada=olimpiada).order_by('posicion_rendimiento')

    return render(request, 'torneo/index.html', {'olimpiada':olimpiada,'noticias':noticias, 
                                                 'competencias':competencias, 'video_portada':video_portada, 
                                                 'video_portada2':video_portada2, 'posiciones':posiciones,
                                                 'posiciones_rendimiento':posiciones_rendimiento,
                                                 'sliders':sliders},)


def copaconjunto(request, id_olimpiada):    
    olimpiada = Olimpiada.objects.get(pk=id_olimpiada) #Menu
    posiciones = Posicion_olimpiada.objects.filter(olimpiada=olimpiada).order_by('posicion')
    posiciones_rendimiento = Posicion_olimpiada.objects.filter(olimpiada=olimpiada).order_by('posicion_rendimiento')

    return render(
        request, 'torneo/copaconjunto.html',
        {'olimpiada':olimpiada, 'posiciones':posiciones,
         'posiciones_rendimiento':posiciones_rendimiento})


def contacto(request, id_olimpiada):    
    olimpiada = Olimpiada.objects.get(pk=id_olimpiada) #Menu
    deportes = Deporte.objects.all()
    delegaciones = Delegacion.objects.all()
    return render(request, 'torneo/contacto.html', {'olimpiada':olimpiada, 'deportes_':deportes, 'delegaciones_':delegaciones})

def post(request, id_olimpiada, id_post):
    post = Post.objects.get(pk=id_post)
    olimpiada = Olimpiada.objects.get(pk=id_olimpiada) #Menu
    return render(request, 'torneo/post2.html', {'post':post,'olimpiada':olimpiada}) 


def deporte(request, id_olimpiada, id_deporte):
    olimpiada = Olimpiada.objects.get(pk=id_olimpiada)
    deporte = Deporte.objects.get(pk=id_deporte)
    disciplinas_del_deporte = Disciplina.objects.filter(deporte=deporte).filter(olimpiada=olimpiada).order_by('nombre')
    posiciones = Posicion_deporte.objects.filter(olimpiada=olimpiada, deporte=deporte).order_by('posicion')
    return render(request, 'torneo/deporte.html', {'deporte':deporte, 'disciplinas_del_deporte':disciplinas_del_deporte, 'olimpiada':olimpiada, 'posiciones':posiciones})


def jugador(request, id_olimpiada, id_jugador):
    from datetime import date
    def calculate_age(born):
        today = date.today()
        return today.year - born.year - ((today.month, today.day) < (born.month, born.day))

    jugador= Persona.objects.get(pk=id_jugador)
    edad = calculate_age(jugador.fecha_nacimiento)
    olimpiada = Olimpiada.objects.get(pk=id_olimpiada) #Menu
    ficha_inscripcion = Inscripcion.objects.get(olimpiada=olimpiada,jugador=jugador)
    return render(request, 'torneo/jugador.html', {'jugador':jugador, 'edad':edad, 'olimpiada':olimpiada,'ficha_inscripcion':ficha_inscripcion})  

def credencial(request, id_olimpiada, id_jugador):
    from datetime import date
    def calculate_age(born):
        today = date.today()
        return today.year - born.year - ((today.month, today.day) < (born.month, born.day))

    jugador= Persona.objects.get(pk=id_jugador)
    edad = calculate_age(jugador.fecha_nacimiento)
    olimpiada = Olimpiada.objects.get(pk=id_olimpiada) #Menu
    return render(request, 'torneo/credencial.html', {'jugador':jugador, 'edad':edad, 'olimpiada':olimpiada})

def credencial_pdf(request, id_olimpiada, id_jugador):
    from datetime import date
    def calculate_age(born):
        today = date.today()
        return today.year - born.year - ((today.month, today.day) < (born.month, born.day))

    jugador= Persona.objects.get(pk=id_jugador)
    edad = calculate_age(jugador.fecha_nacimiento)
    olimpiada = Olimpiada.objects.get(pk=id_olimpiada) #Menu
    return render(request, 'torneo/credencial_printable.html', {'jugador':jugador, 'edad':edad, 'olimpiada':olimpiada})

def credencial_pdf_chrome(request, id_olimpiada, id_jugador):
    from datetime import date
    def calculate_age(born):
        today = date.today()
        return today.year - born.year - ((today.month, today.day) < (born.month, born.day))

    jugador= Persona.objects.get(pk=id_jugador)
    edad = calculate_age(jugador.fecha_nacimiento)
    olimpiada = Olimpiada.objects.get(pk=id_olimpiada) #Menu
    return render(request, 'torneo/credencial_printable_chrome.html', {'jugador':jugador, 'edad':edad, 'olimpiada':olimpiada})


# from django_xhtml2pdf.utils import generate_pdf
# def credencial_pdf(response, id_olimpiada, id_jugador):
#     from datetime import date
#     def calculate_age(born):
#         today = date.today()
#         return today.year - born.year - ((today.month, today.day) < (born.month, born.day))

#     jugador= Persona.objects.get(pk=id_jugador)
#     edad = calculate_age(jugador.fecha_nacimiento)
#     olimpiada = Olimpiada.objects.get(pk=id_olimpiada) #Menu

#     resp = HttpResponse(content_type='application/pdf')
#     result = generate_pdf('torneo/credencial.html', file_object=resp)
#     return result

    
def equipo(request, id_olimpiada, id_equipo):
    equipo = Equipo.objects.get(pk=id_equipo)
    olimpiada = Olimpiada.objects.get(pk=id_olimpiada) #Menu
    return render(request, 'torneo/equipo.html', {'equipo':equipo, 'olimpiada':olimpiada})  



def delegacion(request, id_olimpiada, id_delegacion):
    olimpiada = Olimpiada.objects.get(pk=id_olimpiada)
    delegacion = Delegacion.objects.get(pk=id_delegacion)
    miembros_delegacion = Persona.objects.filter(delegacion=id_delegacion,olimpiadas=olimpiada).order_by('apellido')
    return render(request, 'torneo/delegacion.html', {'delegacion':delegacion, 'miembros_delegacion':miembros_delegacion, 'olimpiada':olimpiada})   


# =============================================================================
# Disciplinas
# =============================================================================
def disciplina(request, id_olimpiada, id_disciplina):
    disciplina = Disciplina.objects.get(pk=id_disciplina)
    olimpiada = Olimpiada.objects.get(pk=id_olimpiada) #Menu

    if disciplina.modalidad in ['Grupos_mas_Playoff','Liga']:
            if disciplina.grupos_mas_playoff.first():
                grupos = []
                grupos_mas_playoff = disciplina.grupos_mas_playoff.first()

                if disciplina.tipo in 'Grupal':   #creamos lista de tuplas (grupo_X, integrantes_grupo) 
                    grupos.append(('grupo_1',grupos_mas_playoff.grupo_e1.all()))
                    grupos.append(('grupo_2',grupos_mas_playoff.grupo_e2.all()))
                    grupos.append(('grupo_3',grupos_mas_playoff.grupo_e3.all()))
                    grupos.append(('grupo_4',grupos_mas_playoff.grupo_e4.all()))
                    grupos.append(('grupo_5',grupos_mas_playoff.grupo_e5.all()))
                    grupos.append(('grupo_6',grupos_mas_playoff.grupo_e6.all()))
                    grupos.append(('grupo_7',grupos_mas_playoff.grupo_e7.all()))
                    grupos.append(('grupo_8',grupos_mas_playoff.grupo_e8.all()))
                    grupos.append(('grupo_9',grupos_mas_playoff.grupo_e9.all()))
                    grupos.append(('grupo_10',grupos_mas_playoff.grupo_e10.all()))
                else:
                    grupos.append(('grupo_1',grupos_mas_playoff.grupo_p1.all()))
                    grupos.append(('grupo_2',grupos_mas_playoff.grupo_p2.all()))
                    grupos.append(('grupo_3',grupos_mas_playoff.grupo_p3.all()))
                    grupos.append(('grupo_4',grupos_mas_playoff.grupo_p4.all()))
                    grupos.append(('grupo_5',grupos_mas_playoff.grupo_p5.all()))
                    grupos.append(('grupo_6',grupos_mas_playoff.grupo_p6.all()))
                    grupos.append(('grupo_7',grupos_mas_playoff.grupo_p7.all()))
                    grupos.append(('grupo_8',grupos_mas_playoff.grupo_p8.all()))
                    grupos.append(('grupo_9',grupos_mas_playoff.grupo_p9.all()))
                    grupos.append(('grupo_10',grupos_mas_playoff.grupo_p10.all()))
                return render(request, 'torneo/disciplina.html', 
                              {'disciplina':disciplina, 
                               'olimpiada':olimpiada, 
                               'grupos':grupos})            

    return render(request, 'torneo/disciplina.html', {'disciplina':disciplina, 'olimpiada':olimpiada})


# @permission_required('torneo.editarfixture')
# def disciplina_editarfixture(request, id_olimpiada, id_disciplina):
#     disciplina = Disciplina.objects.get(pk=id_disciplina)
#     olimpiada = Olimpiada.objects.get(pk=id_olimpiada) #Menu

#     permiso_requerido = 'torneo.deporte_'+disciplina.deporte.nombre
#     if request.user.has_perm(permiso_requerido):

#         if disciplina.modalidad in ['Grupos_mas_Playoff','Liga']:
#             if disciplina.grupos_mas_playoff.first():
#                 grupos = []
#                 grupos_mas_playoff = disciplina.grupos_mas_playoff.first()

#                 if disciplina.tipo in 'Grupal':   #creamos lista de tuplas (grupo_X, integrantes_grupo) 
#                     grupos.append(('grupo_1',grupos_mas_playoff.grupo_e1.all()))
#                     grupos.append(('grupo_2',grupos_mas_playoff.grupo_e2.all()))
#                     grupos.append(('grupo_3',grupos_mas_playoff.grupo_e3.all()))
#                     grupos.append(('grupo_4',grupos_mas_playoff.grupo_e4.all()))
#                     grupos.append(('grupo_5',grupos_mas_playoff.grupo_e5.all()))
#                     grupos.append(('grupo_6',grupos_mas_playoff.grupo_e6.all()))
#                     grupos.append(('grupo_7',grupos_mas_playoff.grupo_e7.all()))
#                     grupos.append(('grupo_8',grupos_mas_playoff.grupo_e8.all()))
#                     grupos.append(('grupo_9',grupos_mas_playoff.grupo_e9.all()))
#                     grupos.append(('grupo_10',grupos_mas_playoff.grupo_e10.all()))
#                 else:
#                     grupos.append(('grupo_1',grupos_mas_playoff.grupo_p1.all()))
#                     grupos.append(('grupo_2',grupos_mas_playoff.grupo_p2.all()))
#                     grupos.append(('grupo_3',grupos_mas_playoff.grupo_p3.all()))
#                     grupos.append(('grupo_4',grupos_mas_playoff.grupo_p4.all()))
#                     grupos.append(('grupo_5',grupos_mas_playoff.grupo_p5.all()))
#                     grupos.append(('grupo_6',grupos_mas_playoff.grupo_p6.all()))
#                     grupos.append(('grupo_7',grupos_mas_playoff.grupo_p7.all()))
#                     grupos.append(('grupo_8',grupos_mas_playoff.grupo_p8.all()))
#                     grupos.append(('grupo_9',grupos_mas_playoff.grupo_p9.all()))
#                     grupos.append(('grupo_10',grupos_mas_playoff.grupo_p10.all()))
#                 return render(request, 'torneo/disciplina_editarfixture.html', 
#                               {'disciplina':disciplina, 
#                                'olimpiada':olimpiada, 
#                                'grupos':grupos})    

#         if disciplina.modalidad in ['RondasClasificacion']:
#             if disciplina.rondas_clasificacion.first():
#                 grupos = []
#                 grupos_semis = []
#                 rondas_clasificacion = disciplina.rondas_clasificacion.first()

#                 if disciplina.tipo in 'Grupal':   #creamos lista de tuplas (grupo_X, integrantes_grupo) 
#                     grupos.append(('grupo_1',rondas_clasificacion.grupo_e1.all()))
#                     grupos.append(('grupo_2',rondas_clasificacion.grupo_e2.all()))
#                     grupos.append(('grupo_3',rondas_clasificacion.grupo_e3.all()))
#                     grupos.append(('grupo_4',rondas_clasificacion.grupo_e4.all()))
#                     grupos.append(('grupo_5',rondas_clasificacion.grupo_e5.all()))
#                     grupos.append(('grupo_6',rondas_clasificacion.grupo_e6.all()))
#                     grupos.append(('grupo_7',rondas_clasificacion.grupo_e7.all()))
#                     grupos.append(('grupo_8',rondas_clasificacion.grupo_e8.all()))
#                     grupos_semis.append(('semifinal_1',rondas_clasificacion.semifinal_e1.all()))
#                     grupos_semis.append(('semifinal_2',rondas_clasificacion.semifinal_e2.all()))
#                     grupos_semis.append(('semifinal_3',rondas_clasificacion.semifinal_e3.all()))
#                     grupos_semis.append(('semifinal_4',rondas_clasificacion.semifinal_e4.all()))
#                 else:
#                     grupos.append(('grupo_1',rondas_clasificacion.grupo_p1.all()))
#                     grupos.append(('grupo_2',rondas_clasificacion.grupo_p2.all()))
#                     grupos.append(('grupo_3',rondas_clasificacion.grupo_p3.all()))
#                     grupos.append(('grupo_4',rondas_clasificacion.grupo_p4.all()))
#                     grupos.append(('grupo_5',rondas_clasificacion.grupo_p5.all()))
#                     grupos.append(('grupo_6',rondas_clasificacion.grupo_p6.all()))
#                     grupos.append(('grupo_7',rondas_clasificacion.grupo_p7.all()))
#                     grupos.append(('grupo_8',rondas_clasificacion.grupo_p8.all()))
#                     grupos_semis.append(('semifinal_1',rondas_clasificacion.semifinal_p1.all()))
#                     grupos_semis.append(('semifinal_2',rondas_clasificacion.semifinal_p2.all()))
#                     grupos_semis.append(('semifinal_3',rondas_clasificacion.semifinal_p3.all()))
#                     grupos_semis.append(('semifinal_4',rondas_clasificacion.semifinal_p4.all()))
#                 return render(request, 'torneo/disciplina_editarfixture.html', 
#                               {'disciplina':disciplina, 
#                                'olimpiada':olimpiada, 
#                                'grupos':grupos,
#                                'grupos_semis':grupos_semis})            
        
#         #General ('Playoff_8', 'CompetenciaUnica')
#         return render(request, 'torneo/disciplina_editarfixture.html', {'disciplina':disciplina, 'olimpiada':olimpiada})

#     else:
#         return HttpResponse("No tienes permiso para editar esta disciplina.")

@permission_required('torneo.editarfixture')
def disciplina_editarfixture(request, id_olimpiada, id_disciplina):
    disciplina = Disciplina.objects.get(pk=id_disciplina)
    olimpiada = Olimpiada.objects.get(pk=id_olimpiada) #Menu

    formularios = {}
    permiso_requerido = 'torneo.deporte_'+disciplina.deporte.nombre
    if request.user.has_perm(permiso_requerido):
        for competencia in disciplina.competencias.all():
            formularios[competencia.pk] = {}
            
            #Chequeamos si existe el resultado y lo cargamos, sino lo creamos
            if competencia.disciplina.tipo in 'Grupal':
                for equipo in competencia.equipos.all():
                    if Resultado.objects.filter(competencia=competencia, equipo=equipo).exists():
                        resultado = Resultado.objects.get(competencia=competencia, equipo=equipo)
                    else:
                        resultado=Resultado.objects.create(disciplina=competencia.disciplina, competencia=competencia, equipo=equipo)
            else:
                for participante in competencia.participantes.all():
                    if Resultado.objects.filter(competencia=competencia, participante=participante).exists():
                        resultado = Resultado.objects.get(competencia=competencia, participante=participante)
                    else:
                        resultado=Resultado.objects.create(disciplina=competencia.disciplina, competencia=competencia, participante=participante)    
                        print ('Creando Participante: ', participante.nombre, 'en editarfixture')


            #Generamos el FormSet
            if competencia.disciplina.deporte.nombre in ['Fútbol']:
                FutbolFormSet = modelformset_factory(Resultado, form=FutbolForm, extra=0)
                formset = FutbolFormSet(queryset=Resultado.objects.filter(competencia=competencia))
            
            if competencia.disciplina.deporte.nombre in ['Handball']:
                HandballFormSet = modelformset_factory(Resultado, form=HandballForm, extra=0)
                formset = HandballFormSet(queryset=Resultado.objects.filter(competencia=competencia))
            
            if competencia.disciplina.deporte.nombre in ['Hockey']:
                HockeyFormSet = modelformset_factory(Resultado, form=HockeyForm, extra=0)
                formset = HockeyFormSet(queryset=Resultado.objects.filter(competencia=competencia))
            
            if competencia.disciplina.deporte.nombre in ['Basquet','Bochas']:
                PuntosFormSet = modelformset_factory(Resultado, form=PuntosForm, extra=0)
                formset = PuntosFormSet(queryset=Resultado.objects.filter(competencia=competencia))
            
            if competencia.disciplina.deporte.nombre in ['Atletismo']: 
                if competencia.disciplina.nombre in 'Salto en Largo':
                    AtletismoFormSet = modelformset_factory(Resultado, form=AtletismoForm, extra=0)
                    formset = AtletismoFormSet(queryset=Resultado.objects.filter(competencia=competencia))
                else:
                    TiemposFormSet = modelformset_factory(Resultado, form=TiemposForm, extra=0)
                    formset = TiemposFormSet(queryset=Resultado.objects.filter(competencia=competencia))

            if competencia.disciplina.deporte.nombre in ['Natación', 'Ciclismo', 'Vela', 'Caminata/Carrera', 'Trail', 'Kayak']:
                TiemposFormSet = modelformset_factory(Resultado, form=TiemposForm, extra=0)
                formset = TiemposFormSet(queryset=Resultado.objects.filter(competencia=competencia))

            if competencia.disciplina.deporte.nombre in ['Pesca']:
                PescaFormSet = modelformset_factory(Resultado, form=PescaForm, extra=0) 
                formset = PescaFormSet(queryset=Resultado.objects.filter(competencia=competencia))
            
            if competencia.disciplina.deporte.nombre in ['Pádel','Tenis']:
                SetGamesFormSet = modelformset_factory(Resultado, form=SetGamesForm, extra=0)
                formset = SetGamesFormSet(queryset=Resultado.objects.filter(competencia=competencia))

            if competencia.disciplina.deporte.nombre in ['Voley','Tenis de Mesa']: 
                SetPuntosFormSet = modelformset_factory(Resultado, form=SetPuntosForm, extra=0)
                formset = SetPuntosFormSet(queryset=Resultado.objects.filter(competencia=competencia))

            if competencia.zona == 'Manual':
                PosicionFormSet = modelformset_factory(Resultado, form=PosicionForm, extra=0)
                formset = PosicionFormSet(queryset=Resultado.objects.filter(competencia=competencia))
            
            formularios[competencia.pk] = formset


        if disciplina.modalidad in ['Grupos_mas_Playoff','Liga']:
            if disciplina.grupos_mas_playoff.first():
                grupos = []
                grupos_mas_playoff = disciplina.grupos_mas_playoff.first()

                if disciplina.tipo in 'Grupal':   #creamos lista de tuplas (grupo_X, integrantes_grupo) 
                    grupos.append(('grupo_1',grupos_mas_playoff.grupo_e1.all()))
                    grupos.append(('grupo_2',grupos_mas_playoff.grupo_e2.all()))
                    grupos.append(('grupo_3',grupos_mas_playoff.grupo_e3.all()))
                    grupos.append(('grupo_4',grupos_mas_playoff.grupo_e4.all()))
                    grupos.append(('grupo_5',grupos_mas_playoff.grupo_e5.all()))
                    grupos.append(('grupo_6',grupos_mas_playoff.grupo_e6.all()))
                    grupos.append(('grupo_7',grupos_mas_playoff.grupo_e7.all()))
                    grupos.append(('grupo_8',grupos_mas_playoff.grupo_e8.all()))
                    grupos.append(('grupo_9',grupos_mas_playoff.grupo_e9.all()))
                    grupos.append(('grupo_10',grupos_mas_playoff.grupo_e10.all()))
                else:
                    grupos.append(('grupo_1',grupos_mas_playoff.grupo_p1.all()))
                    grupos.append(('grupo_2',grupos_mas_playoff.grupo_p2.all()))
                    grupos.append(('grupo_3',grupos_mas_playoff.grupo_p3.all()))
                    grupos.append(('grupo_4',grupos_mas_playoff.grupo_p4.all()))
                    grupos.append(('grupo_5',grupos_mas_playoff.grupo_p5.all()))
                    grupos.append(('grupo_6',grupos_mas_playoff.grupo_p6.all()))
                    grupos.append(('grupo_7',grupos_mas_playoff.grupo_p7.all()))
                    grupos.append(('grupo_8',grupos_mas_playoff.grupo_p8.all()))
                    grupos.append(('grupo_9',grupos_mas_playoff.grupo_p9.all()))
                    grupos.append(('grupo_10',grupos_mas_playoff.grupo_p10.all()))
                return render(request, 'torneo/disciplina_editarfixture.html', 
                              {'disciplina':disciplina, 
                               'olimpiada':olimpiada, 
                               'grupos':grupos,
                               'formularios':formularios})    

        if disciplina.modalidad in ['RondasClasificacion']:
            if disciplina.rondas_clasificacion.first():
                grupos = []
                grupos_semis = []
                rondas_clasificacion = disciplina.rondas_clasificacion.first()

                if disciplina.tipo in 'Grupal':   #creamos lista de tuplas (grupo_X, integrantes_grupo) 
                    grupos.append(('grupo_1',rondas_clasificacion.grupo_e1.all()))
                    grupos.append(('grupo_2',rondas_clasificacion.grupo_e2.all()))
                    grupos.append(('grupo_3',rondas_clasificacion.grupo_e3.all()))
                    grupos.append(('grupo_4',rondas_clasificacion.grupo_e4.all()))
                    grupos.append(('grupo_5',rondas_clasificacion.grupo_e5.all()))
                    grupos.append(('grupo_6',rondas_clasificacion.grupo_e6.all()))
                    grupos.append(('grupo_7',rondas_clasificacion.grupo_e7.all()))
                    grupos.append(('grupo_8',rondas_clasificacion.grupo_e8.all()))
                    grupos_semis.append(('semifinal_1',rondas_clasificacion.semifinal_e1.all()))
                    grupos_semis.append(('semifinal_2',rondas_clasificacion.semifinal_e2.all()))
                    grupos_semis.append(('semifinal_3',rondas_clasificacion.semifinal_e3.all()))
                    grupos_semis.append(('semifinal_4',rondas_clasificacion.semifinal_e4.all()))
                else:
                    grupos.append(('grupo_1',rondas_clasificacion.grupo_p1.all()))
                    grupos.append(('grupo_2',rondas_clasificacion.grupo_p2.all()))
                    grupos.append(('grupo_3',rondas_clasificacion.grupo_p3.all()))
                    grupos.append(('grupo_4',rondas_clasificacion.grupo_p4.all()))
                    grupos.append(('grupo_5',rondas_clasificacion.grupo_p5.all()))
                    grupos.append(('grupo_6',rondas_clasificacion.grupo_p6.all()))
                    grupos.append(('grupo_7',rondas_clasificacion.grupo_p7.all()))
                    grupos.append(('grupo_8',rondas_clasificacion.grupo_p8.all()))
                    grupos_semis.append(('semifinal_1',rondas_clasificacion.semifinal_p1.all()))
                    grupos_semis.append(('semifinal_2',rondas_clasificacion.semifinal_p2.all()))
                    grupos_semis.append(('semifinal_3',rondas_clasificacion.semifinal_p3.all()))
                    grupos_semis.append(('semifinal_4',rondas_clasificacion.semifinal_p4.all()))
                return render(request, 'torneo/disciplina_editarfixture.html', 
                              {'disciplina':disciplina, 
                               'olimpiada':olimpiada, 
                               'grupos':grupos,
                               'grupos_semis':grupos_semis,
                               'formularios':formularios})            
        
        #General ('Playoff_', 'CompetenciaUnica')
        return render(request, 'torneo/disciplina_editarfixture.html', {'disciplina':disciplina, 
                                                                        'olimpiada':olimpiada,
                                                                        'formularios':formularios})

    else:
        return HttpResponse("No tienes permiso para editar esta disciplina.")


@permission_required('torneo.editarfixture')
def disciplina_crearfixture (request, id_olimpiada, id_disciplina):
    disciplina = Disciplina.objects.get(pk=id_disciplina)    
    olimpiada = Olimpiada.objects.get(pk=id_olimpiada)
    
    permiso_requerido = 'torneo.deporte_'+disciplina.deporte.nombre
    if request.user.has_perm(permiso_requerido):
        disciplina.crear_fixture()
        return redirect('vista_disciplina_editarfixture', id_olimpiada=olimpiada.pk, id_disciplina=id_disciplina)    
    else:
        return HttpResponse("No tienes permiso para editar esta disciplina.")


@permission_required('torneo.editarfixture')
def disciplina_reiniciarfixture (request, id_olimpiada, id_disciplina):
    disciplina = Disciplina.objects.get(pk=id_disciplina)
    olimpiada = Olimpiada.objects.get(pk=id_olimpiada)
    
    permiso_requerido = 'torneo.deporte_'+disciplina.deporte.nombre
    if request.user.has_perm(permiso_requerido):
        disciplina.reiniciar_fixture()
        return redirect('vista_disciplina_editarfixture', id_olimpiada=olimpiada.pk, id_disciplina=id_disciplina)
    else:
        return HttpResponse("No tienes permiso para editar esta disciplina.")

@permission_required('torneo.editarfixture')
def disciplina_actualizarfixture (request, id_olimpiada, id_disciplina):
    disciplina = Disciplina.objects.get(pk=id_disciplina)
    olimpiada = Olimpiada.objects.get(pk=id_olimpiada)
    
    permiso_requerido = 'torneo.deporte_'+disciplina.deporte.nombre
    if request.user.has_perm(permiso_requerido):
        disciplina.actualizar_fixture()
        return redirect('vista_disciplina_editarfixture', id_olimpiada=olimpiada.pk, id_disciplina=id_disciplina)
    else:
        return HttpResponse("No tienes permiso para editar esta disciplina.")


@permission_required('torneo.editarfixture')
def disciplina_actualizartablas (request, id_olimpiada, id_disciplina):
    disciplina = Disciplina.objects.get(pk=id_disciplina)
    olimpiada = Olimpiada.objects.get(pk=id_olimpiada)
    
    permiso_requerido = 'torneo.deporte_'+disciplina.deporte.nombre
    if request.user.has_perm(permiso_requerido):
        disciplina.actualizar_tablas()
        return redirect('vista_disciplina_editarfixture', id_olimpiada=olimpiada.pk, id_disciplina=id_disciplina)
    else:
        return HttpResponse("No tienes permiso para editar esta disciplina.")

@permission_required('torneo.editarfixture')
def disciplina_iniciarmodalidad (request, id_olimpiada, id_disciplina):
    disciplina = Disciplina.objects.get(pk=id_disciplina)    
    olimpiada = Olimpiada.objects.get(pk=id_olimpiada)
    
    permiso_requerido = 'torneo.deporte_'+disciplina.deporte.nombre
    if request.user.has_perm(permiso_requerido):
        disciplina.iniciar_modalidad()
        return redirect('vista_disciplina_editarfixture', id_olimpiada=olimpiada.pk, id_disciplina=id_disciplina)    
    else:
        return HttpResponse("No tienes permiso para editar esta disciplina.")

@permission_required('torneo.editarfixture')
def disciplina_reiniciarmodalidad (request, id_olimpiada, id_disciplina):
    disciplina = Disciplina.objects.get(pk=id_disciplina)    
    olimpiada = Olimpiada.objects.get(pk=id_olimpiada)
    
    permiso_requerido = 'torneo.deporte_'+disciplina.deporte.nombre
    if request.user.has_perm(permiso_requerido):
        disciplina.reiniciar_modalidad()
        return redirect('vista_disciplina_editarfixture', id_olimpiada=olimpiada.pk, id_disciplina=id_disciplina)    
    else:
        return HttpResponse("No tienes permiso para editar esta disciplina.")

@permission_required('torneo.editarfixture')
def disciplina_iniciargrupos (request, id_olimpiada, id_disciplina):
    disciplina = Disciplina.objects.get(pk=id_disciplina)    
    olimpiada = Olimpiada.objects.get(pk=id_olimpiada)
    
    permiso_requerido = 'torneo.deporte_'+disciplina.deporte.nombre
    if request.user.has_perm(permiso_requerido):
        disciplina.iniciar_grupos()
        return redirect('vista_disciplina_editarfixture', id_olimpiada=olimpiada.pk, id_disciplina=id_disciplina)    
    else:
        return HttpResponse("No tienes permiso para editar esta disciplina.")

@permission_required('torneo.editarfixture')
def disciplina_reiniciargrupos (request, id_olimpiada, id_disciplina):
    disciplina = Disciplina.objects.get(pk=id_disciplina)    
    olimpiada = Olimpiada.objects.get(pk=id_olimpiada)
    
    permiso_requerido = 'torneo.deporte_'+disciplina.deporte.nombre
    if request.user.has_perm(permiso_requerido):
        disciplina.reiniciar_grupos()
        return redirect('vista_disciplina_editarfixture', id_olimpiada=olimpiada.pk, id_disciplina=id_disciplina)    
    else:
        return HttpResponse("No tienes permiso para editar esta disciplina.")

@permission_required('torneo.editarfixture')
def disciplina_actualizargrupos (request, id_olimpiada, id_disciplina):
    disciplina = Disciplina.objects.get(pk=id_disciplina)    
    olimpiada = Olimpiada.objects.get(pk=id_olimpiada)
    
    permiso_requerido = 'torneo.deporte_'+disciplina.deporte.nombre
    if request.user.has_perm(permiso_requerido):
        disciplina.actualizar_grupos()
        return redirect('vista_disciplina_editarfixture', id_olimpiada=olimpiada.pk, id_disciplina=id_disciplina)    
    else:
        return HttpResponse("No tienes permiso para editar esta disciplina.")


@permission_required('torneo.editarfixture')
def disciplina_iniciarsemifinales (request, id_olimpiada, id_disciplina):
    disciplina = Disciplina.objects.get(pk=id_disciplina)    
    olimpiada = Olimpiada.objects.get(pk=id_olimpiada)
    
    permiso_requerido = 'torneo.deporte_'+disciplina.deporte.nombre
    if request.user.has_perm(permiso_requerido):
        disciplina.iniciar_semifinales()
        return redirect('vista_disciplina_editarfixture', id_olimpiada=olimpiada.pk, id_disciplina=id_disciplina)    
    else:
        return HttpResponse("No tienes permiso para editar esta disciplina.")

@permission_required('torneo.editarfixture')
def disciplina_reiniciarsemifinales (request, id_olimpiada, id_disciplina):
    disciplina = Disciplina.objects.get(pk=id_disciplina)    
    olimpiada = Olimpiada.objects.get(pk=id_olimpiada)
    
    permiso_requerido = 'torneo.deporte_'+disciplina.deporte.nombre
    if request.user.has_perm(permiso_requerido):
        disciplina.reiniciar_semifinales()
        return redirect('vista_disciplina_editarfixture', id_olimpiada=olimpiada.pk, id_disciplina=id_disciplina)    
    else:
        return HttpResponse("No tienes permiso para editar esta disciplina.")

@permission_required('torneo.editarfixture')
def disciplina_actualizarsemifinales (request, id_olimpiada, id_disciplina):
    disciplina = Disciplina.objects.get(pk=id_disciplina)    
    olimpiada = Olimpiada.objects.get(pk=id_olimpiada)
    
    permiso_requerido = 'torneo.deporte_'+disciplina.deporte.nombre
    if request.user.has_perm(permiso_requerido):
        disciplina.actualizar_semifinales()
        return redirect('vista_disciplina_editarfixture', id_olimpiada=olimpiada.pk, id_disciplina=id_disciplina)    
    else:
        return HttpResponse("No tienes permiso para editar esta disciplina.")


# =============================================================================
# Competencias
# =============================================================================
def competencia(request, id_olimpiada, id_competencia):
    competencia = Competencia.objects.get(pk=id_competencia)
    
    olimpiada = Olimpiada.objects.get(pk=id_olimpiada) #Menu
    return render(request, 'torneo/competencia.html', {'competencia':competencia, 'olimpiada':olimpiada})   


@permission_required('torneo.editarfixture')
def competencia_notificarcambios (request, id_olimpiada, id_competencia):
    competencia = Competencia.objects.get(pk=id_competencia)
    olimpiada = Olimpiada.objects.get(pk=id_olimpiada) #Menu

    permiso_requerido = 'torneo.deporte_'+competencia.disciplina.deporte.nombre
    if request.user.has_perm(permiso_requerido):
        return render(request, 'torneo/competencia_notificarcambios.html', {'competencia':competencia, 'olimpiada':olimpiada})
    else:
        return HttpResponse("No tienes permiso para realizar esta acción.")


def competencia_listabuenafe(request, id_olimpiada, id_competencia):
    competencia = Competencia.objects.get(pk=id_competencia)
    
    olimpiada = Olimpiada.objects.get(pk=id_olimpiada) #Menu
    return render(request, 'torneo/listabuenafe.html', {'competencia':competencia, 'olimpiada':olimpiada})   


# =============================================================================
# Resultados
# =============================================================================
@permission_required('torneo.editarfixture')
def cargarresultado (request, id_olimpiada, id_competencia, id_participante):    
    competencia = Competencia.objects.get(pk=id_competencia)

    permiso_requerido = 'torneo.deporte_'+competencia.disciplina.deporte.nombre
    if request.user.has_perm(permiso_requerido):
    
        #Chequeamos si existe el resultado y lo cargamos, sino lo creamos -------------------------------------------------------------
        #CASO GRUPAL
        if competencia.disciplina.tipo in 'Grupal':
            equipo = Equipo.objects.get(pk=id_participante)
            
            if Resultado.objects.filter(competencia=competencia).filter(equipo=equipo).exists():
                resultado = Resultado.objects.get(competencia=competencia, equipo=equipo)
            else:
                resultado=Resultado.objects.create(disciplina=competencia.disciplina, competencia=competencia, equipo=equipo)
            
            participante=equipo
            
        #CASO INDIVIDUAL
        else:
            participante = Persona.objects.get(pk=id_participante)
            
            if Resultado.objects.filter(competencia=competencia).filter(participante=participante).exists():
                resultado = Resultado.objects.get(competencia=competencia, participante=participante)
            else:
                resultado=Resultado.objects.create(disciplina=competencia.disciplina, competencia=competencia, participante=participante)    
            
           

        #Grabamos los resultados devueltos por el usuario
        if request.method == 'POST':  
            if competencia.disciplina.deporte.nombre in ['Fútbol']:
                formulario = FutbolForm(request.POST, instance=resultado)
            
            if competencia.disciplina.deporte.nombre in ['Handball']:
                formulario = HandballForm(request.POST, instance=resultado)
            
            if competencia.disciplina.deporte.nombre in ['Hockey']:
                formulario = HockeyForm(request.POST, instance=resultado)
                       
            if competencia.disciplina.deporte.nombre in ['Basquet','Bochas']:
                formulario = PuntosForm(request.POST, instance=resultado)

            if competencia.disciplina.deporte.nombre in ['Atletismo']: 
                if competencia.disciplina.nombre in 'Salto en Largo':
                    print ('Resultados para Salto en Largo')
                    formulario = AtletismoForm(request.POST, instance=resultado)
                else:
                    print ('Resultados para todas las otras disciplinas de atletismo, nat, ci, vela, camin')
                    formulario = TiemposForm(request.POST, instance=resultado)
                       
            if competencia.disciplina.deporte.nombre in ['Natación','Ciclismo','Vela','Caminata/Carrera']:
                formulario = TiemposForm(request.POST, instance=resultado)
            
            if competencia.disciplina.deporte.nombre in ['Pesca']: 
                formulario = PescaForm(request.POST, instance=resultado)
            
            if competencia.disciplina.deporte.nombre in ['Pádel','Tenis']:
                formulario = SetGamesForm(request.POST, instance=resultado)

            if competencia.disciplina.deporte.nombre in ['Voley','Tenis de Mesa']: 
                formulario = SetPuntosForm(request.POST, instance=resultado) 

            if competencia.zona == 'Manual':
                formulario = PosicionForm(request.POST, instance=resultado) 
            
            if formulario.is_valid():
                resultado = formulario.save(commit=False)
                resultado.competencia = competencia
                resultado.disciplina = competencia.disciplina
                if competencia.disciplina.tipo in 'Grupal':
                    resultado.equipo = equipo
                else:
                    resultado.participante = participante   
                
                resultado.save()

                #Carga de resultados cruzados según deporte
                if competencia.resultados.count() == 2:
                    resultado0=Resultado.objects.filter(competencia=competencia)[0]
                    resultado1=Resultado.objects.filter(competencia=competencia)[1]

                    if competencia.disciplina.deporte.nombre in ['Fútbol','Handball','Hockey']:
                        resultado0.goles_contra = resultado1.goles_favor
                        resultado1.goles_contra = resultado0.goles_favor
                        resultado0.save()
                        resultado1.save()

                    if competencia.disciplina.deporte.nombre in ['Basquet','Bochas']:
                        resultado0.puntos_contra = resultado1.puntos_favor
                        resultado1.puntos_contra = resultado0.puntos_favor
                        resultado0.save()
                        resultado1.save()

                    if competencia.disciplina.deporte.nombre in ['Voley','Tenis de Mesa']:
                        resultado0.puntos_contra_1set = resultado1.puntos_favor_1set
                        resultado1.puntos_contra_1set = resultado0.puntos_favor_1set
                        resultado0.puntos_contra_2set = resultado1.puntos_favor_2set
                        resultado1.puntos_contra_2set = resultado0.puntos_favor_2set
                        resultado0.puntos_contra_3set = resultado1.puntos_favor_3set
                        resultado1.puntos_contra_3set = resultado0.puntos_favor_3set
                        resultado0.save()
                        resultado1.save()

                    if competencia.disciplina.deporte.nombre in ['Tenis','Pádel']:
                        resultado0.games_contra_1set = resultado1.games_favor_1set
                        resultado1.games_contra_1set = resultado0.games_favor_1set
                        resultado0.games_contra_2set = resultado1.games_favor_2set
                        resultado1.games_contra_2set = resultado0.games_favor_2set
                        resultado0.games_contra_3set = resultado1.games_favor_3set
                        resultado1.games_contra_3set = resultado0.games_favor_3set
                        resultado0.save()
                        resultado1.save()

                return redirect('vista_disciplina_editarfixture', id_olimpiada=id_olimpiada, id_disciplina=competencia.disciplina.pk)


        #Abrimos el formulario precargado o en blanco
        else:
            if competencia.disciplina.deporte.nombre in ['Fútbol']:
                formulario = FutbolForm(instance=resultado)
            
            if competencia.disciplina.deporte.nombre in ['Handball']:
                formulario = HandballForm(instance=resultado)
            
            if competencia.disciplina.deporte.nombre in ['Hockey']:
                formulario = HockeyForm(instance=resultado)
            
            if competencia.disciplina.deporte.nombre in ['Basquet','Bochas']:
                formulario = PuntosForm(instance=resultado)
            
            if competencia.disciplina.deporte.nombre in ['Atletismo']: 
                if competencia.disciplina.nombre in 'Salto en Largo':
                    print ('Resultados para Salto en Largo')
                    formulario = AtletismoForm(instance=resultado)
                else:
                    formulario = TiemposForm(instance=resultado)
                    print ('Resultados para todas las otras disciplinas de atletismo, nat, ci, vela, camin')

            if competencia.disciplina.deporte.nombre in ['Natación','Ciclismo','Vela','Caminata/Carrera']:
                formulario = TiemposForm(instance=resultado)

            if competencia.disciplina.deporte.nombre in ['Pesca']: 
                formulario = PescaForm(instance=resultado)
            
            if competencia.disciplina.deporte.nombre in ['Pádel','Tenis']:
                formulario = SetGamesForm(instance=resultado)

            if competencia.disciplina.deporte.nombre in ['Voley','Tenis de Mesa']: 
                formulario = SetPuntosForm(instance=resultado)     

            if competencia.zona == 'Manual':
                formulario = PosicionForm(instance=resultado)        

        
        olimpiada = Olimpiada.objects.get(pk=id_olimpiada) #Menu
        return render(request, 'torneo/resultado_cargar.html', {'formulario': formulario, 'participante':participante, 'competencia':competencia, 'olimpiada':olimpiada})    
    
    else:
        return HttpResponse("No tienes permiso para realizar esta acción.")



@permission_required('torneo.editarfixture')
def cargarresultados (request, id_olimpiada, id_competencia):    
   
    competencia = Competencia.objects.get(pk=id_competencia)

    permiso_requerido = 'torneo.deporte_'+competencia.disciplina.deporte.nombre
    if request.user.has_perm(permiso_requerido):
    
        #Grabamos los resultados devueltos por el usuario
        if request.method == 'POST':  
            if competencia.disciplina.deporte.nombre in ['Fútbol']:
                FutbolFormSet = modelformset_factory(Resultado, form=FutbolForm)
                formset = FutbolFormSet(data=request.POST)
            
            if competencia.disciplina.deporte.nombre in ['Handball']:
                HandballFormSet = modelformset_factory(Resultado, form=HandballForm)
                formset = HandballFormSet(data=request.POST)
            
            if competencia.disciplina.deporte.nombre in ['Hockey']:
                HockeyFormSet = modelformset_factory(Resultado, form=HockeyForm)
                formset = HockeyFormSet(data=request.POST)
                       
            if competencia.disciplina.deporte.nombre in ['Basquet','Bochas']:
                PuntosFormSet = modelformset_factory(Resultado, form=PuntosForm)
                formset = PuntosFormSet(data=request.POST)

            if competencia.disciplina.deporte.nombre in ['Atletismo']: 
                if competencia.disciplina.nombre in 'Salto en Largo':
                    AtletismoFormSet = modelformset_factory(Resultado, form=AtletismoForm)
                    formset = AtletismoFormSet(data=request.POST)
                else:
                    TiemposFormSet = modelformset_factory(Resultado, form=TiemposForm)
                    formset = TiemposFormSet(data=request.POST)
                       
            if competencia.disciplina.deporte.nombre in ['Natación', 'Ciclismo', 'Vela', 'Caminata/Carrera', 'Trail', 'Kayak']:
                TiemposFormSet = modelformset_factory(Resultado, form=TiemposForm)
                formset = TiemposFormSet(data=request.POST)
            
            if competencia.disciplina.deporte.nombre in ['Pesca']: 
                PescaFormSet = modelformset_factory(Resultado, form=PescaForm)
                formset = PescaFormSet(data=request.POST)
            
            if competencia.disciplina.deporte.nombre in ['Pádel','Tenis']:
                SetGamesFormSet = modelformset_factory(Resultado, form=SetGamesForm)
                formset = SetGamesFormSet(data=request.POST)

            if competencia.disciplina.deporte.nombre in ['Voley','Tenis de Mesa']: 
                SetPuntosFormSet = modelformset_factory(Resultado, form=SetPuntosForm)
                formset = SetPuntosFormSet(data=request.POST)

            if competencia.zona == 'Manual':
                PosicionFormSet = modelformset_factory(Resultado, form=PosicionForm)
                formset = PosicionFormSet(data=request.POST) 
            
            if formset.is_valid():
                resultados = formset.save()

                #Carga de resultados cruzados según deporte
                if competencia.resultados.count() == 2:
                    resultado0=Resultado.objects.filter(competencia=competencia)[0]
                    resultado1=Resultado.objects.filter(competencia=competencia)[1]

                    if competencia.disciplina.deporte.nombre in ['Fútbol','Handball','Hockey']:
                        resultado0.goles_contra = resultado1.goles_favor
                        resultado1.goles_contra = resultado0.goles_favor
                        resultado0.save()
                        resultado1.save()

                    if competencia.disciplina.deporte.nombre in ['Basquet','Bochas']:
                        resultado0.puntos_contra = resultado1.puntos_favor
                        resultado1.puntos_contra = resultado0.puntos_favor
                        resultado0.save()
                        resultado1.save()

                    if competencia.disciplina.deporte.nombre in ['Voley','Tenis de Mesa']:
                        resultado0.puntos_contra_1set = resultado1.puntos_favor_1set
                        resultado1.puntos_contra_1set = resultado0.puntos_favor_1set
                        resultado0.puntos_contra_2set = resultado1.puntos_favor_2set
                        resultado1.puntos_contra_2set = resultado0.puntos_favor_2set
                        resultado0.puntos_contra_3set = resultado1.puntos_favor_3set
                        resultado1.puntos_contra_3set = resultado0.puntos_favor_3set
                        resultado0.save()
                        resultado1.save()

                    if competencia.disciplina.deporte.nombre in ['Tenis','Pádel']:
                        resultado0.games_contra_1set = resultado1.games_favor_1set
                        resultado1.games_contra_1set = resultado0.games_favor_1set
                        resultado0.games_contra_2set = resultado1.games_favor_2set
                        resultado1.games_contra_2set = resultado0.games_favor_2set
                        resultado0.games_contra_3set = resultado1.games_favor_3set
                        resultado1.games_contra_3set = resultado0.games_favor_3set
                        resultado0.save()
                        resultado1.save()

                return redirect('vista_disciplina_editarfixture', id_olimpiada=id_olimpiada, id_disciplina=competencia.disciplina.pk)

    else:
        return HttpResponse("No tienes permiso para realizar esta acción.")


# =============================================================================
# Inscripciones
# =============================================================================
def inscripcion_parte0 (request, id_olimpiada):  
    olimpiada = Olimpiada.objects.get(pk=id_olimpiada)    

    if request.user.is_authenticated:
        id_jugador = request.user.persona.pk
        return redirect('vista_inscripcion_parte1b_logged', id_olimpiada=olimpiada.pk, id_jugador=id_jugador)    

    # =============================================================================
    # Chequeo Periodo Inscripción: chequea si estamos o no en periodo de inscripcion
    # =============================================================================
    now=datetime.date.today()
    if (olimpiada.fecha_inicio_inscripcion > now) or (olimpiada.fecha_fin_inscripcion < now):
        return redirect('vista_inscripcion_cerrada', id_olimpiada=id_olimpiada)

    return render(request, 'torneo/inscripcion_parte0.html', {'olimpiada':olimpiada})



def inscripcion_parte1a (request, id_olimpiada):  
    olimpiada = Olimpiada.objects.get(pk=id_olimpiada)    
    
    # =============================================================================
    # Chequeo Periodo Inscripción: chequea si estamos o no en periodo de inscripcion
    # =============================================================================
    now=datetime.date.today()
    if (olimpiada.fecha_inicio_inscripcion > now) or (olimpiada.fecha_fin_inscripcion < now):
        return redirect('vista_inscripcion_cerrada', id_olimpiada=id_olimpiada)


    # =============================================================================
    # Renderizamos / Procesamos el Formulario de Inscripcion
    # =============================================================================
    from .forms import InscripcionParte1Form
    if request.method == 'POST':               
        formulario = InscripcionParte1Form(request.POST, request.FILES, id_olimpiada=id_olimpiada)
       
        if formulario.is_valid():                        
            jugador = formulario.save(commit=False)

            #Chequeamos que no exista un usuario con el mismo DNI ni mail
            if Persona.objects.filter(numero_documento=jugador.numero_documento).exists():
                jugador = Persona.objects.get(numero_documento=jugador.numero_documento)
                #return redirect('vista_inscripcion_parte1b', id_olimpiada=id_olimpiada, id_jugador=jugador.pk)
                return redirect('vista_inscripcion_usuarioexistente', id_olimpiada=id_olimpiada)

            if Persona.objects.filter(mail=jugador.mail).exists():
                return redirect('vista_inscripcion_usuarioexistente', id_olimpiada=id_olimpiada)
            
            jugador.inscripcion_fecha = timezone.now()
            jugador.save()

            # =============================================================================
            # Creamos Usuario y Enviamos mail de Reseteo de Contraseña
            # =============================================================================  
            from django.utils.crypto import get_random_string
            from allauth.account.views import PasswordResetView
            from django.middleware.csrf import get_token
            from django.conf import settings
            from django.http import HttpRequest

            #Creamos el usuario asociado 
            user = User.objects.create_user(username=jugador.numero_documento, email=jugador.mail)
            user.set_password(get_random_string())
            user.save()

            try:
                #Enviamos mail de recuperación
                # First create a post request to pass to the view
                request = HttpRequest()
                request.method = 'POST'

                # add the absolute url to be be included in email
                if settings.DEBUG:
                    #request.META['HTTP_HOST'] = '127.0.0.1:8000'
                    request.META['HTTP_HOST'] = 'Juegos Deportivos CONICET | '
                else:
                    request.META['HTTP_HOST'] = 'Juegos Deportivos CONICET | '
                # pass the post form data
                request.POST = {
                    'email': user.email,
                    'csrfmiddlewaretoken': get_token(HttpRequest())
                }
                PasswordResetView.as_view()(request)  # email will be sent!
                jugador.mail_reset_password_enviado = True
            except Exception as e:
                print (e) #TODO: agregar logger

            jugador.usuario = user
            jugador.save()

            #Nando: I have to do this in this way because a limitation of many to many filds
            #It is required that field has an pk before link with another entity.
            if 'artes' in formulario.cleaned_data.keys():
                for arte in formulario.cleaned_data['artes']:
                    jugador.artes.add(arte)

            return redirect('vista_inscripcion_parte2', id_olimpiada=id_olimpiada, id_jugador=jugador.pk)

    else:
        formulario = InscripcionParte1Form(id_olimpiada=id_olimpiada)
        olimpiada = Olimpiada.objects.get(pk=id_olimpiada) #Menu
    return render(request, 'torneo/inscripcion_parte1a.html', {'olimpiada':olimpiada, 'formulario':formulario})


def inscripcion_parte1b (request, id_olimpiada, id_jugador):
    periodo_tolerancia = settings.PERIODO_TOLERANCIA #min
    olimpiada = Olimpiada.objects.get(pk=id_olimpiada)
    jugador = Persona.objects.get(pk=id_jugador)

    if (timezone.now() > jugador.inscripcion_fecha + datetime.timedelta(seconds=60*periodo_tolerancia)):
        return redirect('vista_inscripcion_parte1b_logged', id_olimpiada=olimpiada.pk, id_jugador=jugador.pk)
    else:
        return redirect('vista_inscripcion_parte1b_notlogged', id_olimpiada=olimpiada.pk, id_jugador=jugador.pk)


@login_required
def inscripcion_parte1b_logged (request, id_olimpiada, id_jugador):
    periodo_tolerancia = settings.PERIODO_TOLERANCIA #min
    olimpiada = Olimpiada.objects.get(pk=id_olimpiada)
    id_jugador = request.user.persona.pk
    jugador = Persona.objects.get(pk=id_jugador)

    # =============================================================================
    # Chequeo Periodo Inscripción:: chequea si estamos o no en periodo de inscripcion
    # =============================================================================
    now=datetime.date.today()
    if (olimpiada.fecha_inicio_inscripcion > now) or (olimpiada.fecha_fin_inscripcion < now):
        return redirect('vista_inscripcion_cerrada', id_olimpiada=id_olimpiada)

    # =============================================================================
    # Renderizamos / Procesamos el Formulario de Inscripcion
    # =============================================================================
    from .forms import InscripcionParte1Form
    if request.method == 'POST':       
        formulario = InscripcionParte1Form(request.POST, request.FILES, instance=jugador, id_olimpiada=id_olimpiada)
        
        if formulario.is_valid():
            jugador = formulario.save()
            jugador.inscripcion_fecha = timezone.now()
            jugador.save()

            return redirect('vista_inscripcion_parte2', id_olimpiada=id_olimpiada, id_jugador=jugador.pk)

    else:
        formulario = InscripcionParte1Form(instance=jugador, id_olimpiada=id_olimpiada)
        olimpiada = Olimpiada.objects.get(pk=id_olimpiada) #Menu

    vista_form = 'vista_inscripcion_parte1b_logged'
    return render(request, 'torneo/inscripcion_parte1b.html', {'olimpiada':olimpiada, 'jugador':jugador, 'formulario':formulario, 'vista_form':vista_form})


def inscripcion_parte1b_notlogged (request, id_olimpiada, id_jugador):
    periodo_tolerancia = settings.PERIODO_TOLERANCIA #min
    olimpiada = Olimpiada.objects.get(pk=id_olimpiada)
    jugador = Persona.objects.get(pk=id_jugador)

    # =============================================================================
    # Chequeo Periodos Inscripción/Última Edición
    # =============================================================================
    # Chequeo Periodo de Inscripcion: chequea si estamos o no en periodo de inscripcion
    now=datetime.date.today()
    if (olimpiada.fecha_inicio_inscripcion > now) or (olimpiada.fecha_fin_inscripcion < now):
        return redirect('vista_inscripcion_cerrada', id_olimpiada=id_olimpiada)

    # Chequeo Tiempo Ultima Edicion: si pasaron más de 20' se bloquea el perfil
    if (timezone.now() > jugador.inscripcion_fecha + datetime.timedelta(seconds=60*periodo_tolerancia)):
        return redirect('vista_inscripcion_errormodificacion', id_olimpiada=id_olimpiada)

    # =============================================================================
    # Renderizamos / Procesamos el Formulario de Inscripcion
    # =============================================================================
    from .forms import InscripcionParte1Form
    if request.method == 'POST':       
        formulario = InscripcionParte1Form(request.POST, request.FILES, instance=jugador, id_olimpiada=id_olimpiada)
        
        if formulario.is_valid():
            jugador = formulario.save()
            jugador.inscripcion_fecha = timezone.now()
            jugador.save()
            return redirect('vista_inscripcion_parte2', id_olimpiada=id_olimpiada, id_jugador=jugador.pk)

    else:
        formulario = InscripcionParte1Form(instance=jugador, id_olimpiada=id_olimpiada)
        olimpiada = Olimpiada.objects.get(pk=id_olimpiada) #Menu
    vista_form = 'vista_inscripcion_parte1b_notlogged'
    return render(request, 'torneo/inscripcion_parte1b.html', {'olimpiada':olimpiada, 'jugador':jugador, 'formulario':formulario, 'vista_form':vista_form})



def inscripcion_parte2 (request, id_olimpiada, id_jugador):
    periodo_tolerancia = settings.PERIODO_TOLERANCIA #min
    olimpiada = Olimpiada.objects.get(pk=id_olimpiada)
    jugador = Persona.objects.get(pk=id_jugador)   
    
    if Inscripcion.objects.filter(olimpiada=olimpiada,jugador=jugador).exists():
        inscripcion_jugador = Inscripcion.objects.get(olimpiada=olimpiada,jugador=jugador)
    else:
        inscripcion_jugador = Inscripcion.objects.create(olimpiada=olimpiada,jugador=jugador)
    
    
    # =============================================================================
    # Chequeo Periodos Inscripción/Última Edición
    # =============================================================================
    # Chequeo Periodo de Inscripcion: chequea si estamos o no en periodo de inscripcion
    now=datetime.date.today()
    if (olimpiada.fecha_inicio_inscripcion > now) or (olimpiada.fecha_fin_inscripcion < now):
        return redirect('vista_inscripcion_cerrada', id_olimpiada=id_olimpiada)

    # Chequeo Tiempo Ultima Edicion: si pasaron más de 20' se bloquea el perfil
    if (timezone.now() > jugador.inscripcion_fecha + datetime.timedelta(seconds=60*periodo_tolerancia)):
        return redirect('vista_inscripcion_parte1b_logged', id_olimpiada=id_olimpiada, id_jugador=id_jugador)


    # =============================================================================
    # Renderizamos / Procesamos el Formulario de Inscripcion
    # =============================================================================
    from .forms import InscripcionParte2Form
    if request.method == 'POST':       
        formulario = InscripcionParte2Form(request.POST, request.FILES, instance=inscripcion_jugador, id_jugador=id_jugador, id_olimpiada=id_olimpiada)
        if formulario.is_valid():
            inscripcion_jugador = formulario.save()            
            inscripcion_jugador.inscripcion_fecha = timezone.now()
            inscripcion_jugador.olimpiada = olimpiada
            inscripcion_jugador.jugador = jugador
            inscripcion_jugador.save()
            
            jugador.inscripcion_fecha = timezone.now()
            jugador.save()
            return redirect('vista_inscripcion_parte2b', id_olimpiada=id_olimpiada, id_jugador=jugador.pk)

    else:
        formulario = InscripcionParte2Form(instance=inscripcion_jugador, id_jugador=id_jugador, id_olimpiada=id_olimpiada)
    
    return render(request, 'torneo/inscripcion_parte2.html', {'olimpiada':olimpiada,'jugador':jugador,'formulario':formulario})



def inscripcion_parte2b (request, id_olimpiada, id_jugador):
    periodo_tolerancia = settings.PERIODO_TOLERANCIA #min
    olimpiada = Olimpiada.objects.get(pk=id_olimpiada)
    jugador = Persona.objects.get(pk=id_jugador)
    inscripcion_jugador = Inscripcion.objects.get(olimpiada=olimpiada,jugador=jugador)

    # =============================================================================
    # Chequeo Periodos Inscripción/Última Edición
    # =============================================================================
    # Chequeo Periodo de Inscripcion: chequea si estamos o no en periodo de inscripcion
    now=datetime.date.today()
    if (olimpiada.fecha_inicio_inscripcion > now) or (olimpiada.fecha_fin_inscripcion < now):
        return redirect('vista_inscripcion_cerrada', id_olimpiada=id_olimpiada)

    # Chequeo Tiempo Ultima Edicion: si pasaron más de 20' se bloquea el perfil
    if (timezone.now() > jugador.inscripcion_fecha + datetime.timedelta(seconds=60*periodo_tolerancia)):
        return redirect('vista_inscripcion_parte1b_logged', id_olimpiada=id_olimpiada, id_jugador=id_jugador)


    # =============================================================================
    # Renderizamos / Procesamos el Formulario de Inscripcion
    # =============================================================================
    from .forms import InscripcionParte2bForm
    if request.method == 'POST':       
        formulario = InscripcionParte2bForm(request.POST, request.FILES, instance=inscripcion_jugador, id_jugador=id_jugador, id_olimpiada=id_olimpiada)
        if formulario.is_valid():
            inscripcion_jugador = formulario.save()            
            inscripcion_jugador.inscripcion_fecha = timezone.now()
            inscripcion_jugador.save()
            jugador.inscripcion_fecha = timezone.now()
            jugador.save()

            return redirect('vista_inscripcion_parte3', id_olimpiada=id_olimpiada, id_jugador=jugador.pk)

    else:
        formulario = InscripcionParte2bForm(instance=inscripcion_jugador, id_jugador=id_jugador, id_olimpiada=id_olimpiada)
    
    return render(request, 'torneo/inscripcion_parte2b.html', {'olimpiada':olimpiada,'jugador':jugador,'formulario':formulario})


def inscripcion_parte3 (request, id_olimpiada, id_jugador):
    periodo_tolerancia = settings.PERIODO_TOLERANCIA #min
    jugador = Persona.objects.get(pk=id_jugador)
    olimpiada = Olimpiada.objects.get(pk=id_olimpiada) #Menu
    inscripcion_jugador = Inscripcion.objects.get(olimpiada=olimpiada, jugador=jugador) #Menu

    # ------------------------------------------------------------------------------------------------------------------
    # Chequeo Períodos Inscripción/Última Edición
    # ------------------------------------------------------------------------------------------------------------------
    # Chequeo Periodo de Inscripcion: chequea si estamos o no en periodo de inscripcion
    now=datetime.date.today()
    if (olimpiada.fecha_inicio_inscripcion > now) or (olimpiada.fecha_fin_inscripcion < now):
        return redirect('vista_inscripcion_cerrada', id_olimpiada=id_olimpiada)

    # Chequeo Tiempo Ultima Edicion: si pasaron más de 20' se bloquea el perfil
    if (timezone.now() > jugador.inscripcion_fecha + datetime.timedelta(seconds=60*periodo_tolerancia)):
        #return redirect('vista_inscripcion_errormodificacion', id_olimpiada=id_olimpiada)
        return redirect('vista_inscripcion_parte1b_logged', id_olimpiada=id_olimpiada, id_jugador=id_jugador)

    # ------------------------------------------------------------------------------------------------------------------
    # Inscripción/Edicion a JuegoDeportivo
    # ------------------------------------------------------------------------------------------------------------------
    inscripcion_jugador.deportes.clear()  #reinicio del campo deportes
    jugador.deportes.clear()  #reinicio del campo deportes
    
    # Si se modifican los deportes/disciplinas para la presente olimpiada, 
    # hay que eliminar el jugador de las disciplinas seleccionadas previamente.
    disciplinas_previas = Disciplina.objects.filter(olimpiada=olimpiada, participantes=jugador)
    if disciplinas_previas:
        for disciplina in disciplinas_previas:
            disciplina.participantes.remove(jugador)

    # ------------------------------------------------------------------------------------------------------------------
    # Registramos Deportes y Disciplinas Seleccionadas
    # ------------------------------------------------------------------------------------------------------------------
    # Caso Pesca -------------------------------------------------------------------------------------------------------
    # Se muestra una sola categoría en la selección, pero registramos las dos.
    for deporte in [inscripcion_jugador.deporte1,inscripcion_jugador.deporte2,
                    inscripcion_jugador.deporte3]:
        if deporte and ('Pesca' in [deporte.nombre]):
            pesca = Deporte.objects.get(nombre='Pesca') 
            disciplina_pesca = Disciplina.objects.get(olimpiada=olimpiada, deporte=pesca, nombre='Cantidad')
            disciplina_pesca.participantes.add(jugador)
    
    # Caso Carrera/Caminata --------------------------------------------------------------------------------------------
    # Se registra el participante en la disciplina correspondiente en funcion de la edad y género
    edad_jugador = jugador.calcular_edad_en_olimpiada(olimpiada)
    if edad_jugador < 35:
        edad=0
    elif edad_jugador >= 35 and edad_jugador < 45:
        edad=35
    elif edad_jugador >= 45 and edad_jugador < 55:
        edad=45
    elif edad_jugador >= 55 and edad_jugador < 65:
        edad=55
    else:
        edad=65   

    maraton = Deporte.objects.get(nombre='Caminata/Carrera')
    if inscripcion_jugador.deporte_maraton not in 'No':
        # con este filtro hay una sola opcion posible pero el queryset es para poder registrarlo en el form de abajo
        disciplina_maraton = Disciplina.objects.filter(olimpiada=olimpiada, deporte=maraton, genero=jugador.genero, limite_edad=edad, nombre=inscripcion_jugador.deporte_maraton)   
    else:
        disciplina_maraton = Disciplina.objects.filter(nombre='xxx') #queryset vacio
        maraton = None

    # ------------------------------------------------------------------------------------------------------------------
    # Registramos los deportes 1,2 y 3 y sus respectivas disciplinas    
    # ------------------------------------------------------------------------------------------------------------------
    registro_disciplinas = []
    for deporte, disciplinas in [(inscripcion_jugador.deporte1, inscripcion_jugador.disciplinas_deporte1),
                                 (inscripcion_jugador.deporte2, inscripcion_jugador.disciplinas_deporte2),
                                 (inscripcion_jugador.deporte3, inscripcion_jugador.disciplinas_deporte3),
                                 (maraton, disciplina_maraton)]:
        if deporte:
            inscripcion_jugador.deportes.add(deporte)
            inscripcion_jugador.save()
            jugador.deportes.add(deporte)   #agregamos los deportes seleccionados 
            jugador.save()                  #a la lista general de deportes
            for disciplina in disciplinas.all():
                #print (deporte.nombre, disciplina.nombre)
                disciplina.participantes.add(jugador)   
                disciplina.save()                       
                registro_disciplinas.append(str(disciplina))

    # ------------------------------------------------------------------------------------------------------------------
    # Registramos las actividades recreativas
    # ------------------------------------------------------------------------------------------------------------------
    recreativo = Deporte.objects.get(nombre='Recreativos')
    for deporte, disciplinas in [(recreativo, inscripcion_jugador.actividades_recreativas),]:
        if inscripcion_jugador.actividades_recreativas:
            inscripcion_jugador.deportes.add(deporte)
            inscripcion_jugador.save()
            jugador.deportes.add(deporte)   #agregamos los deportes seleccionados 
            jugador.save()                  #a la lista general de deportes
            for disciplina in disciplinas.all():
                #print (deporte.nombre, disciplina.nombre)
                disciplina.participantes.add(jugador)   
                disciplina.save()      
    
    # ------------------------------------------------------------------------------------------------------------------
    # Seteamos la finalización completa de la inscripción y marca de tiempo
    # ------------------------------------------------------------------------------------------------------------------
    jugador.inscripcion_fecha = timezone.now()
    jugador.olimpiadas.add(olimpiada)
    jugador.save()

    inscripcion_jugador.inscripcion_completa = True   #inscripcion finalizada
    inscripcion_jugador.inscripcion_fecha = timezone.now()
    inscripcion_jugador.save()

    # ------------------------------------------------------------------------------------------------------------------
    # Enviamos e-mail de confirmación    
    # ------------------------------------------------------------------------------------------------------------------
    asunto = 'Juegos Deportivos CONICET | La inscripción fue realizada con éxito'
    
    titulo = 'La inscripción fue realizada con éxito.'

    resumen_inscripcion =\
        '\n\nRESUMEN DE INSCRIPCIÓN:\nTe inscribiste en las siguientes disciplinas: '
    for info_disciplina in registro_disciplinas:
        resumen_inscripcion += '\n- ' + info_disciplina

    pendientes =\
        '\n\nPENDIENTES: recordá que para finalizar la inscripción deberás entregar ' +\
        'el certificado médico junto a una copia de la ergometría al coordinador de tu delegación.'

    cuenta =\
        '\n\nACTIVACIÓN CUENTA: si acabas de crear tu cuenta, además de este correo electrónico, ' +\
        'deberías haber recibido previamente un e-mail de reestablecimiento de constraseña, mediante el cual ' +\
        'podrás activar tu nueva cuenta de usuario. Con esta cuenta podés gestionar tu perfil, realizar ' +\
        'modificaciones, subir documentos pendientes, subir fotos, entre otras opciones.'

    consultas =\
        '\n\nCONSULTAS:\nSi tenés consultas, dudas o sugerencias comunicate con tu delegado.' +\
        '\nNO respondas este correo electrónico.'

    cuerpo_mail = titulo + resumen_inscripcion + pendientes + cuenta + consultas

    try:
        send_mail(
            asunto,
            cuerpo_mail,
            'inscripciones.juegosdeportivos@gmail.com',  #from e-mail
            [jugador.mail],                              #to e-mail
            fail_silently=False,)
        
        inscripcion_jugador.mail_inscripcion_enviado = True
        inscripcion_jugador.save()
        print ('Inscripción finalizada con exito')

    except Exception as e:
        print('There was an error sending an email: ', e) 
        print ('Inscripción finalizada, error al enviar correo electrónico.')

    return render(request, 'torneo/inscripcion_parte3.html',
                  {'olimpiada':olimpiada, 'jugador':jugador,
                   'inscripcion':True, 'resumen_inscripcion': resumen_inscripcion})

def inscripcion_usuarioexistente (request, id_olimpiada):
    olimpiada = Olimpiada.objects.get(pk=id_olimpiada)    
    return render(request, 'torneo/inscripcion_usuarioexistente.html', {'olimpiada':olimpiada})    


def inscripcion_cerrada (request, id_olimpiada):
    olimpiada = Olimpiada.objects.get(pk=id_olimpiada)    
    return render(request, 'torneo/inscripcion_cerrada.html', {'olimpiada':olimpiada})    



def inscripcion_errormodificacion (request, id_olimpiada):
    olimpiada = Olimpiada.objects.get(pk=id_olimpiada)    
    return render(request, 'torneo/inscripcion_errormodificacion.html', {'olimpiada':olimpiada})    


# =============================================================================
# Registración (Login,Logout,Profiles)    
# =============================================================================
from allauth.account.views import SignupView,LoginView,LogoutView
class SignupViewExt(SignupView):
    template_name = "account/singup.html"

class LoginViewExt(LoginView):
    template_name = "account/login.html"

class LogoutViewExt(LogoutView):
    template_name = "account/logout.html"    

@login_required
def account_redirect(request):
    #olimpiada = Olimpiada.objects.all().order_by('-ano').first()
    #return redirect('vista_jugador', id_olimpiada=olimpiada.pk, id_jugador=request.user.persona.pk)

    olimpiada = Olimpiada.objects.all().order_by('-ano').first()
    jugador = Persona.objects.get(pk=request.user.persona.pk)

    disciplinas_con_permisos = []
    for permiso in request.user.get_all_permissions():
        if 'deporte_' in permiso:
            nombre_deporte = permiso.split('_')[1]
            disciplinas = Disciplina.objects.filter(olimpiada=olimpiada,deporte__nombre=nombre_deporte)
            disciplinas_con_permisos.append(disciplinas)

            # permisos.append(permiso.split('_')[1])
            


    # =============================================================================
    # Renderizamos / Procesamos el Formulario de Inscripcion
    # =============================================================================
    from .forms import profileForm
    if request.method == 'POST':       
        formulario = profileForm(request.POST, request.FILES, instance=jugador)
        
        if formulario.is_valid():
            jugador = formulario.save()
            #jugador.inscripcion_fecha = timezone.now() #TODO:agregar campo ultima modificacion
            jugador.save()
            #return redirect('vista_inscripcion_parte2', id_olimpiada=id_olimpiada, id_jugador=jugador.pk)
            return render(request, 'torneo/profile.html', {'olimpiada':olimpiada, 'jugador':jugador, 'formulario':formulario, 'disciplinas_con_permisos':disciplinas_con_permisos})

    else:
        formulario = profileForm(instance=jugador)
        
        # Chequeo Periodo de Inscripcion: chequea si estamos o no en periodo de inscripcion
        inscripcion_activa = True
        now=datetime.date.today()
        if (olimpiada.fecha_inicio_inscripcion > now) or (olimpiada.fecha_fin_inscripcion < now):
            inscripcion_activa = False

    return render(request, 'torneo/profile.html', {'olimpiada':olimpiada, 'jugador':jugador, 'formulario':formulario, 'inscripcion_activa':inscripcion_activa, 'disciplinas_con_permisos':disciplinas_con_permisos})



