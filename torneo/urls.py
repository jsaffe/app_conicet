from django.conf.urls import url
from django.views.generic import RedirectView
from . import views

urlpatterns = [      
    url(r'^index/(?P<id_olimpiada>[0-9]+)/$', views.index, name='vista_index'),
    url(r'^contacto/(?P<id_olimpiada>[0-9]+)/$', views.contacto, name='vista_contacto'),
    url(r'^copaconjunto/(?P<id_olimpiada>[0-9]+)/$', views.copaconjunto, name='vista_copaconjunto'),
    url(r'^post/(?P<id_olimpiada>[0-9]+)/(?P<id_post>[0-9]+)/$', views.post, name='vista_post'),

    url(r'^jugador/(?P<id_olimpiada>[0-9]+)/(?P<id_jugador>[0-9]+)/$', views.jugador, name='vista_jugador'),
    url(r'^credencial/(?P<id_olimpiada>[0-9]+)/(?P<id_jugador>[0-9]+)/$', views.credencial, name='vista_credencial'),
    url(r'^credencial_pdf/(?P<id_olimpiada>[0-9]+)/(?P<id_jugador>[0-9]+)/$', views.credencial_pdf, name='vista_credencial_pdf'),
    url(r'^credencial_pdf_chrome/(?P<id_olimpiada>[0-9]+)/(?P<id_jugador>[0-9]+)/$', views.credencial_pdf_chrome, name='vista_credencial_pdf_chrome'),
    url(r'^deporte/(?P<id_olimpiada>[0-9]+)/(?P<id_deporte>[0-9]+)/$', views.deporte, name='vista_deporte'),
    url(r'^equipo/(?P<id_olimpiada>[0-9]+)/(?P<id_equipo>[0-9]+)/$', views.equipo, name='vista_equipo'),
    url(r'^delegacion/(?P<id_olimpiada>[0-9]+)/(?P<id_delegacion>[0-9]+)/$', views.delegacion, name='vista_delegacion'),

    url(r'^disciplina/(?P<id_olimpiada>[0-9]+)/(?P<id_disciplina>[0-9]+)/$', views.disciplina, name='vista_disciplina'),
    url(r'^disciplina/(?P<id_olimpiada>[0-9]+)/(?P<id_disciplina>[0-9]+)/editarfixture/$', views.disciplina_editarfixture, name='vista_disciplina_editarfixture'),    
    url(r'^disciplina/(?P<id_olimpiada>[0-9]+)/(?P<id_disciplina>[0-9]+)/crearfixture/$', views.disciplina_crearfixture, name='vista_disciplina_crearfixture'),
    url(r'^disciplina/(?P<id_olimpiada>[0-9]+)/(?P<id_disciplina>[0-9]+)/reiniciarfixture/$', views.disciplina_reiniciarfixture, name='vista_disciplina_reiniciarfixture'),
    url(r'^disciplina/(?P<id_olimpiada>[0-9]+)/(?P<id_disciplina>[0-9]+)/actualizarfixture/$', views.disciplina_actualizarfixture, name='vista_disciplina_actualizarfixture'),
    url(r'^disciplina/(?P<id_olimpiada>[0-9]+)/(?P<id_disciplina>[0-9]+)/actualizartablas/$', views.disciplina_actualizartablas, name='vista_disciplina_actualizartablas'),
    url(r'^disciplina/(?P<id_olimpiada>[0-9]+)/(?P<id_disciplina>[0-9]+)/iniciarmodalidad/$', views.disciplina_iniciarmodalidad, name='vista_disciplina_iniciarmodalidad'),
    url(r'^disciplina/(?P<id_olimpiada>[0-9]+)/(?P<id_disciplina>[0-9]+)/reiniciarmodalidad/$', views.disciplina_reiniciarmodalidad, name='vista_disciplina_reiniciarmodalidad'),
    url(r'^disciplina/(?P<id_olimpiada>[0-9]+)/(?P<id_disciplina>[0-9]+)/iniciargrupos/$', views.disciplina_iniciargrupos, name='vista_disciplina_iniciargrupos'),
    url(r'^disciplina/(?P<id_olimpiada>[0-9]+)/(?P<id_disciplina>[0-9]+)/actualizargrupos/$', views.disciplina_actualizargrupos, name='vista_disciplina_actualizargrupos'),
    url(r'^disciplina/(?P<id_olimpiada>[0-9]+)/(?P<id_disciplina>[0-9]+)/reiniciargrupos/$', views.disciplina_reiniciargrupos, name='vista_disciplina_reiniciargrupos'),
    url(r'^disciplina/(?P<id_olimpiada>[0-9]+)/(?P<id_disciplina>[0-9]+)/iniciarsemifinales/$', views.disciplina_iniciarsemifinales, name='vista_disciplina_iniciarsemifinales'),
    url(r'^disciplina/(?P<id_olimpiada>[0-9]+)/(?P<id_disciplina>[0-9]+)/actualizarsemifinales/$', views.disciplina_actualizarsemifinales, name='vista_disciplina_actualizarsemifinales'),
    url(r'^disciplina/(?P<id_olimpiada>[0-9]+)/(?P<id_disciplina>[0-9]+)/reiniciarsemifinales/$', views.disciplina_reiniciarsemifinales, name='vista_disciplina_reiniciarsemifinales'),
    
    url(r'^competencia/(?P<id_olimpiada>[0-9]+)/(?P<id_competencia>[0-9]+)/$', views.competencia, name='vista_competencia'),
    url(r'^competencia/(?P<id_olimpiada>[0-9]+)/(?P<id_competencia>[0-9]+)/notificarcambios/$', views.competencia_notificarcambios, name='vista_competencia_notificarcambios'),
    url(r'^competencia/(?P<id_olimpiada>[0-9]+)/(?P<id_competencia>[0-9]+)/listabuenafe/$', views.competencia_listabuenafe, name='vista_competencia_listabuenafe'),

    url(r'^resultado/(?P<id_olimpiada>[0-9]+)/(?P<id_competencia>[0-9]+)/(?P<id_participante>[0-9]+)/$', views.cargarresultado, name='vista_cargarresultado'),
    url(r'^resultado/(?P<id_olimpiada>[0-9]+)/(?P<id_competencia>[0-9]+)/$', views.cargarresultados, name='vista_cargarresultados'),
    
    url(r'^inscripcion/(?P<id_olimpiada>[0-9]+)/paso0/$', views.inscripcion_parte0, name='vista_inscripcion_parte0'),
    url(r'^inscripcion/(?P<id_olimpiada>[0-9]+)/paso1a/$', views.inscripcion_parte1a, name='vista_inscripcion_parte1a'),
    url(r'^inscripcion/(?P<id_olimpiada>[0-9]+)/(?P<id_jugador>[0-9]+)/paso1b_lg/$', views.inscripcion_parte1b_logged, name='vista_inscripcion_parte1b_logged'),
    url(r'^inscripcion/(?P<id_olimpiada>[0-9]+)/(?P<id_jugador>[0-9]+)/paso1b_nlg/$', views.inscripcion_parte1b_notlogged, name='vista_inscripcion_parte1b_notlogged'),
    url(r'^inscripcion/(?P<id_olimpiada>[0-9]+)/(?P<id_jugador>[0-9]+)/paso1b/$', views.inscripcion_parte1b, name='vista_inscripcion_parte1b'),
    url(r'^inscripcion/(?P<id_olimpiada>[0-9]+)/(?P<id_jugador>[0-9]+)/paso2/$', views.inscripcion_parte2, name='vista_inscripcion_parte2'),
    url(r'^inscripcion/(?P<id_olimpiada>[0-9]+)/(?P<id_jugador>[0-9]+)/paso2b/$', views.inscripcion_parte2b, name='vista_inscripcion_parte2b'),
    url(r'^inscripcion/(?P<id_olimpiada>[0-9]+)/(?P<id_jugador>[0-9]+)/paso3/$', views.inscripcion_parte3, name='vista_inscripcion_parte3'),
    url(r'^inscripcion/(?P<id_olimpiada>[0-9]+)/usuarioexistente/$', views.inscripcion_usuarioexistente, name='vista_inscripcion_usuarioexistente'),
    url(r'^inscripcion/(?P<id_olimpiada>[0-9]+)/cerrada/$', views.inscripcion_cerrada, name='vista_inscripcion_cerrada'),
    url(r'^inscripcion/(?P<id_olimpiada>[0-9]+)/errormodificacion/$', views.inscripcion_errormodificacion, name='vista_inscripcion_errormodificacion'),
    
    url(r'^juegosconicet/$', RedirectView.as_view(url='http://www.juegosconicet.gov.ar/'), name='redireccion_webjuegosconicet'),
    
    url(r'^account/$', views.account_redirect, name='account_redirect'),
    url(r'^singup/$',views.SignupViewExt.as_view(), name='vista_singup'),
    url(r'^login/$',views.LoginViewExt.as_view(), name='vista_login'),
    url(r'^logout/$',views.LogoutViewExt.as_view(), name='vista_logout'),

    url(r'^$', views.inicio, name='vista_inicio'),
]