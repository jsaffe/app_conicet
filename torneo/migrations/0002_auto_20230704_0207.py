# Generated by Django 2.0.6 on 2023-07-04 05:07

from django.db import migrations, models
import torneo.models


class Migration(migrations.Migration):

    dependencies = [
        ('torneo', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='inscripcion',
            name='aceptacion_terminos',
            field=models.BooleanField(default=False, verbose_name='Acepto los términos y condiciones'),
        ),
        migrations.AddField(
            model_name='inscripcion',
            name='estado_salud',
            field=models.BooleanField(default=False, verbose_name='Tengo afecciones de salud que condicionan las actividades deportivas en las que puedo participar'),
        ),
        migrations.AddField(
            model_name='inscripcion',
            name='mail_inscripcion_enviado',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='inscripcion',
            name='certificado_medico',
            field=models.FileField(blank=True, max_length=200, null=True, upload_to=torneo.models.Inscripcion.generate_path_personas, verbose_name='Certificado Médico + Ergometría (max.:3.5MB)'),
        ),
        migrations.AlterField(
            model_name='inscripcion',
            name='colaborador',
            field=models.CharField(choices=[('No', 'No'), ('Sí', 'Sí'), ('Tal vez', 'Tal vez')], help_text='Podes colaborar en la organización de los juegos en las áreas de administración, deportes,             comunicación, publicidad, etc., además podrás participar en los deportes en los que te inscribiste.', max_length=200, verbose_name='¿Desea colaborar con la Organización?'),
        ),
        migrations.AlterField(
            model_name='inscripcion',
            name='divulgacion',
            field=models.CharField(choices=[('No', 'No'), ('Sí', 'Sí'), ('Tal vez', 'Tal vez')], help_text='En caso de responder sí o tal vez, deberá completar el link a su sitio web laboral.', max_length=200, verbose_name='¿Desea Participar en el Espacio de Divulgación?'),
        ),
        migrations.AlterField(
            model_name='inscripcion',
            name='vinculacion_conicet',
            field=models.FileField(blank=True, help_text='Es obligatorio adjuntar en caso de NO ser personal de CONICET o ser EX-CONICET.', max_length=200, null=True, upload_to=torneo.models.Inscripcion.generate_path_personas, verbose_name='Certificado de Vinculación (max.:3.5MB)'),
        ),
        migrations.AlterField(
            model_name='persona',
            name='genero',
            field=models.CharField(choices=[('Masculino', 'Masculino'), ('Femenino', 'Femenino'), ('No Binario', 'No Binario')], max_length=10, verbose_name='Género'),
        ),
    ]
