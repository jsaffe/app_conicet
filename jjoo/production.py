from os.path import dirname, isfile
import environ
from .common import *

ROOT_DIR = environ.Path(dirname(dirname(dirname(__file__))))

env = environ.Env()

if isfile(ROOT_DIR('.env')):
    env.read_env(ROOT_DIR('.env'))

DEBUG = env.bool("DEBUG", False)

DOMAIN = env.list('DOMAIN', default=[])
ALLOWED_HOSTS = DOMAIN + ['127.0.0.1', 'localhost', '*']

# Database
# https://docs.djangoproject.com/en/2.0/ref/settings/#databases

# Database
DATABASES = {
    'default': {
        'ENGINE': env.str("DB_ENGINE", 'django.db.backends.postgresql_psycopg2'),
        'HOST': env.str("DB_HOST", 'postgresdb'),
        'PORT': env.int("DB_PORT", '5456'),
        'USER': env.str("DB_USER", 'jdconicet'),
        'PASSWORD': env.str("DB_PASS", 'iV2wThb_conicettt'),
        'NAME': env.str("DB_NAME", 'jdconicet'),
    },
}

#E-MAIL
EMAIL_HOST = 'smtp.gmail.com'
# EMAIL_HOST_USER = 'inscripciones.juegosdeportivos'
# EMAIL_HOST_PASSWORD= 'conicet20182018'
# EMAIL_PORT= 587
EMAIL_HOST_USER = env.str("EMAIL_USER", 'inscripciones.juegosdeportivos')
EMAIL_HOST_PASSWORD = env.str("EMAIL_PASSWORD", 'mnwfjuzjkwyjlooc')
EMAIL_PORT = env.str("EMAIL_PORT", 587)
EMAIL_USE_TLS = True
