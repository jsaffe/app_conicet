from os.path import dirname, isfile
import environ
from .common import *

ROOT_DIR = environ.Path(dirname(dirname(dirname(__file__))))

env = environ.Env()

if isfile(ROOT_DIR('.env')):
    env.read_env(ROOT_DIR('.env'))

DEBUG = env.bool("DEBUG", True)

DOMAIN = env.list('DOMAIN', default=[])
ALLOWED_HOSTS = DOMAIN + ['127.0.0.1', 'localhost', '*']

# Database
# https://docs.djangoproject.com/en/2.0/ref/settings/#databases

# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.sqlite3',
#         'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
#     }
# }

# Database
DATABASES = {
    'default': {
        'ENGINE': env.str("DB_ENGINE", 'django.db.backends.postgresql_psycopg2'),
        'HOST': env.str("DB_HOST", 'postgresdb'),
        'PORT': env.int("DB_PORT", '5432'),
        'USER': env.str("DB_USER", 'postgres'),
        'PASSWORD': env.str("DB_PASS", 'postgres'),
        'NAME': env.str("DB_NAME", 'jdconicet'),
    },
}

#E-MAIL
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'inscripciones.juegosdeportivos'
# EMAIL_HOST_PASSWORD= 'conicet20182018'  # pass de la cuenta 
EMAIL_HOST_PASSWORD= 'mnwfjuzjkwyjlooc'  # pass app
EMAIL_PORT= 587
EMAIL_USE_TLS = True







