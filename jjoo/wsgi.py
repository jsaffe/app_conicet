"""
WSGI config for jjoo project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.0/howto/deployment/wsgi/
"""

# import os

# from django.core.wsgi import get_wsgi_application

# os.environ.setdefault("DJANGO_SETTINGS_MODULE", "jjoo.settings")

# application = get_wsgi_application()


import os
from os.path import dirname, isfile

import environ
from django.core.wsgi import get_wsgi_application

ROOT_DIR = environ.Path(dirname(dirname(dirname(__file__))))

env = environ.Env()

settings = env.str('DJANGO_SETTINGS', 'local')
os.environ.setdefault('DJANGO_SETTINGS_MODULE', f'jjoo.{settings}')

application = get_wsgi_application()