upstream app {
    server web:8000;
}

server {
    listen 80 default_server;

    client_max_body_size 4G;

    #Hide nginx version number in error pages and Server header
    server_tokens off;

    set $base /var/www/html;

    #Accept POST to static files
    error_page 405 =200 $uri;
    error_page 500 502 503 504 /page500/index.html;
    error_page 404 /page404/index.html;

    location / {
        proxy_pass http://app;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }

    location /static {
        alias $base/static;
    }

    location /media {
        alias $base/media;
        access_log off;
    }
}