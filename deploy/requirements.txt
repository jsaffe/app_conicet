django==2.0.6
django-widget-tweaks==1.4.2
django-allauth==0.36.0
sorl-thumbnail==12.4.1
Pillow==9.1.0
redis==2.10.6
django-import-export==1.0.1
django-admin-view-permission==1.7
django-qr-code==1.0.0
qrcode==6.0
django-tinymce==2.7.0
psycopg2-binary==2.8.6; python_version >= "3.6"
django-environ==0.8.1; python_version >= "3.4" and python_version < "4"

gunicorn==20.1.0
uWSGI==2.0.20

chardet==3.0.4    
defusedxml==0.5.0    
diff-match-patch==20121119 
et-xmlfile==1.0.1    
idna==2.7      
jdcal==1.4      
oauthlib==2.1.0    
odfpy==1.3.6    
openpyxl==2.5.5    
Pillow==9.1.0
psutil==5.4.7    
public==1.0.3    
python3-openid==3.1.0    
pytz==2018.5   
PyYAML==3.13     
requests==2.19.1   
requests-oauthlib==1.0.0    
runcmd==0.0.3    
setuptools==39.2.0   
six==1.11.0   
tablib==0.12.1   
unicodecsv==0.14.1   
urllib3==1.23     
wheel==0.31.1   
whitenoise==3.3.1    
xlrd==1.1.0    
xlwt==1.3.0